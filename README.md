# DevFutures

### How to install
```php
composer require devfutures/sources
```
After install execute commands
```php
php artisan libair:install
```

* `config/emitters.php` - copy file
* `App/LibAir/` - copy all libs to directory
* `config/app.php` - change privoder for load libair

```php
php artisan migrate
```

Add trait to `App\User`

```php
use Tymon\JWTAuth\Contracts\JWTSubject;
use Emitters\UsersData\Traits\AddUpline;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, AddUpline;
```

Open file `App\Http\Kernel.php`
```php
protected $middleware = [
    // ...
    \Barryvdh\Cors\HandleCors::class,
]

protected $routeMiddleware = [
    // ...
    'jwt.verify' => \Emitters\LibrariesAir\Middleware\JwtMiddleware::class,
]
```

---
### Develope packeges
Install laravel last version
```php
composer create-project --prefer-dist laravel/laravel dev_futures
```
Add docker files to main dir from `devfutures/sources/docker.zip` - unzip

Run docker
```php
docker-compose up -d
docker-compose exec app bash
```
* `http://localhost:3099` - site
* `http://localhost:8989` - phpmyadmin
* `DB name` - `emitters`
* `DB user` - `root`
* `DB password` - `root`

Create dir `packages/` and git clone `devfutures/sources`

Add to file `composer.json`
```json
"autoload": {
	"psr-4": {
		"Emitters\\": "packages/DevFutures/Sources/src/Emitters/"
    }
}
```

Add providers `config/app.php` `providers`
```php
Emitters\AdminControl\AdminControlServiceProvider::class,
Emitters\Balance\BalanceServiceProvider::class,
Emitters\ConvertingCurrencyRate\ConvertingCurrencyRateServiceProvider::class,
Emitters\DepositPlans\DepositPlansServiceProvider::class,
Emitters\DepositUsers\DepositUsersServiceProvider::class,
Emitters\Faq\FaqServiceProvider::class,
Emitters\LibrariesAir\LibrariesAirServiceProvider::class,
Emitters\News\NewsManagerServiceProvider::class,
Emitters\PaymentSystems\PaymentSystemsServiceProvider::class,
Emitters\UsersData\UsersDataServiceProvider::class,
```



---
### Payment Systems
Использование в коде
```php
use App\LibrariesAir\PaymentSystems\PaymentSystemsManager;
function load(PaymentSystemsManager $payment_systems_manager){
	// Get list from db
    $payment_systems_manager->get();
}

$payment_systems_manager = app()->make('libair.paymentsystems');
// Get list from db
$payment_systems_manager->get();

// Get balance and users wallet
$payment_systems_manager->getAll(App\User::find(1));
```


Получение не модифицированого списка платежных систем
```php
public function get(array $sort = ['sort', 'asc']);
```
Изменить платежную систему по идентификатору
```php
public function change($id, array $data, $field = 'id');
```
Добавить платежную систему
```php
public function addPaymentSystem(array $data);
```
Удалить платежную систему по идентификатору
```php
public function destroy($id, $field = 'id');
```
Востановить удаленную платежную систему
```php
public function restore($id, $field = 'id');
```
Получить платежную систему по идентификатору 1 результат
```php
public function getOne($id, $field = 'id');
```
Получить платежные системы с кошельками и балансом
```php
public function getAll($user, array $sort = ['sort', 'asc']);
```
Модификатор добавления ссылки на blockchain
```php
public function addBlockchainLink($currency, $transaction);
```
Модификатор ссылки для платежа в блокчейне
```php
public function addBlockchainPayLink($currency, $value, $amount);
```
Платежные системы с кошельками пользователя
```php
abstract protected function addWallets($user, $payment_system);
```
Платежные системы с балансом
```php
abstract protected function addBalance($user, $payment_system);
```
Получить платежные системы с балансом и кошельками
```php
abstract protected function getWithWalletBalance($user, array $sort);
```
Получить одно уникальное значение из платежных систем, например только уникальные title или currency, вторым аргументом массив который нужно исключить из конечного результата
```php
public function getPluckUniqValues($search_key = 'currency', $ignore = ['usd']);
```


---
### Balance
#### Извлечение BalanceManager
##### Получение через IoC
```php
$balanceManager = resolve('librariesair.balance');
```
##### Использование в контроллере с помощью type-hinting
```php
use App\LibrariesAir\Balance\BalanceManager;
...
public function index(BalanceManager $balanceManager) {
    ...
}
```
#### Методы BalanceManager

##### Получение всех балансов пользователя

```php
$balanceManager->getAll($user, $paymentSystemIds, $original);
```
* $user - объект модели User
* $paymentSystemIds - массив id платежных систем для которых требуется получить баланс
* $original - получение не округленных балансов в случае если указан true, по умолчанию false
* return type - array

##### Округление балансов
```php
$balanceManager->decimalBalance($balances);
```
* $balances - массив балансов
* return type - array

##### Изменение баланса
```php
$balanceManager->changeBalance($data);
```
##### $data - массив данных для изменения баланса
```php
$exampleData = [
    'user_id'           => 1, //id пользователя для которого необходимо изменить баланс.
    'payment_system_id' => 3, //id платежной системы
    'type'              => 'buy' // 'buy' в случае покупки, 'sell' в случае продажы.
];
```
* return type - boolean true/false.


---
### Deposit Plans
#### Извлечение DepositPlansManager
##### Получение через IoC
```php
$depositPlans = resolve('libair.depositplans');
```

##### Использование в контроллере с помощью type-hinting
```php
use App\LibrariesAir\DepositPlans\DepositPlansManager;
...
public function index(DepositPlansManager $depositPlans) {
    ...
}
```

### Validation create/update
```php
use Emitters\DepositPlans\Requests\DepositPlanRequest;

function save(DepositPlanRequest $request, DepositPlansManager $depositPlans){
	$depositPlans->store($request->all());
}
```

#### Методы DepositPlansManager

##### Создание нового плана
```php
$balanceManager->store(array $data);
```
* $data['name'] - Название плана
* $data['description'] - Описание плана
* $data['duration_plan'] - количество начислений по плану
* $data['accrual_period'] - период начислений депозита в секунда, 24 часа 86400 секунд
* $data['status'] - (int) 0 - не активный, 1 - активный, 2 - скрыт
* $data['minimum_amount'] - минимальная сумма депозита в USD
* $data['maximum_amount'] - максимальная сумма депозита в USD
* $data['payments_days_percent'] - array ключ день недели, в нем статус и процент
* $data['principal_return_percent'] - процент возврата по окончанию депозита
* return type model object

##### Список всех доступных планов по всем статусам
```php
$balanceManger->getPlans();
```

##### Получить план по идентификатору
```php
$balanceManger->getPlanById($id);
```
##### Получить план по сумме
```php
$balanceManger->getPlanByAmount($amount)
```
##### Модифицирование результата при фунциях `getPlans` & `getPlanById`
```php
$balanceManger->modifyPlan();
```
##### Дата выхода в безубыток
```php
$balanceManger->getBreakEvenDate($plan);
```
* Добавляет значение `break_event` - Carbon\Carbon;

##### Дата когда депозит будет закрыт
```php
$balanceManger->getDateCloseDeposit($plan);
```
* Добавляет значение `deposit_close` - Carbon\Carbon;

##### Получить дату следующего начисления
```php
$balanceManger->getNextAccruals($plan, $date = null);
```
##### Получить процент по идентификатору плана
```php
$balanceManger->getPercent($plan_id, $date = null);
```
* return percent or false

##### Извленеие процента из плана, проходим по массиву `payments_days_percent`
```php
$balanceManger->extractionPercent($plan, $date = null);
```
##### Получение конвертируемых сумм для всех платежных систем
```php
$balanceManger->getMinMaxAmount($plan);
```
* добавляем значенеи `minimum_amount_{CURRENCY}` - минимальная сумма в валюте
* добавляем значенеи `maximum_amount_{CURRENCY}` - максимальная сумма в валюте

##### Считаем сумму к профиту через bcmath( $amount*$percent/100)
```php
$balanceManger->getAmountToProfit(string $amount, string $percent);
```
##### Массив для хранения процентов по дням недели
```php
$balanceManger->defaultPaymentDaysPercent();
```
##### Заполняем перед занесением в базу данных начисления по дням недели
```php
$balanceManger->comparePaymentDaysPercent(array $preCreatedData);
```



---
### Deposit Users
#### Извлечение DepositUsersManager
##### Получение через IoC
```php
$depositUsers = resolve('libair.depositusers');
```

##### Использование в контроллере с помощью type-hinting
```php
use App\LibrariesAir\DepositUsers\DepositUsersManager;
...
public function index(DepositUsersManager $depositUsers) {
    ...
}
```

#### Методы DepositUsersManager

##### Создание нового депозита
```php
$depositUsers->createDeposit($user, $plan, $amount, $payment_system);
```
* App\User - object
* Emitters\DepositPlans\Models\Deposit_Plans - object
* amount - сумма депозита
* платежная система ID
* return type model object

##### Активные депозиты пользователя
```php
$depositUsers->getDepositUser(User $user);
```
##### Получит информацию о депозите с планом
```php
$depositUsers->getDepositOne(int $deposit_id);
```
##### Неактивные депозиты пользователя
```php
$depositUsers->getDepositUserEnd(User $user);
```
##### Список депозитов всех пользователей с постраничной навигацией и фильтрами
```php
$depositUsers->getDeposits(array $filter = [])
```
##### Изменить количество депозитов на одной странице
```php
$depositUsers->setPaginateNum(20);
```
##### Проверить может ли пользователь иметь такое количество депозитов по условия плана
```php
$this->checkCountDepositsUsers(User $user, Deposit_Plans $plan);
```
##### Количество депозитов пользователя с дополнительным условием плана
```php
$depositUsers->countDepositsUsers(User $user, Deposit_Plans $plan = null);
```
##### Обновить сумму депозита
```php
$depositUsers->updateDepositAmount($depositId = null, $amount, $fromBalance = false);
```
* depositId - int идентификатор депозита
* сколько необходимо добавить к сумме депозита
* операция добавления с баланса или нет


