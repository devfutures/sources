<?php


namespace Emitters\Abstracts\Affiliate;

use Emitters\Balance\BalanceManager;
use Emitters\Contracts\Affiliate\AffiliateManagerContract;
use Emitters\Contracts\Affiliate\AffiliateStructuresContract;
use Emitters\Contracts\Balance\BalanceManagerInterface;
use Emitters\Contracts\UsersData\UsersDataContract;
use Emitters\Contracts\UsersOperations\UsersOperationsContract;

class PartnershipAccrualAbstract {
    /**
     * @var AffiliateManagerContract
     */
    protected $affiliateManager;

    /**
     * @var AffiliateStructuresContract
     */
    protected $affiliateStructures;
    /**
     * @var BalanceManagerInterface
     */
    protected $balanceManager;

    /**
     * @var UsersOperationsContract
     */
    protected $usersOperations;

    /**
     * @var UsersDataContract
     */
    protected $usersData;

    /**
     * PartnershipAccrual constructor.
     * @param AffiliateManagerContract $affiliateManager
     * @param AffiliateStructuresContract $affiliateStructures
     * @param BalanceManagerInterface $balanceManager
     * @param UsersOperationsContract $usersOperations
     */
    public function __construct(AffiliateManagerContract $affiliateManager, AffiliateStructuresContract $affiliateStructures, BalanceManagerInterface $balanceManager, UsersOperationsContract $usersOperations, UsersDataContract $usersData) {
        $this->affiliateManager = $affiliateManager;
        $this->affiliateStructures = $affiliateStructures;
        $this->balanceManager = $balanceManager;
        $this->usersOperations = $usersOperations;
        $this->usersData = $usersData;
    }

    /**
     * @param $type
     * @param $user
     * @return bool|\Illuminate\Support\Collection
     */
    protected function validationRunning($type, $user) {
        $levels = $this->affiliateManager->getAffiliateLevels();
        if(!$levels){
            return false;
        }

        $by_type = $levels->where('name', $type)->first();
        if(!$by_type){
            return false;
        }

        $result_check = $this->checkProfitPercentNotNull($by_type->percents);
        if(!$result_check){
            return false;
        }

        if(!$user){
            return false;
        }

        return collect($by_type);
    }

    /**
     * @param $condition
     * @return string
     */
    protected function generateFunctionName($condition): string {
        $bb_function = strtolower($condition);
        $func_call   = '';
        foreach(explode('_', $bb_function) as $row) {
            $func_call .= ucfirst($row);
        }
        $func_call = 'accruals' . $func_call;
        return $func_call;
    }

    /**
     * @param $percents
     * @return bool
     */
    protected function checkProfitPercentNotNull($percents): bool {
        $returned = true;
        foreach($percents as $row) {
            foreach($row['percentage'] as $item) {
                if($item['profit'] == null) {
                    $returned = false;
                    break;
                }
            }
        }
        return $returned;
    }
}