<?php
namespace Emitters\Abstracts\Balance;

abstract class CalculateBalanceAbstract{
    protected $model;
    protected $drive;

    /**
     * @param       $user
     * @param array $paymentSystemIds
     * @param bool  $no_bonuse
     *
     * @return mixed
     */
    abstract protected function getWithGroup($user, array $paymentSystemIds, bool $no_bonuse);

    /**
     * @param       $user
     * @param array $paymentSystemIds
     * @param bool  $no_bonuse
     *
     * @return mixed
     */
    abstract protected function getWithUnion($user, array $paymentSystemIds, bool $no_bonuse);

    /**
     * @param       $user
     * @param array $paymentSystemIds
     * @param bool  $no_bonuse
     *
     * @return mixed
     */
    abstract protected function getWithMathRealTime($user, array $paymentSystemIds, bool $no_bonuse);
}