<?php
namespace Emitters\Abstracts\Balance;

abstract class CalculateBalanceAbstract{
    protected $model;
    protected $drive;

    abstract protected function getWithGroup($user, array $paymentSystemIds);
    abstract protected function getWithUnion($user, array $paymentSystemIds);
    abstract protected function getWithMathRealTime($user, array $paymentSystemIds);
}