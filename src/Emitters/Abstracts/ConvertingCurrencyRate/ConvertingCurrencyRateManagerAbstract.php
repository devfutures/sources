<?php


namespace Emitters\Abstracts\ConvertingCurrencyRate;


use Emitters\Contracts\ConvertingCurrencyRate\ConvertingCurrencyRateManagerContract;

abstract class ConvertingCurrencyRateManagerAbstract implements ConvertingCurrencyRateManagerContract {
    protected $cache_name = 'currency_rate';
    protected $parser;
    protected $fiatParser;
    protected $model;
    protected $payment_systems;
    public $courses = null;

    abstract protected function store(array $data);
}