<?php


namespace Emitters\Abstracts\DepositAccruals;

use Emitters\Contracts\Affiliate\PartnershipAccrualContract;
use Emitters\Contracts\DepositPlans\DepositPlansContract;
use Emitters\Contracts\UserDeposits\UserDepositsContract;
use Emitters\Contracts\Balance\BalanceManagerInterface;
use Emitters\Contracts\UsersOperations\UsersOperationsContract;
use Illuminate\Foundation\Auth\User;

abstract class DepositAccrualsAbstract {
    /**
     * @var UserDepositsContract
     */
    protected $libDepositUsers;

    /**
     * @var DepositPlansContract
     */
    protected $libDepositPlans;

    /**
     * @var BalanceManagerInterface
     */
    protected $balanceManager;

    /**
     * @var UsersOperationsContract
     */
    protected $libUsersOperations;

    /**
     * @var PartnershipAccrualContract
     */
    protected $partnershipAccrual;

    /**
     * DepositAccrualsManager constructor.
     *
     * @param UserDepositsContract $libDepositUsers
     * @param DepositPlansContract $libDepositPlans
     * @param BalanceManagerInterface $balanceManager
     * @param UsersOperationsContract $libUsersOperations
     * @param PartnershipAccrualContract $partnershipAccrual
     */
    public function __construct(UserDepositsContract $libDepositUsers, DepositPlansContract $libDepositPlans, BalanceManagerInterface $balanceManager, UsersOperationsContract $libUsersOperations, PartnershipAccrualContract $partnershipAccrual){
        $this->libDepositUsers = $libDepositUsers;
        $this->libDepositPlans = $libDepositPlans;
        $this->balanceManager = $balanceManager;
        $this->libUsersOperations = $libUsersOperations;
        $this->partnershipAccrual = $partnershipAccrual;
    }

    /**
     * @param $deposits
     *
     * @return mixed
     */
    abstract protected function verificationListDeposits($deposits);

    /**
     * @param $deposits
     *
     * @return mixed
     */
    abstract protected function runAccruals($deposits);

    /**
     * @param int    $deposit_id
     * @param User   $user
     * @param int    $payment_system_id
     * @param string $amount_for_accruals
     * @param string $deposit_amount
     * @param string $percent_accruals
     * @param        $accrual_at
     * @param        $next_accruals
     * @param int    $count_accruals
     *
     * @return mixed
     */
    abstract protected function accrualDeposit(int $deposit_id,User $user,int $payment_system_id,string $amount_for_accruals,string $deposit_amount,string $percent_accruals,$accrual_at,$next_accruals,int $count_accruals = 0);

    /**
     * @param int  $deposit_id
     * @param User $user
     * @param int  $payment_system_id
     * @param      $amount_if_deposit_close
     * @param      $deposit_amount
     * @param int  $count_accruals
     *
     * @return mixed
     */
    abstract protected function closeDeposit(int $deposit_id,User $user,int $payment_system_id,$amount_if_deposit_close,$deposit_amount,int $count_accruals = 0);

    /**
     * @param $deposits
     */
    abstract protected function transformDepositForAccruals($deposits);
}