<?php

namespace Emitters\Abstracts\DepositPlans;

use Emitters\DepositPlans\Models\Deposit_Plans;
use Emitters\DepositUsers\DepositUsersManager;
use Emitters\PaymentSystems\PaymentSystemsManager;
use Emitters\ConvertingCurrencyRate\ConvertingCurrencyRateManager;

/**
 * Class DepositPlansAbstract
 * @package Emitters\Abstracts\DepositPlans
 */
abstract class DepositPlansAbstract {
    /**
     * @var Deposit_Plans
     */
    protected $model;
    /**
     * @var PaymentSystemsManager
     */
    public $paymentSystems;
    /**
     * @var ConvertingCurrencyRateManager
     */
    public $convertingCurrencyRate;

    /**
     * @var DepositUsersManager
     */
    public $depositUsers;

    protected $availableFilters = ['orderBy', 'amount', 'where', 'dateBetween'];

    protected $dayOfWeek = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
}