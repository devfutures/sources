<?php
namespace Emitters\Abstracts\DepositPlans;

/**
 * Class DepositPlansAbstract
 * @package Emitters\Abstracts\DepositPlans
 */
abstract class DepositPlansAbstract{
    /**
     * @var model Deposit plans
     */
    protected $model;
    /**
     * @var Emitters\PaymentSystems\Manager
     */
    public $paymentSystems;
    /**
     * @var Emitters\ConvertingCurrencyRate\ConvertingCurrencyRateManager
     */
    public $convertingCurrencyRate;
}