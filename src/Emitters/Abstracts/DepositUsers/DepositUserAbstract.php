<?php


namespace Emitters\Abstracts\DepositUsers;
use App\User;
use Emitters\DepositPlans\Models\Deposit_Plans;
abstract class DepositUserAbstract {
    /**
     * @var array
     */
    protected $availableFilters = ['where' , 'whereBetween' , 'user' , 'dateBetween', 'orderBy'];
    /**
     * @var $model
     */
    protected $model;

    /**
     * @var $depositPlans
     */
    protected $depositPlans;

    /**
     * @var $paginateNum null
     */
    protected $paginateNum = null;

    /**
     * @param array $data
     *
     * @return mixed
     */
    protected function store(array $data){
        return $this->model::create($data);
    }

    /**
     * @param User          $user
     * @param Deposit_Plans $plan
     */
    abstract protected function checkCountDepositsUsers(User $user, Deposit_Plans $plan);
}