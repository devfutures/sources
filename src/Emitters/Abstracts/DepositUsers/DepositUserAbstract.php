<?php


namespace Emitters\Abstracts\DepositUsers;


abstract class DepositUserAbstract {
    /**
     * @var array
     */
    protected $avalibleFiltered = ['user' , 'plan' , 'payment_system' , 'status_deposit'];
    /**
     * @var $model
     */
    protected $model;

    /**
     * @var $depositPlans
     */
    protected $depositPlans;

    /**
     * @var $paginateNum null
     */
    protected $paginateNum = null;

    /**
     * @param array $data
     *
     * @return mixed
     */
    protected function store(array $data){
        return $this->model::create($data);
    }
}