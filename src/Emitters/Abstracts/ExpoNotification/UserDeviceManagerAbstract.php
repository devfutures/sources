<?php


namespace Emitters\Abstracts\ExpoNotification;


use Emitters\Contracts\ExpoNotification\UserDeviceManagerContract;

abstract class UserDeviceManagerAbstract implements UserDeviceManagerContract {
    protected $userDevices;
}