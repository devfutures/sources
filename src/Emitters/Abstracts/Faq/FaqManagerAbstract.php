<?php


namespace Emitters\Abstracts\Faq;


use Emitters\Faq\Models\Faqs;
use Emitters\Faq\Models\Faqs_Category;
use Emitters\Faq\Models\Faqs_Content;
use Emitters\Contracts\Faq\FaqContract;

abstract class FaqManagerAbstract implements FaqContract {
    /** @var Faqs */
    protected $faqModel;

    /** @var Faqs_Category */
    protected $faqCategoryModel;

    /** @var Faqs_Content */
    protected $faqContentModel;

    /** @var array  */
    protected $availableFaqFilters = ['whereIn', 'orderBy'];

    /** @var array  */
    protected $availableFaqCategoryFilters = ['where'];

    /** @var array  */
    protected $availableFaqContentFilters = ['whereIn'];
}