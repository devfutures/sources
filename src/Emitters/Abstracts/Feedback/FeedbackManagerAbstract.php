<?php


namespace Emitters\Abstracts\Feedback;


use Emitters\Feedback\Models\Feedback;
use Emitters\Contracts\Feedback\FeedbackManagerContract;

abstract class FeedbackManagerAbstract implements FeedbackManagerContract {
    /** @var Feedback */
    protected $model;

    protected $availableFilters = ['where', 'orderBy', 'dateBetween'];
}