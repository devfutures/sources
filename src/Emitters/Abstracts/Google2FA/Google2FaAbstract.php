<?php


namespace Emitters\Abstracts\Google2FA;


use Emitters\Contracts\Google2FA\Google2FaContract;
use Emitters\UsersData\UsersDataManager;
use PragmaRX\Google2FA\Google2FA;

abstract class Google2FaAbstract implements Google2FaContract {
    /** @var Google2FA */
    protected $google2FA;
    /** @var UsersDataManager */
    protected $userDataManager;
}