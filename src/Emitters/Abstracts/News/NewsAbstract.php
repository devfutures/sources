<?php


namespace Emitters\Abstracts\News;

use Emitters\Contracts\News\NewsContract;
use Emitters\News\Models\News;
use Emitters\News\Models\News_Contents;

abstract class NewsAbstract implements NewsContract {
    /** @var News */
    protected $newsModel;
    /** @var News_Contents */
    protected $contentsModel;

    protected $availableFiltered = ['dateBetween' , 'orderBy'];
}