<?php
namespace Emitters\Abstracts\PaymentSystems;

abstract class PaymentSystemsAbstract{
	/**
     * Bitcoin url blockchain
     * @var string
     */
    public $btc = 'https://blockchain.info/tx/%s';

    /**
     * [$Ethereum description]
     * @var string
     */
    public $eth = 'https://etherscan.io/tx/%s';

    /**
     * [$Dashcoin description]
     * @var string
     */
    public $dash = 'https://blockdash.info/tx/%s';

    /**
     * [$Litecoin description]
     * @var string
     */
	public $ltc = 'https://live.blockcypher.com/ltc/tx/%s/';

    /**
     * @var array link to pay
     */
	protected $crypto_payment_link = [
		'btc'  => 'bitcoin:%s?amount=%s',
		'dash' => 'dash:%s?amount=%s',
		'ltc'  => 'litecoin:%s?amount=%s',
		'bch'  => 'bitcoincash:%s?amount=%s',
		'xmr'  => 'monero:%s?amount=%s',
	];

    protected $cache_name = 'payment_systems';

	protected $availableFilters = ['orderBy', 'where', 'whereBetween', 'amount'];

    /**
     * @var payment system model
     */
    protected $psModel;

    /**
     * @var UsersDataManager class
     */
    protected $usersdata;

    /**
     * @var BalanceManger class
     */
    protected $balance;

    /**
     * @param array $crypto_payment_link
     *
     * @return PaymentSystemsAbstract
     */
    public function setCryptoPaymentLink (array $crypto_payment_link): PaymentSystemsAbstract {
        $this->crypto_payment_link = $crypto_payment_link;
        return $this;
    }

    /**
     * @return array
     */
    public function getCryptoPaymentLink (): array {
        return $this->crypto_payment_link;
    }

    /**
     * @param $user
     * @param $payment_system
     *
     * @return mixed
     */
    abstract protected function addWallets($user, $payment_system);

    /**
     * @param $user
     * @param $payment_system
     *
     * @return mixed
     */
    abstract protected function addBalance($user, $payment_system);

    /**
     * @param       $user
     * @param array $sort
     *
     * @return mixed
     */
    abstract protected function getWithWalletBalance($user, array $sort);
}