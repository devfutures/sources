<?php

namespace Emitters\Abstracts\Reviews;

use Emitters\Contracts\Reviews\ReviewsContract;
use Emitters\Reviews\Models\Reviews;

abstract class ReviewsAbstract implements ReviewsContract {
    /** @var Reviews */
    protected $reviewModel;

    protected $availableFilters = ['whereIn', 'dateBetween', 'whereNotNull'];
}