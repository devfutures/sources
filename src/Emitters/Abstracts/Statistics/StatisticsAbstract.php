<?php


namespace Emitters\Abstracts\Statistics;


use Emitters\Statistics\Models\Accounting;
use Emitters\UsersOperations\UsersOperations;
use Emitters\PaymentSystems\PaymentSystemsManager;
use Emitters\Contracts\Statistics\StatisticsContract;
use Emitters\ConvertingCurrencyRate\ConvertingCurrencyRateManager;

abstract class StatisticsAbstract implements StatisticsContract {
    /** @var Accounting */
    protected $accountingModel;

    /** @var UsersOperations */
    protected $userOperations;

    /** @var ConvertingCurrencyRateManager */
    protected $convertingCurrencyRate;

    /** @var PaymentSystemsManager */
    protected $paymentSystemsManager;

    protected $months = [
        'January', 'February', 'March', 'April',
        'May', 'June', 'July', 'August',
        'September', 'October', 'November', 'December'
    ];
}