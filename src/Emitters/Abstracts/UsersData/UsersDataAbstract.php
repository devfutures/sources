<?php
namespace Emitters\Abstracts\UsersData;

use Emitters\UsersData\Models\Users_Activity;
use Emitters\UsersData\Traits\ChangePassword;
use Emitters\Contracts\UsersData\UsersDataContract;

abstract class UsersDataAbstract implements UsersDataContract{
    /**
     * trait ChangePassword
     */
    use ChangePassword;
    /**
     * @var $availableFiltered
     */
    protected $availableFiltered = ['sort_id' , 'sort_created_at', 'has_upline', 'created_at_between', 'search_user_keyword'];
     /**
     * @var Users_Wallet
     */
    protected $walletsModel;

    /**
     * @var App/User
     */
    protected $userModel;

    /**
     * @var resolve(balance);
     */
    protected $libBalance;

    /**
     * @var Users_Activity
     */
    protected $userActivity;

    /**
     * @var Users_Contacts
     */
    protected $userContacts;
}