<?php


namespace Emitters\Abstracts\UsersOperations;


use Emitters\Contracts\UsersOperations\BaseActionOperationsContract;
use Emitters\Contracts\UsersOperations\OperationRepositoryContract;

abstract class ControlOperationsAbstract {
    /**
     * @var OperationRepositoryContract
     */
    protected $repository;

    /**
     * ControlOperations constructor.
     * @param OperationRepositoryContract $repository
     */
    public function __construct(OperationRepositoryContract $repository) {
        $this->repository = $repository;
    }

    /**
     * @param $option
     * @param $id
     * @param $type
     * @param $operation
     * @param $status
     * @param $data_to_pass
     * @param null $request
     * @return array
     */
    protected function executionMethods($option, $id, $type, $operation, $status, $data_to_pass, $request = null) {
        $data_returned = ['status' => false];
        // synonym's operations
        if(($option == 'confirm' or $option == 'cancel') && ($operation == 'ADD_FUNDS' or $operation == 'WITHDRAW') && ($status == 'cancel' or $status == 'error')) {
            $status = 'pending';
        }

        $decorator = $this->generateFunctionName($option, $type, $operation, $status);
        $data_returned['method'] = $decorator;

        if($id != 0) {
            $data_returned['id'] = $id;
        }

        if(!class_exists($decorator)) {
            $data_returned['message'] = 'The method to process the request was not found.';
            return $data_returned;

        }

        $action_operation_class = new $decorator;
        if(!$action_operation_class instanceof BaseActionOperationsContract) {
            $data_returned['message'] = 'The method to process need instanceof BaseActionOperationsContract';
            return $data_returned;
        }
        try {
            $res                     = $action_operation_class->perform($data_to_pass, $request);
            $data_returned['status'] = $res;
        } catch (\Exception $e) {
            $data_returned['status']  = false;
            $data_returned['message'] = $e->getMessage();
        }

        unset($data_returned['method']);
        return $data_returned;
    }



    /**
     * @param $option
     * @param $type
     * @param $operation
     * @param $status
     * @return string
     */
    private function generateFunctionName($option, $type, $operation, $status) {
        $operation_word = str_replace(' ', '',
            ucwords(
                strtolower(
                    implode(' ',
                        explode('_', $operation)
                    )
                )
            )
        );
        return str_replace('Abstracts\\', '', __NAMESPACE__).
            '\\OperationsActions\\Operations' .
            ucfirst(strtolower($option)) . '\\' .
            ucfirst($type) .
            $operation_word .
            ucfirst($status);
    }
}