<?php

namespace Emitters\AdminControl;

use Emitters\AdminControl\Models\AdminRole;
use Emitters\AdminControl\Repositories\RoleRepository;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory as EloquentFactory;
class AdminControlServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/migrations');

        $this->publishes([
            __DIR__.'/migrations/' => database_path('migrations')
        ], 'migrations');

        $this->app->make(EloquentFactory::class)->load(__DIR__ . '/factories/');
    }

    public function register()
    {
        $this->app->bind(RoleRepository::class, function ($app) {
            return new RoleRepository(new AdminRole());
        });
        $this->app->alias(RoleRepository::class, 'role_repo');

        $this->app->bind(AdminRoles::class, function ($app) {
            return new AdminRoles(resolve('role_repo'));
        });
        $this->app->alias(AdminRoles::class, 'admin_role');

        $this->app->bind(EnvManager::class, function () {
            return new EnvManager();
        });
        $this->app->alias(EnvManager::class, 'env.manager');

        $this->app->bind(SiteSettings::class, function ($app) {
            return new SiteSettings(resolve('env.manager'));
        });
        $this->app->alias(SiteSettings::class, 'site.settings');
    }
}
