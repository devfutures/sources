<?php


namespace Emitters\AdminControl;


use App\User;
use Emitters\AdminControl\Repositories\RoleRepository;

class AdminRoles {
    protected $repository;
    public function __construct (RoleRepository $repository) {
        $this->repository = $repository;
    }

    public function createRole(array $data) {
        return $this->repository->create($data);
    }

    public function getRoles(){
        return $this->repository->get();
    }

    public function getRole($roleID) {
        return $this->repository->getByID($roleID);
    }

    public function save_role($id, array $data) {
        //если делать update через where а не через экземпляр модели то надо сделать вот так.
        $data['sections'] = json_encode($data['sections']);
        return $this->repository->update($id, $data);
    }

    /**
     * @param $roleID
     * @return mixed
     * @throws \Exception
     */
    public function deleteRole($roleID) {
        if ((new User())::where('role_id', $roleID)->count() > 0) {
            throw new \Exception('Some users exists with given role.');
        }
        return $this->repository->delete($roleID);
    }
}