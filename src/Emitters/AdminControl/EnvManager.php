<?php


namespace Emitters\AdminControl;

use Artisan;
use Emitters\Contracts\AdminControl\EnvManagerContract;
use Illuminate\Support\Str;

class EnvManager implements EnvManagerContract {
    /**
     * @param string $config
     * @return array
     */
    public function getAvailableEnvironmentVariables(string $config) {
        return config($config);
    }

    /**
     * @param array $envValues
     * @return bool
     */
    public function puts_to_env(array $envValues) {
        Artisan::call('cache:clear');
        Artisan::call('config:clear');

        $envFile = $this->envPath();
        if (count($envValues) > 0) {
            foreach ($envValues as $envKey => $envValue) {
                if ($envValue == null) $envValue = 'false';
                if ($envValue == '') $envValue = 'false';

                $oldEnvValue = env($envKey);
                if ($oldEnvValue === true) {
                    $oldEnvValue = "true";
                } elseif ($oldEnvValue === false) {
                    $oldEnvValue = "false";
                }
                if ($oldEnvValue === $envValue) {
                    continue;
                }

                if (Str::contains(file_get_contents($envFile), $envKey) === false) {
                    // create new entry
                    file_put_contents($envFile, PHP_EOL . "{$envKey}={$envValue}", FILE_APPEND);
                } else {
                    //update
                    $strToReplace = "{$envKey}={$oldEnvValue}";
                    $str          = "{$envKey}={$envValue}";
                    file_put_contents($envFile, str_replace(
                        $strToReplace,
                        $str, file_get_contents($envFile)
                    ));
                }
            }
        }
        return true;
    }

    /**
     * Get's project environment file path.
     *
     * @return string
     */
    protected function envPath() {
        if (method_exists(app(), 'environmentFilePath')) {
            return app()->environmentFilePath();
        }
        return app()->basePath('.env');
    }
}