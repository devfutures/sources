<?php

namespace Emitters\AdminControl;

use Emitters\AdminControl\Models\AdminRole;
use Illuminate\Support\Facades\Route;

class MenuAccess {
    protected $currency_rate = 'Курс валют';
    protected $deposit_plans = 'Планы';
    protected $deposit_users = 'Депозиты';
    protected $payment_system = 'Платежные системы';

    protected $action_index = 'Просмотр';
    protected $action_new = 'Создать';
    protected $action_create = 'Создать';
    protected $action_edit = 'Редактировать';
    protected $action_settings = 'Настройки';
    protected $action_destroy = 'Удалить';
    protected $action_view = 'Просмотр';

    public function getSectionsWithTranslate() {
        return $this->translateSections($this->getSections());
    }

    public function getSections(bool $default = false) {
        $sections = [];

        $groups = $this->getGroups();
        foreach ($groups as $group) {
            $groupUris   = $this->getGroupUris($group);
            $sectionUris = [];

            foreach ($groupUris as $uri) {
                $uriName    = $uri;                                         //uri name
                $uriMethods = $this->getUriHTTPMethods($group, $uriName);   //available http methods
                $uriStatus  = [];

                foreach ($uriMethods as $uri) {                             //set methods to false by default.
                    $uriStatus[$uri] = $default;
                }

                $sectionUris[] = [
                    'uri'     => $uriName,
                    'methods' => $uriStatus
                ];
            }
            $section    = [
                'group' => $group,
                'uris'  => $sectionUris
            ];
            $sections[] = $section;
        }

        return $sections;
    }

    /**
     * @return array
     */
    public function getGroups() {
        $groups       = [];
        $admin_prefix = config('emitters.admin_panel_prefix');

        foreach (Route::getRoutes()->getIterator() as $route) {
            if (strpos($route->uri, $admin_prefix) === false) continue;

            $group = str_replace($admin_prefix . '/', '', $route->action['prefix']);
            array_push($groups, $group);
        }
        $groups = array_unique($groups);
        return $groups;
    }

    public function getGroupUris(string $groupName) {
        $uris         = [];
        $admin_prefix = config('emitters.admin_panel_prefix');

        foreach (Route::getRoutes()->getIterator() as $route) {
            if (strpos($route->uri, $admin_prefix) === false) continue;

            $group = str_replace($admin_prefix . '/', '', $route->action['prefix']);
            if ($group == $groupName) {
                $tmp = str_replace($admin_prefix . '/' . $group . '/', '', $route->uri);
                $tmp = str_replace('/{id}', '', $tmp);
                array_push($uris, $tmp);
            }
        }
        return array_unique($uris);
    }

    public function getUriHTTPMethods(string $groupName, string $uri) {
        $uris         = [];
        $admin_prefix = config('emitters.admin_panel_prefix');

        foreach (Route::getRoutes()->getIterator() as $route) {
            if (strpos($route->uri, $admin_prefix) === false) continue;

            $group = str_replace($admin_prefix . '/', '', $route->action['prefix']);
            if ($group == $groupName) {
                $tmp = str_replace($admin_prefix . '/' . $group . '/', '', $route->uri);
                $tmp = str_replace('/{id}', '', $tmp);
                if ($uri == $tmp) {
                    array_push($uris, $route->methods[0]);
                }
            }
        }
        return $uris;
    }

    protected function translateSections($sections) {
        $translate = collect();
        foreach ($sections as $key => $val) {
            $tmp = [];
            foreach ($val as $row) {
                $property = "action_" . $row;
                if (!property_exists($this, $property)) {
                    $this->$property = $row;
                }
                $tmp[$row] = $this->{$property};
            }
            if (!property_exists($this, $key)) {
                $this->{$key} = $key;
            }
            $translate->push([
                'name'    => $this->{$key},
                'key'     => $key,
                'actions' => $tmp
            ]);
        }
        return $translate;
    }

    /**
     * @param  \Illuminate\Http\Request $request
     * @param AdminRole                 $role
     * @return object
     */
    public function checkCollisionBetweenRoleAndRequest($request, AdminRole $role) {
        $uriCollision        = false;
        $httpMethodCollision = false;
        foreach ($role->sections as $section) {
            foreach ($section['uris'] as $uri) {
                $roleURI = '/' . config('emitters.admin_panel_prefix') . '/' . $section['group'] . '/' . $uri['uri'];
                if (strpos($request->getPathInfo(), $roleURI) === 0) {
                    $uriCollision = true;
                    if(array_key_exists($request->getMethod(), $uri['methods'])) {
                        if($uri['methods'][$request->getMethod()] == true) {
                            $httpMethodCollision = true;
                        }
                    }
                }
            }
        }

        return (object)array('uri_collision' => $uriCollision, 'http_method_collision' => $httpMethodCollision);
    }
}