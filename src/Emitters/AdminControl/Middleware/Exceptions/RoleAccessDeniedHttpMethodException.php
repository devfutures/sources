<?php


namespace Emitters\AdminControl\Middleware\Exceptions;


use Exception;

class RoleAccessDeniedHttpMethodException extends Exception {
    protected $message = 'This uri with this HTTP method is forbidden for your role.';
}