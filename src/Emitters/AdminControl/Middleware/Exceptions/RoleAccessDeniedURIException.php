<?php


namespace Emitters\AdminControl\Middleware\Exceptions;


use Exception;

class RoleAccessDeniedURIException extends Exception {
    protected $message = 'This uri is forbidden for your role.';
}