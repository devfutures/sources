<?php


namespace Emitters\AdminControl\Middleware\Exceptions;


use Exception;

class RoleDoesNotExistException extends Exception {
    protected $message = 'Role does not exist.';
}