<?php

namespace Emitters\AdminControl\Middleware;

use Closure;
use Emitters\AdminControl\AdminRoles;
use Emitters\AdminControl\MenuAccess;
use Emitters\AdminControl\Middleware\Exceptions\RoleAccessDeniedHttpMethodException;
use Emitters\AdminControl\Middleware\Exceptions\RoleAccessDeniedURIException;
use Emitters\AdminControl\Middleware\Exceptions\RoleDoesNotExistException;
use Illuminate\Validation\UnauthorizedException;

class RoleAccess {

    /** @var AdminRoles */
    protected $roles;

    /** @var MenuAccess */
    protected $menuAccess;

    /**
     * RoleAccessMiddleware constructor.
     * @param AdminRoles $roles
     * @param MenuAccess $menuAccess
     */
    public function __construct(AdminRoles $roles, MenuAccess $menuAccess) {
        $this->roles      = $roles;
        $this->menuAccess = $menuAccess;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        try {
            $this->isLegitRequest($request);
        } catch (UnauthorizedException | RoleAccessDeniedHttpMethodException | RoleAccessDeniedURIException | RoleDoesNotExistException $exception) {
            return response()->json([
                'msg' => $exception->getMessage()
            ], 403);
        }
        return $next($request);
    }

    /**
     * @param  \Illuminate\Http\Request $request
     * @throws RoleAccessDeniedURIException
     * @throws RoleAccessDeniedHttpMethodException
     * @throws RoleDoesNotExistException
     */
    protected function isLegitRequest($request) {
        $role       = $this->extractRole($request->user());
        $collisions = $this->menuAccess->checkCollisionBetweenRoleAndRequest($request, $role);
        if ($collisions->uri_collision != true) {
            throw new RoleAccessDeniedURIException();
        } elseif ($collisions->http_method_collision != true) {
            throw new RoleAccessDeniedHttpMethodException();
        }
    }

    /**
     * @param $user
     * @return mixed
     * @throws RoleDoesNotExistException
     */
    protected function extractRole($user) {
        if(!isset($user)) throw new UnauthorizedException('Please login.');
        if(!isset($user->role_id)) throw new RoleDoesNotExistException();
        $role = $this->roles->getRole($user->role_id);
        return $role;
    }
}
