<?php

namespace Emitters\AdminControl\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminRole extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'sections', 'status'
    ];

    protected $casts = [
        'sections' => 'array'
    ];
}
