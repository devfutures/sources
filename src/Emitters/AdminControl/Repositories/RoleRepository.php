<?php


namespace Emitters\AdminControl\Repositories;


class RoleRepository {
    protected $model;
    public function __construct ($model) {
        $this->model = $model;
    }

    public function create($data) {
        return $this->model::create($data);
    }

    public function get() {
        return $this->model::get();
    }

    public function update($id, array $data) {
        return $this->model::query()->where('id', $id)->update($data);
    }

    public function getByID($roleID) {
        return $this->model::find($roleID);
    }

    public function delete($roleID) {
        return $this->model::destroy($roleID);
    }
}