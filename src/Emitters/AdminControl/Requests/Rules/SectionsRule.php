<?php

namespace Emitters\AdminControl\Requests\Rules;

use Emitters\AdminControl\MenuAccess;
use Emitters\AdminControl\Requests\RolesRequest;
use Illuminate\Contracts\Validation\Rule;

class SectionsRule implements Rule {
    protected $menuAccess;
    protected $request;
    protected $errorMessage = [];

    /**
     * Create a new rule instance.
     *
     * @param RolesRequest $request
     * @param MenuAccess   $menuAccess
     */
    public function __construct(RolesRequest $request, MenuAccess $menuAccess) {
        $this->request    = $request;
        $this->menuAccess = $menuAccess;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value) {
        $this->checkSectionsAttributes($value);
        return empty($this->errorMessage) ? true : false;
    }

    /**
     * Checks section array structure
     *
     * @param $sections
     */
    public function checkSectionsAttributes($sections) {
        foreach ($sections as $section) {
            $this->checkGroupAttribute($section);
            $this->checkUrisAttribute($section);
        }
    }

    /**
     * @param $section
     */
    public function checkGroupAttribute($section) {
        if (!array_key_exists('group', $section)) {
            $this->applyMessage('Field [group] is required in sections.* array.');
        } elseif (!is_string($section['group'])) {
            $this->applyMessage('Filed [group] must be type of string.');
        } else {
            $groupList = $this->menuAccess->getGroups();
            if (!in_array($section['group'], $groupList)) $this->applyMessage('Given wrong router group name');
        }
    }

    /**
     * @param $section
     */
    public function checkUrisAttribute($section) {
        if (!array_key_exists('uris', $section) || !is_array($section['uris'])) {
            $this->applyMessage('Each item of sections must contain uris array field.');
        } else {
            $this->checkUriAttribute($section);
        }
    }

    /**
     * @param $section
     */
    public function checkUriAttribute($section) {
        foreach ($section['uris'] as $uri) {
            if (!array_key_exists('uri', $uri)) {
                $this->applyMessage('Each item of array [uris] must contain uri field.');
            } else {
                $uriList = $this->menuAccess->getGroupUris($section['group']);
                if (!in_array($uri['uri'], $uriList)) {
                    $this->applyMessage('Given wrong URI for [' . $section['group'] . '] group.');
                } else {
                    $this->checkMethodsForUri($section, $uri);
                }
            }
        }
    }

    /**
     * @param $section
     * @param $uri
     */
    public function checkMethodsForUri($section, $uri) {
        if (!array_key_exists('methods', $uri) || !is_array($uri['methods'])) {
            $this->applyMessage('Each item of array[uris] must contain methods array field.');
        } else {
            $availableMethods = $this->menuAccess->getUriHTTPMethods($section['group'], $uri['uri']);
            foreach ($availableMethods as $routeMethod) {
                if(array_key_exists($routeMethod, $uri['methods'])) {
                    if(!is_bool($uri['methods'][$routeMethod])) {
                        $this->applyMessage('Given wrong HTTP method status for '. $section['group'] .' URI ' . $uri['uri'] . ', must be false or true.');
                    }
                } else {
                    $this->applyMessage('Given wrong HTTP method for group '. $section['group'] .' URI ' . $uri['uri'] . '.');
                }
            }
        }
    }

    /**
     * @param string $errorMessage
     */
    public function applyMessage(string $errorMessage) {
        array_push($this->errorMessage, $errorMessage);
    }

    /**
     * Get the validation error message.
     *
     * @return array
     */
    public function message() {
        return $this->errorMessage;
    }
}
