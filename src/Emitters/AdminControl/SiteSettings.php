<?php


namespace Emitters\AdminControl;


use Emitters\Contracts\AdminControl\EnvManagerContract;

class SiteSettings {
    protected $envManager;

    public function __construct(EnvManagerContract $manager) {
        $this->envManager = $manager;
    }

    /**
     * @return array
     */
    public function getEnvironment() {
        $envVariables = $this->envManager->getAvailableEnvironmentVariables('emitters.site_settings');
        $settings     = [];
        foreach ($envVariables as $envVar) {
            if($envVar == 'APP_NAME') {
                $value = config('app.name');
            } else {
                $value = config('emitters.' . strtolower($envVar));
            }
            if ($value === "true") $value = true;
            if ($value === "false") $value = false;
            $settings[$envVar] = $value;
        }
        return $settings;
    }

    /**
     * @param array $envValues
     * @return bool
     */
    public function setEnvVariable(array $envValues) {
        foreach ($envValues as $envVar) {
            if (!$this->checkThatEnvironmentVariableIsAbleToBeSet($envVar['env_var'])) {
                return false;
            };
        }
        $envArray = [];
        foreach ($envValues as $envValue) {
            $envArray[$envValue['env_var']] = $envValue['env_var_val'];
        }
        return $this->envManager->puts_to_env($envArray);
    }


    /**
     * Determinate that variable is able to be set.
     *
     * @param string $envVariable
     * @return bool
     */
    public function checkThatEnvironmentVariableIsAbleToBeSet(string $envVariable) {
        return (in_array($envVariable, $this->envManager->getAvailableEnvironmentVariables('emitters.site_settings'))) ? true : false;
    }
}