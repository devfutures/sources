<?php

use Faker\Generator as Faker;

$factory->define(\Emitters\AdminControl\Models\AdminRole::class, function (Faker $faker) {
    return [
        'name'     => 'Administrator',
        'sections' => (new \Emitters\AdminControl\MenuAccess())->getSections(true),
        'status'   => 1
    ];
});