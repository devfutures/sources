<?php

namespace Emitters\Affiliate;


use Emitters\Affiliate\Exceptions\AffiliateLevelsException;
use Emitters\Affiliate\Models\Affiliate;
use Emitters\Contracts\AdminControl\EnvManagerContract;
use Emitters\Contracts\Affiliate\AffiliateManagerContract;

class AffiliateManager implements AffiliateManagerContract {
    /**
     * @var
     */

    protected $model;
    /**
     * @var EnvManagerContract
     */
    protected $envManager;

    /**
     * AffiliateManager constructor.
     *
     * @param $model
     * @param $envManager
     */
    public function __construct($model,EnvManagerContract $envManager){
        $this->model = $model;
        $this->envManager = $envManager;
    }

    /**
     * @return array
     */
    public function getAffiliateSettings(){
        $config = [];
        foreach(config()->get('emitters_affiliate') as $key => $value) {
            $config[strtoupper($key)] = $value;
        }
        return $config;
    }

    /**
     * @return mixed
     */
    public function getAffiliateLevels(){
        return $this->model::get();
    }

    /**
     * @param string $type
     * @return Affiliate
     */
    public function getAffiliateLevelByType(string $type) {
        return $this->model::where('name', $type)->first();
    }

    /**
     * @param $data
     *
     * @return bool
     * @throws AffiliateLevelsException
     */
    public function saveAffiliateLevels(array $data){
        if(count($data) == 0) {
            return false;
        }
        foreach($data as $row) {
            if(is_array($row) && array_key_exists('name', $row) && array_key_exists('condition', $row) && array_key_exists('percents', $row)){
                $this->model::updateOrCreate(
                    [
                        'name' => $row['name']
                    ],
                    [
                        'condition' => $row['condition'],
                        'percents'  => $row['percents']
                    ]
                );
            }else{
                throw new AffiliateLevelsException('ERROR_CREATE_LEVELS');
            }
        }
        return true;
    }

    /**
     * @param $data
     *
     * @return bool
     */
    public function saveSettings($data){
        return $this->envManager->puts_to_env($data);
    }

    /**
     * @return mixed
     */
    public function getAffiliatePercentsDefault() {
        $result = collect($this->model::query()
            ->where('name', config('emitters_affiliate.aff_default_name'))
            ->where('condition', config('emitters_affiliate.aff_default_condition'))
            ->value('percents'));

        return $result->transform(function($item){
            $keys = array_column($item['percentage'], 'from');
            array_multisort($keys, SORT_ASC, $item['percentage']);
            return $item;
        });
    }
}
