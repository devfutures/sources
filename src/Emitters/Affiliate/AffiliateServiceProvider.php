<?php
namespace Emitters\Affiliate;

use App\User;
use Emitters\Affiliate\Models\Affiliate;
use Emitters\Affiliate\Models\Users_Affiliate_Structure;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory as EloquentFactory;

class AffiliateServiceProvider extends ServiceProvider {
    public function boot() {
        $this->loadMigrationsFrom(__DIR__ . '/migrations');

        $this->publishes([
            __DIR__ . '/migrations/' => database_path('migrations')
        ], 'migrations');

        $this->publishes([
            __DIR__.'/config/emitters_affiliate.php' => config_path('emitters_affiliate.php'),
        ], 'LibrariesAir');

        $this->app->make(EloquentFactory::class)->load(__DIR__ . '/factories/');
    }

    public function register() {
        $this->mergeConfigFrom(
            __DIR__.'/config/emitters_affiliate.php', 'emitters_affiliate'
        );
        //
        $this->app->singleton(AffiliateManager::class, function () {
            return new AffiliateManager(Affiliate::class, resolve('env.manager'));
        });
        $this->app->alias(AffiliateManager::class, 'affiliate');

        //
        $this->app->singleton(AffiliateStructures::class, function () {
            return new AffiliateStructures(Users_Affiliate_Structure::class, new User(), resolve('affiliate'), resolve('users_operations'));
        });
        $this->app->alias(AffiliateStructures::class, 'affiliate.structures');

        //
        $this->app->singleton(PartnershipAccrual::class, function(){
           return new PartnershipAccrual(
               resolve('affiliate'),
               resolve('affiliate.structures'),
               balance(), users_operations(),
               resolve('libair.usersdata')
           );
        });
        $this->app->alias(PartnershipAccrual::class, 'partnership.accrual');
    }
}