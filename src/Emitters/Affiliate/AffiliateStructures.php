<?php

namespace Emitters\Affiliate;

use Emitters\Filters\Filters;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\User;
use Emitters\Affiliate\Models\Users_Affiliate_Structure;
use Emitters\Contracts\Affiliate\AffiliateStructuresContract;
use Emitters\Contracts\UsersOperations\UsersOperationsContract;

class AffiliateStructures implements AffiliateStructuresContract {
    /**
     * @var int
     */
    public $max_level = 200;
    /**
     * @var Users_Affiliate_Structure
     */
    protected $model;
    /**
     * @var User
     */
    protected $userModel;
    /**
     * @var AffiliateManager
     */
    protected $affiliateManager;
    /**
     * @var UsersOperationsContract
     */
    protected $usersOperations;
    /**
     * @var array
     */
    protected $availableFilters = ['search_partner', 'orderBy', 'where'];

    /**
     * AffiliateStructures constructor.
     *
     * @param                         $model
     * @param                         $userModel
     * @param AffiliateManager $affiliateManager
     * @param UsersOperationsContract $usersOperations
     */
    public function __construct($model, $userModel, AffiliateManager $affiliateManager, UsersOperationsContract $usersOperations) {
        $this->model            = $model;
        $this->userModel        = $userModel;
        $this->affiliateManager = $affiliateManager;
        $this->usersOperations  = $usersOperations;
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    public function createUserStructures(User $user) {
        $PathA = [];
        $this->getUserParentLevelPath($user->id, $PathA);
        if($PathA) {
            foreach($PathA as $level => $userid) {
                $level++;
                $this->model::query()->create([
                    'user_id'  => $userid,
                    'child_id' => $user->id,
                    'level'    => $level,
                ]);
            }
        }

        return true;
    }

    /**
     * @param User $user
     * @param int $max_level
     * @return array|mixed
     */
    public function getUserOriginStructure(User $user, $max_level = 0) {
        $PathA    = [];
        $BeReturn = [];
        $this->getUserParentLevelPath($user->id, $PathA);
        if($PathA) {
            foreach($PathA as $level => $userid) {
                if($max_level != 0) {
                    if($max_level <= $level) break;
                }
                $level++;
                $BeReturn[] = [
                    'child_id' => $userid,
                    'level'    => $level,
                ];
            }
        }

        return $BeReturn;
    }

    /**
     * @param     $user_id
     * @param     $PathA
     * @param int $Limit
     *
     * @return bool|int
     */
    public function getUserParentLevelPath($user_id, &$PathA, $Limit = 0) {
        $pid = $this->userModel::query()->where('id', $user_id)->value('upline_id');
        if(in_array($pid, $PathA) || $Limit == $this->max_level) {
            return 0;
        }
        if($pid) {
            $PathA[] = $pid;
            $this->getUserParentLevelPath($pid, $PathA, ($Limit + 1));
        }

        return true;
    }

    /**
     * @param User $user
     * @param int $level
     * @param array $filters
     * @param array $with
     *
     * @return mixed
     */
    public function getUserAffiliateStructure(User $user, $level = 1, array $filters = [], array $with = []) {
        if(count($with) == 0) {
            $with = ['info', 'user'];
        }

        if(!array_key_exists('orderBy', $filters)) $filters['orderBy'] = [
            [
                'by'        => 'id',
                'direction' => 'asc'
            ]
        ];

        $structure = $this->model::query()
            ->where('user_id', $user->id)
            ->where('level', $level)
            ->filters(new Filters($filters, $this->availableFilters))
            ->with($with)
            ->paginate(config('emitters.paginate_num'), ['*'], 'structure_page')
            ->appends($filters);

        $structure = $this->addAdditionalData($user, $structure);

        return $structure;
    }

    /**
     * @param User $user
     * @param null $level
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getUserAffiliateStructureSimple(User $user, $level = null) {
        $structure = $this->model::query()
            ->where('user_id', $user->id)
            ->where(function ($query) use ($level) {
                if($level) {
                    $query->where('level', $level);
                }
            })
            ->get();
        return $structure;
    }

    /**
     * @param $user
     * @param $structure
     * @return mixed
     */
    public function addAdditionalData($user, $structure) {
        $structure = $this->addAffiliateCommission($user, $structure);
        return $structure;
    }

    /**
     * @param $user
     * @param $structure
     * @return mixed
     */
    public function addAffiliateCommission($user, $structure) {
        $res = $this->usersOperations->getCommissionByUsersId($user, $structure->pluck('child_id')->toArray());
        $structure->each(function ($item) use ($res) {
            $find             = $res->where('from_user_id', $item->child_id)->values();
            $item->active     = 0;
            $item->commission = 0;
            $by_group         = null;
            if(count($find) > 0) {
                $item->commission = convert_from_bigInteger($find->sum('default_amount'));
                $by_group         = $find;
                $item->active     = 1;
            }
            
            $item->commission_by_group = $by_group;
        });
        return $structure;
    }

    /**
     * @param $user
     * @return array
     */
    public function getStatisticsByUser($user) {
        $affiliate = $this->affiliateManager->getAffiliatePercentsDefault();
        $userRefs = $this->model::where('user_id', $user->id)->where('level', '<=', $affiliate->max('level'))->get();

        list($commissionByLevels, $activeInLevel) = $this->usersOperations->getCommissionAndActiveUsers($user, $affiliate->max('level'));
        $investedByRefs = $this->sumInvestedByRefs($userRefs);
        $returnArray    = [];
        foreach($affiliate as $level) {
            $lvl            = $level['level'];
            $users_in_level = $userRefs->where('level', $lvl)->pluck('child_id');
            $invested_level = $investedByRefs->whereIn('user_id', $users_in_level)->sum('aggregate');
            $comm           = $commissionByLevels->where('level', $lvl)->first();
            if($comm)
                $comm = convert_from_bigInteger($comm->aggregate, 2);

            $actives = $activeInLevel->where('level', $lvl)->first();

            if($actives)
                $actives = $actives->active_in_level;

            $accessKey               = 'lvl_' . $lvl;
            $returnArray[$accessKey] = [
                'commission' => [
                    'active_refs'    => $actives ?? 0,
                    'total_refs'     => $users_in_level->count() ?? 0,
                    'commission_sum' => $comm ?? number(0, 2),
                ],
                'invested'   => [
                    'sum' => convert_from_bigInteger($invested_level, 2) ?? 0
                ]
            ];
        }

        return $returnArray;
    }

    /**
     * @param $user
     * @param $userRefs
     * @return array
     */
    protected function getAffiliateBonusFromUsers($user, $userRefs) {
        $refsIds      = $userRefs->pluck('child_id')->toArray();
        $levelBonuses = $this->usersOperations->getAffiliateBonusesFromUsers($user, $refsIds);
        $response     = [];
        $levelBonuses->each(function ($item, $lvl) use (&$response, $userRefs) {
            $response['lvl_' . $lvl] = [
                'active_refs'    => $item->count(),
                'total_refs'     => $userRefs->where('level', $lvl)->count(),
                'commission_sum' => convert_from_bigInteger($item->sum('amount_default'))
            ];
        });
        return $response;
    }

    /**
     * @param $userRefs
     * @return array
     */
    protected function sumInvestedByRefs($userRefs) {
        return $sumsByLevels = $this->usersOperations->sumInvestsByRefs($userRefs->pluck('child_id')->toArray());
    }

    /**
     * @param $user_id
     * @param int $max_level
     * @return mixed
     */
    public function getCountUserStructures($user_id, $max_level = 0) {
        return $this->model::where('user_id', $user_id)->where(function ($query) use ($max_level) {
            if($max_level > 0) {
                $query->where('level', '<=', $max_level);
            }
        })->count('id');
    }

    /**
     * @param $user
     * @param $userRefs
     * @param string $field_to_extract
     * @return int
     */
    public function getActiveUsersAllLines($user, $userRefs, $field_to_extract = 'id') {
        if(is_array($userRefs)) {
            $userRefs = collect($userRefs);
        }
        $refsIds      = $userRefs->pluck($field_to_extract)->toArray();
        $levelBonuses = $this->usersOperations->getAffiliateBonusesFromUsers($user, $refsIds);
        $total        = 0;
        foreach($levelBonuses as $row) {
            $total += $row->count();
        }
        return $total;
    }

    /**
     * @param null $orderBy
     * @param null $direction
     * @param null $hasIncome
     * @param null $hasReferrer
     * @return mixed
     */
    public function getUsersTeamInfo($orderBy = null, $direction = null, $hasIncome = null, $hasReferrer = null) {
        $info = DB::table('users')
            ->select(DB::raw("users.id, users.name, users.upline_id, users.email,  upline.name as upline_name, upline.email as upline_email"))
            ->selectSub(DB::table('users__operations')
                ->selectRaw("sum(default_amount)")
                ->whereRaw("user_id = users.id AND operation = 'REFFERAL'"), "team_ref_bonus_sum")
            ->selectSub(DB::table('users__operations')
                ->selectRaw("sum(default_amount)")
                ->whereRaw("user_id = ANY (
                    SELECT
                        child_id
                    FROM
                        users__affiliate__structures
                    WHERE
                        user_id = users.id
                ) AND operation = 'CREATE_DEPOSIT'"), "team_deposit_sum")
            ->selectSub(DB::table('users__operations')
                ->selectRaw("sum(default_amount)")
                ->whereRaw("user_id = ANY (
                    SELECT
                        child_id
                    FROM
                        users__affiliate__structures
                    WHERE
                        user_id = users.id
                ) AND operation = 'WITHDRAW'"), "team_withdraw_sum")
            ->selectRaw("COUNT(aff.user_id) as team_size")
            ->join("users__affiliate__structures as aff", "users.id", "=", "aff.user_id")
            ->join("users as upline", "users.upline_id", "=", "upline.id", 'left')
            ->groupBy('users.id');

        $info = $this->filter($info, $orderBy, $direction, $hasIncome, $hasReferrer);

        $info = $info->simplePaginate(config('emitters.paginate_num'))->appends([
            'orderBy'     => $orderBy,
            'direction'   => $direction,
            'hasIncome'   => $hasIncome,
            'hasReferrer' => $hasReferrer
        ]);
        $info = $this->convertResults($info);

        return $info;
    }

    /**
     * @param int $userID
     * @return \Illuminate\Support\Collection
     */
    public function getUserTeamBaseInfoByLevels(int $userID) {
        $info = DB::table('users__affiliate__structures')
            ->select(DB::raw("users__affiliate__structures.level, COUNT(id) as team_size"))
            ->selectSub(DB::table('users__operations')
                ->selectRaw("sum(default_amount)")
                ->whereRaw("user_id = {$userID}
                                AND operation = 'REFFERAL'
                                AND users__operations.level = users__affiliate__structures.level"),
                "team_ref_bonus_sum")
            ->selectSub(DB::table('users__operations')
                ->selectRaw("sum(default_amount)")
                ->whereRaw("users__operations.user_id = ANY (
                                    SELECT child_id
                                    FROM users__affiliate__structures as refs
                                    WHERE
                                        user_id = {$userID}
                                        AND refs.level = users__affiliate__structures.level
                                    )
                                AND users__operations.operation = 'CREATE_DEPOSIT'"),
                "team_deposit_sum")
            ->selectSub(DB::table('users__operations')
                ->selectRaw("sum(default_amount)")
                ->whereRaw("users__operations.user_id = ANY (
                                    SELECT child_id
                                    FROM users__affiliate__structures as refs
                                    WHERE
                                        user_id = {$userID}
                                        AND refs.level = users__affiliate__structures.level
                                    )
                                AND users__operations.operation = 'WITHDRAW'"),
                "team_withdraw_sum")
            ->where('user_id', '=', $userID)
            ->groupBy("users__affiliate__structures.level")
            ->get();

        $info = $this->convertResults($info);
        return $info;
    }

    /**
     * @param $userID
     * @param $level
     * @param null $orderBy
     * @param null $direction
     * @param null $hasIncome
     * @param null $hasReferrer
     * @return mixed
     */
    public function inspectUserTeam($userID, $level = 1, $orderBy = null, $direction = null, $hasIncome = null, $hasReferrer = null) {
        $info = DB::table("users__affiliate__structures")
            ->select(DB::raw("user_id,
                              child_id,
                              child_data.name,
                              child_data.email,
                              child_data.upline_id AS upline_id,
                              parent_data.name AS upline_name,
                              parent_data.email as upline_email,
                              level"))
            ->selectSub(DB::table("users__operations")
                ->selectRaw("sum(default_amount)")
                ->whereRaw("users__operations.user_id = users__affiliate__structures.child_id
                                AND
                                operation = 'REFFERAL'"
                ),
                "team_ref_bonus_sum")
            ->selectSub(DB::table("users__operations")
                ->selectRaw("sum(default_amount)")
                ->whereRaw("user_id = ANY (
                                    SELECT child_id
                                    FROM users__affiliate__structures as ref_of_refs
                                    WHERE
                                        ref_of_refs.user_id = users__affiliate__structures.child_id)
                                AND operation = 'CREATE_DEPOSIT'"),
                "team_deposit_sum")
            ->selectSub(DB::table("users__operations")
                ->selectRaw("sum(default_amount)")
                ->whereRaw("user_id = ANY (
                                    SELECT child_id
                                    FROM users__affiliate__structures as ref_of_refs
                                    WHERE
                                        ref_of_refs.user_id = users__affiliate__structures.child_id)
                                AND operation = 'WITHDRAW'"),
                "team_withdraw_sum")
            ->selectSub(DB::table("users__affiliate__structures as refs_of_ref")
                ->selectRaw("COUNT(id)")
                ->whereRaw("refs_of_ref.user_id = users__affiliate__structures.child_id "),
                "team_size")
            ->join("users as child_data", "users__affiliate__structures.child_id", "=", "child_data.id", "LEFT")
            ->join("users as parent_data", "child_data.upline_id", "=", "parent_data.id", "LEFT")
            ->where('user_id', '=', $userID)
            ->where('level', '=', $level);


        $info = $this->filter($info, $orderBy, $direction, $hasIncome);

        $info = $info->simplePaginate(config('emitters.paginate_num'))->appends([
            'orderBy'     => $orderBy,
            'direction'   => $direction,
            'hasIncome'   => $hasIncome,
            'hasReferrer' => $hasReferrer
        ]);
        $info = $this->convertResults($info);
        return $info;
    }

    /**
     * @param $filterable
     * @param null $orderBy
     * @param null $direction
     * @param null $hasIncome
     * @param null $hasReferrer
     * @return mixed
     */
    protected function filter($filterable, $orderBy = null, $direction = null, $hasIncome = null, $hasReferrer = null) {
        if($orderBy) {
            $filterable = $filterable->orderBy($orderBy, $direction ?? 'asc');
        }

        switch($hasIncome) {
            case "true" :
                $filterable = $filterable->havingRaw("team_ref_bonus_sum IS NOT NULL");
                break;
            case "false" :
                $filterable = $filterable->havingRaw("team_ref_bonus_sum IS NULL");
                break;
        }

        switch($hasReferrer) {
            case "true" :
                $filterable = $filterable->where('users.upline_id', '!=', 0);
                break;
            case "false" :
                $filterable = $filterable->where('users.upline_id', '=', 0);
                break;
        }
        return $filterable;
    }

    /**
     * Convert sum's from big integer.
     *
     * @param $info
     * @return mixed
     */
    protected function convertResults($info) {
        $info->each(function ($item) {
            $item->team_ref_bonus_sum = convert_from_bigInteger($item->team_ref_bonus_sum ?? 0, 2);
            $item->team_deposit_sum   = convert_from_bigInteger($item->team_deposit_sum ?? 0, 2);
            $item->team_withdraw_sum  = convert_from_bigInteger($item->team_withdraw_sum ?? 0, 2);
        });
        return $info;
    }

    /**
     * @return mixed
     */
    public function getCountPartners() {
        return $this->model::query()
            ->selectRaw('count(DISTINCT user_id) as cnt')
            ->value('cnt');
    }

    public function getTopUsersReferral() {
        return $this->model::query()
            ->selectRaw('user_id, count(id) as team')
            ->selectSub('(SELECT sum(default_amount) FROM `users__operations` where operation = "REFFERAL" and user_id = `users__affiliate__structures`.`user_id` and `users__operations`.`deleted_at` is null)', 'commission')
            ->groupBy('user_id')
            ->orderBy('commission', 'desc')
            ->with('user')
            ->having('commission', '>', 0)
            ->take(10)
            ->get();
    }
}