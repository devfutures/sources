<?php


namespace Emitters\Affiliate\Exceptions;


class AffiliateLevelsException extends \Exception {
    const ERROR_CREATE_LEVELS = 'The array has an error and does not contain the necessary keys to insert';
}