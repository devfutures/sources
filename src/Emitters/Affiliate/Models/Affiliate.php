<?php

namespace Emitters\Affiliate\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Affiliate extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'condition',
        'percents'
    ];
    protected $casts = [
        'percents' => 'array'
    ];
}
