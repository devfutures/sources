<?php

namespace Emitters\Affiliate\Models;

use Emitters\Filters\Traits\AddFilters;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Emitters\UsersOperations\Models\Users_Operations;

class Users_Affiliate_Structure extends Model {
    use SoftDeletes, AddFilters;

    protected $fillable = [
        'user_id', 'child_id', 'level'
    ];

    public function info() {
        return $this->hasOne('App\User', 'id', 'child_id');
    }

    public function user() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
