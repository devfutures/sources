<?php

namespace Emitters\Affiliate;


use Emitters\Abstracts\Affiliate\PartnershipAccrualAbstract;
use Emitters\Contracts\Affiliate\PartnershipAccrualContract;

class PartnershipAccrual extends PartnershipAccrualAbstract implements PartnershipAccrualContract {

    /**
     * @param string $type
     * @param $user
     * @param $amount
     * @param $deposit_id
     * @param $payment_system_id
     * @return bool|mixed
     */
    public function affiliateBonus(string $type, $user, $amount, $deposit_id, $payment_system_id) {
        $pass_data = $this->validationRunning($type, $user);
        if(!$pass_data) return false;
        $affiliate = $this->affiliateManager->getAffiliatePercentsDefault();
        $max_level = 1;
        if($affiliate){
            $max_level = $affiliate->max('level');
        }
        $user_structure = $this->affiliateStructures->getUserOriginStructure($user, $max_level);
        if(count($user_structure) == 0) return false;

        $option_affiliate = $this->generateFunctionName($pass_data['condition']);
        $result_running = [];
        foreach($user_structure as $row) {
            $percent   = call_user_func_array(
                array($this, $option_affiliate),
                array(collect($pass_data['percents']), $row['level'], $row['child_id'])
            );
            $amount_profit   = calc_of_amount_percent($amount, $percent);
            $result_running[] = [
                'from_user_id'      => $user->id,
                'user_id'           => $row['child_id'],
                'amount'            => $amount_profit,
                'level'             => $row['level'],
                'deposit_id'        => $deposit_id,
                'payment_system_id' => $payment_system_id,
                'from_amount'       => $amount,
            ];
        }

        return $this->runningCharges(
            $result_running
        );
    }

    /**
     * @param $data
     * @return bool|mixed
     */
    public function runningCharges($data) {
        if(!is_array($data)){
            return false;
        }

        if(count($data) == 0) {
            return false;
        }

        foreach($data as $row) {
            $user = $this->usersData->getUserInfo($row['user_id'], []);
            if(config('emitters_affiliate.aff_pay_active_referral') === true) {
                $deposits = $this->usersOperations->getUserOperations($user, ['CREATE_DEPOSIT'], ['completed']);
                if($deposits->isEmpty()){
                    continue;
                }
            }
            $this->balanceManager->addRefferalToBalance($user, $row['amount'], $row['payment_system_id'], $row['level'], $row['from_user_id'], $row['deposit_id'], $row['from_amount']);
        }
        return true;
    }

    /**
     * @param $data
     * @param $level
     * @return array
     */
    protected function accrualsNormal($data, $level) {
        return $percent = $data->where('level', $level)->first()['percentage'][0]['profit'];
    }

    /**
     * @param $data
     * @param $level
     * @param $child_id
     * @return array
     */
    protected function accrualsCountActiveRefLine($data, $level, $child_id) {
        $conditions         = collect($data->where('level', $level)->first());
        $conditions = collect($conditions['percentage'])->sortBy('from');

        $user_to_get_bonus = $this->usersData->getUserInfo($child_id, []);
        $user_to_get_bonus_structure = $this->affiliateStructures->getUserAffiliateStructureSimple($user_to_get_bonus, $level);
        $active_users_in_line = $this->affiliateStructures->getActiveUsersAllLines($user_to_get_bonus, $user_to_get_bonus_structure, 'child_id');

        $percent = $conditions->where('from', '<=', $active_users_in_line)->last()['profit'];
        return $percent;
    }

    /**
     * @param $data
     * @param $level
     * @param $child_id
     * @return array
     */
    protected function accrualsCountActiveRefAllLine($data, $level, $child_id){
        $conditions         = collect($data->where('level', $level)->first());
        $conditions = collect($conditions['percentage'])->sortBy('from');

        $user_to_get_bonus = $this->usersData->getUserInfo($child_id, []);
        $user_to_get_bonus_structure = $this->affiliateStructures->getUserAffiliateStructureSimple($user_to_get_bonus);
        $active_users_all_lines = $this->affiliateStructures->getActiveUsersAllLines($user_to_get_bonus, $user_to_get_bonus_structure, 'child_id');
        $percent = $conditions->where('from', '<=', $active_users_all_lines)->last()['profit'];
        return $percent;
    }

    /**
     * @param $data
     * @param $level
     * @param $child_id
     * @return array
     */
    protected function accrualsAmountDepositRef($data, $level, $child_id){
        $conditions         = collect($data->where('level', $level)->first());
        $conditions = collect($conditions['percentage'])->sortBy('from');

        $user_to_get_bonus = $this->usersData->getUserInfo($child_id, []);

        $user_to_get_bonus_structure = $this->affiliateStructures->getUserAffiliateStructureSimple($user_to_get_bonus);

        $users_to_check_amount_deposits = $user_to_get_bonus_structure->pluck('child_id')->toArray();
        $sum_deposit_users = $this->usersOperations->getSumUsersDeposits($users_to_check_amount_deposits);
        $sum_deposit_users = convert_from_bigInteger($sum_deposit_users->total);
        $percent = $conditions->where('from', '<=', $sum_deposit_users)->last()['profit'];

        return $percent;
    }

    /**
     * @param $data
     * @param $level
     * @param $child_id
     * @return array
     */
    protected function accrualsAmountActiveDeposits($data, $level, $child_id){
        $conditions         = collect($data->where('level', $level)->first());
        $conditions = collect($conditions['percentage'])->sortBy('from');

        $user_to_get_bonus = $this->usersData->getUserInfo($child_id, []);

        $sum_deposit_user = $this->usersOperations->getSumUsersActiveDeposits([$user_to_get_bonus->id]);
        $sum_deposit_user = convert_from_bigInteger($sum_deposit_user);
        $percent = $conditions->where('from', '<=', $sum_deposit_user)->last()['profit'];
        return $percent;
    }

    /**
     * @param $data
     * @param $level
     * @param $child_id
     * @return array
     */
    protected function accrualsAmountTotalDeposits($data, $level, $child_id){
        $conditions         = collect($data->where('level', $level)->first());
        $conditions = collect($conditions['percentage'])->sortBy('from');

        $user_to_get_bonus = $this->usersData->getUserInfo($child_id, []);

        $sum_deposit_user = $this->usersOperations->getSumUsersDeposits([$user_to_get_bonus->id]);
        $sum_deposit_user = convert_from_bigInteger($sum_deposit_user->total);

        $percent = $conditions->where('from', '<=', $sum_deposit_user)->last()['profit'];
        return $percent;
    }
}
