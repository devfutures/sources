<?php

return [
    /**
     * Make to pick upline during registration
     * Determines whether the user must have a top level for registration.
     */
    'aff_need_upline'                => env('AFF_NEED_UPLINE',false),

    /**
     * Get random upline
     * Require the option "Forced upline during registration".
     * If the "Forced upline" option is enabled, and the user does not have a higher level, system give a random option.
     */
    'aff_random_upline_sign_up'      => env('AFF_RANDOM_UPLINE_SIGN_UP',false),

    /**
     * Pay referral commission to users who have made a deposit
     * Check this box and referral commission will be paid only to users make a deposit.
     */
    'aff_pay_active_referral'        => env('AFF_PAY_ACTIVE_REFERRAL',false),

    /**
     * Do not add referral commission for deposits from the balance
     * Check this box to prevent referral commission from being added to the top level when making user funds from the account balance.
     */
    'aff_no_commission_balance'      => env('AFF_NO_COMMISSION_BALANCE',false),

    /**
     * Allow users to automatically set a refback commission
     * Check this box to allow automatic return commissions for direct referrals.The user can set the RCB percentage in your account settings.
     */
    'aff_set_personal_referral_back' => env('AFF_SET_PERSONAL_REFERRAL_BACK',true),

    /**
     * Default Affiliate program
     */
    'aff_default_name' => env('AFF_DEFAULT_NAME', 'BY_CREATE_DEPOSIT'),

    'aff_default_condition' => env('AFF_DEFAULT_CONDITION', 'NORMAL'),
];