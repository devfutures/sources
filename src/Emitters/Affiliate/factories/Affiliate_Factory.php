<?php

use Faker\Generator as Faker;

$factory->define(\Emitters\Affiliate\Models\Affiliate::class,function (Faker $faker){
    $types = ['BY_CREATE_DEPOSIT','ADD_FUNDS','BY_PROFIT'];
    return [
        'name'      => $faker->randomElement($types),
        'condition' => 'NORMAL',
        'percents'  => [
            ['level' => 1, 'percentage' => ['from' => 0, 'profit' => $faker->randomFloat(2,8, 25)]],
            ['level' => 2, 'percentage' => ['from' => 0, 'profit' => $faker->randomFloat(2,8, 25)]],
            ['level' => 3, 'percentage' => ['from' => 0, 'profit' => $faker->randomFloat(2,8, 25)]],
        ]
    ];
});