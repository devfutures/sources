<?php

use Faker\Generator as Faker;

$factory->define(\Emitters\Affiliate\Models\Users_Affiliate_Structure::class, function (Faker $faker) {
    return [
        'user_id'  => function () {
            return factory(\App\User::class)->create()->id;
        },
        'child_id' => function () {
            return factory(\App\User::class)->create()->id;
        },
        'level'    => 1
    ];
});