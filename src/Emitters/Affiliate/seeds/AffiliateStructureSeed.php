<?php


namespace Emitters\Affiliate\seeds;


use App\User;
use Emitters\Affiliate\Models\Users_Affiliate_Structure;
use Illuminate\Database\Seeder;

class AffiliateStructureSeed extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $users = factory(User::class, 50)->create([
            'role_id' => null
        ]);

        $users->each(function ($user) {
            $firstLevelChilds = factory(User::class, rand(3, 10))->create([
                'role_id'   => null,
                'upline_id' => $user->id
            ]);
            $firstLevelChilds->each(function ($firstLevelChild) use ($user) {
                factory(Users_Affiliate_Structure::class)->create([
                    'user_id'  => $user->id,
                    'child_id' => $firstLevelChild->id,
                    'level'    => 1
                ]);

                $secondLevelChilds = factory(User::class, rand(3, 10))->create([
                    'role_id'   => null,
                    'upline_id' => $firstLevelChild->id
                ]);
                $secondLevelChilds->each(function ($secondLevelChild) use ($firstLevelChild, $user) {
                    factory(Users_Affiliate_Structure::class)->create([
                        'user_id'  => $firstLevelChild->id,
                        'child_id' => $secondLevelChild->id,
                        'level'    => 1
                    ]);
                    factory(Users_Affiliate_Structure::class)->create([
                        'user_id'  => $user->id,
                        'child_id' => $secondLevelChild->id,
                        'level'    => 2
                    ]);

                    $thirdLevelChilds = factory(User::class, rand(3, 10))->create([
                        'role_id'   => null,
                        'upline_id' => $secondLevelChild->id
                    ]);
                    $thirdLevelChilds->each(function ($thirdLevelChild) use ($firstLevelChild, $secondLevelChild, $user) {
                        factory(Users_Affiliate_Structure::class)->create([
                            'user_id'  => $secondLevelChild->id,
                            'child_id' => $thirdLevelChild->id,
                            'level'    => 1
                        ]);
                        factory(Users_Affiliate_Structure::class)->create([
                            'user_id'  => $firstLevelChild->id,
                            'child_id' => $thirdLevelChild->id,
                            'level'    => 2
                        ]);
                        factory(Users_Affiliate_Structure::class)->create([
                            'user_id'  => $user->id,
                            'child_id' => $thirdLevelChild->id,
                            'level'    => 3
                        ]);
                    });
                });
            });
        });
    }
}