<?php

namespace Emitters\Balance;

use Emitters\Abstracts\Balance\BalanceManagerAbstract;
use Emitters\Contracts\Balance\BalanceManagerInterface;

class BalanceManager extends BalanceManagerAbstract implements BalanceManagerInterface {

    /**
     * @param $user
     * @param array $paymentSystemIds
     * @param bool $original
     * @return bool|array
     */
    public function getAll($user, array $paymentSystemIds, $original = false) {
        if (count($paymentSystemIds) == 0) return false;
        $balance = $this->calculateBalance->get($user, $paymentSystemIds);

        return ($original) ? $balance : $this->decimalBalance($balance);
    }

    /**
     * @param $balance
     * @return mixed
     */
    public function decimalBalance($balance) {
        if ($balance) {
            foreach ($balance as $key => $value) {
                $balance[$key]['balance_now'] = bcdiv((string)$value['balance_now'], (string)100000000, (string)8);
            }
        }
        return $balance;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function changeBalance($data) {
        $balance_now = $this->calculateBalance->getBalanceNow($data['user_id'], $data['payment_system_id']);

        $data['balance_before'] = $balance_now;

        if ($data['type'] == 'buy') {
            $data['balance_now'] = bcadd($balance_now, $data['amount']);
        } elseif ($data['type'] == 'sell') {
            $data['balance_now'] = bcsub($balance_now, $data['amount']);
        }
        return $this->model::create($data);
    }
}