<?php

namespace Emitters\Balance;

use Emitters\Abstracts\Balance\BalanceManagerAbstract;
use Emitters\Contracts\Balance\BalanceManagerInterface;

class BalanceManager extends BalanceManagerAbstract implements BalanceManagerInterface {

    /**
     * @param       $user
     * @param array $paymentSystemIds
     * @param bool $original
     *
     * @param bool $no_bonus
     * @return bool|array
     */
    public function getAll($user, array $paymentSystemIds, $original = false, bool $no_bonus = false) {
        if(count($paymentSystemIds) == 0) return false;
        $balance = $this->calculateBalance->get($user, $paymentSystemIds, $no_bonus);
        foreach($balance as $key => $value) {
            $balance[$key]['balance_now'] = df_div((string)$value['balance_now'], (string)1);
        }
        return ($original) ? $balance : $this->decimalBalance($balance);
    }

    /**
     * @param $balance
     *
     * @return mixed
     */
    public function decimalBalance($balance) {
        if($balance) {
            foreach($balance as $key => $value) {
                $balance[$key]['balance_now'] = df_div((string)$value['balance_now'], (string)100000000);
            }
        }
        return $balance;
    }

    public function totalUserBalanceActions($type, $user_id = null, array $operations, $paymentSystem) {
        if(count($paymentSystem) == 0) return false;
        $purchases                = $this->calculateBalance->getSumByOperation($type, $user_id, $operations);
        $total                    = 0;
        $converting_currency_rate = converting_currency_rate();
        $purchases->each(function ($row) use ($paymentSystem, $converting_currency_rate, &$total) {
            $currency     = $paymentSystem->where('id', $row->payment_system_id)->first()->currency;
            $clear_amount = df_div($row->aggregate, 100000000);
            $amount       = $converting_currency_rate->convertAmount($clear_amount, $currency, config('emitters.default_currency_systems'));
            $total        = df_add($total, $amount);
        });
        return $total;
    }

    public function allOperationsGroupBalance($type, $user_id = null, array $operations) {
        return $this->calculateBalance->getSumByOperation($type, $user_id, $operations, true)->each(function ($row) {
            $row->aggregate = df_div($row->aggregate, 100000000);
        });
    }

    /**
     * @param $user
     * @param $amount
     * @param $from_payment_system
     * @param $to_payment_system
     * @return mixed
     * @throws \Exception
     */
    public function exchange($user, $amount, $from_payment_system, $to_payment_system) {
        $balance = $this->getAll($user, [$from_payment_system->id], true, true);
        if(count($balance) == 0){
            throw new \Exception('NOT_ENOUGH_FUNDS_ON_BALANCE_ARRAY_EXCHANGE');
        }

        if($amount > $balance[0]['balance_now']) {
            throw new \Exception('NOT_ENOUGH_FUNDS_ON_BALANCE_EXCHANGE');
        }

        if(!$from_payment_system){
            throw new \Exception('FROM_PAYMENT_SYSTEM_EMPTY_OBJECT');
        }

        if(!$to_payment_system){
            throw new \Exception('TO_PAYMENT_SYSTEM_EMPTY_OBJECT');
        }

        $this->changeBalance([
            'operation'         => 'EXCHANGE',
            'type'              => 'sell',
            'user_id'           => $user->id,
            'payment_system_id' => $from_payment_system->id,
            'amount'            => $amount
        ]);

        $new_amount = converting_currency_rate()->convertAmount($amount, $from_payment_system->currency, $to_payment_system->currency);
        $this->changeBalance([
            'operation'         => 'EXCHANGE',
            'type'              => 'buy',
            'user_id'           => $user->id,
            'payment_system_id' => $to_payment_system->id,
            'amount'            => $new_amount
        ]);
        return users_operations()->createOperationExchange($user, $to_payment_system->id, $from_payment_system->id, $new_amount, $amount);
    }

    /**
     * @param $data = [
     *              'type',
     *              'user_id',
     *              'payment_system_id',
     *              'amount',
     *              ]
     *
     * @return mixed
     */
    public function changeBalance($data) {
        $balance_now = $this->calculateBalance->getBalanceNow($data['user_id'], $data['payment_system_id']);

        $data['balance_before'] = $balance_now;

        if($data['type'] == 'buy') {
            $data['balance_now'] = df_add($balance_now, $data['amount']);
        } elseif($data['type'] == 'sell') {
            $data['balance_now'] = df_sub($balance_now, $data['amount']);
        }
        return $this->model::create($data);
    }

    /**
     * @param $user
     * @param $amount
     * @param $payment_system
     * @return mixed
     * @throws \Exception
     */
    public function withdrawBalance($user, $amount, $payment_system) {
        if(!$user) {
            throw new \Exception('USER_NOT_CORRECTLY');
        }

        if($amount <= 0) {
            throw new \Exception('AMOUNT_DOES_NOT_FIT');
        }

        $wallet = usersdata()->getWallet($user, $payment_system);
        if(!$wallet) {
            throw new \Exception('FOR_WITHDRAW_NEED_FILL_WALLET');
        }

        // exchange from system coin to original payment system
        if(config('emitters.after_add_funds') == 'EXCHANGE') {
            $ps              = paymentsystems()->get();
            $from            = $ps->where('id', config('emitters.default_payment_system_id'))->first();
            $to              = $ps->where('id', $payment_system)->first();
            $amount_exchange = convert_to_bigInteger($amount);

            $res    = $this->exchange($user, $amount_exchange, $from, $to);
            $amount = convert_from_bigInteger($res->amount);
        }
        return $this->withdrawBalanceHandler($user, $amount, $payment_system);
    }

    /**
     * @param $user
     * @param $amount
     * @param $payment_system
     * @return mixed
     * @throws \Exception
     */
    protected function withdrawBalanceHandler($user, $amount, $payment_system) {
        $amount              = df_mul($amount, 100000000, 0);
        $amount_for_withdraw = $amount;
        $commission          = config('emitters.withdrawal_commission');
        $amount_commission   = 0;
        $this->checkBalanceAvalible($user, $amount, $payment_system);

        if($commission > 0 && $commission < 100) {
            $amount_commission   = df_div(df_mul($amount, config('emitters.withdrawal_commission'), 0), 100, 0);
            $amount_for_withdraw = df_sub($amount, $amount_commission, 0);
        }

        $sell = $this->changeBalance([
            'type'              => 'sell',
            'operation'         => 'WITHDRAW',
            'user_id'           => $user->id,
            'amount'            => $amount,
            'payment_system_id' => $payment_system,
        ]);
        return users_operations()->createOperationWithdraw($user, $sell->id, $payment_system, $amount_for_withdraw, $amount_commission);
    }

    /**
     * @param      $user
     * @param      $amount
     * @param      $payment_system
     *
     * @param bool $no_bonus
     *
     * @throws \Exception
     */
    public function checkBalanceAvalible($user, $amount, $payment_system, $no_bonus = true): void {
        $balance = $this->getAll($user, [$payment_system], true, $no_bonus);
        if(count($balance) == 0) {
            throw new \Exception('NOT_ENOUGH_FUNDS_ON_BALANCE_ARRAY');
        }

        if($amount > $balance[0]['balance_now']) {
            throw new \Exception('NOT_ENOUGH_FUNDS_ON_BALANCE');
        }
    }

    /**
     * @param $user
     * @param $amount
     * @param $payment_system_id
     * @param int $is_hold
     * @param bool $no_history
     * @return array
     */
    public function addBonusToBalance($user, $amount, $payment_system_id, $is_hold = 1, $no_history = false) {
        $res = $this->changeBalance([
            'type'              => 'buy',
            'operation'         => 'BONUS',
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system_id,
            'amount'            => $amount,
            'is_hold'           => $is_hold
        ]);
        if($no_history) {
            return $res;
        } else {
            $operation = users_operations()->createOperationBonus($user, $res->id, $amount, $payment_system_id);
            return [$res, $operation];
        }
    }

    /**
     * @param $user
     * @param $amount
     * @param $payment_system_id
     * @param int $is_hold
     * @return mixed
     */
    public function addBonusToBalanceByConfirmed($user, $amount, $payment_system_id, $is_hold = 0) {
        return $this->changeBalance([
            'type'              => 'buy',
            'operation'         => 'BONUS',
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system_id,
            'amount'            => $amount,
            'is_hold'           => $is_hold
        ]);
    }

    /**
     * @param $user
     * @param $amount
     * @param $payment_system_id
     * @param $level
     * @param $from_user_id
     * @param int $deposit_id
     * @param int $from_amount
     * @return array
     */
    public function addRefferalToBalance($user, $amount, $payment_system_id, $level, $from_user_id, $deposit_id = 0, $from_amount = 0) {
        $res       = $this->changeBalance([
            'type'              => 'buy',
            'operation'         => 'REFFERAL',
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system_id,
            'amount'            => $amount,
            'from_user_id'      => $from_user_id,
            'level'             => $level,
        ]);
        $operation = users_operations()->createOperationRefferal($user, $res->id, $amount, $payment_system_id, $level, $from_user_id, $deposit_id, $from_amount);

        return [$res, $operation];
    }

    /**
     * @param $id
     * @param $user_id
     * @return mixed
     */
    public function delete_operation_withdraw($id, $user_id) {
        return $this->model::query()->where('id', $id)->where('type', 'sell')->where('operation', 'WITHDRAW')->where('user_id', $user_id)->delete();
    }
}