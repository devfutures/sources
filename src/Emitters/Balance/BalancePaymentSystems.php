<?php


namespace Emitters\Balance;


use Emitters\Contracts\PaymentSystems\PaymentSystemsContract;

class BalancePaymentSystems {
    /**
     * @var PaymentSystemsContract
     */
    protected $payment_systems;

    public function __construct(PaymentSystemsContract $payment_systems) {
        $this->payment_systems = $payment_systems;
    }

    /**
     * @param array $ids
     * @return array
     */
    public function showBalance(array $ids = []) {
        $ps = $this->payment_systems->grabAll();
        if(count($ids) > 0){
            $ps = $ps->filter(function($row) use ($ids){
                return (in_array($row->id, $ids));
            });
        }
        $returned = [];
        if($ps){
            foreach($ps as $row){
                try {
                    $returned[] = ['status' => true, 'balance' => $this->getBalance($row), 'id' => $row->id];
                } catch (\Exception $e) {
                    $returned[] = ['status' => false, 'message' => $e->getMessage(), 'id' => $row->id];
                }
            }
        }
        return $returned;
    }

    /**
     * @param $payment_system
     * @return mixed
     * @throws \Exception
     */
    public function getBalance($payment_system) {
        if(!class_exists($payment_system->class_name)) {
            throw new \Exception('PAYMENT_SYSTEM_CLASS_NAME_ERROR');
        }

        $abstract_payment_class = $payment_system->class_name;
        $ex = app()->make($abstract_payment_class);

        if(!method_exists($ex, 'form')) {
            throw new \Exception('PAYMENT_SYSTEM_CLASS_NAME_ERROR');
        }

        $res = $ex->balance($payment_system->currency);
        if(!is_array($res)){
            return $res;
        }else{
            return $res['confirmed'];
        }
    }
}