<?php

namespace Emitters\Balance;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory as EloquentFactory;

class BalanceServiceProvider extends ServiceProvider {
    public function boot() {
        $this->loadMigrationsFrom(__DIR__ . '/migrations');

        $this->publishes([
            __DIR__ . '/migrations/' => database_path('migrations')
        ], 'migrations');

        $this->app->make(EloquentFactory::class)->load(__DIR__ . '/factories/');
    }

    public function register()
    {
        $this->app->bind(BalancePaymentSystems::class, function ($app) {
            return new BalancePaymentSystems(paymentsystems());
        });
        $this->app->alias(BalancePaymentSystems::class, 'balance_payment_system');
    }
}
