<?php

namespace Emitters\Balance;

use Emitters\Contracts\Balance\CalculateBalanceContract;
use Emitters\Abstracts\Balance\CalculateBalanceAbstract;

class CalculateBalance extends CalculateBalanceAbstract implements CalculateBalanceContract {
    public function __construct($model, $drive) {
        $this->model = $model;
        $this->drive = $drive;
    }

    public function get($user, array $paymentSystemIds) {
        return $this->{$this->drive}($user, $paymentSystemIds);
    }

    /**
     * Get group results, SLOW work
     * @param  $user               object of user
     * @param  array $paymentSystemIds pluck id payment system
     * @return array balance
     */
    protected function getWithGroup($user, array $paymentSystemIds) {
        $balance = $this->model::select('balance_now', 'payment_system_id')->whereIn('id', function ($query) use ($user) {
            $query->
            selectRaw('max(id)')->
            where('user_id', $user->id)->
            from($this->model->getTable())->
            whereNull('deleted_at')->
            groupBy('payment_system_id');
        })->get();
        return $balance->toArray();
    }

    /**
     * Get results with union all FAST
     * @param  $user               object of user
     * @param  array $paymentSystemIds pluck id payment system
     * @return array                     result balance
     */
    protected function getWithUnion($user, array $paymentSystemIds) {
        $ips_list = $this->model->
        select('balance_now', 'payment_system_id')->
        where('user_id', $user->id)->
        where('payment_system_id', $paymentSystemIds[0])->
        orderBy('id', 'desc')->
        limit(1);
        unset($paymentSystemIds[0]);
        if (count($paymentSystemIds) > 0) {
            foreach ($paymentSystemIds as $row) {
                $ip_list_subquery = $this->model->
                select('balance_now', 'payment_system_id')->
                where('user_id', $user->id)->
                where('payment_system_id', $row)->
                orderBy('id', 'desc')->
                limit(1);
                $ips_list         = $ips_list->unionAll($ip_list_subquery);
            }
        }
        $balance = $ips_list->get();
        return $balance->toArray();
    }

    /**
     * Result math (buy-sell) in real time MIDDLE FAST
     * @param  $user               object of user
     * @param  array $paymentSystemIds pluck id payment system
     * @return array                     result balance
     */
    protected function getWithMathRealTime($user, array $paymentSystemIds) {
        $buy  = $this->getSumByOperation('buy', $user->id);
        $sell = $this->getSumByOperation('sell', $user->id);

        $balance = collect([]);

        foreach ($paymentSystemIds as $row) {
            $buy_find = $buy->where('payment_system_id', $row)->first();
            if (!$buy_find) {
                $buy_find = 0;
            } else {
                $buy_find = $buy_find->aggregate;
            }

            $sell_find = $sell->where('payment_system_id', $row)->first();
            if (!$sell_find) {
                $sell_find = 0;
            } else {
                $sell_find = $sell_find->aggregate;
            }

            $balance->push([
                'balance_now'       => bcsub($buy_find, $sell_find),
                'payment_system_id' => $row,
            ]);
        }
        return $balance->toArray();
    }

    public function getSumByOperation(string $type, $user_id) {
        return $this->model::selectRaw('payment_system_id, sum(amount) as aggregate')->where('type', $type)->where('user_id', $user_id)->groupBy('payment_system_id')->get();
    }

    public function getBalanceNow(int $user_id, int $payment_system_id) {
        $balance_now = $this->model::where('user_id', $user_id)->
        where('payment_system_id', $payment_system_id)->
        orderBy('id', 'desc')->
        value('balance_now');
        return ($balance_now) ? $balance_now : 0;
    }
}