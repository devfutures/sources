<?php

namespace Emitters\Balance;

use Emitters\Contracts\Balance\CalculateBalanceContract;
use Emitters\Abstracts\Balance\CalculateBalanceAbstract;

class CalculateBalance extends CalculateBalanceAbstract implements CalculateBalanceContract {
    public function __construct($model, $drive) {
        $this->model = $model;
        $this->drive = $drive;
    }

    /**
     * @param       $user
     * @param array $paymentSystemIds
     * @param bool $no_bonus
     *
     * @return mixed
     */
    public function get($user, array $paymentSystemIds, bool $no_bonus) {
        return $this->{$this->drive}($user, $paymentSystemIds, $no_bonus);
    }

    /**
     * Get group results, SLOW work
     *
     * @param        $user               object of user
     * @param  array $paymentSystemIds pluck id payment system
     * @param bool $no_bonus
     *
     * @return array balance
     */
    protected function getWithGroup($user, array $paymentSystemIds, bool $no_bonus = false) {
        $balance = $this->model::select('balance_now', 'payment_system_id')->whereIn('id', function ($query) use ($user, $paymentSystemIds) {
            $query->
            selectRaw('max(id)')->
            where('user_id', $user->id)->
            whereIn('payment_system_id', $paymentSystemIds)->
            from($this->model->getTable())->
            whereNull('deleted_at')->
            groupBy('payment_system_id');
        })->get();
        return $balance->toArray();
    }

    /**
     * Get results with union all FAST
     *
     * @param        $user               object of user
     * @param  array $paymentSystemIds pluck id payment system
     * @param bool $no_bonus
     *
     * @return array                     result balance
     */
    protected function getWithUnion($user, array $paymentSystemIds, bool $no_bonus = false) {
        $ips_list = $this->model->
        select('balance_now', 'payment_system_id')->
        where('user_id', $user->id)->
//        where('is_hold', 0)->
        where('payment_system_id', $paymentSystemIds[0])->
        orderBy('id', 'desc')->
        limit(1);
        unset($paymentSystemIds[0]);
        if(count($paymentSystemIds) > 0) {
            foreach($paymentSystemIds as $row) {
                $ip_list_subquery = $this->model->
                select('balance_now', 'payment_system_id')->
                where('user_id', $user->id)->
//                where('is_hold', 0)->
                where('payment_system_id', $row)->
                orderBy('id', 'desc')->
                limit(1);
                $ips_list         = $ips_list->unionAll($ip_list_subquery);
            }
        }
        $balance = $ips_list->get();
        return $balance->toArray();
    }

    /**
     * Result math (buy-sell) in real time MIDDLE FAST
     *
     * @param        $user               object of user
     * @param  array $paymentSystemIds pluck id payment system
     * @param bool $no_bonus
     *
     * @return array                     result balance
     */
    protected function getWithMathRealTime($user, array $paymentSystemIds, bool $no_bonus = false) {
        $buy  = $this->getSumByOperation('buy', $user->id, null, $no_bonus);
        $sell = $this->getSumByOperation('sell', $user->id, null, $no_bonus);

        $balance = collect([]);

        foreach($paymentSystemIds as $row) {
            $buy_find = $buy->where('payment_system_id', $row)->first();
            if(!$buy_find) {
                $buy_find = 0;
            } else {
                $buy_find = $buy_find->aggregate;
            }

            $sell_find = $sell->where('payment_system_id', $row)->first();
            if(!$sell_find) {
                $sell_find = 0;
            } else {
                $sell_find = $sell_find->aggregate;
            }

            $balance->push([
                'balance_now'       => df_sub($buy_find, $sell_find),
                'payment_system_id' => $row,
            ]);
        }
        return $balance->toArray();
    }

    /**
     * @param $user
     *
     * @return \Illuminate\Support\Collection
     */
    public function sumBonusesBalance($user) {
        $finish_balance_bonuses = collect();
        $hold_operations        = $this->model::selectRaw('payment_system_id, sum(amount) as aggregate, type')->
        where('user_id', $user->id)->
        where('is_hold', 1)->
        groupBy(['payment_system_id', 'type'])->
        get();
        if($hold_operations) {
            foreach($hold_operations as $row) {
                $amount = 0;
                if($row->type == 'buy') {
                    $amount     = $row->aggregate;
                    $sell_bonus = $hold_operations->where('type', 'sell')->where('payment_system_id', $row->payment_system_id)->first();
                    if($sell_bonus) {
                        $amount = df_sub($row->aggregate, $sell_bonus->aggregate);
                    }
                } elseif($row->type == 'sell') {
                    $buy_bonus = $hold_operations->where('type', 'buy')->where('payment_system_id', $row->payment_system_id)->first();
                    if(!$buy_bonus) {
                        $amount = df_sub(0, $row->aggregate);
                    }
                }
                if($amount != 0) {
                    $finish_balance_bonuses->push([
                        'payment_system_id' => $row->payment_system_id,
                        'amount'            => $amount
                    ]);
                }
            }
        }
        dump($finish_balance_bonuses);
        return $finish_balance_bonuses;
    }

    /**
     * @param string $type
     * @param null $user_id
     * @param array|null $operations
     * @param bool $no_bonus
     *
     * @return mixed
     */
    public function getSumByOperation(string $type, $user_id = null, array $operations = null, bool $no_bonus = false) {
        return $this->model::
        selectRaw('payment_system_id, sum(amount) as aggregate')
            ->where('type', $type)
            ->where(function ($query) use ($user_id) {
                if($user_id) {
                    $query->where('user_id', $user_id);
                }
            })
            ->where(function ($query) use ($no_bonus) {
                if($no_bonus) {
                    $query->where('is_hold', 0);
                }
            })
            ->where(function ($query) use ($operations) {
                if($operations) {
                    $query->whereIn('operation', $operations);
                }
            })->groupBy('payment_system_id')->get();
    }

    /**
     * @param int $user_id
     * @param int $payment_system_id
     *
     * @return int
     */
    public function getBalanceNow(int $user_id, int $payment_system_id) {
        $balance_now = $this->model::where('user_id', $user_id)->
        where('payment_system_id', $payment_system_id)->
        orderBy('id', 'desc')->
        value('balance_now');
        return ($balance_now) ? $balance_now : 0;
    }
}