<?php

namespace Emitters\Balance\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Users_Balance extends Model {
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'payment_system_id',
        'type',
        'operation',
        'parent_id',
        'amount',
        'balance_before',
        'balance_now',
        'from_user_id',
        'level'
    ];
}
