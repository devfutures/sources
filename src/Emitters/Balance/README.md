---
### Balance
#### Извлечение BalanceManager
##### Получение через IoC
```php
$balanceManager = resolve('librariesair.balance');
```
##### Использование в контроллере с помощью type-hinting
```php
use App\LibrariesAir\Balance\BalanceManager;
...
public function index(BalanceManager $balanceManager) {
    ...
}
```
#### Методы BalanceManager

##### Получение всех балансов пользователя

```php
$balanceManager->getAll($user, $paymentSystemIds, $original);
```
* $user - объект модели User
* $paymentSystemIds - массив id платежных систем для которых требуется получить баланс
* $original - получение не округленных балансов в случае если указан true, по умолчанию false
* return type - array

##### Округление балансов
```php
$balanceManager->decimalBalance($balances);
```
* $balances - массив балансов
* return type - array

##### Изменение баланса
```php
$balanceManager->changeBalance($data);
```
##### $data - массив данных для изменения баланса
```php
$exampleData = [
    'user_id'           => 1, //id пользователя для которого необходимо изменить баланс.
    'payment_system_id' => 3, //id платежной системы
    'type'              => 'buy' // 'buy' в случае покупки, 'sell' в случае продажы.
];
```
* return type - boolean true/false.