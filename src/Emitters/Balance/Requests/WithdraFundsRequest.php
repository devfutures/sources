<?php

namespace Emitters\Balance\Requests;

use App\LibrariesAir\PaymentSystems\PaymentSystemsManager;
use App\LibrariesAir\UsersData\UsersDataManager;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class WithdrawFundsRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(PaymentSystemsManager $paymentSystemsManager,UsersDataManager $usersDataManager){
        $ps = $paymentSystemsManager->getOne($this->request->filter('payment_system'));
        if($ps == null) {
            $ps = new \StdClass();
            $ps->id = 0;
            $ps->withdraw = 0;
            $ps->status = 0;
            $ps->min_sum_withdraw = 0;
            $ps->max_sum_withdraw = 0;
            $ps->class_name = "";
        }

        $wallet = $usersDataManager->getWallet(Auth::user(),$ps->id);
        return [
            'payment_system' => [
                'required','integer',
                function ($attribute,$value,$fail) use ($ps,$wallet){
                    if($ps == null){
                        $fail(__('validation.exists'));
                        return ;
                    }

                    if($ps->withdraw == 0 || $ps->status == 0){
                        $fail(__('validation.exists'));
                        return ;
                    }

                    if(!$wallet){
                        $fail(__('validation.filled',['attribute' => 'wallet for the payment system']));
                        return ;
                    }
                }
            ],
            'amount'         => [
                'required_with_all:payment_system','numeric','min:'.$ps->min_sum_withdraw,
            ],
        ];
    }

    public function messages(){
        return [
            'payment_system.filled' => 'Email is required!'
        ];
    }
}
