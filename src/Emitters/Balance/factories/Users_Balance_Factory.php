<?php

use App\User;
use Faker\Generator as Faker;

$factory->define(\Emitters\Balance\Models\Users_Balance::class, function (Faker $faker) {
    $opt     = [
        "1" => ['sell', 'SELL_FUNDS'],
        "2" => ['buy', 'ADD_FUNDS'],
    ];
    $rand    = mt_rand(1, 2);
    $balance = mt_rand(500000, 100000000000);
    return [
        'type'              => $opt[$rand][0],
        'operation'         => $opt[$rand][1],
        'user_id'           => function () {
            return factory(User::class)->create()->id;
        },
        'payment_system_id' => function () {
            return factory(\Emitters\PaymentSystems\Models\Payment_System::class)->create()->id;
        },
        'amount'            => $balance,
        'balance_now'       => $balance,
        'is_hold'           => 0
    ];
});