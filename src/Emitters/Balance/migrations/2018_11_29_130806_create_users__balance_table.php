<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersBalanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users__balances', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type')->default('')->index();
            $table->string('operation')->default('')->index();
            $table->integer('parent_id')->default(0)->index();
            $table->integer('user_id')->index();
            $table->integer('payment_system_id')->index();
            
            $table->biginteger('amount')->default(0);
            $table->biginteger('balance_before')->default(0);
            $table->biginteger('balance_now')->default(0);

            $table->integer('from_user_id')->default(0)->index();
            $table->tinyInteger('level')->default(0)->index();

            $table->tinyInteger('is_hold')->default(0)->index();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users__balances');
    }
}
