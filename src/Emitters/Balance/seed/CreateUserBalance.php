<?php
namespace Emitters\Balance\Seed;

use Illuminate\Database\Seeder;
use App\LibrariesAir\Balance\BalanceManager;

class CreateUserBalance extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(BalanceManager $manager)
    {   
        $i = 0;
        while($i <= 50000){
            $opt = [
                "1" => ['sell', 'SELL_FUNDS'],
                "2" => ['buy', 'ADD_FUNDS'],
            ];
            $rand = mt_rand(1, 2);
            $manager->changeBalance([
                'type'              => $opt[$rand][0],
                'operation'         => $opt[$rand][1],
                'user_id'           => mt_rand(1, 50),
                'payment_system_id' => mt_rand(1, 12),
                'amount'            => mt_rand(500000, 100000000000),
            ]);
            $i++;
        }
    }
}
