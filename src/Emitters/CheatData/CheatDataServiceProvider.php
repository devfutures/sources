<?php

namespace Emitters\CheatData;

use Emitters\CheatData\CheatManager;
use Emitters\CheatData\Commands\StartCheat;
use Emitters\CheatData\Models\Cheat;
use Emitters\CheatData\Repositories\CheatRepository;

use Illuminate\Support\ServiceProvider;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Database\Eloquent\Factory as EloquentFactory;
class CheatDataServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/migrations');

        $this->publishes([
            __DIR__.'/migrations/' => database_path('migrations')
        ], 'migrations');

        $this->app->booted(function ($app) {
            $schedule = $this->app->make(Schedule::class);
            $schedule->command('cheat:start')->cron('* * * * *')->withoutOverlapping();
        });

        $this->app->make(EloquentFactory::class)->load(__DIR__ . '/factories/');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands([
            StartCheat::class
        ]);


        $this->app->bind(CheatManager::class, function () {
            return new CheatManager(new CheatRepository(new Cheat()));
        });

        $this->app->alias(CheatManager::class, 'cheat');
    }
}