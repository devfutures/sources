<?php


namespace Emitters\CheatData;


use Carbon\Carbon;
use Emitters\CheatData\Repositories\CheatRepository;

class CheatManager {
    /**
     * @var CheatRepository
     */
    protected $cheatRepository;

    /**
     * CheatManager constructor.
     * @param CheatRepository $cheatRepository
     */
    public function __construct(CheatRepository $cheatRepository) {
        $this->cheatRepository = $cheatRepository;
    }

    /**
     * @return CheatRepository[]|\Illuminate\Database\Eloquent\Collection|mixed
     */
    public function getCheatData() {
        $data = $this->cheatRepository->get()->keyBy('type')->transform(function ($item_original){
            $item = $item_original->value_now;
            return $item;
        })->toArray();
        $data = $this->addDataDefault($data);
        return $data;
    }

    /**
     * @return bool
     */
    public function runSchedule() {
        return $this->act($this->cheatRepository->getCheatSchedule());
    }

    /**
     * @param $getCheatSchedule
     * @return bool
     */
    private function act($getCheatSchedule) {
        if(count($getCheatSchedule) == 0) return false;

        foreach($getCheatSchedule as $row) {
            $value_now = $row->value_now + mt_rand($row->condition_from, $row->condition_to);
            $next_run  = Carbon::now()->addMinutes(mt_rand($row->period_from, $row->period_to));
            $this->cheatRepository->update($row->id, [
                'value_now' => $value_now,
                'next_run'  => $next_run
            ]);
        }

        return true;
    }

    /**
     * @param $data
     * @return mixed
     */
    protected function addDataDefault(array $data) {
        if(!array_key_exists('deposit', $data)) {
            $data['deposit'] = "0";
        }
        if(!array_key_exists('user', $data)) {
            $data['user'] = "0";
        }
        if(!array_key_exists('partners', $data)) {
            $data['partners'] = "0";
        }
        return $data;
    }
}