<?php

namespace Emitters\CheatData\Commands;

use Emitters\CheatData\CheatManager;
use Illuminate\Console\Command;

class StartCheat extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cheat:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start cheat run';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param CheatManager $cheatManager
     * @return mixed
     */
    public function handle(CheatManager $cheatManager)
    {
        $this->info($cheatManager->runSchedule());
        return true;
    }
}
