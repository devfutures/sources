<?php

namespace Emitters\CheatData\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cheat extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'type', 'condition_from', 'condition_to', 'period_from', 'period_to', 'next_run', 'value_now'
    ];
}
