<?php


namespace Emitters\CheatData\Repositories;


use Carbon\Carbon;
use Emitters\CheatData\Models\Cheat;

class CheatRepository {
    /**
     * @var Cheat
     */
    protected $model;

    /**
     * CheatRepository constructor.
     * @param Cheat $model
     */
    public function __construct(Cheat $model) {
        $this->model = $model;
    }


    public function create($data) {
        return $this->model::query()->create($data);
    }

    public function delete($id, $field = 'id') {
        return $this->model::query()->where($field, $id)->delete();
    }

    public function first($val, $colom = 'id') {
        return $this->model::query()->where($colom, $val)->first();
    }

    public function update($id, array $data) {
        return $this->model::query()->where('id', $id)->update($data);
    }

    public function get() {
        return $this->model::query()->get();
    }

    public function getCheatSchedule() {
        return $this->model::query()->where('next_run', '<=', Carbon::now())->get();
    }
}