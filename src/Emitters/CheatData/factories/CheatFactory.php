<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Emitters\CheatData\Models\Cheat;
use Faker\Generator as Faker;
use Carbon\Carbon;
$factory->define(Cheat::class, function (Faker $faker) {
    return [
        'type'           => $faker->randomElement(['deposit', 'user', 'partners']),
        'condition_from' => $faker->randomDigit,
        'condition_to'   => $faker->randomDigit,

        'period_from' => 40,
        'period_to'   => 60,

        'next_run'  => Carbon::now(),
        'value_now' => 0,
    ];
});
