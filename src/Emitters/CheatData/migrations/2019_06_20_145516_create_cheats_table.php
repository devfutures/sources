<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cheats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type');
            // in integer or float
            $table->double('condition_from', 14, 8);
            $table->double('condition_to', 14, 8);
            // in minutes
            $table->integer('period_from');
            $table->integer('period_to');
            $table->string('value_now')->default(0);
            $table->timestamp('next_run');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cheats');
    }
}
