<?php
namespace Emitters\CheatData\seed;

use Illuminate\Database\Seeder;
use Emitters\CheatData\Models\Cheat;
class CheatSeed extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        factory(Cheat::class)->create([
            'type'           => 'deposit',
            'condition_from' => 50,
            'condition_to'   => 100,
            'period_from'    => 40,
            'period_to'      => 60,
        ]);

        factory(Cheat::class)->create([
            'type'           => 'user',
            'condition_from' => 1,
            'condition_to'   => 1,
            'period_from'    => 45,
            'period_to'      => 55,
        ]);

        factory(Cheat::class)->create([
            'type'           => 'partners',
            'condition_from' => 1,
            'condition_to'   => 1,
            'period_from'    => 240,
            'period_to'      => 260,
        ]);
    }
}
