<?php


namespace Emitters\Contracts\AdminControl;


interface EnvManagerContract {
    public function getAvailableEnvironmentVariables(string $config);

    public function puts_to_env(array $envValues);
}