<?php


namespace Emitters\Contracts\Affiliate;


interface AffiliateManagerContract {
    /**
     * @return mixed
     */
    public function getAffiliateSettings();

    /**
     * @return mixed
     */
    public function getAffiliateLevels();

    /**
     * @param string $type
     * @return mixed
     */
    public function getAffiliateLevelByType(string $type);

    /**
     * @param $data
     *
     * @return mixed
     */
    public function saveAffiliateLevels(array $data);

    /**
     * @param $data
     *
     * @return mixed
     */
    public function saveSettings($data);

    /**
     * @return mixed
     */
    public function getAffiliatePercentsDefault();
}