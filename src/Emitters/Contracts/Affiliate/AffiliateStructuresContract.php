<?php


namespace Emitters\Contracts\Affiliate;


use Illuminate\Foundation\Auth\User;

interface AffiliateStructuresContract {
    /**
     * @param User $user
     *
     * @return mixed
     */
    public function createUserStructures(User $user);

    /**
     * @param     $user_id
     * @param     $PathA
     * @param int $Limit
     *
     * @return mixed
     */
    public function getUserParentLevelPath($user_id,&$PathA,$Limit = 0);

    /**
     * @param User $user
     * @return mixed
     */
    public function getUserOriginStructure(User $user, $max_level = 0);

    /**
     * @param User  $user
     * @param int   $level
     * @param array $filters
     * @param array $with
     *
     * @return mixed
     */
    public function getUserAffiliateStructure(User $user, $level = 1, array $filters = [],array $with = []);

    /**
     * @param User $user
     * @param null $level
     * @return mixed
     */
    public function getUserAffiliateStructureSimple(User $user, $level = null);

    /**
     * @param $user
     * @param $structure
     *
     * @return mixed
     */
    public function addAdditionalData($user, $structure);

    /**
     * @param $user
     * @param $structure
     *
     * @return mixed
     */
    public function addAffiliateCommission($user, $structure);

    /**
     * @param $user_id
     * @param int $max_level
     * @return mixed
     */
    public function getCountUserStructures($user_id, $max_level = 0);

    /**
     * @param $user
     * @param $userRefs
     * @param string $field_to_extract
     * @return mixed
     */
    public function getActiveUsersAllLines($user, $userRefs, $field_to_extract = 'id');
}