<?php


namespace Emitters\Contracts\Affiliate;


interface PartnershipAccrualContract {
    /**
     * @param string $type
     * @param $user
     * @param $amount
     * @param $deposit_id
     * @param $payment_system_id
     * @return mixed
     */
    public function affiliateBonus(string $type, $user, $amount, $deposit_id, $payment_system_id);

    /**
     * @param $data
     * @return mixed
     */
    public function runningCharges($data);
}