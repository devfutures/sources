<?php

namespace Emitters\Contracts\Balance;


interface BalanceManagerInterface {
    public function getAll($user, array $paymentSystemIds, $original = false);

    public function decimalBalance($balance);

    public function changeBalance($data);
}