<?php

namespace Emitters\Contracts\Balance;


interface BalanceManagerInterface {
    /**
     * @param       $user
     * @param array $paymentSystemIds
     * @param bool $original
     * @param bool $no_bonus
     *
     * @return mixed
     */
    public function getAll($user, array $paymentSystemIds, $original = false, bool $no_bonus = false);

    /**
     * @param $balance
     *
     * @return mixed
     */
    public function decimalBalance($balance);

    /**
     * @param $data
     *
     * @return mixed
     */
    public function changeBalance($data);

    /**
     * @param $user
     * @param $amount
     * @param $from_payment_system
     * @param $to_payment_system
     * @return mixed
     */
    public function exchange($user, $amount, $from_payment_system, $to_payment_system);

    /**
     * @param $user
     * @param $amount
     * @param $payment_system_id
     * @param int $is_hold
     * @param bool $no_history
     * @return mixed
     */
    public function addBonusToBalance($user, $amount, $payment_system_id, $is_hold = 1, $no_history = false);

    /**
     * @param $user
     * @param $amount
     * @param $payment_system_id
     * @param $level
     * @param $from_user_id
     * @param int $deposit_id
     * @param int $from_amount
     * @return mixed
     */
    public function addRefferalToBalance($user, $amount, $payment_system_id, $level, $from_user_id, $deposit_id = 0, $from_amount = 0);

    /**
     * @param $id
     * @param $user_id
     * @return mixed
     */
    public function delete_operation_withdraw($id, $user_id);

    /**
     * @param $user
     * @param $amount
     * @param $payment_system
     * @return mixed
     */
    public function withdrawBalance($user, $amount, $payment_system);
}