<?php
namespace Emitters\Contracts\Balance;

interface CalculateBalanceContract{
    /**
     * @param       $user
     * @param array $paymentSystemIds
     * @param bool  $no_bonus
     *
     * @return mixed
     */
    public function get($user, array $paymentSystemIds, bool $no_bonus);

    /**
     * @param string     $type
     * @param null       $user_id
     * @param array|null $operations
     *
     * @return mixed
     */
    public function getSumByOperation(string $type, $user_id = null, array $operations = null);

    /**
     * @param int $user_id
     * @param int $payment_system_id
     *
     * @return mixed
     */
    public function getBalanceNow(int $user_id, int $payment_system_id);
}