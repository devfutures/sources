<?php
namespace Emitters\Contracts\Balance;

interface CalculateBalanceContract{
	public function get($user, array $paymentSystemIds);
	public function getSumByOperation(string $type, $user_id);
	public function getBalanceNow(int $user_id, int $payment_system_id);
}