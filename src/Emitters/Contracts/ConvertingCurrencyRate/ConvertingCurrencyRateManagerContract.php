<?php


namespace Emitters\Contracts\ConvertingCurrencyRate;


interface ConvertingCurrencyRateManagerContract {
    public function getCurrencyRate();

    public function parseCurrencyRate();

    public function insertNewCurrencyRate(array $data);

    public function getExpectedCoins($fiat = 0);

    public function getDataForFill($data, $in_currency = 'usd');

    public function convertAmount($amount = 0, $from_currency = null, $to_currency = 'USD');
}