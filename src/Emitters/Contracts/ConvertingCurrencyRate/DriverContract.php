<?php

namespace Emitters\Contracts\ConvertingCurrencyRate;


interface DriverContract
{
    public function getAllCurrencyRate($expected_id);
}