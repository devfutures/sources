<?php


namespace Emitters\Contracts\DepositAccruals;


use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Collection;

interface DepositAccrualsContract {
    /**
     * @return mixed
     */
    public function accrualsDataPreparation();

    /**
     * @param User $user
     * @param int  $deposit_id
     * @param int  $duration_plan
     *
     * @return mixed
     */
    public function checkDepositClose(User $user,int $deposit_id,int $duration_plan = 0);

    /**
     * @param $deposit
     * @return Collection|mixed
     */
    public function accrualsByDeposit($deposit);
}