<?php
namespace Emitters\Contracts\DepositPlans;

/**
 * Interface DepositPlansContract
 * @package Emitters\Contracts\DepositPlans
 */
interface DepositPlansContract{
    /**
     * @param array $data
     *
     * @return mixed
     */
    public function store(array $data);

    /**
     * @param      $filters
     * @param bool $by_paginate
     *
     * @return mixed
     */
    public function getPlans($filters, $by_paginate = true);

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getPlanById($id);

    /**
     * @param $amount
     *
     * @return mixed
     */
    public function getPlanByAmount($amount);

    /**
     * @param $plan
     *
     * @return mixed
     */
    public function modifyPlan($plan);

    /**
     * @param $plan
     *
     * @return mixed
     */
    public function getBreakEvenDate($plan);

    /**
     * @param $plan
     *
     * @return mixed
     */
    public function getDateCloseDeposit($plan);


    /**
     * @param      $plan
     * @param null $date
     *
     * @return mixed
     */
    public function getNextAccruals($plan, $date = null);

    /**
     * @param      $plan_id
     * @param null $date
     *
     * @return mixed
     */
    public function getPercent($plan_id, $date = null);

    /**
     * @param      $plan
     * @param null $date
     *
     * @return mixed
     */
    public function extractionPercent($plan, $date = null);

    /**
     * @param $plan
     *
     * @return mixed
     */
    public function getMinMaxAmount($plan);

    /**
     * @param string $amount
     * @param string $percent
     *
     * @return mixed
     */
    public function getAmountToProfit(string $amount, string $percent);

    /**
     * @return mixed
     */
    public function defaultPaymentDaysPercent();

    /**
     * @param array $preCreatedData
     *
     * @return mixed
     */
    public function comparePaymentDaysPercent(array $preCreatedData);

    /**
     * @param array $planIDs
     *
     * @return mixed
     */
    public function delete(array $planIDs);

    /**
     * @param $planID
     * @param array $data
     * @return mixed
     */
    public function update($planID, array $data);

    /**
     * @param $date
     * @param $payments_days_percent
     *
     * @return mixed
     */
    public function getPercentByDate($date, $payments_days_percent);

    /**
     * @param $data
     *
     * @return mixed
     */
    public function modifyDate($data);

    /**
     * @param $plan
     *
     * @return mixed
     */
    public function sortPaymentDaysPercent($plan);

    /**
     * @param array $filters
     *
     * @return mixed
     */
    public function getPlansThatAbleToDeposit($filters = []);

    /**
     * @return mixed
     */
    public function toNextPlanSeconds();

    /**
     * @return mixed
     */
    public function getPlanForOpenFuture();
}