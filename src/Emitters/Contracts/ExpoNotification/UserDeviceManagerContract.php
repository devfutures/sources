<?php

namespace Emitters\Contracts\ExpoNotification;


interface UserDeviceManagerContract {
    public function createDevice(array $data);

    public function getDevices();

    public function getUserDevices(int $userId);

    public function getDeviceBy($value, $field = 'id');

    public function updateDevice(int $deviceID, array $data);

    public function deleteDevices(array $deviceIDs);
}