<?php

namespace Emitters\Contracts\Faq;


use Emitters\Faq\Models\Faqs_Category;
use Illuminate\Foundation\Auth\User;

interface FaqContract {
    public function createFaqCategory(array $faqCategoryData);

    public function createFaqWithContent(User $user, array $faqData = null, array $faqContentData = null);

    public function getFaq($lang = 'en', $sort = false);

    public function getAllFaq($filters = []);

    public function getFaqByID($faqID, $lang = null);

    public function getFaqByCategory($value, $lang = null);

    public function getDraftFaq();

    public function updateFaq(array $faqData);

    public function updateContent(array $contentData);

    public function updateCategory(Faqs_Category $faqs_Category, array $categoryData);

    public function deleteFaq(array $faqIDs, $withContent = true);

    public function deleteContent(array $contentIDs);

    public function toDraft(array $draftIDs);
}