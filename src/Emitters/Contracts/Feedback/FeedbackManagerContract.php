<?php

namespace Emitters\Contracts\Feedback;


interface FeedbackManagerContract {
    /**
     * @param $value
     * @param string $field
     * @return mixed
     */
    public function getOneBy($value, string $field);

    /**
     * @param array $filters
     * @param bool $by_paginate
     * @return mixed
     */
    public function getAll(array $filters = [], $by_paginate = true);

    /**
     * @param array $data
     * @return mixed
     */
    public function add(array $data);

    /**
     * @param array $ids
     * @return mixed
     */
    public function delete(array $ids);

    /**
     * @return mixed
     */
    public function getCountingMessages();

    /**
     * @param array $ids
     * @param $status
     * @return mixed
     */
    public function changeStatus(array $ids, $status);

    /**
     * @param $id
     * @param $user_id
     * @param $answer
     * @return mixed
     */
    public function preAnswer($id, $user_id, $answer);

    /**
     * @param $id
     * @param null $answer
     * @return mixed
     */
    public function sendAnswer($id, $answer = null);
}