<?php

namespace Emitters\Contracts\Google2FA;


use App\User;

interface Google2FaContract {
    public function getSecuredFeatures(): array;

    public function getDataForSetUpGoogle2FA(User $user);

    public function saveGoogle2FADataToUser(User $user, array $data);

    public function updateGoogle2FASettings(User $user, array $data);

    public function disableGoogle2FA($userID);

    public function getGoogle2FaSettings(User $user);

    public function checkSecuredFeatureStatus(User $user, string $feature);

    public function getUserSecret(User $user);

    public function checkAuthStatus(User $user);

    public function validateLogin(User $user, string $googleSecret);

    public function checkSecret(string $oneTimePassword = null, string $google2faSecret = null);
}