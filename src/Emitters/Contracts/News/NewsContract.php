<?php


namespace Emitters\Contracts\News;

use Illuminate\Http\UploadedFile;

interface NewsContract {
    public function createNews(array $request, UploadedFile $file = null);

    public function update(array $newsData, UploadedFile $file = null);

    public function storeNews(array $data = null);

    public function storeContent(int $newsID, array $data = null);

    public function updateNews($newsID, array $newsData = null);

    public function updateContent($contentID, array $contentData = null);

    public function getAll(array $filters);

    public function getByID($newsID, $lang = 'en', $isAdmin = false);

    public function deleteNews($newsID, $withContent = false);

    public function getNewsForPublish($lang = 'en');
}