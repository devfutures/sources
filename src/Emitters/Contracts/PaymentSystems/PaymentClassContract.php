<?php


namespace Emitters\Contracts\PaymentSystems;


interface PaymentClassContract {
    /**
     * @param string $unit
     * @return mixed
     */
    function balance($unit = "USD");

    /**
     * @param $payment_id
     * @param $sum
     * @param string $unit
     * @return mixed
     */
    public function form($payment_id, $sum, $unit = 'USD');
}