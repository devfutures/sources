<?php
namespace Emitters\Contracts\PaymentSystems;

use Illuminate\Http\UploadedFile;

interface PaymentSystemsContract{
    /**
     * @param array $filters
     * @param int $status
     * @return mixed
     */
    public function get(array $filters = [], $status = 1);

    /**
     * @return mixed
     */
    public function grabAll();

    /**
     * @param        $id
     * @param array $data
     * @param string $field
     *
     * @return true or false save data
     */
    public function change($id, array $data, $field = 'id');

    /**
     * @param array $data
     *
     * @return true or false insert data
     */
    public function addPaymentSystem(array $data);

    /**
     * @param        $id
     * @param string $field
     *
     * @return mixed
     */
    public function destroy($id, $field = 'id');

    /**
     * @param        $id
     * @param string $field
     *
     * @return mixed
     */
    public function restore($id, $field = 'id');

    /**
     * @param        $id
     * @param string $field
     *
     * @return mixed
     */
    public function getOne($id, $field = 'id');

    /**
     * @param       $user
     * @param array $sort
     *
     * @return mixed
     */
    public function getAll($user, array $sort = ['sort', 'asc']);

    /**
     * @param $currency
     * @param $transaction
     *
     * @return mixed
     */
    public function addBlockchainLink($currency, $transaction);

    /**
     * @param $currency
     * @param $value
     * @param $amount
     *
     * @return mixed
     */
    public function addBlockchainPayLink($currency, $value, $amount);

    /**
     * [merge description]
     * @param  [type] $original      [description]
     * @param  [type] $to_merge      [description]
     * @param  [type] $add_field     [description]
     * @param  [type] $field_to_find [description]
     * @param  [type] $foreign_key   [description]
     * @param  [type] $local_key     [description]
     * @param  [type] $default_value [description]
     * @return [type]                [description]
     */
    public function merge($original, $to_merge, $add_field, $field_to_find, $foreign_key, $local_key, $default_value = null);
    
    /**
     * @param  string $search_key pluck one key
     * @param  array  $ignore     for filter ignore
     * @return array             values
     */
    public function getPluckUniqValues($search_key = 'currency', $ignore = ['usd']);
}