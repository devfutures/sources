<?php

namespace Emitters\Contracts\Reviews;


use App\User;

interface ReviewsContract {
    public function createReview(User $user, array $data);

    public function getReviews($filters = []);

    public function updateReview(int $reviewID, array $data);

    public function deleteReview(int $id);
}