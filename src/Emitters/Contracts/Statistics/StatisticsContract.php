<?php


namespace Emitters\Contracts\Statistics;


interface StatisticsContract {
    public function getAccounting(int $year = null);

    public function addRow(array $data);
}