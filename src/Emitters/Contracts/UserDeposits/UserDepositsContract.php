<?php


namespace Emitters\Contracts\UserDeposits;
use App\User;

interface UserDepositsContract {
    /**
     * @param User $user
     * @param      $plan
     * @param      $amount
     * @param      $payment_system_id
     *
     * @return mixed
     */
    public function createDeposit (User $user, $plan, $amount, $payment_system_id);

    /**
     * @param User $user
     *
     * @return mixed
     */
    public function getDepositUser(User $user);

    /**
     * @param User $user
     *
     * @return mixed
     */
    public function getDepositUserEnd(User $user);

    /**
     * @param array $filter
     *
     * @return mixed
     */
    public function getDeposits (array $filter = []);

    /**
     * @param $paginateNum
     *
     * @return mixed
     */
    public function setPaginateNum ($paginateNum);
}