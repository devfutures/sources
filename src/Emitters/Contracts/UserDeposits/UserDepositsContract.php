<?php


namespace Emitters\Contracts\UserDeposits;
use App\User;
use Emitters\DepositPlans\Models\Deposit_Plans;

interface UserDepositsContract {
    /**
     * @param User $user
     * @param Deposit_Plans $plan
     * @param      $amount
     * @param      $payment_system_id
     *
     * @param $sell
     * @return mixed
     */
    public function createDeposit(User $user, Deposit_Plans $plan, $amount, $payment_system_id, $sell = null);

    /**
     * @param User $user
     *
     * @return mixed
     */
    public function getDepositUser(User $user);

    /**
     * @param $id
     * @param $data
     *
     * @return mixed
     */
    public function update($id, $data);

    /**
     * @param $depositId
     *
     * @return mixed
     */
    public function getDepositOne($depositId);

    /**
     * @param User $user
     *
     * @return mixed
     */
    public function getDepositUserEnd(User $user);

    /**
     * @param array $filter
     *
     * @return mixed
     */
    public function getDeposits (array $filter = []);

    /**
     * @param $paginateNum
     *
     * @return mixed
     */
    public function setPaginateNum ($paginateNum);

    /**
     * @param User               $user
     * @param Deposit_Plans|null $plan
     *
     * @return mixed
     */
    public function countDepositsUsers(User $user, Deposit_Plans $plan = null);

    /**
     * @param null $depositId
     * @param      $amount
     * @param bool $fromBalance
     *
     * @return mixed
     */
    public function updateDepositAmount($depositId = null, $amount, $fromBalance = false);

    /**
     * @return mixed
     */
    public function getDepositsOfAccruals();

    /**
     * @param $user
     * @param array $request
     * @param bool $no_transition
     * @return mixed
     */
    public function addFundsToDeposit($user, array $request, $no_transition = false);

    /**
     * @param $event
     * @return mixed
     */
    public function eventAddFunds($event);

    /**
     * @param $users_id
     * @param int $status
     * @return mixed
     */
    public function getDepositUsersByStatus($users_id, $status = 0);
}