<?php
namespace Emitters\Contracts\UsersData;

interface UsersDataContract{
    /**
     * @param $user
     *
     * @return return results of users wallets
     */
    public function getWallets($user);
}