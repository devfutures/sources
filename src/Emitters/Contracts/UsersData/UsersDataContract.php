<?php

namespace Emitters\Contracts\UsersData;


use Illuminate\Http\Request;

interface UsersDataContract {
    /**
     * @param $user
     *
     * @return return results of users wallets
     */
    public function getWallets($user);

    /**
     * @param $user
     * @param $payment_system_id
     * @param $wallet
     * @return mixed
     */
    public function addWalletUser($user, $payment_system_id, $wallet);

    /**
     * @param $user
     * @param $payment_system_id
     * @param $wallet
     * @param $id
     * @param string $field
     * @return mixed
     */
    public function changeWalletUser($user, $payment_system_id, $wallet, $id, $field = 'id');

    /**
     * @param $user
     * @param $id
     * @param string $field
     * @return mixed
     */
    public function destroyWalletUser($user, $id, $field = 'id');

    /**
     * @param $id
     * @param string $field
     * @return mixed
     */
    public function restoreWalletUser($id, $field = 'id');

    /**
     * @param array $filters
     *
     * @return mixed
     */
    public function getUsers(array $filters = []);

    /**
     * @param $id
     *
     * @param array $with
     * @return mixed
     */
    public function getUserInfo($id, array $with = ['upline', 'contacts']);

    /**
     * @param $user
     *
     * @return mixed
     */
    public function getUserActivity($user);

    /**
     * @param $users
     *
     * @return mixed
     */
    public function addSpecifyAttributes($users);

    /**
     * @param int $days
     *
     * @return mixed
     */
    public function countNewUsers($days = 2);

    /**
     * @return mixed
     */
    public function countTotalUsers();

    /**
     * @param $user_id
     * @param $message
     * @param $blocked_up
     *
     * @return mixed
     */
    public function updateBlocked($user_id, $message, $blocked_up);

    /**
     * @param $user_id
     * @param $data
     *
     * @return mixed
     */
    public function updateInfo($user_id, $data);

    /**
     * @param $user_id
     * @param $wallets
     *
     * @return mixed
     */
    public function saveWallets($user_id, $wallets);

    /**
     * @param array $request
     * @param $user
     * @return mixed
     */
    public function user_registered(array $request, $user);

    /**
     * @param Request $request
     * @param       $user
     *
     * @return mixed
     */
    public function logActivity(Request $request, $user);

    /**
     * @param $userID
     * @param $roleID
     *
     * @return mixed
     */
    public function setRoleToUser($userID, $roleID);
}