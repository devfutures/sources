<?php
namespace Emitters\Contracts\UsersOperations;

interface BaseActionOperationsContract {

    /**
     * @param $operation
     * @param null $request
     * @return mixed
     */
    public function perform($operation, $request = null);
}