<?php


namespace Emitters\Contracts\UsersOperations;


interface ControlOperationsContract {
    /**
     * @param array $request
     * @param $option
     * @return mixed
     */
    public function handlerActionOperations(array $request, $option);

    /**
     * @param array $request
     * @return mixed
     */
    public function createOperation(array $request);

    /**
     * @param array $request
     * @param string $status
     * @return mixed
     */
    public function setStatusOperations(array $request, $status = 'completed');
}