<?php
namespace Emitters\Contracts\UsersOperations;


interface OperationRepositoryContract {
    /**
     * @param $data
     * @return mixed
     */
    public function create($data);

    /**
     * @param $id
     * @param string $field
     * @return mixed
     */
    public function delete($id, $field = 'id');

    /**
     * @param $val
     * @param string $colom
     * @return mixed
     */
    public function first($val, $colom = 'id');

    /**
     * @param array $filters
     * @param bool $no_paginate
     * @return mixed
     */
    public function get(array $filters = [], $no_paginate = false);

    /**
     * @param string $operation
     * @return mixed
     */
    public function getByOperation(string $operation);

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * @param $user
     * @param array $operation
     * @param array $where
     * @param array $status
     * @return mixed
     */
    public function getCount($user, array $operation = [], array $where = [], array $status = []);

    /**
     * @param $arg1
     * @param $val1
     * @param $arg2
     * @param $val2
     * @return mixed
     */
    public function getByTwo($arg1, $val1, $arg2, $val2);

    /**
     * @param $arg1
     * @param $val1
     * @param $arg2
     * @param $val2
     * @param $arg3
     * @param $val3
     * @param string $order
     * @param string $order_type
     * @return mixed
     */
    public function getByThird($arg1, $val1, $arg2, $val2, $arg3, $val3, $order = 'id', $order_type = 'asc');

    /**
     * @param $arg1
     * @param $val1
     * @param $arg2
     * @param $val2
     * @param $arg3
     * @param $val3
     * @param $arg4
     * @param $val4
     * @param string $order
     * @param string $order_type
     * @return mixed
     */
    public function getByFour($arg1, $val1, $arg2, $val2, $arg3, $val3, $arg4, $val4, $order = 'id', $order_type = 'asc');

    /**
     * @param $user_id
     * @param array $from_user_ids
     * @param array $operation
     * @param string $type
     * @return mixed
     */
    public function getSumOperations($user_id, array $from_user_ids = [], array $operation = [], string $type = 'buy');

    /**
     * @param $ids
     * @return mixed
     */
    public function getOperationsByIds($ids);

    /**
     * @param array $user_ids
     * @return mixed
     */
    public function getTotalSumDepositsByUser(array $user_ids);

    /**
     * @param array $users_id
     * @param $operation
     * @return mixed
     */
    public function getSumByUsersAndOperations(array $users_id, $operation);

    /**
     * @param array $deposit_ids
     * @return mixed
     */
    public function getSumByDeposits(array $deposit_ids);

    /**
     * @param $deposits_ids
     * @return mixed
     */
    public function getTotalAccrualsByDeposits($deposits_ids);

    /**
     * @param $address
     * @param null $destination_tag
     * @return mixed
     */
    public function getOperationByPaymentAddress($address, $destination_tag = null);

    /**
     * @param $user_id
     * @param $users_ids
     * @return mixed
     */
    public function getCommissionByUsersId($user_id, $users_ids);

    /**
     * @param array $operations
     * @param int $items
     * @return mixed
     */
    public function getOperationLast(array $operations, $items = 10);

//    /**
//     * @param $users_id
//     * @param int $status
//     * @return mixed
//     */
//    public function getDepositsWithStatus($users_id, $status = 0);
}