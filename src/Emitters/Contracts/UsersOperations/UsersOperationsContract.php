<?php


namespace Emitters\Contracts\UsersOperations;


interface UsersOperationsContract {
    /**
     * @param $user
     * @param $payment_system_id
     * @param $amount
     * @param int $plan_id
     * @param int $hidden
     * @return mixed
     */
    public function createOperationAddFunds($user, $payment_system_id, $amount, $plan_id = 0, $hidden = 0);

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function updateOperation($id, array $data);

    /**
     * @param $user
     * @param $payment_system_id
     * @param $from_payment_system_id
     * @param $amount
     * @param $from_amount
     * @return mixed
     */
    public function createOperationExchange($user, $payment_system_id, $from_payment_system_id, $amount, $from_amount);

    /**
     * @param $user
     * @param $sell_id
     * @param $payment_system_id
     * @param $amount
     * @param $commission
     * @return mixed
     */
    public function createOperationWithdraw($user, $sell_id, $payment_system_id, $amount, $commission);

    /**
     * @param $user
     * @param int $parent_id
     * @param int $amount
     * @param null $payment_system_id
     * @return mixed
     */
    public function createOperationBonus($user, $parent_id = 0, $amount = 0, $payment_system_id = null);

    /**
     * @param $user
     * @param $buy_id
     * @param $deposit_id
     * @param $payment_system_id
     * @param $amount
     *
     * @param $from_amount
     * @param $data_info
     *
     * @return mixed
     */
    public function createOperationAccruals($user, $buy_id, $deposit_id, $payment_system_id, $amount, $from_amount, $data_info);

    /**
     * @param       $user
     * @param array $operations
     * @param array $status
     *
     * @return mixed
     */
    public function getUserOperations($user, array $operations = [], array $status = []);

    /**
     * @param $operation
     *
     * @return mixed
     */
    public function getByOperation($operation);

    /**
     * @param $user
     * @param $deposit_id
     *
     * @return mixed
     */
    public function getCountAccruals($user, $deposit_id);


    /**
     * @param $user
     * @param $buy_id
     * @param $deposit_id
     * @param $payment_system_id
     * @param $amount
     * @param $from_amount
     * @param $data_info
     *
     * @return mixed
     */
    public function createOperationDepositClose($user, $buy_id, $deposit_id, $payment_system_id, $amount, $from_amount, $data_info);

    /**
     * @param $user
     * @param $sell_id
     * @param $plan_id
     * @param $deposit_id
     * @param $payment_system_id
     * @param $amount
     * @return mixed
     */
    public function createOperationCreateDeposit($user, $sell_id, $plan_id, $deposit_id, $payment_system_id, $amount);

    /**
     * @param $deposit_id
     *
     * @return mixed
     */
    public function getOperationDepositClose($deposit_id);

    /**
     * @param $user
     * @param $deposit_id
     *
     * @return mixed
     */
    public function getLastAccruals($user, $deposit_id);

    /**
     * @param       $user
     * @param array $user_ids
     *
     * @return mixed
     */
    public function getAffiliateBonusesFromUsers($user, array $user_ids = []);

    /**
     * @param $user
     * @param $max_level
     * @return mixed
     */
    public function getCommissionAndActiveUsers($user, $max_level);

    /**
     * @param array $users_id
     * @return mixed
     */
    public function getSumUsersDeposits(array $users_id);

    /**
     * @param array $users_id
     * @return mixed
     */
    public function getSumUsersActiveDeposits(array $users_id);

    /**
     * @param $address
     * @param null $destination_tag
     * @return mixed
     */
    public function getOperationByPaymentAddress($address, $destination_tag = null);

    /**
     * @param array $operations
     * @param int $max_item
     * @return mixed
     */
    public function getOperationLast(array $operations, $max_item = 10);
}