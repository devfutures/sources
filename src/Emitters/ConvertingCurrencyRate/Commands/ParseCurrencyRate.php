<?php

namespace Emitters\ConvertingCurrencyRate\Commands;

use Illuminate\Console\Command;

class ParseCurrencyRate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'convertingcurrency:parse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get new currency rate';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $manager = app()->make('libair.converting_currency_rate');
        $manager->parseCurrencyRate();
        $this->info('Done, currency rate new insert');
    }
}
