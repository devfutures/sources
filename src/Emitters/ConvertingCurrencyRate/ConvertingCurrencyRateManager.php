<?php

namespace Emitters\ConvertingCurrencyRate;

use Cache;

class ConvertingCurrencyRateManager
{
    protected $cache_name = 'currency_rate';
    public $courses = null;

    public function getCurrencyRate()
    {
        if($this->courses) return $this->courses;

        $this->courses = Cache::rememberForever($this->cache_name, function () {
            if(isset($this->model->usd_btc)){
                $courses = $this->model;
            }else{
                $courses = $this->model::orderBy('id', 'desc')->first();
            }
            return $courses;
        });
        return $this->courses;
    }

    /**
     * @return Currency_Rate|\Illuminate\Database\Eloquent\Model
     */
    public function insertNewCurrencyRate()
    {
        $expected_coins = $this->getExpectedCoins();
        $currencyRate   = $this->parser->getAllCurrencyRate($expected_coins);
        $fill           = $this->getDataForFill($currencyRate, 'usd');
        $created        = $this->model::create($fill);
        Cache::forget($this->cache_name);

        return $created;
    }

    public function getExpectedCoins(){
        $expected_coins = $this->payment_systems->getPluckUniqValues();
        /* for php unit test */
        if(!in_array('bch', $expected_coins))
            array_push($expected_coins, 'bch');

        if(!in_array('etc', $expected_coins))
            array_push($expected_coins, 'etc');
        /* end */

        return $expected_coins;
    }

    /**
     * @return array
     */
    public function getDataForFill($data, $in_currency = 'usd'){
        $toFill = [];
        if($data){
            foreach($data as $key => $value){
                $symbol = strtolower($value->symbol);
                $key = $symbol.'_'.$in_currency;
                $toFill[$key] = $value->price_usd;
            }
        }
        $finalyFill = [];
        if($toFill){
            foreach($this->model->getFillable() as $key => $val){
                if(array_key_exists($val, $toFill)){
                    $finalyFill[$val] = $toFill[$val];
                }
            }
        }
        return $finalyFill;
    }


    public function convertAmount($amount = 0, $from_currency = null, $to_currency = 'USD'){
        $this->getCurrencyRate();
        
        if($amount == 0 || $from_currency == null || $from_currency == $to_currency || $this->courses == null){
            return $amount;
        }

        $to_currency = mb_strtolower($to_currency);
        $from_currency = mb_strtolower($from_currency);

        $amount_result = $amount;
        if($to_currency == 'usd'){
            $the_var = "{$from_currency}_{$to_currency}";
            if($this->courses->{$the_var}){
                $amount_result = bcmul($this->courses->{$the_var}, $amount, 8);
            }       
        }
        if($from_currency == 'usd'){
            $the_var = "{$to_currency}_{$from_currency}";

            if($this->courses->{$the_var}){
                $amount_result = bcdiv($amount,$this->courses->{$the_var}, 8);
            }
        }
        return $amount_result;
    }
}