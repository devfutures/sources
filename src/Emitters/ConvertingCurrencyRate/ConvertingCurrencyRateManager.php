<?php

namespace Emitters\ConvertingCurrencyRate;

use Emitters\AdminControl\EnvManager;
use Emitters\ConvertingCurrencyRate\Models\Currency_Rate;
use Emitters\Abstracts\ConvertingCurrencyRate\ConvertingCurrencyRateManagerAbstract;
use Illuminate\Support\Facades\Cache;

class ConvertingCurrencyRateManager extends ConvertingCurrencyRateManagerAbstract {

    /**
     * @return Currency_Rate|\Illuminate\Database\Eloquent\Model
     */
    public function parseCurrencyRate(){
        $expected_coins = $this->getExpectedCoins();
        $expected_fiats = $this->getExpectedCoins(1);

        $fiatCurrencyRate = $this->fiatParser->getAllCurrencyRate($expected_fiats);
        $cryptoCurrencyRate = $this->parser->getAllCurrencyRate($expected_coins);

        $fill = $this->getDataForFill($cryptoCurrencyRate,'usd');
        $currencyToInsert = array_merge($fill,$fiatCurrencyRate);
        return $this->insertNewCurrencyRate($currencyToInsert);
    }

    public function getExpectedCoins($fiat = 0){
        $expected_coins = $this->payment_systems->getPluckUniqValues('currency',['usd'],$fiat);
        /* for php unit test */
        /*if (!in_array('bch', $expected_coins))
            array_push($expected_coins, 'bch');

        if (!in_array('etc', $expected_coins))
            array_push($expected_coins, 'etc');*/
        /* end */

        return $expected_coins;
    }

    /**
     * @param        $data
     * @param string $in_currency
     *
     * @return array
     */
    public function getDataForFill($data,$in_currency = 'usd'){
        $toFill = [];
        if($data) {
            foreach($data as $key => $value) {
                $symbol = strtolower($value->symbol);
                $key = $symbol.'_'.$in_currency;
                $toFill[$key] = $value->price_usd;
            }
        }
        $finalyFill = [];
        if($toFill) {
            foreach($this->model->getFillable() as $key => $val) {
                if(array_key_exists($val,$toFill)) {
                    $finalyFill[$val] = $toFill[$val];
                }
            }
        }
        return $finalyFill;
    }

    /**
     * @param array $data
     *
     * @return Currency_Rate|\Illuminate\Database\Eloquent\Model
     */
    public function insertNewCurrencyRate(array $data){
        $created = $this->store($data);
//        Cache::pull($this->cache_name);

        return $created;
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    protected function store(array $data){
        return $this->model::create($data);
    }

    public function convertAmount($amount = 0,$from_currency = null,$to_currency = 'USD',$points = 0){
        $this->getCurrencyRate();

        if($amount == 0 || $from_currency == null || $from_currency == $to_currency || $this->courses == null) {
            return $amount;
        }

        $to_currency = mb_strtolower($to_currency);
        $from_currency = mb_strtolower($from_currency);

        $amount_result = $amount;
        if($to_currency == 'usd') {
            $the_var = "{$from_currency}_{$to_currency}";
            if($this->courses->{$the_var}) {
                $amount_result = df_mul($this->courses->{$the_var},$amount,$points);
            }
        }
        if($from_currency == 'usd') {
            $the_var = "{$to_currency}_{$from_currency}";
            if($this->courses->{$the_var}) {
                $amount_result = df_div($amount,$this->courses->{$the_var},$points);
            }
        }
        return $amount_result;
    }

    public function getCurrencyRate(){
        if($this->courses) return $this->courses;

        $this->courses = Cache::remember($this->cache_name, 300,function (){
            if(isset($this->model->usd_btc)) {
                $courses = $this->model;
            } else {
                $courses = $this->model::orderBy('id','desc')->first();
            }
            return $courses;
        });
        if ($this->courses == null) {
            $structure = array_flip($this->model->getFillable());
            foreach ($structure as $key => $value) {
                $structure[$key] = 0;
            }
            return $structure;
        }
        return $this->courses;
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function updateCurrencyRate($id, array $data) {
        Cache::forget($this->cache_name);
        return $this->model::where('id', $id)->update($data);
    }

    /**
     * Set's emitters auto update and period (in minutes).
     *
     * @param $status
     * @param $period
     */
    public function setAutoUpdate($status, $period = null) {
        $envManager = new EnvManager();
        $toEnvArr = ['AUTO_UPDATE' => $status];
        if(isset($period)) $toEnvArr['MINUTES_UPDATE'] = $period;
        $envManager->puts_to_env($toEnvArr);
    }
}