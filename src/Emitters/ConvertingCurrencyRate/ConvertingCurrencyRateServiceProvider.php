<?php

namespace Emitters\ConvertingCurrencyRate;

use Emitters\ConvertingCurrencyRate\Drivers\CoinMarketCap;
use Illuminate\Support\ServiceProvider;
use Emitters\ConvertingCurrencyRate\Models\Currency_Rate;
use Emitters\ConvertingCurrencyRate\Commands\ParseCurrencyRate;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Database\Eloquent\Factory as EloquentFactory;
class ConvertingCurrencyRateServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/migrations');

        $this->publishes([
            __DIR__.'/migrations/' => database_path('migrations')
        ], 'migrations');
        
        $this->app->booted(function ($app) {
            
            $config = $app['config']['emitters'];
            if($config['auto_update']){
                $schedule = $this->app->make(Schedule::class);
                $schedule->command('convertingcurrency:parse')->cron('*/'.$config['minutes_update'].' * * * *')->withoutOverlapping();
            }
        });

        $this->app->make(EloquentFactory::class)->load(__DIR__ . '/factories/');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands([
            ParseCurrencyRate::class
        ]);
    }
}