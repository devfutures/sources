<?php


namespace Emitters\ConvertingCurrencyRate\Drivers;


use Emitters\Contracts\ConvertingCurrencyRate\DriverContract;
use GuzzleHttp\Client;

class ApiExchangerRateDriver implements DriverContract {

    /**
     * @param $fiat_currencies
     * @return \Illuminate\Support\Collection
     */
    public function getAllCurrencyRate($fiat_currencies) {
        $rates = json_decode((new Client())->get('https://api.exchangeratesapi.io/latest?base=USD')->getBody());
        $currencyRates = collect();
        foreach ($fiat_currencies as $currency) {
            $currencyRates->put($currency . '_usd', $rates->rates->{strtoupper($currency)});
        }
        return $currencyRates->toArray();
    }
}