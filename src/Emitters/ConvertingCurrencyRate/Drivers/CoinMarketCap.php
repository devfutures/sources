<?php

namespace Emitters\ConvertingCurrencyRate\Drivers;

use Emitters\Contracts\ConvertingCurrencyRate\DriverContract;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class CoinMarketCap implements DriverContract
{
    /**
     * @param $expected_coins_id
     * @return \Illuminate\Support\Collection
     */
    public function getAllCurrencyRate($expected_coins_id)
    {
        $client = $this->getClient();
        $currencyRate = collect();
        //Received coins
        $received_coins_id = [];

        //Get request 50 top coins
        try {
            $coin_list = $client->request('GET', 'ticker', [
                'query' => ['limit' => '50']
            ])->getBody()->getContents();

            $dcoin_list = json_decode($coin_list);
            //push coins to collection
            if (!empty($dcoin_list) && !empty($expected_coins_id)) {
                foreach ($dcoin_list as $dcoin) {
                    array_push($received_coins_id, strtolower($dcoin->symbol));
                    if (in_array(strtolower($dcoin->symbol), $expected_coins_id)) {
                        $currencyRate->push($dcoin);
                    }
                }
            }

            //if some coins have not received by previous request
            if (!empty($expected_coins_id)) {
                foreach ($expected_coins_id as $coin_id) {
                    //checking that coin_id present in received coins id
                    if (!in_array($coin_id, $received_coins_id)) {
                        $list_id = [$coin_id];
                        $missing_coin = $this->getListCurrencyRate($list_id);
                        $currencyRate->push($missing_coin[0]);
                    }
                }
            }

            return $currencyRate;

        } catch (GuzzleException | \Exception $e) {
            report($e);
            return $currencyRate;
        }
    }

    /**
     * @param $list_id
     * @return \Illuminate\Support\Collection
     */
    public function getListCurrencyRate($list_id)
    {
        $client = $this->getClient();
        $currencyRateCollection = collect();
        if (!empty($list_id)) {
            foreach ($list_id as $id) {
                $item = $client->get("ticker/{$id}")->getBody()->getContents();
                $ditem = json_decode($item);
                $currencyRateCollection->push($ditem);
            }
        }

        return $currencyRateCollection;
    }

    /**
     * @return Client
     */
    private function getClient()
    {
        return $client = new Client([
            'base_uri' => 'https://api.coinmarketcap.com/v1/'
        ]);
    }
}