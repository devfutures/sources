<?php

namespace Emitters\ConvertingCurrencyRate\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Currency_Rate extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'btc_usd', 'eth_usd', 'dash_usd', 'ltc_usd', 'bch_usd', 'etc_usd', 'xlm_usd', 'xrp_usd', 'usdt_usd', 'zec_usd', 'eur_usd', 'rub_usd'
    ];

    protected $hidden = [
        'created_at', 'deleted_at'
    ];
}
