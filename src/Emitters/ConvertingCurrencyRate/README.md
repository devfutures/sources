---
### Converting currency rate
#### Извлечение ConvertingCurrencyRateManager
##### Получение через IoC

```php
$convertingCurrencyRateManager = resolve('libair.converting_currency_rate');
```

##### Использование в контроллере с помощью type-hinting

```php
use App\LibrariesAir\ConvertingCurrencyRate\ConvertingCurrencyRateManager;
...
public function index(ConvertingCurrencyRateManager $convertingCurrencyRateManager) {
    ...
}
```

#### Методы ConvertingCurrencyRateManager

##### Получение курсов валют с базы
##### Команда для Artisan - 'convertingcurrency:parse'

```php
$convertingCurrencyRateManager->parseCurrencyRate();
```

* return - object Currency_Rate

##### Парс курсов валют со вставкой в базу данных (CoinMarketCup&ExchangeRatesApi)

```php
$convertingCurrencyRateManager->parseCurrencyRate();
```

* return - object Currency_Rate

##### Вставка в базу данных

```php
$convertingCurrencyRateManager->insertNewCurrencyRate($data);
```

##### $data - массив с курсами

```php
$data = [
    'btc_usd' => 3500,
    'eth_usd' => 500,
    ...
];
```
* return - object Currency_Rate

##### Запрос символов валют для которых есть платежные системы
```php
$convertingCurrencyRateManager->getExpectedCoins($fiat);
```

* $fiat - 1/0 получить фиатные валюты при 1 или крипту при 0.
* return - array символы содержащий валюты.

##### Конвертация валют
```php
public function convertAmount($amount = 0, $from_currency = null, $to_currency = 'USD');
```

* $amount - количество валюты для конвертации
* $from_currency - с какой валюты конвертировать 
* $to_currency - в какую валюту конвертировать

```php
$convertingCurrencyRateManager->convertAmount(10, 'eth', 'btc');
```

* return string с результатом (вычисления производятся с помощью bcmath).