<?php


namespace Emitters\ConvertingCurrencyRate\Requests;


use Emitters\ConvertingCurrencyRate\Models\Currency_Rate;
use Illuminate\Foundation\Http\FormRequest;

class CurrencyRateUpdateRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $fillable = (new Currency_Rate())->getFillable();
        return [
            'id'                 => 'exists:currency__rates,id',
            'auto_update'        => 'required|array',
            'auto_update.status' => 'required|string|in:true,false',
            'auto_update.period' => 'integer|between:1,1440',
            'rates'              => 'required|array',
            'rates.*'            => [
                'required',
                function ($attribute, $value, $fail) use ($fillable) {
                    if (array_key_exists(str_replace('rates.', '', $attribute), array_flip($fillable))) {
                        if (!is_numeric($value)) {
                            $fail($value . ' is invalid, must be numeric');
                        }
                    } else {
                        $fail($attribute . ' is invalid');
                    }
                }
            ],
        ];
    }
}