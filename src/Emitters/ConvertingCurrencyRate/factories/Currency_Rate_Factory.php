<?php

use App\User;
use Faker\Generator as Faker;

$factory->define(\Emitters\ConvertingCurrencyRate\Models\Currency_Rate::class, function (Faker $faker) {
    return [
        'btc_usd'  => $faker->randomFloat(8, 3500, 10000),
        'eth_usd'  => $faker->randomFloat(8, 80, 1000),
        'dash_usd' => $faker->randomFloat(8, 80, 1000),
        'ltc_usd'  => $faker->randomFloat(8, 20, 222),
        'bch_usd'  => $faker->randomFloat(8, 300, 3500),
        'etc_usd'  => $faker->randomFloat(8, 10, 200),
        'xlm_usd'  => $faker->randomFloat(8, 10, 200),
        'xrp_usd'  => $faker->randomFloat(8, 10, 200),
        'usdt_usd' => $faker->randomFloat(8, 10, 200),
        'zec_usd'  => $faker->randomFloat(8, 10, 200),
        'rub_usd'  => $faker->randomFloat(8, 70, 100),
        'eur_usd'  => $faker->randomFloat(8, 1.1, 1.3),
    ];
});