<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrencyRateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currency__rates', function (Blueprint $table) {
            $table->increments('id');
            $table->double('btc_usd', 14, 8)->default(0);
            $table->double('eth_usd', 14, 8)->default(0);
            $table->double('dash_usd', 14, 8)->default(0);
            $table->double('ltc_usd', 14, 8)->default(0);
            $table->double('bch_usd', 14, 8)->default(0);
            $table->double('etc_usd', 14, 8)->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currency__rate');
    }
}
