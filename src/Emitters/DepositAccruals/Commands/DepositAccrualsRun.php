<?php

namespace Emitters\DepositAccruals\Commands;

use Emitters\DepositAccruals\DepositAccrualsManager;
use Illuminate\Console\Command;

class DepositAccrualsRun extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deposit_accruals:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deposit accruals for deposits';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param DepositAccrualsManager $accrualsManager
     *
     * @return mixed
     */
    public function handle(DepositAccrualsManager $accrualsManager)
    {
        $accrualsManager->accrualsDataPreparation();

        $this->info('Done');
    }
}
