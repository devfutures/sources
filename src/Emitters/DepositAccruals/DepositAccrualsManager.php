<?php

namespace Emitters\DepositAccruals;

//
use Emitters\Abstracts\DepositAccruals\DepositAccrualsAbstract;
use Emitters\Contracts\DepositAccruals\DepositAccrualsContract;
//

use Illuminate\Foundation\Auth\User;
use Carbon\Carbon;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class DepositAccrualsManager extends DepositAccrualsAbstract implements DepositAccrualsContract {
    /**
     * @return Collection
     */
    public function accrualsDataPreparation(){
        $deposits = $this->libDepositUsers->getDepositsOfAccruals();
        $deposits = $this->verificationListDeposits($deposits);
        $deposits = $this->transformDepositForAccruals($deposits);
        return $this->runAccruals($deposits->reject(function ($item,$key){
            return ($item->percent_accruals <= 0 || $item->amount_for_accruals <= 0 || $item->plan->status == 0);
        }));
    }

    /**
     * @param $deposit
     * @return Collection|mixed
     */
    public function accrualsByDeposit($deposit){
        $deposits = $this->verificationListDeposits($deposit);
        $deposits = $this->transformDepositForAccruals($deposits);
        return $this->runAccruals($deposits->reject(function ($item,$key){
            return ($item->percent_accruals <= 0 || $item->amount_for_accruals <= 0 || $item->plan->status == 0);
        }));
    }

    /**
     * @param $deposits
     *
     * @return Collection
     */
    protected function verificationListDeposits($deposits){
        /** @var Collection $deposits */
        return $deposits->reject(function ($item,$key){
            try{
                // check if date from created at to accrual at not correctly
                $execution_time = $item->created_at->addSeconds($item->plan->accrual_period)->lte(Carbon::parse($item->accrual_at));
                if(!$execution_time) {
                    throw new \Exception('Allowed execution time incorrect');
                }

                // if the previous accruals was recently
                $last_accruals = $this->libUsersOperations->getLastAccruals($item->owner, $item->id);
                if($last_accruals){
                    $diff_seconds_prev_accruals = Carbon::now()->diffInSeconds(Carbon::parse($last_accruals->getOriginal('created_at')));
                    /**
                     * если начисления попало в промежуток секунд
                     */
                    $diff_seconds_prev_accruals += 60;
                    if($diff_seconds_prev_accruals < $item->plan->accrual_period){
                        throw new \Exception('The period between charges is too small');
                    }
                }

                return false;
            }catch(\Exception $e){
                $item->status = 2;
                $item->status_text = $e->getMessage();
                $item->save();
                return true;
            }
        });
    }

    /**
     * @param $deposits
     *
     * @return Collection
     */
    protected function transformDepositForAccruals($deposits){
        /** @var Collection $deposits */
        return $deposits->transform(function ($item){

            $item->percent_accruals = $this->libDepositPlans->getPercentByDate($item->accrual_at,$item->plan->payments_days_percent);

            $item->amount_for_accruals = calc_of_amount_percent($item->amount,$item->percent_accruals);
            $item->amount_if_deposit_close = 0;
            if($item->plan->principal_return_percent > 0 && $item->plan->principal_return_percent <= 100) {
                $item->amount_if_deposit_close = calc_of_amount_percent($item->amount,$item->plan->principal_return_percent);
            }
            $item->next_accruals = $this->libDepositPlans->getNextAccruals($item->plan,Carbon::now());
            $item->count_accruals = $this->libUsersOperations->getCountAccruals($item->owner,$item->id);

            if($item->plan->duration_plan > 0) {
                $item->condition_close_deposit = ($item->count_accruals < $item->plan->duration_plan) ? true : false;
            } else {
                $item->condition_close_deposit = true;
            }

            return $item;
        });
    }

    /**
     * @param $deposits
     *
     * @return Collection
     */
    protected function runAccruals($deposits){
        /** @var Collection $deposits */
        $deposits->each(function ($item){
            DB::beginTransaction();
            try {
                if($item->condition_close_deposit) {
                    $this->accrualDeposit($item->id,$item->owner,$item->payment_system_id,$item->amount_for_accruals,$item->amount,$item->percent_accruals,$item->accrual_at,$item->next_accruals,$item->count_accruals);
                    // need if deposit plan duration is 1, need the close deposit at now
                    $close_deposit = $this->checkDepositClose($item->owner,$item->id,$item->plan->duration_plan);
                    if($close_deposit) {
                        $this->closeDeposit($item->id,$item->owner,$item->payment_system_id,$item->amount_if_deposit_close,$item->amount,$item->count_accruals);
                    }
                } else {
                    $this->closeDeposit($item->id,$item->owner,$item->payment_system_id,$item->amount_if_deposit_close,$item->amount,$item->count_accruals);
                }
                DB::commit();
            } catch(\Exception $e) {
                report($e);
                DB::rollBack();
            }
        });
        return $deposits;
    }

    /**
     * @param int    $deposit_id
     * @param User   $user
     * @param int    $payment_system_id
     * @param string $amount_for_accruals
     * @param string $deposit_amount
     * @param string $percent_accruals
     * @param        $accrual_at
     * @param        $next_accruals
     *
     * @param int    $count_accruals
     *
     * @return bool
     */
    protected function accrualDeposit(int $deposit_id,User $user,int $payment_system_id,string $amount_for_accruals,string $deposit_amount,string $percent_accruals,$accrual_at,$next_accruals,int $count_accruals = 0){
        $add_balance = $this->balanceManager->changeBalance([
            'type'              => 'buy',
            'operation'         => 'ACCRUALS',
            'parent_id'         => $deposit_id,
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system_id,
            'amount'            => $amount_for_accruals,
        ]);

        $this->libUsersOperations->createOperationAccruals($user,$add_balance->id,$deposit_id,$payment_system_id,$add_balance->amount,$deposit_amount,[
            'deposit_id'     => $deposit_id,
            'percent'        => $percent_accruals,
            'accrual_at'     => $accrual_at,
            'next_accruals'  => $next_accruals->format('Y-m-d H:i:s'),
            'count_accruals' => $count_accruals
        ]);

        $this->libDepositUsers->update($deposit_id,[
            'accrual_at' => $next_accruals
        ]);


        //referral by accruals
        $this->partnershipAccrual->affiliateBonus('BY_PROFIT', $user, $amount_for_accruals, $deposit_id, $payment_system_id);
        return true;
    }

    /**
     * @param int  $deposit_id
     * @param User $user
     * @param int  $payment_system_id
     * @param      $amount_if_deposit_close
     * @param      $deposit_amount
     * @param int  $count_accruals
     *
     * @return mixed
     */
    protected function closeDeposit(int $deposit_id,User $user,int $payment_system_id,$amount_if_deposit_close,$deposit_amount,int $count_accruals = 0){
        $operation_dep_close = $this->libUsersOperations->getOperationDepositClose($deposit_id);
        if($operation_dep_close == null && $amount_if_deposit_close > 0) {
            $add_balance = $this->balanceManager->changeBalance([
                'type'              => 'buy',
                'operation'         => 'DEPOSIT_CLOSE',
                'parent_id'         => $deposit_id,
                'user_id'           => $user->id,
                'payment_system_id' => $payment_system_id,
                'amount'            => $amount_if_deposit_close,
            ]);

            $this->libUsersOperations->createOperationDepositClose($user,$add_balance->id,$deposit_id,$payment_system_id,$amount_if_deposit_close,$deposit_amount,[
                'deposit_id'     => $deposit_id,
                'count_accruals' => $count_accruals
            ]);
        }
        return $this->libDepositUsers->update($deposit_id,[
            'status'         => 1,
            'accrual_at'     => null,
            'deposit_end_at' => Carbon::now()
        ]);
    }

    /**
     * @param User $user
     * @param int  $deposit_id
     * @param int  $duration_plan
     *
     * @return bool
     */
    public function checkDepositClose(User $user,int $deposit_id,int $duration_plan = 0){
        $count_accruals = $this->libUsersOperations->getCountAccruals($user,$deposit_id);
        if($duration_plan == 0) return false;
        return ($count_accruals >= $duration_plan) ? true : false;
    }
}