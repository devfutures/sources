<?php

namespace Emitters\DepositAccruals;

use Emitters\DepositAccruals\Commands\DepositAccrualsRun;
use Illuminate\Support\ServiceProvider;
use Illuminate\Console\Scheduling\Schedule;

class DepositAccrualsServiceProvider extends ServiceProvider{

    /**
     *
     */
    public function boot(){
        $this->app->booted(function ($app) {
            $schedule = $this->app->make(Schedule::class);
            $schedule->command('deposit_accruals:run')->everyMinute()->withoutOverlapping();
        });
    }

    /**
     *
     */
    public function register(){
        $this->app->singleton(DepositAccrualsManager::class, function () {
            return new DepositAccrualsManager(resolve('libair.depositusers'), resolve('libair.depositplans'), resolve('librariesair.balance'), users_operations(), resolve('partnership.accrual'));
        });

        $this->app->alias(DepositAccrualsManager::class, 'deposit.accruals');

        $this->commands([
            DepositAccrualsRun::class
        ]);
    }
}