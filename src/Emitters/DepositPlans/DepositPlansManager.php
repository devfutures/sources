<?php

namespace Emitters\DepositPlans;

use Emitters\Abstracts\DepositPlans\DepositPlansAbstract;
use Emitters\Contracts\DepositPlans\DepositPlansContract;

use Carbon\Carbon;
class DepositPlansManager extends DepositPlansAbstract implements DepositPlansContract{

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function store(array $data){
        if(!array_key_exists('payments_days_percent', $data)){
            $data['payments_days_percent'] = [];
        }
        $data['payments_days_percent'] = $this->comparePaymentDaysPercent($data['payments_days_percent']);
        return $this->model::create($data);
    }

    /**
     * @return mixed
     */
    public function getPlans(){
        return $this->model::orderBy('sort', 'asc')->get()->each(function($row){
            $row = $this->modifyPlan($row);
        });
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getPlanById($id){
        return $this->modifyPlan($this->model::where('id', $id)->firstOrFail());
    }

    /**
     * @param $amount
     *
     * @return mixed
     */
    public function getPlanByAmount($amount){
        return $this->model::where(function($query) use ($amount){
            $query->where('minimum_amount', '<=', $amount);
            $query->where('maximum_amount', '>=', $amount);
        })->orderBy('minimum_amount', 'asc')->first();
    }

    /**
     * @param $plan
     *
     * @return mixed
     */
    public function modifyPlan($plan){
        $plan = $this->getMinMaxAmount($plan);
        $plan = $this->getBreakEvenDate($plan);
        $plan = $this->getDateCloseDeposit($plan);
        return $plan;
    }

    /**
     * @param $plan
     *
     * @return mixed
     */
    public function getBreakEvenDate($plan){
        $total_return = 0;
        $break_event = null;
        $tmp = 999;

        $now = Carbon::now();
        while($tmp > 0){
            $now->addSeconds($plan->accrual_period);
            $now_week = strtolower($now->format('l'));
            if($plan->payments_days_percent[$now_week]['status'] == true){
                $total_return += $plan->payments_days_percent[$now_week]['percent'];
                if($total_return >= 100) {
                    $break_event = $now;
                    break;
                }
            }
            $tmp--;
        }
        $plan->break_event = $break_event;
        return $plan;
    }

    /**
     * @param      $plan
     * @param null $date
     *
     * @return mixed|void
     */
    public function getNextAccruals($plan, $date = null) {
        if($date == null) $date = Carbon::now();

        $i = 0;
        while($i<999){
            $date->addSecond($plan->accrual_period);
            if($this->extractionPercent($plan, $date)){
                break;
            }
            $i++;
        }

        return $date;
    }

    /**
     * @param $plan
     *
     * @return mixed
     */
    public function getDateCloseDeposit($plan){
        $deposit_close = null;
        $tmp = $plan->duration_plan;

        $now = Carbon::now();
        while($tmp > 0){
            $now->addSeconds($plan->accrual_period);
            $now_week = strtolower($now->format('l'));
            if($plan->payments_days_percent[$now_week]['status'] == true){                
                $deposit_close = $now;
            }
            $tmp--;
        }
        $plan->deposit_close = $deposit_close;
        return $plan;
    }

    /**
     * @param      $plan_id
     * @param null $date
     *
     * @return bool|mixed
     */
    public function getPercent($plan_id, $date = null){
        $plan = $this->getPlanById($plan_id);        
        return $this->extractionPercent($plan, $date);
    }

    /**
     * @param      $plan
     * @param null $date
     *
     * @return bool|mixed
     */
    public function extractionPercent($plan, $date = null){
        if($date == null) $date = Carbon::now();

        $date = Carbon::parse($date);
        $now_week = strtolower($date->format('l'));
        
        if(!array_key_exists($now_week, $plan->payments_days_percent))
            return false;
        
        $day = $plan->payments_days_percent[$now_week];
        
        if($day['status'] == true && $day['percent'] > 0){
            return $day['percent'];
        }
        return false;
    }

    /**
     * @param $plan
     *
     * @return mixed
     */
    public function getMinMaxAmount($plan){
        $this->paymentSystems->get()->each(function($row) use ($plan){
            $name_fild_minimum = 'minimum_amount_'.mb_strtolower($row->currency);
            $name_fild_maximum = 'maximum_amount_'.mb_strtolower($row->currency);
            $plan->{$name_fild_minimum} = $this->convertingCurrencyRate->convertAmount($plan->minimum_amount, 'USD', $row->currency);
            $plan->{$name_fild_maximum} = $this->convertingCurrencyRate->convertAmount($plan->maximum_amount, 'USD', $row->currency);
        });
        unset($plan->minimum_amount, $plan->maximum_amount);
        return $plan;
    }

    /**
     * @param string $amount
     * @param string $percent
     *
     * @return mixed|string
     */
    public function getAmountToProfit(string $amount, string $percent){
        return bcdiv(bcmul($amount, $percent, 8), 100, 8);
    }

    /**
     * @return array|mixed
     */
    public function defaultPaymentDaysPercent(){
    	return  [
			"monday"    => ['status' => false, 'percent' => 0],
			"tuesday"   => ['status' => false, 'percent' => 0],
			"wednesday" => ['status' => false, 'percent' => 0],
			"thursday"  => ['status' => false, 'percent' => 0],
			"friday"    => ['status' => false, 'percent' => 0],
			"saturday"  => ['status' => false, 'percent' => 0],
			"sunday"    => ['status' => false, 'percent' => 0]
		];
    }

    /**
     * @param array $preCreatedData
     *
     * @return array|mixed
     */
    public function comparePaymentDaysPercent(array $preCreatedData){
    	$toFill = [];
        foreach($this->defaultPaymentDaysPercent() as $key => $row){
    		if(is_array($preCreatedData) && array_key_exists($key, $preCreatedData)){
    			if(array_key_exists('status', $preCreatedData[$key])){
    				$row['status'] = ($preCreatedData[$key]['status'] == true)? true : false;
    			}
    			if(array_key_exists('percent', $preCreatedData[$key])){
    				$row['percent'] = $preCreatedData[$key]['percent'];
    			}
    		}
    		$toFill[$key] = $row;
    	}
    	return $toFill;
    }
}