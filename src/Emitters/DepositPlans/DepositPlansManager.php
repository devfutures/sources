<?php

namespace Emitters\DepositPlans;

use Emitters\Abstracts\DepositPlans\DepositPlansAbstract;
use Emitters\Contracts\DepositPlans\DepositPlansContract;

use Carbon\Carbon;
use Emitters\DepositPlans\Exceptions\DepositsExistsException;
use Emitters\Filters\Filters;

class DepositPlansManager extends DepositPlansAbstract implements DepositPlansContract {

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function store(array $data){
        if(!array_key_exists('payments_days_percent',$data)) {
            $data['payments_days_percent'] = [];
        }
        $data['payments_days_percent'] = $this->comparePaymentDaysPercent($data['payments_days_percent']);
        $data = $this->modifyDate($data);
        return $this->model::create($data);
    }

    /**
     * @param array $preCreatedData
     *
     * @return array|mixed
     */
    public function comparePaymentDaysPercent(array $preCreatedData){
        $toFill = [];
        foreach($this->defaultPaymentDaysPercent() as $key => $row) {
            if(is_array($preCreatedData) && array_key_exists($key,$preCreatedData)) {
                if(array_key_exists('status',$preCreatedData[$key])) {
                    $row['status'] = ($preCreatedData[$key]['status'] == true) ? true : false;
                }
                if(array_key_exists('percent',$preCreatedData[$key])) {
                    $row['percent'] = $preCreatedData[$key]['percent'];
                }
            }
            $toFill[$key] = $row;
        }
        return $toFill;
    }

    /**
     * @return array|mixed
     */
    public function defaultPaymentDaysPercent(){
        return [
            "monday"    => ['status' => false,'percent' => 0],
            "tuesday"   => ['status' => false,'percent' => 0],
            "wednesday" => ['status' => false,'percent' => 0],
            "thursday"  => ['status' => false,'percent' => 0],
            "friday"    => ['status' => false,'percent' => 0],
            "saturday"  => ['status' => false,'percent' => 0],
            "sunday"    => ['status' => false,'percent' => 0]
        ];
    }

    /**
     * @param $data
     *
     * @return array
     */
    public function modifyDate($data) {
        $data['able_to_deposit_from'] = (array_key_exists('able_to_deposit_from', $data) && $data['able_to_deposit_from'] != null) ? Carbon::parse($data['able_to_deposit_from']) : null;
        $data['able_to_deposit_to']   = (array_key_exists('able_to_deposit_to', $data) && $data['able_to_deposit_to'] != null) ? Carbon::parse($data['able_to_deposit_to']) : null;
        return $data;
    }

    /**
     * @param array $filters
     *
     * @param bool  $by_paginate
     *
     * @return mixed
     */
    public function getPlans($filters = [], $by_paginate = true){
        $plansCollection = $this->model::filters(new Filters($filters,$this->availableFilters));
        if($by_paginate){
            $plansCollection = $plansCollection->paginate(config('emitters.paginate_num'))->appends($filters);
        }else{
            $plansCollection = $plansCollection->get();
        }

        $deposits = $this->depositUsers->countDepositsByPlansIds($plansCollection->pluck('id')->toArray());
        $plansCollection->each(function ($row) use ($deposits){
            $row = $this->modifyPlan($row);
            $total_deposits = 0;
            if($deposits->where('plan_id',$row->id)->count() > 0) {
                $total_deposits = $deposits->where('plan_id',$row->id)->first()->count;
            }
            $row->total_deposits = $total_deposits;
        });
        return $plansCollection;
    }

    /**
     * @param $plan
     *
     * @return mixed
     */
    public function modifyPlan($plan){
        if(!$plan) return $plan;
        $plan = $this->getMinMaxAmount($plan);
        $plan = $this->getBreakEvenDate($plan);
        $plan = $this->getDateCloseDeposit($plan);
        $plan = $this->sortPaymentDaysPercent($plan);
        return $plan;
    }

    /**
     * @param $plan
     *
     * @return mixed
     */
    public function getMinMaxAmount($plan){
        $this->paymentSystems->get()->each(function ($row) use ($plan){
            $name_fild_minimum = 'minimum_amount_'.mb_strtolower($row->currency);
            $name_fild_maximum = 'maximum_amount_'.mb_strtolower($row->currency);
            $min_amount = $this->convertingCurrencyRate->convertAmount($plan->minimum_amount,'USD',$row->currency);
            $max_amount = $this->convertingCurrencyRate->convertAmount($plan->maximum_amount,'USD',$row->currency);
            if($row->min_sum_purchase > $min_amount){
                $min_amount = $row->min_sum_purchase;
            }
            if($row->max_sum_purchase > $max_amount){
                $max_amount = $row->max_sum_purchase;
            }
            $plan->{$name_fild_minimum} = $min_amount;
            $plan->{$name_fild_maximum} = $max_amount;
        });
        unset($plan->minimum_amount,$plan->maximum_amount);
        return $plan;
    }

    /**
     * @param $plan
     *
     * @return mixed
     */
    public function getBreakEvenDate($plan){
        $total_return = 0;
        $break_event = null;
        $tmp = 999;

        $now = Carbon::now();
        while($tmp > 0) {
            $now->addSeconds($plan->accrual_period);
            $now_week = strtolower($now->format('l'));
            if($plan->payments_days_percent[$now_week]['status'] == true) {
                $total_return += $plan->payments_days_percent[$now_week]['percent'];
                if($total_return >= 100) {
                    $break_event = $now;
                    break;
                }
            }
            $tmp--;
        }
        $plan->break_event = $break_event;
        return $plan;
    }

    /**
     * @param $plan
     *
     * @return mixed
     */
    public function getDateCloseDeposit($plan){
        $deposit_close = null;
        $tmp = $plan->duration_plan;

        $now = Carbon::now();
        while($tmp > 0) {
            $now->addSeconds($plan->accrual_period);
            $now_week = strtolower($now->format('l'));
            if($plan->payments_days_percent[$now_week]['status'] == true) {
                $deposit_close = $now;
            }
            $tmp--;
        }
        $plan->deposit_close = $deposit_close;
        return $plan;
    }

    public function sortPaymentDaysPercent($plan){
        $daysPercents = $plan->payments_days_percent;
        $tmpArr = [];
        foreach($this->dayOfWeek as $key => $value) {
            foreach($daysPercents as $dpKey => $dpValue) {
                if($value == $dpKey) {
                    $tmpArr[$dpKey] = $dpValue;
                }
            }
        }
        $plan->payments_days_percent = $tmpArr;
        return $plan;
    }

    /**
     * @return mixed
     */
    public function getPlansThatAbleToDeposit($filters = []){
        $plans = $this->model::query()
            ->where([['able_to_deposit_from','<=',Carbon::now()],['able_to_deposit_to','>=',Carbon::now()]])
            ->orWhere(function ($query){
                $query->whereNull('able_to_deposit_from')->whereNull('able_to_deposit_to');
            })
            ->orWhere(function ($query){
                $query->whereNull('able_to_deposit_from')->where('able_to_deposit_to','>=',Carbon::now());
            })
            ->orWhere(function ($query){
                $query->where('able_to_deposit_from','<=',Carbon::now())->whereNull('able_to_deposit_to');
            })
            ->filters(new Filters($filters,$this->availableFilters))
            ->get();
        //count of deposits by each plan which returned to $plans
        $deposits = $this->depositUsers->countDepositsByPlansIds($plans->pluck('id')->toArray());
        //add depositToLimit property that show how many deposit can be done with this plan.
        $plans = $plans->transform(function ($item,$key) use ($deposits){
            $depositsToLimit = $item->available_count_deposits;
            if($deposits->where('plan_id',$item->id)->count() > 0) {
                $countOfDepositByPlan = $deposits->where('plan_id',$item->id)->first()->count;
                $depositsToLimit = $depositsToLimit == 0 ? $depositsToLimit : $depositsToLimit - $countOfDepositByPlan;
            }
            $item->deposits_to_limit = $depositsToLimit;
            return $item;
        });
        //delete from collection plans that cant be used for deposits.
        $plans = $plans->reject(function ($item,$key){
            return $item->available_count_deposits > 0 && $item->deposits_to_limit <= 0;
        });

        $plans->each(function ($row){
            $row = $this->modifyPlan($row);
        });
        return $plans;
    }

    /**
     * @param $amount
     *
     * @return mixed
     */
    public function getPlanByAmount($amount){
        return $this->model::where(function ($query) use ($amount){
            $query->where('minimum_amount','<=',$amount);
            $query->where('maximum_amount','>=',$amount);
        })->orderBy('minimum_amount','asc')->first();
    }

    /**
     * @param      $plan
     * @param null $date
     *
     * @return mixed|void
     */
    public function getNextAccruals($plan,$date = null){
        if($date == null) $date = Carbon::now();

        $i = 0;
        while($i < 999) {
            $date->addSecond($plan->accrual_period);
            if($this->extractionPercent($plan,$date)) {
                break;
            }
            $i++;
        }

        return $date;
    }

    /**
     * @param      $plan
     * @param null $date
     *
     * @return bool|mixed
     */
    public function extractionPercent($plan,$date = null){
        if($date == null) $date = Carbon::now();

        $date = Carbon::parse($date);
        $now_week = strtolower($date->format('l'));

        if(!array_key_exists($now_week,$plan->payments_days_percent))
            return false;

        $day = $plan->payments_days_percent[$now_week];

        if($day['status'] == true && $day['percent'] > 0) {
            return $day['percent'];
        }
        return false;
    }

    /**
     * @param      $plan_id
     * @param null $date
     *
     * @return bool|mixed
     */
    public function getPercent($plan_id,$date = null){
        $plan = $this->getPlanById($plan_id);
        return $this->extractionPercent($plan,$date);
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getPlanById($id){
        return $this->modifyPlan($this->model::where('id',$id)->first());
    }

    /**
     * @param string $amount
     * @param string $percent
     *
     * @return mixed|string
     */
    public function getAmountToProfit(string $amount,string $percent){
        return df_div(df_mul($amount,$percent),100);
    }

    /**
     * @param array $planIDs
     *
     * @return int
     * @throws DepositsExistsException
     */
    public function delete(array $planIDs){
        foreach($planIDs as $row)
            $this->checkDepositsExists($row);
        return $this->model::destroy($planIDs);
    }

    /**
     * @param $planID
     *
     * @throws DepositsExistsException
     */
    protected function checkDepositsExists($planID){
        if($this->depositUsers->countDepositsByPlan($planID) >= 1) {
            throw new DepositsExistsException('DEPOSITS_WITH_PLAN_EXISTS');
        }
    }

    /**
     * @param       $planID
     * @param array $data
     *
     * @return int
     * @throws DepositsExistsException
     */
    public function update($planID,array $data){
        // $this->checkDepositsExists($planID);
        if(array_key_exists('payments_days_percent',$data)) {
            $data['payments_days_percent'] = $this->comparePaymentDaysPercent($data['payments_days_percent']);
        }
        $data = $this->modifyDate($data);
        $model = $this->model::query()->find($planID);
        return $model->update($data);
    }

    public function getPlansWillBuyAllParticipants(){
        return $this->model::
            selectRaw('id, available_count_deposits, (SELECT count(id) FROM deposit__users WHERE deposit__users.plan_id = deposit__plans.id AND deposit__users.status = 0) as count_deposits')->
            where('will_buy_all_participants',1)->
            groupBy('deposit__plans.id')->
            havingRaw('count_deposits >= available_count_deposits')->
            get();
    }

    /**
     * @return int
     */
    public function toNextPlanSeconds(){
        $plan = $this->getPlanForOpenFuture();
        if(!$plan) return 0;
        return Carbon::now()->diffInSeconds($plan->able_to_deposit_from);
    }

    /**
     * @return mixed
     */
    public function getPlanForOpenFuture(){
        return $this->model::where('able_to_deposit_from','>=',Carbon::now())->orderBy('able_to_deposit_from','asc')->first();
    }

    /**
     * @param $date
     * @param $payments_days_percent
     *
     * @return int|string
     */
    public function getPercentByDate($date,$payments_days_percent){
        if(!$payments_days_percent) return 0;
        $date = Carbon::parse($date);
        $now_week = strtolower($date->format('l'));
        $day = $payments_days_percent[$now_week];
        if($day['status'] && $day['percent'] > 0) {
            return df_div($day['percent'],1);
        } else {
            return 0;
        }
    }
}