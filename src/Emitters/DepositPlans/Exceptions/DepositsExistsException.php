<?php

namespace Emitters\DepositPlans\Exceptions;


class DepositsExistsException extends \Exception {
    const DEPOSITS_WITH_PLAN_EXISTS = 'Some users have deposits with that deposit plan.';
}