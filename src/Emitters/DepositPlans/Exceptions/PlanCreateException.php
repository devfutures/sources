<?php


namespace Emitters\DepositPlans\Exceptions;


class PlanCreateException extends \Exception {
    const PLAN_ALREADY_EXIT = 'A plan with such characteristics has already been created.';
}