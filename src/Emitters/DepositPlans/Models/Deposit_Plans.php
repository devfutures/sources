<?php

namespace Emitters\DepositPlans\Models;

use Emitters\Filters\Traits\AddFilters;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Deposit_Plans extends Model {
    use SoftDeletes, AddFilters;

    const NO_ACTIVE_PLAN = 0;
    const ACTIVE_PLAN = 1;
    const HIDDEN_PLAN = 2;

    protected $fillable = [
        'name',
        'description',
        'duration_plan',
        'accrual_period',
        'delay_accruals',
        'will_buy_all_participants',
        'available_count_deposits',
        'limit_deps_user',
        'status',
        'minimum_amount',
        'maximum_amount',
        'payments_days_percent',
        'principal_return_percent',
        'sort',
        'maximum_percent_profit',
        'allow_from_balance',
        'allow_add_funds',
        'limit_deps_user',
        'able_to_deposit_from',
        'able_to_deposit_to',
    ];


    protected $casts = [
        'payments_days_percent' => 'array'
    ];

    protected $dates = [
        'able_to_deposit_from',
        'able_to_deposit_to'
    ];
}
