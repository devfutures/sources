<?php

namespace Emitters\DepositPlans\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Deposit_Plans extends Model
{
    use SoftDeletes;
    
    const NO_ACTIVE_PLAN = 0;
    const ACTIVE_PLAN = 1;
    const HIDDEN_PLAN = 2;

    protected $casts = [
    	'payments_days_percent' => 'array'
    ];

    protected $fillable = [
        'name', 'description', 'duration_plan', 'accrual_period', 'status', 'minimum_amount', 'maximum_amount', 'payments_days_percent', 'principal_return_percent', 'sort'
    ];
}
