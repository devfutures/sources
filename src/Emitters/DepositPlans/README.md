---
### Deposit Plans
#### Извлечение DepositPlansManager
##### Получение через IoC
```php
$depositPlans = resolve('libair.depositplans');
```

##### Использование в контроллере с помощью type-hinting
```php
use App\LibrariesAir\DepositPlans\DepositPlansManager;
...
public function index(DepositPlansManager $depositPlans) {
    ...
}
```

### Validation create/update
```php
use Emitters\DepositPlans\Requests\DepositPlanRequest;

function save(DepositPlanRequest $request, DepositPlansManager $depositPlans){
	$depositPlans->store($request->all());
}
```

#### Методы DepositPlansManager

##### Создание нового плана
```php
$balanceManager->store(array $data);
```
* $data['name'] - Название плана
* $data['description'] - Описание плана
* $data['duration_plan'] - количество начислений по плану
* $data['accrual_period'] - период начислений депозита в секунда, 24 часа 86400 секунд
* $data['status'] - (int) 0 - не активный, 1 - активный, 2 - скрыт
* $data['minimum_amount'] - минимальная сумма депозита в USD
* $data['maximum_amount'] - максимальная сумма депозита в USD
* $data['payments_days_percent'] - array ключ день недели, в нем статус и процент
* $data['principal_return_percent'] - процент возврата по окончанию депозита
* return type model object

##### Список всех доступных планов по всем статусам
```php
$balanceManger->getPlans();
```

##### Получить план по идентификатору
```php
$balanceManger->getPlanById($id);
```
##### Получить план по сумме
```php
$balanceManger->getPlanByAmount($amount)
```
##### Модифицирование результата при фунциях `getPlans` & `getPlanById`
```php
$balanceManger->modifyPlan();
```
##### Дата выхода в безубыток
```php
$balanceManger->getBreakEvenDate($plan);
```
* Добавляет значение `break_event` - Carbon\Carbon;

##### Дата когда депозит будет закрыт
```php
$balanceManger->getDateCloseDeposit($plan);
```
* Добавляет значение `deposit_close` - Carbon\Carbon;

##### Получить дату следующего начисления
```php
$balanceManger->getNextAccruals($plan, $date = null);
```
##### Получить процент по идентификатору плана
```php
$balanceManger->getPercent($plan_id, $date = null);
```
* return percent or false

##### Извленеие процента из плана, проходим по массиву `payments_days_percent`
```php
$balanceManger->extractionPercent($plan, $date = null);
```
##### Получение конвертируемых сумм для всех платежных систем
```php
$balanceManger->getMinMaxAmount($plan);
```
* добавляем значенеи `minimum_amount_{CURRENCY}` - минимальная сумма в валюте
* добавляем значенеи `maximum_amount_{CURRENCY}` - максимальная сумма в валюте

##### Считаем сумму к профиту через bcmath( $amount*$percent/100)
```php
$balanceManger->getAmountToProfit(string $amount, string $percent);
```
##### Массив для хранения процентов по дням недели
```php
$balanceManger->defaultPaymentDaysPercent();
```
##### Заполняем перед занесением в базу данных начисления по дням недели
```php
$balanceManger->comparePaymentDaysPercent(array $preCreatedData);
```
