<?php

namespace Emitters\DepositPlans\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DepositPlanRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'name'                      => 'required|string',
            'description'               => 'string',
            'duration_plan'             => 'required|integer',
            'accrual_period'            => 'required|integer|min:1',
            'delay_accruals'            => 'sometimes|integer',
            'will_buy_all_participants' => 'sometimes|integer',
            'available_count_deposits'  => 'sometimes|integer',
            'limit_deps_user'           => 'integer',
            'status'                    => 'required|integer|in:0,1,2',
            'minimum_amount'            => 'required|numeric',
            'maximum_amount'            => 'required|numeric',
            'payments_days_percent'     => 'required|array',
            'maximum_percent_profit'    => 'required|numeric',
            'principal_return_percent'  => 'required|numeric|between:0,100',
            'allow_from_balance'        => 'required|integer|in:1,0',
            'allow_add_funds'           => 'required|integer|in:1,0',
            'able_to_deposit_from'      => 'sometimes|nullable',
            'able_to_deposit_to'        => 'sometimes|nullable',
        ];
    }
}
