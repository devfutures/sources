<?php

namespace Emitters\DepositPlans\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DepositPlanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                     => 'required',
            'duration_plan'            => 'required|integer',
            'accrual_period'           => 'required|integer|min:60',
            'status'                   => 'required|integer|in:0,1,2',
            'minimum_amount'           => 'required',
            'maximum_amount'           => 'required',
            'principal_return_percent' => 'required',
        ];
    }
}
