<?php


namespace Emitters\DepositPlans\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FilteredDepositPlanRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'orderBy'             => 'array',
            'orderBy.*.by'        => 'string',
            'orderBy.*.direction' => 'string|in:asc,desc',

            'where'         => 'array',
            'where.*.field' => 'string|in:status,allow_from_balance,allow_add_funds',
            'where.*.value' => 'integer|in:0,1,2',

            'amount'              => 'array',
            'amount.*.fields'     => 'array',
            'amount.*.fields.min' => 'string|in:minimum_amount',
            'amount.*.fields.max' => 'string|in:maximum_amount',
            'amount.*.min'        => 'numeric',
            'amount.*.max'        => 'numeric',

            'dateBetween'                    => 'array',
            'dateBetween.*.from_date_column' => 'string|in:created_at,able_to_deposit_from,able_to_deposit_to',
            'dateBetween.*.to_date_column'   => 'string|in:created_at,able_to_deposit_from,able_to_deposit_to',
            'dateBetween.*.from'             => 'date_format: "d.m.Y"',
            'dateBetween.*.to'               => 'date_format: "d.m.Y"',
        ];
    }
}