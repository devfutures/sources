<?php

use App\User;
use Illuminate\Support\Carbon;
use Faker\Generator as Faker;

$factory->define(\Emitters\DepositPlans\Models\Deposit_Plans::class,function (Faker $faker){
    return [
        'name'                      => $faker->name,
        'description'               => $faker->sentences(1,true).' - plan',
        'duration_plan'             => $faker->numberBetween(1,365),
        'accrual_period'            => 86400,
        'delay_accruals'            => $faker->numberBetween(1,7),
        'will_buy_all_participants' => 0,
        'able_to_deposit_from'      => Carbon::now()->addDay(rand(0,15)),
        'able_to_deposit_to'        => Carbon::now()->addMonth(rand(1,2)),
        'status'                    => 1,
        'minimum_amount'            => $faker->numberBetween(1,10),
        'maximum_amount'            => $faker->numberBetween(1000,10000),
        'available_count_deposits'  => $faker->numberBetween(2,5),
        'payments_days_percent'     => [
            "monday"    => ['status' => true,'percent' => $faker->randomFloat(1,0.5,5.0)],
            "tuesday"   => ['status' => true,'percent' => $faker->randomFloat(1,0.5,5.0)],
            "wednesday" => ['status' => true,'percent' => $faker->randomFloat(1,0.5,5.0)],
            "thursday"  => ['status' => true,'percent' => $faker->randomFloat(1,0.5,5.0)],
            "friday"    => ['status' => true,'percent' => $faker->randomFloat(1,0.5,5.0)],
            "saturday"  => ['status' => true,'percent' => $faker->randomFloat(1,0.5,5.0)],
            "sunday"    => ['status' => true,'percent' => $faker->randomFloat(1,0.5,5.0)]
        ],
        'principal_return_percent'  => $faker->numberBetween(1,100),
        'maximum_percent_profit'    => 0,
        'sort'                      => 0,
        'allow_from_balance'        => 0,
        'allow_add_funds'           => 0,
        'limit_deps_user'           => 0,
    ];
});