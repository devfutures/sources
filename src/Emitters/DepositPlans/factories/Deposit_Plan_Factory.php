<?php

use App\User;
use Faker\Generator as Faker;

$factory->define(\Emitters\DepositPlans\Models\Deposit_Plans::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->paragraph.' - plan',
        'duration_plan' => mt_rand(50,100),
        'accrual_period' => 86400,
        'status' => 1,
        'minimum_amount' => 10,
        'maximum_amount' => 100000,
        'payments_days_percent' => [
            "monday"    => ['status' => true, 'percent' => 2],
            "tuesday"   => ['status' => true, 'percent' => 2],
            "wednesday" => ['status' => true, 'percent' => 2],
            "thursday"  => ['status' => true, 'percent' => 2],
            "friday"    => ['status' => true, 'percent' => 2],
            "saturday"  => ['status' => true, 'percent' => 0.5],
            "sunday"    => ['status' => true, 'percent' => 0.5]
        ],
        'principal_return_percent' => 0,
    ];
});