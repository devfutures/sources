<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepositPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposit__plans', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name')->nullable();
            $table->text('description')->nullable();

            //Количество начислений
            $table->integer('duration_plan')->default(0);
            // Период начислений в секундах
            $table->integer('accrual_period')->default(86400);
            // Задержка перед начислением
            $table->integer('delay_accruals')->default(0);
            // if available_count_deposits > 0 прибыль будет начислять после того как будет куплен весь план
            $table->tinyInteger('will_buy_all_participants')->index()->default(0);

            // 0 - infinity
            $table->integer('available_count_deposits')->default(0);
            //Количество депозитов которые может создать один пользователь
            $table->integer('limit_deps_user')->default(0);

            // 1 - active, 0 - no_active, 2 - hidden
            $table->tinyInteger('status')->default(0);

            $table->biginteger('minimum_amount')->default(0);
            $table->biginteger('maximum_amount')->default(0);

            $table->json('payments_days_percent')->nullable();
            // Максимальная сумма прибыли if duration_plan = 0
            $table->double('maximum_percent_profit')->default(0);

            // Сумма возврата после окончания депозита
            $table->integer('principal_return_percent')->default(0);

            $table->tinyInteger('sort')->default(0);

            $table->tinyInteger('allow_from_balance')->default(0);
            $table->tinyInteger('allow_add_funds')->default(0);

            $table->timestamp('able_to_deposit_from')->nullable();
            $table->timestamp('able_to_deposit_to')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposit__plans');
    }
}
