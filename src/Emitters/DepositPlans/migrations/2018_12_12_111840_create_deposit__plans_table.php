<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepositPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposit__plans', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('name')->nullable();
            $table->text('description')->nullable();

            $table->integer('duration_plan')->default(0);
            $table->integer('accrual_period')->default(86400);
            $table->integer('delay_accurals')->default(0);
            
            // 1 - active, 0 - no_active, 2 - hidden
            $table->tinyInteger('status')->default(0);
            
            $table->biginteger('minimum_amount')->default(0);
            $table->biginteger('maximum_amount')->default(0);

            $table->json('payments_days_percent')->nullable();
            $table->integer('principal_return_percent')->default(0);

            $table->tinyInteger('sort')->default(0);

            $table->tinyInteger('allow_from_balance')->default(0);
            $table->tinyInteger('allow_add_funds')->default(0);

            $table->integer('limit_deps_user')->default(0);


            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposit__plans');
    }
}
