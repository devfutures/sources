<?php

namespace Emitters\DepositPlans\seed;

use Illuminate\Database\Seeder;

class DepositPlanSeeder extends Seeder
{
    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        factory(\Emitters\DepositPlans\Models\Deposit_Plans::class, 10)->create();
    }
}
