<?php /** @noinspection ALL */

namespace Emitters\DepositUsers;

use App\User;
use Illuminate\Support\Carbon;
use Emitters\Abstracts\DepositUsers\DepositUserAbstract;
use Emitters\Contracts\UserDeposits\UserDepositsContract;
use Emitters\DepositPlans\Models\Deposit_Plans;
use Emitters\DepositUsers\Exceptions\DepositCreateException;
use Emitters\DepositUsers\Exceptions\DepositUpdateException;
use Emitters\Filters\Filters;
use Illuminate\Support\Facades\DB;
use Mockery;

class DepositUsersManager extends DepositUserAbstract implements UserDepositsContract {
    /**
     * @param $id
     * @param $data
     *
     * @return mixed
     */
    public function update($id, $data) {
        return $this->model::where('id', $id)->update($data);
    }

    /**
     * @param array $deposit_id
     * @return mixed
     */
    public function delete(array $deposit_id) {
        return $this->model::destroy($deposit_id);
    }

    /**
     * @param User $user
     *
     * @return mixed
     */
    public function getDepositUser(User $user, $with = []) {
        if(count($with) == 0) {
            $with = ['plan', 'owner', 'system'];
        }
        return $this->model::where('user_id', $user->id)->where('status', 0)->with($with)->paginate($this->paginateNum);
    }

    /**
     * @param User $user
     *
     * @return mixed
     */
    public function getDepositUserEnd(User $user, $with = []) {
        if(count($with) == 0) {
            $with = ['plan', 'owner', 'system'];
        }
        return $this->model::where('user_id', $user->id)->where('status', 1)->with($with)->paginate($this->paginateNum);
    }

    /**
     * @param array $filter
     *
     * @return mixed
     */
    public function getDeposits(array $filter = []) {
        $deposits      = $this->model::filters(new Filters($filter, $this->availableFilters))->orderBy('id', 'desc')->with(['system', 'owner', 'owner.upline', 'plan'])->paginate($this->paginateNum);
        $info_deposits = users_operations()->getTotalAccrualsByDeposits($deposits->pluck('id'));
        $deposits->transform(function ($item) use ($info_deposits) {
            $res                         = $info_deposits->where('deposit_id', $item->id)->first();
            $item->total_amount_accruals = 0;
            $item->count_accruals        = 0;
            if($res) {
                $item->total_amount_accruals = $res->amount_accruals;
                $item->count_accruals        = $res->count_accruals;
            }
            return $item;
        });
        $deposits->toSmallAmount()->bindPaymentSystems();
        return $deposits;

    }

    /**
     * @param \Illuminate\Config\Repository|mixed|null $paginateNum
     *
     * @return DepositUsersManager
     */
    public function setPaginateNum($paginateNum) {
        $this->paginateNum = $paginateNum;
        return $this;
    }

    /**
     * @param null $depositId
     * @param      $amount
     * @param bool $fromBalance
     *
     * @return mixed
     * @throws DepositCreateException
     * @throws DepositUpdateException
     */
    public function updateDepositAmount($depositId = null, $amount, $fromBalance = false) {
        $deposit = $this->getDepositOne($depositId);
        if(!$deposit->plan->allow_add_funds) {
            throw new DepositUpdateException('NOT_ALLOW_ADD_FUNDS');
        }
        if($deposit->plan->allow_from_balance == 0 && $fromBalance == true) {
            throw new DepositUpdateException('NOT_UPDATE_DEPOSIT_FROM_BALANCE');
        }
        $deposit->amount = df_add($deposit->amount, $amount);
        $deposit->save();
        return $deposit;
    }

    /**
     * @param $depositId
     *
     * @return mixed
     * @throws DepositCreateException
     */
    public function getDepositOne($depositId) {
        $deposit = $this->model::where('id', $depositId)->with('plan')->first();
        if(!$deposit) {
            throw new DepositCreateException('ERROR_FIND_DEPOSIT');
        }

        return $deposit;
    }

    /**
     * @param $planID
     *
     * @return mixed
     */
    public function countDepositsByPlan($planID) {
        return $this->model::where('plan_id', $planID)->count();
    }

    /**
     * @param $event
     * @return bool
     * @throws DepositCreateException
     */
    public function eventAddFunds($event, $with_admin_confrim = false, $pass_class_name = null) {
        DB::beginTransaction();

        $check_class_name = null;

        if($with_admin_confrim === false) {
            if($event instanceof \Selfreliance\PerfectMoney\Events\PerfectMoneyPaymentIncome) {
                $check_class_name = 'Selfreliance\PerfectMoney\PerfectMoney';
            } elseif($event instanceof \Selfreliance\Payeer\Events\PayeerPaymentIncome) {
                $check_class_name = 'Selfreliance\Payeer\Payeer';
            } elseif($event instanceof \Selfreliance\CoinPayments\Events\CoinPaymentsPaymentIncome) {
                $check_class_name = 'Selfreliance\CoinPayments\CoinPayments';
            } elseif($event instanceof \Selfreliance\BitGo\Events\BitGoPaymentIncome) {
                $check_class_name = 'Selfreliance\BitGo\BitGo';
            }
        } else {
            $check_class_name = $pass_class_name;
        }

        if(property_exists($event, 'info')) {
            $event = $event->info;
        }

        $operation = null;
        $ps        = paymentsystems()->grabAll();

        try {
            if($check_class_name === null) {
                throw new \Exception('Error extraction class name from event');
            }

            if(!$operation = $this->checkAddFunds($event->transaction, $event->payment_id, $check_class_name)) {
                throw new \Exception('Error get operation');
            }

            $payment_system_createing_operation = $ps->where('id', $operation->payment_system_id)->where('class_name', $check_class_name)->first();
            if($payment_system_createing_operation == null) {
                throw new \Exception('Error creating operation and send event');
            }

            if($payment_system_createing_operation->min_sum_purchase > $event->amount) {
                throw new \Exception('The minimum replenishment amount is less than the replenishment amount.');
            }

            $amount = convert_to_bigInteger($event->amount);
            $this->updateOperationAndChangeBalance($event, $operation, $amount);

            $user = User::find($operation->user_id);
            switch(config('emitters.after_add_funds')) {
                case "CREATE_DEPOSIT":
                    /**
                     * if plan_id empty
                     */
                    $this->addFundsToDeposit($user, [
                        'payment_system' => $operation->payment_system_id,
                        'amount'         => $event->amount,
                        'plan_id'        => $operation->plan_id,
                        'from_balance'   => 1,
                    ]);
                    break;
                case "EXCHANGE":
                    $from = $ps->where('id', $operation->payment_system_id)->first();
                    $to   = $ps->where('id', config('emitters.default_payment_system_id'))->first();
                    $this->balanceManger->exchange($user, $amount, $from, $to);
                    break;
            }
            DB::commit();
        } catch (DepositCreateException | \Exception $e) {
            DB::rollBack();
            report($e);
            if($operation != null) {
                $operation->status = 'error';
                $arr               = ['error_message' => $e->getMessage()];
                if($operation->data_info && count($operation->data_info) > 0) {
                    $operation->data_info = array_merge($operation->data_info, $arr);
                } else {
                    $operation->data_info = $arr;
                }
                $operation->save();
            }
//            throw new \Exception($e->getMessage());
        }
    }

    public function checkAddFunds($transaction, $payment_id, $class_name) {
        $re_enrollment = $this->operationsManager->getByTransaction($transaction);
        if($re_enrollment) return false;

        $operation = $this->operationsManager->getOperationToAddFunds($payment_id);
        if(!$operation) return false;

        return $operation;
    }

    /**
     * @param $event
     * @param $operation
     * @param $amount
     * @param $res
     */
    protected function updateOperationAndChangeBalance($event, $operation, $amount): void {
        $operation->status      = 'completed';
        $operation->amount      = $amount;
        $operation->transaction = $event->transaction;
        if(array_key_exists('full_data_ipn', $event->add_info))
            $operation->data_info = $event->add_info['full_data_ipn'];

        $res = $this->balanceManger->changeBalance([
            'operation'         => 'ADD_FUNDS',
            'type'              => 'buy',
            'user_id'           => $operation->user_id,
            'payment_system_id' => $operation->payment_system_id,
            'amount'            => $amount,
        ]);

        $operation->parent_id = $res->id;
        $operation->save();
        $user = usersdata()->getUserInfo($operation->user_id, []);
        partnership_accrual()->affiliateBonus('ADD_FUNDS', $user, $amount, 0, $operation->payment_system_id);
    }

    /**
     * @param $user
     * @param array $request
     * @param bool $no_transition
     * @return array|mixed
     * @throws DepositCreateException
     */
    public function addFundsToDeposit($user, array $request, $no_transition = false) {
        $data      = [];
        $payment   = paymentsystems()->getOne($request['payment_system']);
        $amount_db = convert_to_bigInteger($request['amount']);
        if(!array_key_exists('from_balance', $request) || $request['from_balance'] == 0) {
            $plan_id = 0;
            if(array_key_exists('plan_id', $request)) {
                $plan_id = $request['plan_id'];
            }

            $transaction = null;
            if(array_key_exists('transaction', $request)) {
                $transaction = $request['transaction'];
            }

            $hidden = 0;
            if(array_key_exists('hidden', $request)) {
                $hidden = $request['hidden'];
            }

            $operation = $this->operationsManager->createOperationAddFunds($user, $payment->id, $amount_db, $plan_id, $transaction, $hidden);
            if(!$no_transition) {
                $data                 = $this->transitionToPaymentSystem($operation->id, $payment, $request['amount']);
                $operation->data_info = mergeDataInfo($operation->data_info, $data['form']);
                $operation->save();
            } else {
                $data = $operation;
            }
        } else {
            // создание депозита с баланса
            return $this->createDepositFromBalanceBefore($user, $request);
        }
        return $data;
    }

    /**
     * @param array $request
     *
     * @return mixed
     * @throws DepositCreateException
     */
    public function transitionToPaymentSystem($operation_id, $payment, $amount) {
        if(!class_exists($payment->class_name)) {
            throw new DepositCreateException('PAYMENT_SYSTEM_CLASS_NAME_ERROR');
        }

        $abstract_payment_class = $payment->class_name;
        $ex                     = app()->make($abstract_payment_class);

        if(!method_exists($ex, 'form')) {
            throw new DepositCreateException('PAYMENT_SYSTEM_CLASS_FORM_ERROR');
        }

        if((env('APP_ENV') === 'testing' OR env('APP_ENV') === 'local') && env('APP_DEBUG') === true && strtolower($payment->title) === 'stellar') {
            $ex = Mockery::mock($payment->class_name, function ($mock) use ($payment) {
                $PassData               = new \stdClass();
                $PassData->address      = '3' . str_random(34);
                $PassData->another_site = $payment->redirect_another_site;
                $mock->shouldReceive('form')->andReturn($PassData);
            });
        }

//        if(property_exists($ex, 'memo')){
//            $ex->memo = 'ID: '.$operation_id;
//        }

        $res = $ex->form($operation_id, $amount, $payment->currency);
        if(strtolower($payment->title) === 'stellar') {
            $res->destination_tag = $operation_id;
            $tmp                  = explode('?', $res->address);
            $res->address         = $tmp[0];
        }

        return [
            'status'                => 'success',
            'code'                  => 0,
            'redirect_another_site' => $payment->redirect_another_site,
            'form'                  => $res,
            'operation_id'          => $operation_id,
            'status_operation'      => 'pending',
            'amount'                => $amount,
            'payment_system'        => $payment
        ];
    }

    public function createDepositFromBalanceBefore($user, $request) {
        if(!$user) {
            throw new \Exception('USER_NOT_CORRECTLY');
        }

        if($request['amount'] <= 0) {
            throw new \Exception('AMOUNT_DOES_NOT_FIT');
        }

        if($request['plan_id'] == 0) {
            throw new \Exception('OPERATION_PLAN_ID_IS_ZERO');
        }

        $amount = df_mul($request['amount'], 100000000, 0);
        $this->balanceManger->checkBalanceAvalible($user, $amount, $request['payment_system'], false);
        // списать деньги
        $sell = $this->balanceManger->changeBalance([
            'type'              => 'sell',
            'operation'         => 'CREATE_DEPOSIT',
            'user_id'           => $user->id,
            'amount'            => $amount,
            'payment_system_id' => $request['payment_system'],
        ]);
        return $this->createDeposit($user, $this->depositPlans->getPlanById($request['plan_id']), $amount, $request['payment_system'], $sell);
    }


    /**
     * @param User $user
     * @param Deposit_Plans $plan
     * @param $amount
     * @param $payment_system_id
     * @param $sell
     * @return mixed
     * @throws DepositCreateException
     */
    public function createDeposit(User $user, Deposit_Plans $plan, $amount, $payment_system_id, $sell = null) {
        /**
         * TODO: add check for amount
         */
        $this->checkAbleToDeposit($plan);
        $this->checkCountDepositsUsers($user, $plan);
        $this->checkAvailableCountDeposits($plan);

        $deposit = $this->store([
            'user_id'           => $user->id,
            'plan_id'           => $plan->id,
            'payment_system_id' => $payment_system_id,
            'amount'            => $amount,
            'accrual_at'        => $this->depositPlans->getNextAccruals($plan),
            'deposit_end_at'    => $this->depositPlans->getDateCloseDeposit($plan)->deposit_close,
        ]);
        $sell_id = ($sell == null) ? 0 : $sell->id;
        users_operations()->createOperationCreateDeposit($user, $sell_id, $plan->id, $deposit->id, $payment_system_id, $amount);
        partnership_accrual()->affiliateBonus('BY_CREATE_DEPOSIT', $user, $amount, $deposit->id, $payment_system_id);
//
//        if($plan->accrual_period == 0 && $plan->delay_accruals == 0 && $plan->will_buy_all_participants == 0){
//            deposit_accruals()->accrualsByDeposit(collect($deposit));
//        }

        return $deposit;
    }

    /**
     * @param $plan
     *
     * @throws DepositCreateException
     */
    protected function checkAbleToDeposit($plan) {
//        if(Carbon::now()->lt($plan->able_to_deposit_from) || Carbon::now()->gt($plan->able_to_deposit_to)) {
//            throw new DepositCreateException('DEPOSIT_PLAN_PERIOD_EXPIRED');
//        }
    }

    /**
     * @param User $user
     * @param      $plan
     *
     * @throws DepositCreateException
     */
    protected function checkCountDepositsUsers(User $user, Deposit_Plans $plan) {
        $count_user_deposits = $this->countDepositsUsers($user, $plan);
        if($plan->limit_deps_user > 0 && $count_user_deposits >= $plan->limit_deps_user) {
            throw new DepositCreateException('LIMIT_FOR_USER');
        }
    }

    /**
     * @param User $user
     * @param      $plan
     *
     * @return mixed
     */
    public function countDepositsUsers(User $user, Deposit_Plans $plan = null, array $status = []) {
        return $count_user_deposits = $this->model::where('user_id', $user->id)->where(function ($query) use ($plan) {
            if($plan) {
                $query->where('plan_id', $plan->id);
            }
        })->where(function ($query) use ($status) {
            if(count($status) > 0) {
                $query->whereIn('status', $status);
            }
        })->count();
    }

    protected function checkAvailableCountDeposits($plan) {
        $deposits = $this->countDepositsByPlansIds([$plan->id]);

        $cnt_deposits = $deposits->where('plan_id', $plan->id)->first();
        if($plan->available_count_deposits > 0 && $cnt_deposits && $cnt_deposits->count >= $plan->available_count_deposits) {
            throw new DepositCreateException('DEPOSIT_PLAN_NOT_AVAILABLE');
        }
    }

    /**
     * @param $ids
     *
     * @return mixed
     */
    public function countDepositsByPlansIds($ids) {
        return $this->model::selectRaw('plan_id, count(id) as count')->whereIn('plan_id', $ids)->groupBy('plan_id')->get();
    }

    /**
     * @return mixed
     */
    public function getDepositsOfAccruals() {
        $deps = $this->model
            ::where('accrual_at', '<=', Carbon::now())
            ->where('status', '0')
            ->with(['plan', 'owner'])
            ->get();

        $plans = $this->depositPlans->getPlansWillBuyAllParticipants();
        if(count($plans) > 0) {
            $deps2 = $this->model
                ::where(function ($query) use ($plans) {
                    if(count($plans) > 0) {
                        $query->whereIn('plan_id', $plans->pluck('id'));
                    }
                })
                ->whereNotIn('plan_id', $deps->pluck('id'))
                ->where('status', 0)
                ->with(['plan', 'owner'])
                ->get();
        }
        $resulted = (isset($deps2)) ? $deps->merge($deps2) : $deps;
        return $resulted;
    }

    /**
     * @param User $user
     * @return array
     */
    public function getSumOfDeposits(User $user) {
        $deposits = $this->model::query()
            ->where('user_id', $user->id)
            ->whereIn('status', [0, 1])
            ->get();

        $sum = $this->operationsManager->getSumOfDeposits($deposits);
        return $sum;
    }

    /**
     * @param $users_id
     * @param int $status
     * @return mixed
     */
    public function getDepositUsersByStatus($users_id, $status = 0) {
        return $this->model::query()
            ->whereIn('user_id', $users_id)
            ->where('status', $status)
            ->get();
    }

    /**
     * @param $user_id
     * @param $operation_id
     * @return mixed
     * @throws \Exception
     */
    public function getOperationInfo($user_id, $operation_id) {
        $operations = $this->operationsManager->getOperationByUserAndId($operation_id, $user_id, ['ADD_FUNDS']);
        if(count($operations) == 0) throw new \Exception('Operation not found');
        $operation = $operations->first();
        $payment   = paymentsystems()->getOne($operation->payment_system_id);
        if($payment->is_fiat == 1) {
            $form = $this->transitionToPaymentSystem($operation->id, $payment, $operation->amount);
            return $form;
        }

        $form = [
            'address'      => $operation->data_info['address'],
            'another_site' => (bool)$payment->redirect_another_site,
        ];

        if(array_key_exists('destination_tag', $operation->data_info)){
            $form['destination_tag'] = $operation->data_info['destination_tag'];
        }

        $form = $operation->data_info;

        if(strtolower($payment->title) === 'stellar') {
            $form['destination_tag'] = $operation_id;
        }
        return [
            'status'                => 'success',
            'code'                  => 0,
            'redirect_another_site' => $payment->redirect_another_site,
            'form'                  => $form,
            'operation_id'          => $operation_id,
            'status_operation'      => $operation->status,
            'amount'                => $operation->amount,
            'payment_system'        => $payment
        ];

        $data = $this->transitionToPaymentSystem($operation->id, $payment, convert_from_bigInteger($operation->amount));
        return $data;
    }
}