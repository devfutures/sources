<?php

namespace Emitters\DepositUsers;

use App\User;
use Emitters\Abstracts\DepositUsers\DepositUserAbstract;
use Emitters\Contracts\UserDeposits\UserDepositsContract;
use Emitters\Filters\Filters;

class DepositUsersManager extends DepositUserAbstract implements UserDepositsContract {
    /**
     * @param User    $user
     * @param         $amount
     * @param         $plan
     * @param integer $payment_system
     *
     * @return mixed
     */
    public function createDeposit (User $user, $plan, $amount, $payment_system_id) {

        return $this->store([
            'user_id' => $user->id,
            'plan_id' => $plan->id,
            'payment_system_id' => $payment_system_id,
            'amount' => $amount,
            'accrual_at' => $this->depositPlans->getNextAccruals($plan),
            'deposit_end_at' => $this->depositPlans->getDateCloseDeposit($plan)->deposit_close,
        ]);
    }

    /**
     * @param User $user
     *
     * @return mixed
     */
    public function getDepositUser(User $user){
	    return $this->model::where('user_id', $user->id)->paginate($this->paginateNum);
    }

    /**
     * @param User $user
     *
     * @return mixed
     */
    public function getDepositUserEnd(User $user){
	    return $this->model::where('user_id', $user->id)->where('deposit_end', 1)->paginate($this->paginateNum);
    }

    /**
     * @return mixed
     */
    public function getDeposits (array $filter = []) {
        return $this->model::filters(new Filters($filter, $this->avalibleFiltered))->paginate($this->paginateNum);
    }

    /**
     * @param \Illuminate\Config\Repository|mixed|null $paginateNum
     *
     * @return DepositUsersManager
     */
    public function setPaginateNum ($paginateNum) {
        $this->paginateNum = $paginateNum;
        return $this;
    }
}