<?php

namespace Emitters\DepositUsers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory as EloquentFactory;
use Selfreliance\CoinPayments\Events\CoinPaymentsPaymentIncome;
use Selfreliance\PerfectMoney\Events\PerfectMoneyPaymentIncome;
use Selfreliance\Payeer\Events\PayeerPaymentIncome;
use Selfreliance\BitGo\Events\BitGoPaymentIncome;
use Illuminate\Support\Facades\Event;

class DepositUsersServiceProvider extends ServiceProvider
{
    protected $add_funds_events = [
        PerfectMoneyPaymentIncome::class,
        PayeerPaymentIncome::class,
        CoinPaymentsPaymentIncome::class,
        BitGoPaymentIncome::class
    ];

    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/migrations');

        $this->publishes([
            __DIR__.'/migrations/' => database_path('migrations')
        ], 'migrations');

        $this->app->make(EloquentFactory::class)->load(__DIR__ . '/factories/');

        Event::listen($this->add_funds_events, function ($event){
            resolve('libair.depositusers')->eventAddFunds($event);
        });
    }
}
