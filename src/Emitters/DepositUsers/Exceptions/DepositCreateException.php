<?php


namespace Emitters\DepositUsers\Exceptions;


class DepositCreateException extends \Exception {
    const LIMIT_FOR_USER = 'The maximum amount of deposits for one user on this plan';
    const ERROR_FIND_DEPOSIT = 'Deposit by ID not found';
    const DEPOSIT_PLAN_PERIOD_EXPIRED = 'Period for deposit with this plan has expired.';
    const PAYMENT_SYSTEM_CLASS_NAME_ERROR = 'The payment system merchant not supported';
    const PAYMENT_CLASS_FORM_METHOD_ERROR = 'The payment system does not contain the required method';
}