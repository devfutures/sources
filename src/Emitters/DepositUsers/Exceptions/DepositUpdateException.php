<?php


namespace Emitters\DepositUsers\Exceptions;


class DepositUpdateException extends \Exception {
    const NOT_ALLOW_ADD_FUNDS = 'Under the terms of the plan, a change in the amount of the deposit is not available.';
    const NOT_UPDATE_DEPOSIT_FROM_BALANCE = 'Under the terms of the plan, changing the amount of the deposit from the balance is not available.';
}