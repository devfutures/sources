<?php

namespace Emitters\DepositUsers\Models;

use Illuminate\Database\Eloquent\Model;

class Deposit_Users extends Model
{
    protected $fillable = [
        'user_id', 'plan_id', 'payment_system_id', 'amount', 'deposit_end', 'accrual_at', 'deposit_end_at'
    ];

    public function plan(){
    	return $this->hasOne('Emitters\DepositPlans\Models\Deposit_Plans', 'id', 'plan_id');
    }

    public function owner(){
    	return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function scopeFilters($query, $filter){
        return $filter->apply($query);
    }
}
