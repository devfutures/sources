<?php

namespace Emitters\DepositUsers\Models;

use Emitters\Filters\Traits\AddFilters;
use Emitters\PaymentSystems\Models\Payment_System;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Deposit_Users extends Model {
    use SoftDeletes, AddFilters;
    protected $fillable = [
        'user_id',
        'plan_id',
        'payment_system_id',
        'amount',
        'status',
        'status_text',
        'accrual_at',
        'deposit_end_at'
    ];

    protected $dates = [
        'accrual_at',
        'deposit_end_at'
    ];

    public function plan() {
        return $this->hasOne('Emitters\DepositPlans\Models\Deposit_Plans', 'id', 'plan_id');
    }

    public function owner() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function system() {
        return $this->hasOne(Payment_System::class, 'id', 'payment_system_id');
    }
}
