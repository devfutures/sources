---
### Deposit Users
#### Извлечение DepositUsersManager
##### Получение через IoC
```php
$depositUsers = resolve('libair.depositusers');
```

##### Использование в контроллере с помощью type-hinting
```php
use App\LibrariesAir\DepositUsers\DepositUsersManager;
...
public function index(DepositUsersManager $depositUsers) {
    ...
}
```


#### Методы DepositUsersManager

##### Создание нового депозита
```php
$balanceManager->createDeposit($user, $plan, $amount, $payment_system);
```
* App\User - object
* Emitters\DepositPlans\Models\Deposit_Plans - object
* amount - сумма депозита
* платежная система ID
* return type model object

##### Активные депозиты пользователя
```php
public function getDepositUser(User $user);
```

##### Неактивные депозиты пользователя
```php
public function getDepositUserEnd(User $user);
```
##### Список депозитов всех пользователей с постраничной навигацией и фильтрами
```php
$depositUsers->getDeposits(array $filter = [])
```
##### Изменить количество депозитов на одной странице
```php
$balanceManger->setPaginateNum(20);
```