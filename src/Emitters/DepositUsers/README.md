---
### Deposit Users
#### Извлечение DepositUsersManager
##### Получение через IoC
```php
$depositUsers = resolve('libair.depositusers');
```

##### Использование в контроллере с помощью type-hinting
```php
use App\LibrariesAir\DepositUsers\DepositUsersManager;
...
public function index(DepositUsersManager $depositUsers) {
    ...
}
```

#### Методы DepositUsersManager

##### Создание нового депозита
```php
$depositUsers->createDeposit($user, $plan, $amount, $payment_system);
```
* App\User - object
* Emitters\DepositPlans\Models\Deposit_Plans - object
* amount - сумма депозита
* платежная система ID
* return type model object

##### Активные депозиты пользователя
```php
$depositUsers->getDepositUser(User $user);
```
##### Получит информацию о депозите с планом
```php
$depositUsers->getDepositOne(int $deposit_id);
```
##### Неактивные депозиты пользователя
```php
$depositUsers->getDepositUserEnd(User $user);
```
##### Список депозитов всех пользователей с постраничной навигацией и фильтрами
```php
$depositUsers->getDeposits(array $filter = [])
```
##### Изменить количество депозитов на одной странице
```php
$depositUsers->setPaginateNum(20);
```
##### Проверить может ли пользователь иметь такое количество депозитов по условия плана
```php
$this->checkCountDepositsUsers(User $user, Deposit_Plans $plan);
```
##### Количество депозитов пользователя с дополнительным условием плана
```php
$depositUsers->countDepositsUsers(User $user, Deposit_Plans $plan = null);
```
##### Обновить сумму депозита
```php
$depositUsers->updateDepositAmount($depositId = null, $amount, $fromBalance = false);
```
* depositId - int идентификатор депозита
* сколько необходимо добавить к сумме депозита
* операция добавления с баланса или нет