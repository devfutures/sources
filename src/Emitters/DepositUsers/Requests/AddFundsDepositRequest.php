<?php


namespace Emitters\DepositUsers\Requests;


use App\LibrariesAir\DepositPlans\DepositPlansManager;
use App\LibrariesAir\PaymentSystems\PaymentSystemsManager;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AddFundsDepositRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param PaymentSystemsManager $paymentSystemsManager
     *
     * @return array
     */
    public function rules(PaymentSystemsManager $paymentSystemsManager,DepositPlansManager $depositPlansManager){
        $ps = $paymentSystemsManager->getOne($this->request->filter('payment_system'));
        $plan = $depositPlansManager->getPlanById($this->request->filter('plan_id'));

        $data_valid = [
            'payment_system' => [
                'required','integer'
            ],
            'amount'         => [
                'required_with_all:payment_system','numeric'
            ],
            'plan_id'        => [
                'sometimes','required_with_all:payment_system,amount','required','integer'
            ],
            'from_balance'   => [
                'required_with:plan_id','in:0,1'
            ]
        ];

        if($ps == null){
            $data_valid['payment_system'][] = function ($attribute,$value,$fail) use ($ps){
                $fail(__('validation.exists'));
            };
            $data_valid['amount'][] = 'between:0,0';
        }else{
            if($ps->status == 0){
                $data_valid['payment_system'][] = function ($attribute,$value,$fail) use ($ps){
                    $fail(__('validation.exists'));
                };
            }
            if($ps->min_sum_purchase > 0){
                $data_valid['amount'][] = 'min:'.$ps->min_sum_purchase;
            }

            if($ps->max_sum_purchase > 0){
                $data_valid['amount'][] = 'max:'.$ps->max_sum_purchase;
            }
        }

        if(!$plan){
            $data_valid['plan_id'][] = function ($attribute,$value,$fail) use ($plan){
                $fail(__('validation.exists'));
            };
        }elseif($plan->status == 0){
            $data_valid['plan_id'][] = function ($attribute,$value,$fail) use ($plan){
                $fail(__('validation.exists'));
            };
        }

        $data_valid['from_balance'][] = function ($attribute,$value,$fail) use ($plan){
            if($value == 1){
                if($plan && $plan->allow_from_balance == 0){
                    $fail(_i('The plan does not support deposits from the balance.'));
                }
            }
        };
        return $data_valid;
    }
}