<?php


namespace Emitters\DepositUsers\Requests;


use Illuminate\Foundation\Http\FormRequest;

class FilteredDepositsRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'orderBy'             => 'array',
            'orderBy.*.by'        => 'string',
            'orderBy.*.direction' => 'string|in:asc,desc',

            'where'         => 'array',
            'where.*.field' => 'string|in:status,payment_system_id',
            'where.*.value' => 'integer',

            'whereBetween'         => 'array',
            'whereBetween.*.field' => 'string|in:amount',
            'whereBetween.*.from'  => 'numeric',
            'whereBetween.*.to'    => 'numeric',

            'dateBetween'                    => 'array',
            'dateBetween.*.from_date_column' => 'string|in:created_at,deposit_end_at,accrual_at',
            'dateBetween.*.to_date_column'   => 'string|in:created_at,deposit_end_at,accrual_at',
            'dateBetween.*.from'             => 'date_format: "d.m.Y"',
            'dateBetween.*.to'               => 'date_format: "d.m.Y"',
        ];
    }
}