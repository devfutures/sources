<?php


namespace Emitters\DepositUsers\Requests;


use Illuminate\Foundation\Http\FormRequest;

class UpdateDepositsRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'plan_id'           => 'integer',
            'payment_system_id' => 'integer',
            'amount'            => 'numeric',
            'status'            => 'in:0,1,2',
            'status_text'       => 'present',
            'accrual_at'        => 'present',
            'deposit_end_at'    => 'present',
            'created_at'        => 'string',
            'updated_at'        => 'string',
        ];
    }
}