<?php

use App\User;
use Faker\Generator as Faker;

$factory->define(\Emitters\DepositUsers\Models\Deposit_Users::class, function (Faker $faker) {
    return [
        'user_id' => factory('App\User')->create()->id,
        'plan_id' => factory('Emitters\DepositPlans\Models\Deposit_Plans')->create()->id,
        'payment_system_id' => factory('Emitters\PaymentSystems\Models\Payment_System')->create()->id,
        'amount'  => mt_rand(10000000,100000000000),
        'accrual_at' => \Carbon\Carbon::now()->addHour(24),
        'deposit_end' => 0,
    ];
});