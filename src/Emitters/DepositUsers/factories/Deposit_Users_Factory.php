<?php

use Faker\Generator as Faker;

$factory->define(\Emitters\DepositUsers\Models\Deposit_Users::class, function (Faker $faker) {
    return [
        'user_id'           => function () {
            return factory('App\User')->create()->id;
        },
        'plan_id'           => function () {
            return factory('Emitters\DepositPlans\Models\Deposit_Plans')->create()->id;
        },
        'payment_system_id' => function () {
            return factory('Emitters\PaymentSystems\Models\Payment_System')->create()->id;
        },
        'amount'            => mt_rand(10, 10000),
        'accrual_at'        => \Carbon\Carbon::now()->subHour(24),
        'deposit_end_at'    => \Carbon\Carbon::now()->addMonth()->addDay(rand(0, 5)),
        'status'            => 0,
    ];
});