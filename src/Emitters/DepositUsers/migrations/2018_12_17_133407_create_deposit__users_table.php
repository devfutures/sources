<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepositUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposit__users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->index();
            $table->integer('plan_id')->index();
            $table->integer('payment_system_id')->index();
            $table->biginteger('amount');
            // 0 - deposit is active, 1 - deposit end, 2 - deposit error
            $table->tinyInteger('status')->default(0);
            $table->text('status_text')->nullable();
            $table->timestamp('accrual_at')->nullable();
            $table->timestamp('deposit_end_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposit__users');
    }
}
