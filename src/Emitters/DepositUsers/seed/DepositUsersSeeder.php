<?php

namespace Emitters\DepositUsers\seed;

use Emitters\DepositUsers\Models\Deposit_Users;
use Illuminate\Database\Seeder;

class DepositUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Deposit_Users::class, 10)->create();
    }
}
