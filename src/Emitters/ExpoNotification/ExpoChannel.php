<?php


namespace Emitters\ExpoNotification;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class ExpoChannel {
    const EXPO_PUSH_URL = 'https://exp.host/--/api/v2/push/send';

    /** @var Client */
    private $client = null;

    /**
     * @param NotifyMessage $notification
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function send(NotifyMessage $notification) {
        $data = $notification->toPushFormat();
        try {
            return $this->getGuzzleClient()->post(self::EXPO_PUSH_URL, ['json' => $data]);
        } catch (GuzzleException $e) {
            //TODO validate response.
            return null;
        }
    }

    /**
     * @return Client
     */
    private function getGuzzleClient() {
        $this->client = $this->client ?? $this->initGuzzleClient();
        return $this->client;
    }

    /**
     * @return Client
     */
    private function initGuzzleClient() {
        $client = new Client([
            'version' => '2.0',
            'headers'  => [
                'host'            => 'exp.host',
                'accept'          => 'application/json',
                'accept-encoding' => 'gzip, deflate',
                'content-type'    => 'application/json',
            ]
        ]);
        return $client;
    }
}