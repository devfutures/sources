<?php


namespace Emitters\ExpoNotification;


use Emitters\ExpoNotification\Models\Users_Devices;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory as EloquentFactory;

class ExpoNotificationServiceProvider extends ServiceProvider {
    public function boot() {
        $this->loadMigrationsFrom(__DIR__ . '/migrations');

        $this->publishes([
            __DIR__ . '/migrations/' => database_path('migrations')
        ], 'migrations');

        $this->app->make(EloquentFactory::class)->load(__DIR__ . '/factories/');
    }

    public function register() {
        $this->app->bind(UserDeviceManager::class, function ($app) {
            return new UserDeviceManager(new Users_Devices());
        });
        $this->app->alias(UserDeviceManager::class, 'users_devices_manager');

        $this->app->bind(ExpoChannel::class, function () {
            return new ExpoChannel();
        });

        $this->app->alias(ExpoChannel::class, 'expo_notification_driver');
    }
}