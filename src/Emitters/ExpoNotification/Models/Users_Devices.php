<?php

namespace Emitters\ExpoNotification\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Users_Devices extends Model {
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'token',
        'device',
        'operation_system_version'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }
}
