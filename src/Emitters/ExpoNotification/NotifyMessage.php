<?php


namespace Emitters\ExpoNotification;


class NotifyMessage {

    private $recipients;
    private $title;
    private $body;
    private $data;
    private $subtitle;
    private $channelId;

    /**
     * NotifyMessage constructor.
     * @param array $recipients
     * @param string $title
     * @param string $subtitle
     * @param string $body
     * @param array $data
     * @param string $channelId
     */
    public function __construct(
        array $recipients,
        string $title = null,
        string $subtitle = null,
        string $body = null,
        array $data = null,
        string $channelId = null) {

        $this->recipients = $recipients;
        $this->title      = $title;
        $this->body       = $body;
        $this->data       = $data;
        $this->channelId  = $channelId;
        $this->subtitle   = $subtitle;
    }


    /**
     * @return array
     */
    public function toPushFormat() {
        $data = [];
        foreach ($this->recipients as $recipient) {
                                         $messageToOneRecipient['to'] = $recipient;
            if (isset($this->title))     $messageToOneRecipient['title']     = $this->title;
            if (isset($this->subtitle))  $messageToOneRecipient['subtitle']  = $this->subtitle;
            if (isset($this->body))      $messageToOneRecipient['body']      = $this->body;
            if (isset($this->data))      $messageToOneRecipient['data']      = $this->data;
            if (isset($this->channelId)) $messageToOneRecipient['channelId'] = $this->channelId;

            $data[] = $messageToOneRecipient;
        }
        return $data;
    }

}