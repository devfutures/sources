<?php


namespace Emitters\ExpoNotification\Requests;


use Illuminate\Foundation\Http\FormRequest;

class DeviceRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules() {
        return [
            'token'                    => 'required|string',
            'device'                   => 'required|string',
            'operation_system_version' => 'required|string'
        ];
    }
}