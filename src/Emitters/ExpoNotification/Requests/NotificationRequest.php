<?php


namespace Emitters\ExpoNotification\Requests;


use Illuminate\Foundation\Http\FormRequest;

class NotificationRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules() {
        return [
            'title'    => 'required|string',
            'body'     => 'required|string',
            'tokens'   => 'array',
            'tokens.*' => 'exists:users__devices,token'
        ];
    }
}