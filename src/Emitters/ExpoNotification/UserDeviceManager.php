<?php


namespace Emitters\ExpoNotification;

use Emitters\Abstracts\ExpoNotification\UserDeviceManagerAbstract;
use Emitters\ExpoNotification\Models\Users_Devices;

class UserDeviceManager extends UserDeviceManagerAbstract {

    /**
     * UserTokenManager constructor.
     * @param Users_Devices $model
     */
    public function __construct(Users_Devices $model) {
        $this->userDevices = $model;
    }

    /**
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function createDevice(array $data) {
        return $this->userDevices::query()->create($data);
    }

    /**
     * @return mixed
     */
    public function getDevices() {
        return $this->userDevices::query()->with(['user'])->paginate(20);
    }

    /**
     * @param int $userID
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getUserDevices(int $userID) {
        return $this->userDevices::query()->where('user_id', $userID)->get();
    }

    /**
     * @param $value
     * @param string $field
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getDeviceBy($value, $field = 'id') {
        return $this->userDevices::query()->where($field, $value)->get();
    }

    /**
     * @param int $deviceID
     * @param array $data
     * @return int
     */
    public function updateDevice(int $deviceID, array $data) {
        return $this->userDevices::query()->where('id', $deviceID)->update($data);
    }

    /**
     * @param array $deviceIDs
     * @return mixed
     */
    public function deleteDevices(array $deviceIDs) {
        return $this->userDevices::query()->whereIn('id', $deviceIDs)->delete();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getAllTokens() {
        $devices = $this->userDevices::all();
        $tokens = $devices->pluck('token');
        return $tokens;
    }
}