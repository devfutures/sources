<?php

use Faker\Generator as Faker;

$factory->define(\Emitters\ExpoNotification\Models\Users_Devices::class, function (Faker $faker) {
    return [
        'user_id'                  => function () {
            return factory(\App\User::class)->create()->id;
        },
        'token'                    => $faker->text(32),
        'device'                   => 'Samsung S10',
        'operation_system_version' => 'Android 8.0'
    ];
});