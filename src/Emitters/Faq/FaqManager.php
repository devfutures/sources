<?php


namespace Emitters\Faq;


use Emitters\Faq\Models\Faqs;
use Emitters\Faq\Models\Faqs_Category;
use Emitters\Abstracts\Faq\FaqManagerAbstract;
use Emitters\Filters\Filters;
use Illuminate\Foundation\Auth\User;
use Illuminate\Pagination\LengthAwarePaginator;
use Xinax\LaravelGettext\Facades\LaravelGettext;

class FaqManager extends FaqManagerAbstract {

    /**
     * @param array|null $faqCategoryData
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function createFaqCategory(array $faqCategoryData = null) {
        if (!isset($faqCategoryData)) {
            return null;
        }
        foreach ($faqCategoryData as $data) {
            $id = array_key_exists('id', $data) ? $data['id'] : null;
            $category = $this->faqCategoryModel::query()->updateOrCreate(['id' => $id], $data);
        }
        return $this->getAllFaqCategory();
    }


    /**
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAllFaqCategory() {
        return $this->faqCategoryModel::all();
    }

    /**
     * Creates FAQ with several content.
     *
     * @param User       $user
     * @param array|null $faqData
     * @param array|null $faqContentData
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function createFaqWithContent(User $user, array $faqData = null, array $faqContentData = null) {
        if (!isset($faqData) && !isset($faqContentData)) return null;
        $faqData['user_id'] = $user->id;
        $faq = $this->storeFaq($faqData);

        foreach ($faqContentData as $contentDatum) {
            $this->storeContent($faq, $contentDatum);
        }

        return $faq->load(['contents', 'category']);
    }

    /**
     * Get all faq by specific language and ordered by sort.
     *
     * @param string $lang
     * @param bool $sort
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getFaq($lang = null, $sort = false) {
        $faq = $this->faqModel::query()
            ->where('is_draft', false)
            ->orderBy('sort', 'asc')
            ->with(['contents' => function ($query) use ($lang) {
                if (isset($lang)) $query->where('language', $lang);
            }])
            ->get();
        $faq->transform(function ($value) {
            if ($value->contents->first() == null) {
                $value->load(['contents' => function ($query) {
                    $query->where('language', LaravelGettext::getLocaleLanguage());
                }]);
            }
            return $value;
        });
        return $faq;

//        return $sort ? $faq->sortBy('sort')->values()->all() : $faq;
    }

    /**
     * Get all faq for admin page.
     *
     * @param array $filters
     * @return LengthAwarePaginator
     */
    public function getAllFaq($filters = []) {
        if (!array_key_exists('orderBy', $filters['faq_filters'])) $filters['faq_filters']['orderBy'] = [['by' => 'sort', 'direction' => 'asc']];
        $contentFilters = isset($filters['content_filters']) ? $filters['content_filters'] : [];

        $faq = $this->faqModel::filters(new Filters($filters['faq_filters'], $this->availableFaqFilters))
            ->with([
                'contents' => function ($query) use ($contentFilters) {
                    $query->newQuery()->filters(new Filters($contentFilters, $this->availableFaqContentFilters));
                },
                'author', 'category'
            ])->paginate(config('emitters.paginate_num'))->appends($filters);

        return $faq;
    }

    /**
     * @return array
     */
    public function countFaq() {
        $total     = $this->faqModel::query()->count();
        $drafts    = $this->faqModel::query()->where('is_draft', 1)->count();
        $published = $this->faqModel::query()->where('is_draft', 0)->count();
        return [
            'total'     => $total,
            'drafts'    => $drafts,
            'published' => $published
        ];
    }

    /**
     * @param $faqID
     * @param null $lang
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function getFaqByID($faqID, $lang = null) {
        $faq = $this->faqModel::query()->
        with(['contents' => function ($query) use ($lang) {
            if (isset($lang)) $query->where('language', $lang);
        }])->get();
        return $faq;
    }

    /**
     * @param $categoryID
     * @param null $lang
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getFaqByCategory($categoryID, $lang = null) {
        $faq = $this->faqModel::query()
            ->where('category_id', $categoryID)->with(['contents' => function ($query) use ($lang) {
                if (isset($lang)) $query->where('language', $lang);
            }])->get();
        return $faq;
    }

    /**
     * @param $lang
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getFaqGroupedCategory($lang) {
        $faq = $this->faqCategoryModel::query()
            ->with(['faq' => function($query){
                $query->orderBy('sort', 'asc')->where('is_draft', false);
            }, 'faq.contents' => function ($query) use ($lang) {
                if (isset($lang)) $query->where('language', $lang);
            }])
            ->get();

        $faq->transform(function ($value) {
            $value->faq->transform(function ($row) {
                if($row->contents->first() == null) {
                    $row->load(['contents' => function ($query) {
                        $query->where('language', LaravelGettext::getLocaleLanguage());
                    }]);
                }
                return $row;
            });
            return $value;
        });

        return $faq;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getDraftFaq() {
        return $this->faqModel::query()->where('is_draft', true)->with('contents')->get();
    }

    /**
     * @param array|null $faqData
     * @param array|null $contentData
     * @return bool
     */
    public function update(array $faqData = null, array $contentData = null) {
        if (isset($faqData)) {
            foreach ($faqData as $datum) {
                $this->updateFaq($datum);
            }
        }
        if (isset($contentData)) {
            foreach ($contentData as $contentDatum) {
                $this->updateContent($contentDatum);
            }
        }
        return true;
    }

    /**
     * @param array|null $faqData
     * @return bool|null
     */
    public function updateFaq(array $faqData = null) {
        if (!isset($faqData)) return null;
        return $this->faqModel::query()->where('id', $faqData['id'])->update($faqData);
    }


    /**
     * @param array|null $contentData
     * @return null
     */
    public function updateContent(array $contentData = null) {
        if (!isset($contentData)) return null;
        if (array_key_exists('id', $contentData)) {
            $this->faqContentModel::query()->where('id', $contentData['id'])->update($contentData);
        } elseif (array_key_exists('faq_id', $contentData)) {
            $this->faqContentModel::query()->create($contentData);
        }
        return null;
    }

    /**
     * @param Faqs_Category $faqCategory
     * @param array $categoryData
     * @return int
     */
    public function updateCategory(Faqs_Category $faqCategory, array $categoryData) {
        return $faqCategory->update($categoryData);
    }

    /**
     * @param $categoryId
     * @return mixed
     */
    public function deleteCategory($categoryId) {
        if(!$this->faqModel::query()->where('category_id', $categoryId)->count() > 0) {
            return $this->faqCategoryModel::query()->where('id', $categoryId)->delete();
        } else {
            return false;
        }
    }

    /**
     * @param array $faqIDs
     * @param bool  $withContent
     * @return int
     */
    public function deleteFaq(array $faqIDs, $withContent = true) {
        $faq = $this->faqModel::query()->whereIn('id', $faqIDs)->delete();
        if ($withContent) {
            $this->faqContentModel::query()->whereIn('faq_id', $faqIDs)->delete();
        }
        return $faq;
    }

    /**
     * @param $contentIDs
     * @return int
     */
    public function deleteContent(array $contentIDs) {
        return $this->faqContentModel::query()->whereIn('id', $contentIDs)->delete();
    }

    /**
     * @param array $toDraftIDs
     * @return int
     */
    public function toDraft(array $toDraftIDs) {
        return $this->faqModel::query()->whereIn('id', $toDraftIDs)->update(['is_draft' => 1]);
    }

    /**
     * @param array $draftIDs
     * @return int
     */
    public function publishDrafts(array $draftIDs) {
        return $this->faqModel::query()->whereIn('id', $draftIDs)->update(['is_draft' => 0]);
    }

    /**
     * @param array|null $data
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null
     */
    protected function storeFaq(array $data = null) {
        if (!isset($data)) return null;
        return $this->faqModel::query()->create($data);
    }

    /**
     * @param Faqs $faq
     * @param array|null $data
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null
     */
    protected function storeContent(Faqs $faq, array $data = null) {
        if (!isset($data)) return null;
        return $faq->contents()->create($data);
    }
}