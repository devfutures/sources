<?php

namespace Emitters\Faq\Models;

use App\User;
use Emitters\Filters\Traits\AddFilters;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Faqs extends Model {
    use SoftDeletes, AddFilters;

    protected $fillable = [
        'category_id',
        'user_id',
        'sort',
        'is_draft'
    ];

    protected static function boot() {
        parent::boot();

        self::creating(function ($model) {
            $sortValue = $model::max('sort') + 1;
            $model->sort = $sortValue;
        });
    }

    public function category() {
        return $this->belongsTo(Faqs_Category::class);
    }

    public function contents() {
        return $this->hasMany(Faqs_Content::class, 'faq_id');
    }

    public function author() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
