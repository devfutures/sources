<?php

namespace Emitters\Faq\Models;

use Emitters\Filters\Traits\AddFilters;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Faqs_Category extends Model {
    use SoftDeletes, AddFilters;

    protected $fillable = [
        'category'
    ];

    protected $casts = [
        'category' => 'array'
    ];

    public function faq() {
        return $this->hasMany(Faqs::class, 'category_id');
    }
}
