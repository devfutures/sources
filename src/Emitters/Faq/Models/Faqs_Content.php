<?php

namespace Emitters\Faq\Models;

use Emitters\Filters\Traits\AddFilters;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Faqs_Content extends Model {
    use SoftDeletes, AddFilters;

    protected $fillable = [
        'faq_id',
        'language',
        'question',
        'answer'
    ];
}
