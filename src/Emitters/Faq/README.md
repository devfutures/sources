---
### FAQ
#### Извлечение FaqManager
##### Получение через IoC
```php
$faqManager = resolve('librair.faq');
```
##### Использование в контроллере с помощью type-hinting
```php
use App\LibrariesAir\Faq\FaqManager;
...
public function index(FaqManager $faqManager) {
    ...
}
```
#### Методы FaqManager

##### Создание FAQ Category
```php
$faqCategory = ['category' => 'IDK'];
$faqManager->createFaqCategory($faqCategoryData);
```

##### Создание FAQ
```php
$faqManager->createFaqWithContent($faqData, $contentData);
```
* $faqData - массив
```php
$faqData = [
    'category_id' => 1,
    'is_draft' => false // true что бы создать черновик
    'sort'     => 1
];
```
* $contentData - массив
```php
$contentData = [
    'language' => 'en',
    'question' => 'Abc?' 
    'answer'   => 'Cba.'
];
```
* return type - Faq

##### Получение всего FAQ для админ панели.
```php
//TODO
```

##### Получение FAQ по языку, без черновиков.
```php
$faqManager->getFaq($lang, $sort);
```
* $lang - язык FAQ`a
* $sort - отдавать отсортированый FAQ по полю sort
* return type - Collection

##### Получение FAQ по ID
```php
$faqManager->getFaqByID($faqid, $lang);
```
* $lang - язык FAQ (default - null)
* return type - Faq

##### Получение FAQ по категории
```php
$faqManager->getFaqByCategory($categoryID, $lang);
```
* $categoryID - ID категории
* $lang - язык FAQ (default - null)
* return type - Faq

##### Получение всех черновиков
```php
$faqManager->getDraftFaq($faqID, $lang);
```
* $lang - язык FAQ (default - null)
* return type - Collection

##### Изменение FAQ
```php
$faqManager->updateFaq($faq, $faqData);
```
* return type - boolean

##### Изменение FAQ content
```php
$faqManager->updateContent($faq, $contentData);
```
* return type - boolean

##### Изменение FAQ category
```php
$categoryData = ['category' => 'IDK'];
$faqManager->updateCateogory($faq, $categoryData);
```
* return type - boolean

##### Удаление FAQ
```php
$faqManager->deleteFaq($faq, $withContent);
```
* $faq - Faq
* $withContent - boolean, при true удаляет контент FAQ
* return type - int

##### Удаление FAQ content
```php
$faqManager->deleteContent($contentID);
```
* return type - int
