<?php


namespace Emitters\Faq\Requests;


use Illuminate\Foundation\Http\FormRequest;

class CreateCategoryRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            '*'                     => 'required|array',
            '*.category'            => 'required|array',
            '*.category.*.id'       => 'sometimes|id|exists:faqs__categories,id',
            '*.category.*.language' => 'required|string|in:' . implode(',', config('laravel-gettext.supported-locales')),
            '*.category.*.name'     => 'required|string',
        ];
    }
}