<?php


namespace Emitters\Faq\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateFaqRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'faq'             => 'required|array',
            'faq.category_id' => 'required|integer|exists:faqs__categories,id',
            'faq.is_draft'    => 'required|boolean',

            'content'            => 'required|array',
            'content.*.language' => 'required|string|in:' . implode(',', config('laravel-gettext.supported-locales')),
            'content.*.question' => 'required|string',
            'content.*.answer'   => 'required|string'
        ];
    }
}