<?php

namespace Emitters\Faq\Requests;


use Illuminate\Foundation\Http\FormRequest;

class FaqUpdateRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'faq'               => 'required|array',
            'faq.*.id'          => 'required|integer|exists:faqs,id',
            'faq.*.category_id' => 'sometimes|integer|exists:faqs__categories,id',
            'faq.*.is_draft'    => 'sometimes|boolean',
            'faq.*.sort'        => 'sometimes|integer',

            'content'            => 'array',
            'content.*.id'       => 'sometimes|integer|exists:faqs__contents,id',
            'content.*.language' => 'sometimes|string|in:' . implode(',', config('laravel-gettext.supported-locales')),
            'content.*.question' => 'sometimes|string',
            'content.*.answer'   => 'sometimes|string'
        ];
    }
}