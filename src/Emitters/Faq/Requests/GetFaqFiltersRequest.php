<?php


namespace Emitters\Faq\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GetFaqFiltersRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'faq_filters'                     => 'array',
            'faq_filters.whereIn'             => 'array',
            'faq_filters.whereIn.*.field'     => 'string|in:is_draft,category_id',
            'faq_filters.whereIn.*.value'     => 'array',
            'faq_filters.whereIn.*.value.*'   => 'integer',
            'faq.filters.orderBy'             => 'array',
            'faq.filters.orderBy.*.by'        => 'string',
            'faq.filters.orderBy.*.direction' => 'string|in:asc,desc',

            'content_filters'                   => 'array',
            'content_filters.whereIn'           => 'array',
            'content_filters.whereIn.*.field'   => 'string|in:language',
            'content_filters.whereIn.*.value'   => 'array',
            'content_filters.whereIn.*.value.*' => 'string|in:' . implode(',', config('laravel-gettext.supported-locales'))
        ];
    }
}