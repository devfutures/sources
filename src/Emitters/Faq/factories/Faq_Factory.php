<?php

use Faker\Generator as Faker;

$factory->define(\Emitters\Faq\Models\Faqs_Category::class, function (Faker $faker) {
    return [
        'category' => [
            [
                'language' => 'en',
                'name'     => $faker->colorName
            ],
            [
                'language' => 'ru',
                'name'     => $faker->colorName
            ]
        ]
    ];
});

$factory->define(\Emitters\Faq\Models\Faqs::class, function (Faker $faker) {
    return [
        'user_id'     => function () {
            return factory(\App\User::class)->create()->id;
        },
        'category_id' => function () {
            return factory(\Emitters\Faq\Models\Faqs_Category::class)->create()->id;
        },
        'is_draft'    => false
    ];
});

$factory->define(\Emitters\Faq\Models\Faqs_Content::class, function (Faker $faker) {
    return [
        'faq_id'   => function () {
            return factory(\Emitters\Faq\Models\Faqs::class)->create()->id;
        },
        'language' => 'en',
        'question' => $faker->realText(),
        'answer'   => $faker->realText()
    ];
});