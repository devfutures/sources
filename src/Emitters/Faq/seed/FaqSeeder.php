<?php

namespace Emitters\Faq\seed;

use Emitters\Faq\Models\Faqs_Content;
use Illuminate\Database\Seeder;

class FaqSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        factory(Faqs_Content::class, 20)->create();
    }
}
