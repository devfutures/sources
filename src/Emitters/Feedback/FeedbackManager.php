<?php


namespace Emitters\Feedback;


use Emitters\Feedback\Notifications\FeedbackResponse;
use Emitters\Filters\Filters;
use Emitters\Abstracts\Feedback\FeedbackManagerAbstract;
use Illuminate\Support\Facades\Mail;


class FeedbackManager extends FeedbackManagerAbstract {

    /**
     * @param $value
     * @param string $field
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null|object
     */
    public function getOneBy($value, string $field) {
        return $this->model::query()->where($field, $value)->first();
    }

    /**
     * @param array $filters
     * @param bool $by_paginate
     * @return mixed
     */
    public function getAll(array $filters = [], $by_paginate = true) {
        $feedback = $this->model::filters(new Filters($filters, $this->availableFilters))->with(['answered_user'])->orderBy('id', 'desc');
        if($by_paginate) {
            $feedback = $feedback->paginate(config('emitters.paginate_num'))->appends($filters);
        } else {
            $feedback = $feedback->get();
        }
        return $feedback;
    }

    /**
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function add(array $data) {
        return $this->model::query()->create($data);
    }

    /**
     * @param array $ids
     * @return int
     */
    public function delete(array $ids) {
        return $this->model::destroy($ids);
    }

    /**
     * @return array
     */
    public function getCountingMessages() {
        $returned     = [
            'all'          => 0,
            'new_messages' => 0,
            'answered'     => 0,
            'read'         => 0,
        ];
        $all          = $this->model::query()->count();
        $new_messages = $this->model::query()->where('status', 0)->count();
        $answered     = $this->model::query()->where('status', 1)->count();
        $read         = $this->model::query()->where('status', 2)->count();

        $returned['all']          = $all;
        $returned['new_messages'] = $new_messages;
        $returned['answered']     = $answered;
        $returned['read']         = $read;
        return $returned;
    }

    public function changeStatus(array $ids, $status) {
        $res = $this->model::query()->whereIn('id', $ids)->get();
        if($res) {
            foreach($res as $row) {
                $row->status = $status;
                $row->save();
            }
        }
        return true;
    }

    /**
     * @param $id
     * @param $user_id
     * @param $answer
     * @return mixed
     */
    public function preAnswer($id, $user_id, $answer) {
        $message                   = $this->model::query()->where('id', $id)->first();
        $message->message_answer   = $answer;
        $message->answered_user_id = $user_id;
        $message->status           = 1;
        $message->save();
        return $this->sendAnswer($message, $answer);
    }

    /**
     * @param $id
     * @param null $answer
     * @return mixed
     */
    public function sendAnswer($message, $answer = null) {

        return $message->notify(new FeedbackResponse($answer));

//        return Mail::to($message)->send(new FeedbackResponse($message, $answer));
    }
}