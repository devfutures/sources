<?php

namespace Emitters\Feedback\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FeedbackResponse extends Mailable {
    use Queueable, SerializesModels;

    public $feedback_original;
    public $answer;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($feedback_original, $answer) {
        $this->feedback_original = $feedback_original;
        $this->answer  = $answer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->markdown('emails.feedback')->with([
            'feedback_original' => $this->feedback_original,
            'answer'  => $this->answer,
        ]);
    }
}
