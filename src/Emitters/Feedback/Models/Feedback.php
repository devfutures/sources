<?php

namespace Emitters\Feedback\Models;

use Emitters\Filters\Traits\AddFilters;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Feedback extends Model {
    use SoftDeletes, AddFilters, Notifiable;

    protected $fillable = [
        'email',
        'name',
        'message',
        'status',
        'user_id',
        'answered_user_id',
        'message_answer'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function answered_user() {
        return $this->hasOne('App\User', 'id', 'answered_user_id');
    }
}
