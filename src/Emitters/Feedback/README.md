---
### Feedback
#### Извлечение FeedbackManager
##### Получение через IoC
```php
$feedbackManager = resolve('librair.feedback');
```
##### Использование в контроллере с помощью type-hinting
```php
use App\LibrariesAir\Feedback\FeedbackManager;
...
public function index(FeedbackManager $feedbackManager) {
    ...
}
```
#### Методы FeedbackManager

##### Создание Feedback
```php
$feedbackData = ['email' => 'test@test.com', 'name' => 'Some user', 'message' = 'abc'];
$feedbackManager->add($feedbackData);
```

##### Получение feedback по значению одного из свойств
```php
$attName = 'name';
$value = 'test';
$feedbackManager->getOneBy($value, $attName);
```

##### Получение всех feedback
```php
$filters = [...];
// false if you want to get feedback without pagination
$withPagination = false;
$feedbackManager->getAll($filters, $withPagination);
```

##### Удаление feedback
```php
$idsToDelete = [1,2];
$feedbackManager->delete($idsToDelete);
```