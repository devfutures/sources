<?php


namespace Emitters\Feedback\Requests;


use Illuminate\Foundation\Http\FormRequest;

class DeleteFeedbackRequest extends FormRequest {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'required|exists:feedback,id'
        ];
    }
}