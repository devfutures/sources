<?php


namespace Emitters\Feedback\Requests;


use Illuminate\Foundation\Http\FormRequest;

class FeedbackFilterRequest extends FormRequest {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'orderBy'             => 'array',
            'orderBy.*.by'        => 'string',
            'orderBy.*.direction' => 'string|in:asc,desc',

            'where'         => 'array',
            'where.*.field' => 'string|in:name,email,status',
            'where.*.value' => 'integer',

            'dateBetween'                    => 'array',
            'dateBetween.*.from_date_column' => 'string|in:created_at',
            'dateBetween.*.to_date_column'   => 'string|in:created_at',
            'dateBetween.*.from'             => 'date_format: "d.m.Y"',
            'dateBetween.*.to'               => 'date_format: "d.m.Y"',
        ];
    }
}