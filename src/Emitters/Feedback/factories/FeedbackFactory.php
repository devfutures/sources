<?php

use Faker\Generator as Faker;


$factory->define(\Emitters\Feedback\Models\Feedback::class, function (Faker $faker) {
    return [
        'email'   => $faker->email,
        'name'    => $faker->name,
        'message' => $faker->realText(300),
    ];
});