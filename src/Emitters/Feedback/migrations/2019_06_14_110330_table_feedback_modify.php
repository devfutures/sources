<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableFeedbackModify extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('feedback', function (Blueprint $table) {
            $table->tinyInteger('status')->after('id')->default(0)->index();
            $table->integer('user_id')->default(0)->index()->after('id');
            // 0 - new message, 1 - answered, 2 - read
            $table->integer('answered_user_id')->after('message')->default(0)->index();
            $table->text('message_answer')->after('message')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('feedback', function (Blueprint $table) {
            $table->dropColumn(['user_id', 'status', 'message_answer', 'answered_user_id']);
        });
    }
}
