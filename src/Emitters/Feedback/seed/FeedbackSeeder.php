<?php


namespace Emitters\Feedback\seed;


use Emitters\Feedback\Models\Feedback;
use Illuminate\Database\Seeder;

class FeedbackSeeder extends Seeder {

    public function run() {
        factory(Feedback::class, 30)->create();
    }
}