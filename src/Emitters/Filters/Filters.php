<?php


namespace Emitters\Filters;


/**
 * @property array request
 */
class Filters {
    protected $builder, $request;
    protected $avalibleFiltered = [];
    /**
     * Filters constructor.
     *
     * @param array $request
     */
    function __construct (array $request, array $avalibleFiltered) {
        $this->request = $request;
        $this->avalibleFiltered = $avalibleFiltered;
    }

    /**
     * @param $builder
     *
     * @return mixed
     */
    public function apply($builder){
        $this->builder = $builder;
        foreach($this->avalibleFiltered as $filter){

            $decorator = $this->createFileDecorator($filter, __NAMESPACE__);

            if(!array_key_exists($filter, $this->request)) continue;
            if(!$this->classExits($decorator)) continue;
            if(is_array($this->request[$filter])){
                foreach ($this->request[$filter] as $reqFilter) {
                    $this->builder = (new $decorator)->get($this->builder, $reqFilter);
                }
            }else{
                $this->builder = (new $decorator)->get($this->builder, $this->request[$filter]);
            }
        }
        return $this->builder;
    }

    private function createFileDecorator($name, $filtersNamespace) {
        return $filtersNamespace . '\\Methods\\' .
            str_replace(' ', '', ucwords(
                str_replace('_', ' ', $name))) .
                'Method';
    }

    private function classExits ($nameclass) {
        return class_exists($nameclass);
    }
}