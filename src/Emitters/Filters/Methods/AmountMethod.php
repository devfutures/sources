<?php


namespace Emitters\Filters\Methods;


class AmountMethod {
    public function get($builder, $data){
        if (isset($data['min']) && isset($data['max'])) {

            return $builder->where($data['fields']['min'], '>=', $data['min'])->where($data['fields']['max'], '<=', $data['max']);

        } elseif (isset($data['min'])) {
            return $builder->where($data['fields']['min'], '>=', $data['min']);

        } elseif (isset($data['max'])) {
            return $builder->where($data['fields']['max'], '<=', $data['max']);
        }
        return $builder;
    }
}