<?php


namespace Emitters\Filters\Methods;


use Illuminate\Support\Carbon;

class CreatedAtBetweenMethod {
    public function get($builder, $value){
        if (isset($value['from']) && isset($value['to'])) {

            return $builder->whereBetween('created_at', [Carbon::createFromFormat('!d.m.Y', $value['from']), Carbon::createFromFormat('!d.m.Y', $value['to'])->addDay(1)]);

        } elseif (isset($value['from'])) {
            return $builder->where('created_at', '>=', Carbon::createFromFormat('!d.m.Y', $value['from']));

        } elseif (isset($value['to'])) {
            return $builder->where('created_at', '<=', Carbon::createFromFormat('!d.m.Y', $value['to'])->addDay(1));
        }
        return $builder;
    }
}