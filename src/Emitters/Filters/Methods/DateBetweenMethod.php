<?php


namespace Emitters\Filters\Methods;


use Illuminate\Support\Carbon;

class DateBetweenMethod {
    public function get($builder, $data){
        if (isset($data['from']) && isset($data['to'])) {

            return $builder->where($data['from_date_column'], '>=', Carbon::createFromFormat('!d.m.Y', $data['from']))->where($data['to_date_column'], '<=', Carbon::createFromFormat('!d.m.Y', $data['to'])->addDay(1));

        } elseif (isset($data['from'])) {
            return $builder->where($data['from_date_column'], '>=', Carbon::createFromFormat('!d.m.Y', $data['from']));

        } elseif (isset($data['to'])) {
            return $builder->where($data['to_date_column'], '<=', Carbon::createFromFormat('!d.m.Y', $data['to'])->addDay(1));
        }
        return $builder;
    }
}