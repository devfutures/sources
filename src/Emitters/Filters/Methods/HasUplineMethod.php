<?php


namespace Emitters\Filters\Methods;
use App\User;

class HasUplineMethod {
    public function get($builder, $identy){
        if($identy == '1'){
            return $builder->where('upline_id', '<>' , 0);
        }else{
            return $builder->where('upline_id', 0);
        }
    }
}