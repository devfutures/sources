<?php

namespace Emitters\Filters\Methods;


class OrderByMethod {
    public function get($builder, $value){
        return $builder->orderBy($value['by'], $value['direction']);
    }
}