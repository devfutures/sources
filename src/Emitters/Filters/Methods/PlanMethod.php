<?php


namespace Emitters\Filters\Methods;
use App\User;

class PlanMethod {
    public function get($builder, $identy){
        return $builder->where('plan_id', $identy);
    }
}