<?php


namespace Emitters\Filters\Methods;
use App\User;

class SearchPartnerMethod {
    public function get($builder, $identy){
        if($identy != null){
            $user = User::where(function ($query) use ($identy){
                $query->orWhere('name', $identy);
                $query->orWhere('email', $identy);
                $query->orWhere('login', $identy);
                $query->orWhere('aff_ref', $identy);
            })->take(20)->orderBy('id', 'asc')->get();

            if(count($user) > 0){
                return $builder->whereIn('child_id', $user->pluck('id'));
            }else{
                return $builder->where('id', 0);
            }
        }

        return $builder;
    }
}