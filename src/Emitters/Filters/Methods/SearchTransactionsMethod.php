<?php


namespace Emitters\Filters\Methods;


use Carbon\Carbon;

class SearchTransactionsMethod {
    public function get($builder, $value) {
        return $builder->where(function ($query) use ($value) {
            $query->orWhereHas('payment_system', function ($query) use ($value) {
                $query->where('title', 'LIKE', "{$value}");
            });
            $query->orWhere('status', 'LIKE', "%{$value}%");
            $query->orWhere(function($query) use ($value) {
                try {
                    $value = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
                    $query->where('created_at', 'LIKE', "%{$value}%");
                } catch (\InvalidArgumentException $exception) {}
            });
            $query->orWhere('operation', 'LIKE', "%{$value}%");
            $query->orWhere('transaction', 'LIKE', "%{$value}%");
            $query->orWhere('type', 'LIKE', "%{$value}%");
            return $query;
        });
    }
}