<?php

namespace Emitters\Filters\Methods;


class SearchUserKeywordMethod {
    public function get($builder, $data) {
        return $builder->where(function ($query) use ($data){
            $query->orWhere('name', 'LIKE', "%{$data}%");
            $query->orWhere('email', 'LIKE', "%{$data}%");
            $query->orWhere('login', 'LIKE', "%{$data}%");
            $query->orWhere('aff_ref', 'LIKE', "%{$data}%");
            $query->orWhere('country', 'LIKE', "%{$data}%");
            $query->orWhere('city', 'LIKE', "%{$data}%");
            $query->orWhere('user_note', 'LIKE', "%{$data}%");
            $query->orWhere('block_message', 'LIKE', "%{$data}%");
            return $query;
        });
    }
}