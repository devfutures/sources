<?php


namespace Emitters\Filters\Methods;


class SortCreatedAtMethod {
    public function get($builder, $value) {
        return $builder->orderBy('created_at', $value);
    }
}