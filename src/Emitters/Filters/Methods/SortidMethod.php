<?php


namespace Emitters\Filters\Methods;
use App\User;

class SortidMethod {
    public function get($builder, $identy){
        return $builder->orderBy('id', $identy);
    }
}