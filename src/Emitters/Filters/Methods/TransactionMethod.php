<?php

namespace Emitters\Filters\Methods;


class TransactionMethod {
    public function get($builder, $data) {
        return $builder->where('transaction', $data);
    }
}