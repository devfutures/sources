<?php


namespace Emitters\Filters\Methods;
use App\User;

class UserMethod {
    public function get($builder, $identy){
        $user = User::where(function ($query) use ($identy){
            $query->where('id', $identy);
            $query->orWhere('email', $identy);
        })->firstOrFail();

        return $builder->where('user_id', $user->id);
    }
}