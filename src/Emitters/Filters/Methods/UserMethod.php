<?php


namespace Emitters\Filters\Methods;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
class UserMethod {
    public function get($builder, $identy){
        try{
            $user = User::where(function ($query) use ($identy){
                $query->where('id', $identy);
                $query->orWhere('email', $identy);
            })->firstOrFail();
            return $builder->where('user_id', $user->id);
        }catch (ModelNotFoundException $e){
            return $builder->where('user_id', '-1');
        }
    }
}