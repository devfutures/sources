<?php

namespace Emitters\Filters\Methods;


class WhereBetweenMethod {
    public function get($builder, $data){
        return $builder->whereBetween($data['field'], [$data['from'], $data['to']]);
    }
}