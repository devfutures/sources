<?php


namespace Emitters\Filters\Methods;


class WhereInMethod {
    public function get($builder, $data) {
        return $builder->whereIn($data['field'], $data['value']);
    }
}