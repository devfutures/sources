<?php

namespace Emitters\Filters\Methods;


class WhereMethod {
    public function get($builder, $data) {
        return $builder->where($data['field'], $data['value']);
    }
}