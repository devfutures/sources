<?php


namespace Emitters\Filters\Methods;


class WhereNotNullMethod {
    public function get($builder, $data){
        return $builder->whereNotNull($data['field']);
    }
}