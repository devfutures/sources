<?php


namespace Emitters\Filters\Traits;


use Emitters\Filters\Filters;

trait AddFilters {
    public function scopeFilters($query, Filters $filter){
        return $filter->apply($query);
    }
}