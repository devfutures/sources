<?php


namespace Emitters\Google2FA\Exceptions;


use Exception;
use Throwable;

class UserGoogleAuthInvalidSecret extends Exception {

    public function __construct(string $message = "", int $code = 0, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }

    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}