<?php


namespace Emitters\Google2FA;


use App\User;
use Emitters\Abstracts\Google2FA\Google2FaAbstract;

use Emitters\Google2FA\Exceptions\UserGoogleAuthInvalidCredentials;
use Emitters\Google2FA\Exceptions\UserGoogleAuthValidationError;
use Emitters\Google2FA\Exceptions\UserGoogleAuthInvalidSecret;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Google2FAManager extends Google2FaAbstract {

    /**
     * Get's array of features that can be protected by google 2 fa
     *
     * @return array
     */
    public function getSecuredFeatures(): array {
        return config('emitters.google2fa_features');
    }

    /**
     * Gets array that contain feature as a key and $status as a value.
     *
     * @param bool $status
     * @return array
     */
    public function getSecuredFeaturesWithDefaultStatus(bool $status) {
        $defSettings = [];
        foreach ($this->getSecuredFeatures() as $key) {
            $defSettings[$key] = $status;
        }
        return $defSettings;
    }

    /**
     * Get's data for adding auth to Google Authenticator application
     *
     * @param User $user
     * @return array
     */
    public function getDataForSetUpGoogle2FA(User $user) {
        $googleSecret = $this->google2FA->generateSecretKey();

        $imageDataUri = $this->google2FA->getQRCodeInline(
            $user->email,
            config('app.name'),
            $googleSecret
        );

        return [
            'image'            => $imageDataUri,
            'google2fa_secret' => $googleSecret,
        ];
    }

    /**
     * Save auth instance for user and check that he added auth to his app.
     *
     * @param User  $user
     * @param array $data
     * @throws UserGoogleAuthValidationError | UserGoogleAuthInvalidSecret
     */
    public function saveGoogle2FADataToUser(User $user, array $data) {
        $this->checkSecret($data['google_secret'], $data['google2fa_secret']);
        $this->userDataManager->addGoogle2Fa($user->id, [
            'g2fa_status'   => 1,
            'g2fa_secret'   => $data['google2fa_secret'],
            'g2fa_settings' => $this->getSecuredFeaturesWithDefaultStatus(false),
        ]);
    }

    /**
     * Change secured features
     *
     * @param User  $user
     * @param array $data
     * @return void
     * @throws UserGoogleAuthValidationError
     */
    public function updateGoogle2FASettings(User $user, array $data) {
        foreach ($data as $feature => $status) {
            if (!in_array($feature, $this->getSecuredFeatures())) {
                throw new UserGoogleAuthValidationError('Wrong settings.');
            }
        }
        $this->userDataManager->updateGoogle2FaSettings($user->id, $data);
    }

    /**
     * Set user google auth enables as false
     *
     * @param $userID
     * @return void
     */
    public function disableGoogle2FA($userID) {
        $this->userDataManager->disableGoogle2Fa($userID);
    }

    /**
     * Get's array of settings user 2fa.
     *
     * @param User $user
     * @return mixed
     */
    public function getGoogle2FaSettings(User $user) {
        $settings = $user->g2fa_settings;
        return $settings;
    }

    /**
     * Returns status of feature.
     *
     * @param User   $user
     * @param string $feature
     * @return bool
     * @throws UserGoogleAuthValidationError
     */
    public function checkSecuredFeatureStatus(User $user, string $feature) {
        if (in_array($feature, $this->getSecuredFeatures())) {

            $securedFeatures = $this->getGoogle2FaSettings($user);

            $featureStatus   = $securedFeatures[$feature] === true ? true : false;
            return $featureStatus;

        } else {
            throw new UserGoogleAuthValidationError('Given feature is wrong.');
        }
    }

    /**
     * @param string $email
     * @return mixed
     * @throws UserGoogleAuthInvalidCredentials
     */
    public function getUserByEmail(string $email) {
        try {
            $user = $this->userDataManager->getUserByEmail($email);
        } catch (ModelNotFoundException $exception) {
            throw new UserGoogleAuthInvalidCredentials();
        }
        return $user;
    }

    /**
     * Get's user google key
     *
     * @param User $user
     * @return mixed
     */
    public function getUserSecret(User $user) {
        $secret = $user->g2fa_secret;
        return $secret;
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function checkAuthStatus(User $user) {
        $status = $user->g2fa_status;
        return $status == 1 ? true : false;
    }

    /**
     * Check one-time google code.
     *
     * @param User   $user
     * @param string $googleSecret
     * @throws UserGoogleAuthInvalidSecret
     * @throws UserGoogleAuthValidationError
     */
    public function validateLogin(User $user, string $googleSecret) {
        if (config('emitters.google2fa_method') == 'verifyKeyNever') {
            $this->checkSecretNever($user, $googleSecret);
        } else {
            $this->checkSecret($googleSecret, $user->g2fa_secret);
        }
    }

    /**
     * Check user one-time code.
     *
     * @param string $oneTimePassword
     * @param string $google2faSecret
     * @throws UserGoogleAuthInvalidSecret
     * @throws UserGoogleAuthValidationError
     */
    public function checkSecret(string $oneTimePassword = null, string $google2faSecret = null) {
        if (!isset($oneTimePassword) && !isset($google2faSecret)) {
            throw new UserGoogleAuthValidationError('Validation error.', 1);
        }

        $oneTimePassword = preg_replace('/\s+/', '', $oneTimePassword);

        if (!$this->google2FA->verifyKey($google2faSecret, $oneTimePassword)) {
            throw new UserGoogleAuthInvalidSecret('Invalid secret.', 2);
        }
    }

    /**
     * @param User $user
     * @param string|null $oneTimePassword
     * @throws UserGoogleAuthInvalidSecret
     * @throws UserGoogleAuthValidationError
     */
    public function checkSecretNever(User $user, string $oneTimePassword = null) {
        if (!isset($oneTimePassword)) throw new UserGoogleAuthValidationError('Validation error.', 1);
        $oneTimePassword = preg_replace('/\s+/', '', $oneTimePassword);

        $timestamp = $this->google2FA->verifyKeyNewer($user->g2fa_secret, $oneTimePassword, $user->g2fa_ts);

        if ($timestamp !== false) {
            $user->update(['g2fa_ts' => $timestamp]);
        } else {
            throw new UserGoogleAuthInvalidSecret('Invalid secret.', 2);
        }
    }

}