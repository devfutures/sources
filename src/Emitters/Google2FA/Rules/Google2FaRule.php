<?php

namespace Emitters\Google2FA\Rules;

use App\User;
use Illuminate\Contracts\Validation\Rule;

use Emitters\Google2FA\Google2FAManager;
use Emitters\Google2FA\Exceptions\UserGoogleAuthValidationError;
use Emitters\Google2FA\Exceptions\UserGoogleAuthInvalidSecret;


class Google2FaRule implements Rule {
    /** @var Google2FAManager */
    protected $google2FaManager;
    /** @var \App\User */
    protected $user;
    protected $errorMessage;

    /**
     * Create a new rule instance.
     *
     * @param User $user
     * @param Google2FAManager $manager
     */
    public function __construct(User $user, Google2FAManager $manager) {
        $this->user = $user;
        $this->google2FaManager = $manager;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value) {
        try {
            $this->google2FaManager->validateLogin($this->user, $value);
        } catch (UserGoogleAuthValidationError | UserGoogleAuthInvalidSecret $exception) {
            $this->errorMessage = $exception->getMessage();
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return array
     */
    public function message() {
        return $this->errorMessage;
    }
}
