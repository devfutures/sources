<?php


namespace Emitters\Http\Controllers;

use App\Http\Controllers\Controller;
use Emitters\AdminControl\Requests\SetRoleRequest;
use Emitters\AdminControl\AdminRoles;
use Emitters\AdminControl\MenuAccess;
use Emitters\AdminControl\Requests\RolesRequest;
use App\LibrariesAir\UsersData\UsersDataManager;
use Illuminate\Http\Request;

class AdminControlController extends Controller {

    /**
     * @param RolesRequest $request
     * @param AdminRoles $manager
     * @param MenuAccess $menuAccess
     * @return \Illuminate\Http\JsonResponse
     */
    public function addRole(RolesRequest $request, AdminRoles $manager, MenuAccess $menuAccess) {
        $manager->createRole($request->validated());
        $roles = $manager->getRoles();
        $sections = $menuAccess->getSections();
        return response()->json([
            'status' => 'success',
            'code'   => 0,
            'roles'    => $roles,
            'sections' => $sections
        ]);
    }

    /**
     * @param MenuAccess $menuAccess
     * @param AdminRoles $manager
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRoles(MenuAccess $menuAccess, AdminRoles $manager) {
        $roles    = $manager->getRoles();
        $sections = $menuAccess->getSections();
        return response()->json([
            'status'   => 'success',
            'code'     => 0,
            'roles'    => $roles,
            'sections' => $sections
        ]);
    }

    /**
     * @param SetRoleRequest   $request
     * @param UsersDataManager $manager
     * @return \Illuminate\Http\JsonResponse
     */
    public function setRoleToUser(SetRoleRequest $request, UsersDataManager $manager) {
        $manager->setRoleToUser($request->validated()['user_id'], $request->validated()['role_id']);
        return response()->json([
            'status' => 'success',
            'code'   => 0,
        ]);
    }

    /**
     * @param Request $request
     * @param UsersDataManager $manager
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function detachUserRole(Request $request, UsersDataManager $manager) {
        $this->validate($request, [
            'user_id' => 'required|integer|exists:users,id'
        ]);
        $manager->detachUserRole($request->input('user_id'));
        return response()->json([
            'status' => 'success',
            'code'   => 0,
        ]);
    }

    /**
     * @param RolesRequest $request
     * @param $id
     * @param AdminRoles $manager
     * @param MenuAccess $menuAccess
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateRole(RolesRequest $request, $id, AdminRoles $manager, MenuAccess $menuAccess) {
        $manager->save_role($id, $request->validated());
        $roles    = $manager->getRoles();
        $sections = $menuAccess->getSections();
        return response()->json([
            'status'   => 'success',
            'code'     => 0,
            'roles'    => $roles,
            'sections' => $sections
        ]);
    }

    /**
     * @param $id
     * @param AdminRoles $manager
     * @param MenuAccess $menuAccess
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function deleteRole($id, AdminRoles $manager, MenuAccess $menuAccess) {
        try {
            $manager->deleteRole($id);
        } catch (\Exception $exception) {
            return response()->json([
                'status' => 'fail',
                'code'   => 1,
                'msg'    => $exception->getMessage()
            ], 422);
        }
        $roles    = $manager->getRoles();
        $sections = $menuAccess->getSections();
        return response()->json([
            'status'   => 'success',
            'code'     => 0,
            'roles'    => $roles,
            'sections' => $sections
        ]);
    }
}