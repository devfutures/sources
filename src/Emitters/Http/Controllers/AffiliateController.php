<?php

namespace Emitters\Http\Controllers;

use Emitters\Affiliate\AffiliateManager;
use Emitters\Affiliate\Exceptions\AffiliateLevelsException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;

class AffiliateController extends Controller {
    public function index(AffiliateManager $affiliateManager){
        $settings = $affiliateManager->getAffiliateSettings();
        $affiliate = $affiliateManager->getAffiliateLevels();
        return response()->json(compact('settings','affiliate'));
    }

    public function save(Request $request,AffiliateManager $affiliateManager){
        $this->validate($request, [
            'affiliates' => 'required|array',
            'settings'   => 'required|array'
        ]);
        try {
            $affiliateManager->saveAffiliateLevels($request->input('affiliates'));
            $affiliate = $affiliateManager->getAffiliateLevels();
            $affiliateManager->saveSettings($request->input('settings'));
            return response()->json([
                'status'    => 'success',
                'code'      => 0,
                'affiliate' => $affiliate
            ]);
        } catch(AffiliateLevelsException $e) {
            return response()->json(getErrorResponseData('affiliates',$e->getMessage()),422);
        }
    }
}