<?php

namespace Emitters\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Emitters\Google2FA\Exceptions\UserGoogleAuthInvalidCredentials;
use Emitters\Google2FA\Exceptions\UserGoogleAuthValidationError;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\LibrariesAir\Google2FA\Google2FAManager;
use Emitters\Google2FA\Rules\Google2FaRule;
use Symfony\Component\HttpFoundation\Cookie;

class LoginController extends Controller {
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /** @var Google2FAManager */
    protected $g2faManager;

    /**
     * Create a new controller instance.
     *
     * @param Google2FAManager $manager
     */
//    public function __construct(Google2FAManager $manager) {
    public function __construct() {
//        $this->g2faManager = $manager;
        $this->middleware('guest')->except('logout');
    }

    /**
     * @param Request $request
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(Request $request) {
        $request->validate([
            $this->username() => 'required|string',
            'password'        => 'required|string',
        ]);

        if (config('emitters.captcha_status')) $this->validateGoogleCaptcha($request);
        if (config('emitters.google2fa_status')) $this->validateGoogle2FA($request);
    }

    /**
     * @param Request $request
     */
    protected function validateGoogleCaptcha(Request $request) {
        $validationRules['g-recaptcha-response'] = ['required', 'captcha'];
        $request->validate($validationRules);
    }

    /**
     * @param Request $request
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateGoogle2FA(Request $request) {
//        try {
//            $user = $this->g2faManager->getUserByEmail($request->input('email'));
//            if ($this->g2faManager->checkAuthStatus($user) && $this->g2faManager->checkSecuredFeatureStatus($user, 'login')) {
//                $rule = ['g-2fa-secret' => ['required', 'string', new Google2FaRule($user, resolve('librair.google2fa'))]];
//                $request->validate($rule);
//            }
//        } catch (UserGoogleAuthInvalidCredentials | UserGoogleAuthValidationError $e) {
//            $this->sendFailedLoginResponse($request);
//        }
    }

    /**
     * @param Request $request
     * @param $user
     * @return \Illuminate\Http\JsonResponse
     */
    protected function authenticated(Request $request, $user) {
        $token = auth()->attempt([
            $this->username() => $request->input($this->username()),
            'password'        => $request->input('password')
        ]);
        $user  = auth()->user();

        $tokenData = respond_with_token($token);
        $response = response()->json(['user' => $user, 'token' => $tokenData]);

        if (isset($user->role_id)) {
            $user   = $user->load('role');
//            $cookie = Cookie::create('token', $token);
//            $response = response()->json(['user' => $user, 'token' => $tokenData])->cookie($cookie);
        }

        usersdata()->logActivity($request, $user);
        return $response;
    }

    public function logout(Request $request)
    {
        try{
            $this->guard()->logout();

            $request->session()->invalidate();
//            auth()->logout();
//            auth()->guard('web')->logout();
        }catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException | \Tymon\JWTAuth\Exceptions\JWTException $e){

        }

        return $this->loggedOut($request) ?: redirect('/');
    }

    /**
     * @param Request $request
     */
    protected function loggedOut(Request $request) {
//        auth()->logout(true);
        return response()->json(['status' => true]);
    }
}
