<?php

namespace Emitters\Http\Controllers\Auth;

use App\LibrariesAir\UsersData\UsersDataManager;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller {
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data){
        $validationRule = [
            'name'            => ['required','string','max:255'],
            'login'           => ['sometimes','required','string','max:255','unique:users'],
            'email'           => ['required','string','email','max:255','unique:users'],
            'password'        => ['required','string','min:6','confirmed'],
            'inviter_aff_ref' => ['sometimes','string','exists:users,aff_ref'],
        ];
        if(config('emitters.captcha_status')) {
            $validationRule['g-recaptcha-response'] = ['required','captcha'];
        }
        if(config('emitters_affiliate.aff_need_upline')) {
            $validationRule['inviter_aff_ref'] = ['required','string','exists:users,aff_ref'];
        }
        return Validator::make($data,$validationRule);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     *
     * @return \App\User
     */
    protected function create(array $data){
        $upline = null;
        if(array_key_exists('inviter_aff_ref', $data)) {
            $upline = usersdata()->getUserByAffRef($data['inviter_aff_ref']);
        }elseif(config('emitters_affiliate.aff_random_upline_sign_up') === true){
            $upline = usersdata()->getRandomUpline();
        }
        return User::create([
            'name'      => $data['name'],
            'upline_id' => ($upline) ? $upline->id : 0,
            'login'     => (array_key_exists('login',$data)) ? $data['login'] : null,
            'email'     => $data['email'],
            'aff_ref'   => usersdata()->createUserAffRef(),
            'password'  => Hash::make($data['password']),
        ]);
    }

    protected function registered(Request $request,$user){
        $token = auth()->tokenById($user->id);
        resolve('affiliate.structures')->createUserStructures($user);
        usersdata()->user_registered($request->all(),$user);
        usersdata()->logActivity($request, $user);
        return response()->json(['user' => $user,'token' => respond_with_token($token)]);
    }
}
