<?php

namespace Emitters\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest');
    }

    /**
     * @param $response
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendResetResponse($response) {
        return response()->json([
            'status' => 'success',
            'msg'    => 'Password was reset successful',
            'code'   => 0,
        ]);
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $user->password = Hash::make($password);

        $user->setRememberToken(Str::random(60));

        $user->save();
    }

    /**
     * @param Request $request
     * @param $response
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendResetFailedResponse(Request $request, $response) {
        return response()->json(getErrorResponseData('email', 'Given wrong data', 422), 422);
    }

    /**
     * @param Request $request
     * @param null $token
     */
    public function showResetForm(Request $request, $token = null) {
        return redirect()->away(env('APP_URL_FRONTEND')."/restore?token=".$request->input('token'));
    }
}
