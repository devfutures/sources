<?php


namespace Emitters\Http\Controllers\Auth;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use App\LibrariesAir\UsersData\UsersDataManager;
use App\LibrariesAir\PaymentSystems\PaymentSystemsManager;
use App\LibrariesAir\ConvertingCurrencyRate\ConvertingCurrencyRateManager;

class UsersDataController extends Controller {

    /**
     * @param Request $request
     * @param UsersDataManager $manager
     * @param ConvertingCurrencyRateManager $currencyRateManager
     * @param PaymentSystemsManager $psManager
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserInfo(Request $request, UsersDataManager $manager, ConvertingCurrencyRateManager $currencyRateManager, PaymentSystemsManager $psManager) {
        $user  = $manager->getUserInfo($request->user()->id);
        $rates = $currencyRateManager->getCurrencyRate();
        $date  = Carbon::now()->format('H:i:s');
        return response()->json([
            'status'          => 'success',
            'code'            => 0,
            'user'            => $user,
            'currency_rate'   => $rates,
            'payment_systems' => $psManager->grabAll(),
            'time'            => $date
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function isAdmin() {
        // return response(null, 200);
        try {
            $user = JWTAuth::parseToken()->authenticate();
            $code = 403;
            if($user->role_id != 0) {
                $code = 200;
            }
        } catch (JWTException $e) {
            return response(null, 403);
        }
        return response(null, $code);
    }
}