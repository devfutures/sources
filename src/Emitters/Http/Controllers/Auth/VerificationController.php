<?php

namespace Emitters\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Verified;

class VerificationController extends Controller {
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    use VerifiesEmails;

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function verify(Request $request) {
        if ($request->user()->hasVerifiedEmail()) {
            return response()->json([
                'status' => 'fail',
                'code'   => 1,
                'msg'    => 'You already have verified email.'
            ], 422);
        }

        if ($request->user()->markEmailAsVerified()) {
            event(new Verified($request->user()));
        }

        return response()->json([
            'status' => 'success',
            'code'   => 1,
            'msg'    => 'You successfully verified email.'
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function resend(Request $request) {
        if ($request->user()->hasVerifiedEmail()) {
            return response()->json([
                'status' => 'fail',
                'code'   => 1,
                'msg'    => 'You already have verified email.'
            ], 422);
        }

        $request->user()->sendEmailVerificationNotification();

        return response()->json([
            'status' => 'success',
            'code'   => 1,
            'msg'    => 'New verification email was sent to your email.'
        ]);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('jwt.auth');
    }
}
