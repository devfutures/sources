<?php


namespace Emitters\Http\Controllers\Clients;

use App\Http\Controllers\Controller;
use App\Models\MerchantPosts;
use Illuminate\Http\Request;
use Selfreliance\BitGo\Events\BitGoPaymentIncome;

class BitGoController extends Controller {
    public function new_block(Request $request){
        MerchantPosts::create([
            'type'      => 'BitGoBlockType'.$request->input('type'),
            'ip'        => real_ip(),
            'post_data' => $request->all()
        ]);

        $payment_systems = paymentsystems()->get();
        if($request->input('type') == 'block') {
            $find_ps = $payment_systems->where('title', ucfirst($request->input('coin')))->first();
        }elseif($request->input('type') == 'transfer'){
            $find_ps = $payment_systems->where('currency', strtoupper($request->input('coin')))->first();
        }else{
            return response()->json(['status' => 'fail'], 422);
        }

        $BitGo = resolve('payment.bitgo');
        $coin = false;

        if($find_ps){
            $coin = $find_ps->currency;
        }

        $resp = $BitGo->getListTransfersReceive($coin);
        if(!$resp) return false;


        MerchantPosts::create([
            'type'      => 'BitGoListTransferBy'.$coin,
            'ip'        => real_ip(),
            'post_data' => $resp
        ]);

        foreach($resp->transfers as $row){
            if($row->confirmations >= 1){
                foreach($row->entries as $row2){
                    if($row2->value > 0){
                        $address = [$row2->address, null];
                        if($coin === 'XLM'){
                            $tmp = explode('?', $row2->address);
                            if(count($tmp) == 2){
                                $address[0] = $tmp[0];
                            }else{
                                if(property_exists($row2, 'coinSpecific')){
                                    $address[0] = $tmp[0];
                                    $address[1] = $row2->coinSpecific->memo->value;
                                }
                            }
                            $address[0] = $tmp[0];
                            $address[1] = $row->coinSpecific->memo->value;
                            if($address[1] == 0) continue;
                        }
                        $operation = users_operations()->getOperationByPaymentAddress($address[0], $address[1]);
                        if($operation === null) continue;

                        $amount = number($row2->value/100000000, 8);
                        if($coin === 'XLM'){
                            $amount = number($row2->value/10000000, 8);
                        }

                        $PassData                     = new \stdClass();
                        $PassData->amount             = $amount;
                        $PassData->payment_id         = $operation->id;
                        $PassData->transaction        = $row->txid;
                        $PassData->add_info           = [
                            "full_data_ipn" => $row
                        ];
                        event(new BitGoPaymentIncome($PassData));
                    }
                }
            }
        }
    }
}