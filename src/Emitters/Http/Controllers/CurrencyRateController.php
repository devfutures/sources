<?php

namespace Emitters\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Emitters\ConvertingCurrencyRate\Requests\CurrencyRateUpdateRequest;
use App\LibrariesAir\ConvertingCurrencyRate\ConvertingCurrencyRateManager;

class CurrencyRateController extends Controller {

    /**
     * @param ConvertingCurrencyRateManager $manager
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(ConvertingCurrencyRateManager $manager) {
        return response()->json($manager->getCurrencyRate());
    }

    /**
     * @param Request $request
     * @param ConvertingCurrencyRateManager $manager
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, ConvertingCurrencyRateManager $manager) {
        $manager->insertNewCurrencyRate($request->all());
        return response()->json($manager->getCurrencyRate(), 200);
    }

    /**
     * @param CurrencyRateUpdateRequest $request
     * @param ConvertingCurrencyRateManager $manager
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CurrencyRateUpdateRequest $request, ConvertingCurrencyRateManager $manager) {
        $data = $request->all();
        $manager->setAutoUpdate($data['auto_update']['status'], $data['auto_update']['period']);
        if(array_key_exists('id', $data)) {
            $manager->updateCurrencyRate($data['id'], $data['rates']);
        }else {
            $manager->insertNewCurrencyRate($data['rates']);
        }
        return response()->json($manager->getCurrencyRate());
    }
}