<?php

namespace Emitters\Http\Controllers;

use App\LibrariesAir\DepositPlans\DepositPlansManager;
use App\LibrariesAir\ConvertingCurrencyRate\ConvertingCurrencyRateManager;
use Emitters\DepositPlans\Exceptions\PlanCreateException;
use Emitters\DepositPlans\Exceptions\DepositsExistsException;
use Emitters\DepositPlans\Requests\DepositPlanRequest;
use Emitters\DepositPlans\Requests\FilteredDepositPlanRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DepositPlansController extends Controller {

    /**
     * @param FilteredDepositPlanRequest $request
     * @param DepositPlansManager $plansManager
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(FilteredDepositPlanRequest $request, DepositPlansManager $plansManager) {
        $plans = $plansManager->getPlans($request->validated());
        return response()->json([
            'status' => 'success',
            'code'   => 0,
            'plans'  => $plans
        ]);
    }

    public function getDataForCreateDepositPlan(DepositPlansManager $plansManager, ConvertingCurrencyRateManager $rateManager) {
        $data = [
            'currency_rate'        => $rateManager->getCurrencyRate(),
            'payment_days_percent' => $plansManager->defaultPaymentDaysPercent()
        ];
        return response()->json($data);
    }

    /**
     * @param DepositPlanRequest $request
     * @param DepositPlansManager $plansManager
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(DepositPlanRequest $request, DepositPlansManager $plansManager) {
        $plan = $plansManager->store($request->validated());
        return response()->json([
            'status' => 'success',
            'code'   => 0,
            'plan'   => $plan
        ]);
    }

    /**
     * @param DepositPlanRequest $request
     * @param $id
     * @param DepositPlansManager $plansManager
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(DepositPlanRequest $request, $id, DepositPlansManager $plansManager) {
        try {
            $plansManager->update($id, $request->validated());
            return response()->json([
                'status' => 'success',
                'code'   => 0,
            ]);
        } catch (DepositsExistsException $e) {
            return response()->json(getErrorResponseData('name', $e->getMessage(), 1), 422);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @param DepositPlansManager $plansManager
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request, $id, DepositPlansManager $plansManager) {
        try {
            $plansManager->delete([$id]);
        } catch (DepositsExistsException $e) {
            return response()->json(getErrorResponseData('id', $e->getMessage(), 1), 422);
        }
        return response()->json([
            'status' => 'success',
            'code'   => 0,
        ]);
    }
}