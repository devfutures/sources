<?php

namespace Emitters\Http\Controllers;

use Emitters\DepositUsers\Requests\UpdateDepositsRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LibrariesAir\DepositUsers\DepositUsersManager;
use Emitters\DepositUsers\Requests\FilteredDepositsRequest;

class DepositUsersController extends Controller {

    /**
     * @param FilteredDepositsRequest $request
     * @param DepositUsersManager $manager
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(FilteredDepositsRequest $request, DepositUsersManager $manager) {
        $deposits = $manager->getDeposits($request->validated());
        return response()->json([
            'status'   => 'success',
            'code'     => 0,
            'deposits' => $deposits
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function operations($id) {
        $operations = users_operations()->getOperationsDeposit($id);
        return response()->json([
            'status'             => 'success',
            'code'               => 0,
            'deposit_operations' => $operations
        ]);
    }

    /**
     * @param $id
     * @param UpdateDepositsRequest $request
     * @param DepositUsersManager $manager
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, UpdateDepositsRequest $request, DepositUsersManager $manager) {
        $data = $request->validated();

        if(array_key_exists('amount', $data)){
            $data['amount'] = convert_to_bigInteger($data['amount']);
        }
        $manager->update($id, $data);
        return response()->json([
            'status' => 'success',
            'code'   => 0,
        ]);
    }


    /**
     * @param $id
     * @param DepositUsersManager $manager
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id, DepositUsersManager $manager) {
        $manager->delete([$id]);
        return response()->json([
            'status' => 'success',
            'code'   => 0,
        ]);
    }
}