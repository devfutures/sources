<?php


namespace Emitters\Http\Controllers;

use App\Http\Controllers\Controller;
use App\LibrariesAir\Faq\FaqManager;
use Auth;
use Emitters\Faq\Requests\CreateCategoryRequest;
use Emitters\Faq\Requests\CreateFaqRequest;
use Emitters\Faq\Requests\GetFaqFiltersRequest;
use Emitters\Faq\Requests\FaqUpdateRequest;
use Illuminate\Http\Request;

class FaqController extends Controller {

    /**
     * @param GetFaqFiltersRequest $request
     * @param FaqManager           $faqManager
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllFaq(GetFaqFiltersRequest $request, FaqManager $faqManager) {
        $faq = $faqManager->getAllFaq([
            'faq_filters'      => $request->input('faq_filters'),
            'content_filters'  => $request->input('content_filters')
        ]);

        $faqCounts = $faqManager->countFaq();

        return response()->json([
            'status' => 'success',
            'code'   => 0,
            'faq'    => $faq,
            'counts' => $faqCounts
        ]);
    }

    /**
     * @param FaqManager $faqManager
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllFaqCategory(FaqManager $faqManager){
        $categories = $faqManager->getAllFaqCategory();
        return response()->json([
            'status'     => 'success',
            'code'       => 0,
            'categories' => $categories
        ]);
    }

    /**
     * @param CreateCategoryRequest $request
     * @param FaqManager            $faqManager
     * @return \Illuminate\Http\JsonResponse
     */
    public function createFaqCategory(CreateCategoryRequest $request, FaqManager $faqManager) {
        $categories = $faqManager->createFaqCategory($request->validated());
        return response()->json([
            'status'     => 'success',
            'code'       => 0,
            'categories' => $categories
        ]);
    }

    /**
     * @param CreateFaqRequest $request
     * @param FaqManager       $faqManager
     * @return \Illuminate\Http\JsonResponse
     */
    public function createFaqWithContent(CreateFaqRequest $request, FaqManager $faqManager) {
        $faqManager->createFaqWithContent(\Auth::user(), $request->input('faq'), $request->input('content'));

        $faq       = $faqManager->getAllFaq(['faq_filters' => [], 'content_filters' => []]);
        $faqCounts = $faqManager->countFaq();

        return response()->json([
            'status' => 'success',
            'code'   => 0,
            'faq'     => $faq,
            'counts'  => $faqCounts,
        ]);
    }

    /**
     * @param FaqUpdateRequest $request
     * @param FaqManager       $faqManager
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateFaq(FaqUpdateRequest $request, FaqManager $faqManager) {
        $faqData        = $request->input('faq');
        $faqContentData = !empty($request->input('content')) ? $request->input('content') : null;

        $faqManager->update($faqData, $faqContentData);

        $faq       = $faqManager->getAllFaq(['faq_filters' => [], 'content_filters' => []]);
        $faqCounts = $faqManager->countFaq();

        return response()->json([
            'status' => 'success',
            'code'   => 0,
            'faq'    => $faq,
            'counts' => $faqCounts
        ]);
    }

    /**
     * @param Request    $request
     * @param FaqManager $faqManager
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function deleteCategory(Request $request, FaqManager $faqManager) {
        $this->validate($request, [
            'category_id' => 'required|integer|exists:faqs__categories,id'
        ]);
        $deleted = $faqManager->deleteCategory($request->input('category_id'));
        return response()->json([
            'status'  => ($deleted != false) ? 'success' : 'fail',
            'code'    => ($deleted != false) ? 0 : 1,
            'msg'     => ($deleted != false) ? 'Category deleted successfully' : 'FAQ`s with that category exists.'
        ]);
    }

    /**
     * @param Request    $request
     * @param FaqManager $faqManager
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function deleteFaq(Request $request, FaqManager $faqManager) {
        $this->validate($request, [
            '*.*' => 'required|integer|exists:faqs,id'
        ]);
        $deleted = $faqManager->deleteFaq($request->all());

        $faq       = $faqManager->getAllFaq(['faq_filters' => [], 'content_filters' => []]);
        $faqCounts = $faqManager->countFaq();

        return response()->json([
            'status'  => 'success',
            'code'    => 0,
            'deleted' => $deleted,
            'faq'     => $faq,
            'counts'  => $faqCounts,
        ]);
    }

    /**
     * @param Request    $request
     * @param FaqManager $faqManager
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function toDraft(Request $request, FaqManager $faqManager) {
        $this->validate($request, [
            '*.*' => 'required|integer|exists:faqs,id'
        ]);
        $faqManager->toDraft($request->all());

        $faq       = $faqManager->getAllFaq(['faq_filters' => [], 'content_filters' => []]);
        $faqCounts = $faqManager->countFaq();

        return response()->json([
            'status' => 'success',
            'code'   => 0,
            'faq'     => $faq,
            'counts'  => $faqCounts,
        ]);
    }

    /**
     * @param Request    $request
     * @param FaqManager $faqManager
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function publishDrafts(Request $request, FaqManager $faqManager) {
        $this->validate($request, [
            '*.*' => 'required|integer|exists:faqs,id'
        ]);
        $faqManager->publishDrafts($request->all());

        $faq       = $faqManager->getAllFaq(['faq_filters' => [], 'content_filters' => []]);
        $faqCounts = $faqManager->countFaq();

        return response()->json([
            'status' => 'success',
            'code'   => 0,
            'faq'     => $faq,
            'counts'  => $faqCounts,
        ]);
    }
}