<?php


namespace Emitters\Http\Controllers;


use App\Http\Controllers\Controller;
use App\LibrariesAir\Feedback\FeedbackManager;
use Emitters\Feedback\Requests\DeleteFeedbackRequest;
use Emitters\Feedback\Requests\FeedbackFilterRequest;
use Illuminate\Http\Request;

class FeedbackController extends Controller {

    /**
     * @param FeedbackFilterRequest $request
     * @param FeedbackManager $feedbackManager
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(FeedbackFilterRequest $request, FeedbackManager $feedbackManager) {
        $feedback          = $feedbackManager->getAll($request->validated());
        $counting_messages = $feedbackManager->getCountingMessages();
        return response()->json([
            'status'            => 'success',
            'code'              => 0,
            'feedback'          => $feedback,
            'counting_messages' => $counting_messages
        ]);
    }

    /**
     * @param DeleteFeedbackRequest $request
     * @param FeedbackManager $feedbackManager
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(DeleteFeedbackRequest $request, FeedbackManager $feedbackManager) {
        $feedbackManager->delete($request->validated()['ids']);
        return response()->json([
            'status' => 'success',
            'code'   => 0,
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @param FeedbackManager $feedbackManager
     * @return \Illuminate\Http\JsonResponse
     */
    public function response_answer(Request $request, $id, FeedbackManager $feedbackManager) {
        $feedbackManager->preAnswer($id, auth()->id(), $request->input('message'));
        return response()->json([
            'status' => 'success',
            'code'   => 0,
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @param FeedbackManager $feedbackManager
     * @return \Illuminate\Http\JsonResponse
     */
    public function change_status(Request $request, FeedbackManager $feedbackManager) {
        $feedbackManager->changeStatus($request->input('ids'), $request->input('status'));
        return response()->json([
            'status' => 'success',
            'code'   => 0,
        ]);
    }
}