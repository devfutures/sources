<?php

namespace Emitters\Http\Controllers\ForTests;

use App\LibrariesAir\ConvertingCurrencyRate\ConvertingCurrencyRateManager;
use App\LibrariesAir\DepositUsers\DepositUsersManager;
use Emitters\DepositUsers\Requests\AddFundsDepositRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class CreateDepositClientController extends Controller {
    public function create(AddFundsDepositRequest $request, DepositUsersManager $depositUsersManager) {
        try {
            $result = $depositUsersManager->addFundsToDeposit(Auth::user(), $request->all());
            return response()->json($result);
        } catch (\Exception $e) {
            $data = getErrorResponseData('amount', $e->getMessage());
            return response()->json($data, 422);
        }
    }
}