<?php

namespace Emitters\Http\Controllers\ForTests;

use App\LibrariesAir\Reviews\ReviewsManager;
use Emitters\Reviews\Requests\AddReviewsRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CreateReviewClientController extends Controller
{
	public function create(AddReviewsRequest $request, ReviewsManager $reviewsManager){
	    $data = $reviewsManager->createReview(Auth::user(), $request->only('review_text', 'youtube_link'));
	    return response()->json($data);
	}
}