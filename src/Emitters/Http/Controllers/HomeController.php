<?php

namespace Emitters\Http\Controllers;

use Emitters\AdminControl\MenuAccess;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;

class HomeController extends Controller
{
	public function index(MenuAccess $menu){
		dd($menu->getSectionsWithTranslate());
	}
}