<?php


namespace Emitters\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class LoginWithUserController extends Controller {
    /**
     * @param  Request $request
     * @return mixed
     */
    public function login(Request $request) {
        $adminToken = $this->parseAdminTokenFromAuth($request);
        $token      = $this->loginWithUser($request);
        return $this->sendSuccessResponse($token, $adminToken);
    }

    /**
     * @param string $token
     * @param string $adminToken
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendSuccessResponse(string $token, string $adminToken) {
        return response()->json([
            'status'                      => 'success',
            'code'                        => 0,
            'token_for_access_under_user' => respond_with_token($token),
            'admin_token'                 => respond_with_token($adminToken)
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    protected function parseUserForAuthenticate(Request $request) {
        $request->validate([
            'user_id' => 'required|integer|exists:users,id'
        ]);
        $user = User::query()->find($request->input('user_id'));
        return $user;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    protected function parseAdminTokenFromAuth(Request $request) {
        $adminToken = auth()->tokenById($request->user()->id);
        return $adminToken;
    }

    /**
     * @param Request $request
     * @return string
     */
    protected function loginWithUser(Request $request) {
        $user  = $this->parseUserForAuthenticate($request);
        $token = auth()->login($user);
        return $token;
    }
}