<?php

namespace Emitters\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
	public function index(){
		$routes = [];
		
	   	$admin_prefix = 'new_admin';
		foreach (\Route::getRoutes()->getIterator() as $route){
		    if (strpos($route->uri, $admin_prefix) !== false){
		    	$new_prefix = str_replace($admin_prefix.'/', '', $route->action['prefix']);
		    	$tmp = str_replace('new_admin', '', $route->uri);
		    	$menus = explode('/', $tmp);
		    	if(count($menus) >=3){
					$routes[$new_prefix][] = $menus[2];
		    	}
		    }
		}
	   	dd($routes);
	}
}