<?php


namespace Emitters\Http\Controllers;


use App\Http\Controllers\Controller;
use App\LibrariesAir\News\NewsManager;
use Auth;
use Emitters\News\Requests\StoreNewsRequest;
use Emitters\News\Requests\UpdateNewsRequest;
use Emitters\News\Requests\AddContentRequest;
use Illuminate\Http\Request;

class NewsController extends Controller {

    /**
     * @param Request $request
     * @param NewsManager $manager
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll(Request $request, NewsManager $manager) {
        $news = $manager->getAll($request->all());
        return response()->json($news);
    }

    /**
     * @param StoreNewsRequest $request
     * @param NewsManager $manager
     * @return \Illuminate\Http\JsonResponse
     */
    public function addNews(StoreNewsRequest $request, NewsManager $manager) {
        $newsData = $manager->extractStoreNewsData(Auth::user(), $request);
        $file     = $request->has('image') ? $request->file('image') : null;
        $news     = $manager->createNews($newsData, $file);
        return response()->json([
            'status' => 'success',
            'code'   => 0,
            'news'   => $news
        ]);
    }

    public function updateNews(UpdateNewsRequest $request, NewsManager $manager) {
        $file = $request->has('news.image') ? $request->file('news.image') : null;
        $news = $manager->update($request->all(), $file);
        return response()->json([
            'status' => 'success',
            'code'   => 0,
            'news'   => $news
        ]);
    }

    public function deleteNews(Request $request, NewsManager $manager) {
        $manager->deleteNews($request->input('news_id'), true);
        return response()->json([
            'status' => 'success',
            'code'   => 0
        ]);
    }
}