<?php


namespace Emitters\Http\Controllers;

use App\Http\Controllers\Controller;
use Emitters\AdminControl\Requests\SetRoleRequest;
use Emitters\AdminControl\AdminRoles;
use Emitters\AdminControl\MenuAccess;
use Emitters\AdminControl\Requests\RolesRequest;
use App\LibrariesAir\UsersData\UsersDataManager;
use Emitters\UsersOperations\ControlOperations;
use Emitters\UsersOperations\Models\Users_Operations;
use Emitters\UsersOperations\UsersOperations;
use Illuminate\Http\Request;

class OperationController extends Controller {
    /**
     * @param Request $request
     * @param UsersOperations $controlOperations
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, UsersOperations $controlOperations) {
        $operations = $controlOperations->getOperationsWithFilter($request->all());
        $operations->load(['admin_created']);
        return response()->json(compact('operations'));
    }

    /**
     * @param Request $request
     * @param ControlOperations $controlOperations
     * @return \Illuminate\Http\JsonResponse
     */
    public function action(Request $request, ControlOperations $controlOperations) {
        $default_rule = [
            'type' => 'required|in:cancel,create,confirm',
            'id'   => 'sometimes|required|array',
            'id.*' => 'sometimes|required|exists:users__operations,id',
        ];
        $request->validate($default_rule);
        try {
            $res = $controlOperations->handlerActionOperations($request->all(), $request->input('type'));
            return response()->json($res);
        } catch (\Exception $e) {
            return response()->json(getErrorResponseData('id', $e->getMessage()), 422);
        }
    }

    /**
     * @param Request $request
     * @param ControlOperations $controlOperations
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request, ControlOperations $controlOperations) {
        $default_rule = [
            'type'              => 'required|in:buy,sell',
            'operation'         => 'required',
            'user'              => 'required|email|exists:users,email',
            'payment_system_id' => 'required',
            'amount'            => 'required',
            'plan_id'           => 'sometimes|required|exists:deposit__plans,id'
        ];
        $request->validate($default_rule);
        $result = $controlOperations->createOperation($request->all());
        return response()->json($result);
    }

    /**
     * @param Request $request
     * @param ControlOperations $controlOperations
     * @return \Illuminate\Http\JsonResponse
     */
    public function status(Request $request, ControlOperations $controlOperations) {
        $default_rule = [
            'id'     => 'sometimes|required|array',
            'id.*'   => 'sometimes|required|exists:users__operations,id',
            'status' => 'required'
        ];
        $request->validate($default_rule);
        try {
            $res = $controlOperations->setStatusOperations($request->all(), $request->input('status'));
            return response()->json($res);
        } catch (\Exception $e) {
            return response()->json(getErrorResponseData('id', $e->getMessage()), 422);
        }
    }

    /**
     * @param $id
     * @param Request $request
     * @param UsersOperations $usersOperations
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id, Request $request, UsersOperations $usersOperations) {
        $status = $usersOperations->updateOperation($id, $request->all());
        return response()->json(['status' => $status]);
    }

    /**
     * @param $id
     * @param UsersOperations $usersOperations
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id, UsersOperations $usersOperations) {
        $status = $usersOperations->deleteOperation($id);
        return response()->json(['status' => $status]);
    }

    /**
     * @param Request $request
     * @param ControlOperations $controlOperations
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function massive(Request $request, ControlOperations $controlOperations) {
        $default_rule = [
            'id'     => 'sometimes|required|array',
            'id.*'   => 'sometimes|required|exists:users__operations,id',
        ];
        $request->validate($default_rule);
        $result = $controlOperations->massiveOperation($request->all());
        return response()->json($result);
    }
}