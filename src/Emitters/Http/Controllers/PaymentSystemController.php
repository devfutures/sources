<?php

namespace Emitters\Http\Controllers;

use App\LibrariesAir\PaymentSystems\PaymentSystemsManager;
use Emitters\PaymentSystems\Requests\GetPaymentSystemsRequest;
use Emitters\PaymentSystems\Requests\PaymentSystemRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentSystemController extends Controller {
    /**
     * @param GetPaymentSystemsRequest $request
     * @param PaymentSystemsManager $manager
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(GetPaymentSystemsRequest $request, PaymentSystemsManager $manager) {
        $systems = $manager->get($request->validated(), null);
        $counts  = $manager->getCounts();
        return response()->json([
            'status'  => 'success',
            'code'    => 0,
            'systems' => $systems,
            'counts'  => $counts
        ]);
    }

    /**
     * @param Request $request
     * @param PaymentSystemsManager $manager
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateSort(Request $request, PaymentSystemsManager $manager) {
        $this->validate($request, [
            '*.id'   => 'required|integer|exists:payment__systems,id',
            '*.sort' => 'required|integer',
        ]);
        $manager->updateSort($request->all());

        return response()->json([
            'status' => 'success',
            'code'   => 0,
        ]);
    }

    /**
     * @param PaymentSystemsManager $manager
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getModulesList(PaymentSystemsManager $manager) {
        $modules = $manager->getAvailableModules();
        return response()->json([
            'status'  => 'success',
            'code'    => 0,
            'modules' => $modules
        ]);
    }

    /**
     * @param PaymentSystemRequest $request
     * @param PaymentSystemsManager $manager
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(PaymentSystemRequest $request, PaymentSystemsManager $manager) {
        $paymentSystem = $manager->addPaymentSystem($request->validated());
        return response()->json([
            'status' => 'success',
            'code'   => 0,
            'system' => $paymentSystem
        ]);
    }

    /**
     * @param PaymentSystemRequest $request
     * @param                       $id
     * @param PaymentSystemsManager $manager
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(PaymentSystemRequest $request, $id, PaymentSystemsManager $manager) {
        $paymentSystem = $manager->change($id, $request->validated(), null);
        return response()->json([
            'status' => 'success',
            'code'   => 0,
        ]);
    }

    /**
     * @param Request $request
     * @param                       $id
     * @param PaymentSystemsManager $manager
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request, $id, PaymentSystemsManager $manager) {
        $manager->destroy($id);
        return response()->json([
            'status' => 'success',
            'code'   => 0,
        ]);
    }
}