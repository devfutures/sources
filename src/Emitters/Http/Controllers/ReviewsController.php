<?php


namespace Emitters\Http\Controllers;


use App\Http\Controllers\Controller;
use Emitters\Reviews\Requests\FilteredReviewsRequest;
use Emitters\Reviews\Requests\ReviewsRequest;
use App\LibrariesAir\Reviews\ReviewsManager;

class ReviewsController extends Controller {

    /**
     * @param ReviewsRequest $request
     * @param ReviewsManager $manager
     * @return \Illuminate\Http\JsonResponse
     */
    public function addReview(ReviewsRequest $request, ReviewsManager $manager) {
        $manager->createReview($request->user(), $request->validated()[0]);
        return response()->json([
            'status'  => 'success',
            'code'    => 0,
            'reviews' => $manager->getReviews()
        ]);
    }

    /**
     * @param FilteredReviewsRequest $request
     * @param ReviewsManager         $manager
     * @return \Illuminate\Http\JsonResponse
     */
    public function getReviews(FilteredReviewsRequest $request, ReviewsManager $manager) {
        $reviews = $manager->getReviews($request->validated());
        return response()->json([
            'status'  => 'success',
            'code'    => 0,
            'reviews' => $reviews,
            'counts'  => $manager->countByStatus()
        ]);
    }

    /**
     * @param ReviewsRequest $request
     * @param ReviewsManager $manager
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateReviews(ReviewsRequest $request, ReviewsManager $manager) {
        foreach ($request->validated() as $review) {
            if(array_key_exists('id', $review)) {
                $manager->updateReview($review['id'], $review);
            }
        }
        return response()->json([
            'status'  => 'success',
            'code'    => 0,
            'reviews' => $manager->getReviews()
        ]);
    }

    /**
     * @param ReviewsRequest $request
     * @param ReviewsManager $manager
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteReviews(ReviewsRequest $request, ReviewsManager $manager) {
        foreach ($request->validated() as $review) {
            if(array_key_exists('id', $review)) {
                $manager->deleteReview($review['id']);
            }
        }
        return response()->json([
            'status'  => 'success',
            'code'    => 0,
            'reviews' => $manager->getReviews()
        ]);
    }
}