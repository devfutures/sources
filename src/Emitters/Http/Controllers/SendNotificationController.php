<?php


namespace Emitters\Http\Controllers;


use App\Http\Controllers\Controller;
use Emitters\ExpoNotification\ExpoChannel;
use Emitters\ExpoNotification\NotifyMessage;
use Emitters\ExpoNotification\Requests\NotificationRequest;
use Emitters\ExpoNotification\UserDeviceManager;

class SendNotificationController extends Controller {

    /**
     * @param NotificationRequest $request
     * @param UserDeviceManager $manager
     * @param ExpoChannel $channel
     * @return \Illuminate\Http\JsonResponse
     */
    public function send(NotificationRequest $request, UserDeviceManager $manager, ExpoChannel $channel) {
        $data          = $request->validated();

        if(array_key_exists('tokens', $data)) {
            $notifyMessage = new NotifyMessage($data['tokens'], $data['title'], null, $data['body']);
        } else {
            $notifyMessage = new NotifyMessage($manager->getAllTokens()->toArray(), $data['title'], null, $data['body']);
        }

        $channel->send($notifyMessage);

        return response()->json([
            'status' => 'success',
            'code'   => 0,
        ]);
    }

}