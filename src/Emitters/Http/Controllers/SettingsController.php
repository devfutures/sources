<?php


namespace Emitters\Http\Controllers;


use Emitters\AdminControl\EnvManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Emitters\AdminControl\SiteSettings;

class SettingsController extends Controller {

    /**
     * @param Request $request
     * @param SiteSettings $siteSettings
     * @param EnvManager $manager
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function setEnvironmentVariable(Request $request, SiteSettings $siteSettings, EnvManager $manager) {
        $this->validate($request, [
            '*.env_var'     => 'required|string|in:' . implode(',', $manager->getAvailableEnvironmentVariables('emitters.site_settings')),
            '*.env_var_val' => 'required|string'
        ]);
        $siteSettings->setEnvVariable($request->all());
        return response()->json([
            'status' => 'success',
            'code'   => 0
        ]);
    }

    /**
     * @param SiteSettings $siteSettings
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEnvironment(SiteSettings $siteSettings) {
        return response()->json([
            'status' => 'success',
            'code'   => 0,
            'env'    => $siteSettings->getEnvironment()
        ]);
    }
}