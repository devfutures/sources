<?php


namespace Emitters\Http\Controllers;


use Carbon\Carbon;
use Emitters\Balance\BalancePaymentSystems;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LibrariesAir\Statistics\StatisticsManager;
use Emitters\Statistics\Requests\AccountingRequest;

class StatisticsController extends Controller {

    /**
     * @param Request $request
     * @param StatisticsManager $manager
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request, StatisticsManager $manager) {
        $this->validate($request, [
            'accounting_year' => 'date_format:Y'
        ]);
        return response()->json([
            'status'     => 'success',
            'code'       => 0,
            'currency'   => config('emitters.default_currency_systems'),
            'statistic'  => $manager->getStatistic(),
            'accounting' => $manager->getAccounting($request->input('accounting_year')),
        ]);
    }

    /**
     * @param Request $request
     * @param StatisticsManager $manager
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function graphData(Request $request, StatisticsManager $manager) {
        $this->validate($request, [
            'period_from' => 'date_format:d.m.Y',
            'period_to'   => 'date_format:d.m.Y'
        ]);

        $periodFrom = $request->input('period_from') ?? Carbon::now()->startOfMonth()->format('d.m.Y');
        $periodTo   = $request->input('period_to') ?? Carbon::now()->endOfDay()->format('d.m.Y');

        $graphData = $manager->getGraphData($periodFrom, $periodTo);
        return response()->json([
            'status'     => 'success',
            'code'       => 0,
            'graph_data' => $graphData
        ]);
    }

    /**
     * @param AccountingRequest $request
     * @param StatisticsManager $manager
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(AccountingRequest $request, StatisticsManager $manager) {
        try{
            list($insert, $message) = $manager->addRow($request->validated());
            return response()->json([
                'status'     => 'success',
                'code'       => 0,
                'message'    => $message,
                'accounting' => $manager->getAccounting(),
                'statistic'  => $manager->getStatistic(),
            ]);
        }catch (\Exception $e){
            return response()->json(getErrorResponseData('payment_system_module',$e->getMessage()), 422);
        }

    }

    public function updateAccounting(AccountingRequest $request, StatisticsManager $manager) {
        $manager->updateRow($request->validated());
        return response()->json([
            'status'     => 'success',
            'code'       => 0,
            'statistic'  => $manager->getStatistic(),
            'accounting' => $manager->getAccounting()
        ]);
    }

    /**
     * @param                   $id
     * @param StatisticsManager $manager
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteAccounting($id, StatisticsManager $manager) {
        $manager->deleteRow($id);
        return response()->json([
            'status'     => 'success',
            'code'       => 0,
            'statistic'  => $manager->getStatistic(),
            'accounting' => $manager->getAccounting()
        ]);
    }

    /**
     * @param Request $request
     * @param BalancePaymentSystems $balancePaymentSystems
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getBalance(Request $request, BalancePaymentSystems $balancePaymentSystems) {
        $this->validate($request, [
            'id'   => 'array',
            'id.*' => 'required'
        ]);
        $result = $balancePaymentSystems->showBalance($request['id']);
        return response()->json([
            'balance' => $result
        ]);
    }
}