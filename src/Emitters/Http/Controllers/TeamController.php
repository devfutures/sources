<?php


namespace Emitters\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Emitters\Affiliate\AffiliateStructures;

class TeamController extends Controller {

    /** @var AffiliateStructures */
    protected $affiliateStructures;

    /**
     * TeamController constructor.
     * @param AffiliateStructures $affiliateStructures
     */
    public function __construct(AffiliateStructures $affiliateStructures) {
        $this->affiliateStructures = $affiliateStructures;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request) {
        $team = $this->affiliateStructures->getUsersTeamInfo(
            $request->query->get('orderBy'),
            $request->query->get('direction'),
            $request->query->get('hasIncome'),
            $request->query->get('hasReferrer')
        );
        return response()->json([
            'status' => 'success',
            'code'   => 0,
            'team'   => $team
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserTeamInfoByLevel($id) {
        $info = $this->affiliateStructures->getUserTeamBaseInfoByLevels($id);
        return response()->json([
            'status' => 'success',
            'code'   => 0,
            'data'   => $info
        ]);
    }

    /**
     * @param                       $id
     * @param int $level
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function inspectUserTeam($id, $level = 1, Request $request) {
        $team = $this->affiliateStructures->inspectUserTeam(
            $id,
            $level,
            $request->query->get('orderBy'),
            $request->query->get('direction'),
            $request->query->get('hasIncome'),
            $request->query->get('hasReferrer')
        );
        return response()->json([
            'status' => 'success',
            'code'   => 0,
            'team'   => $team
        ]);
    }
}