<?php


namespace Emitters\Http\Controllers;


use App\Http\Controllers\Controller;
use Emitters\ExpoNotification\Requests\DeviceRequest;
use Emitters\ExpoNotification\UserDeviceManager;

class UserDevicesController extends Controller {
    /**
     * @param UserDeviceManager $manager
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getDevices(UserDeviceManager $manager) {
        $devices = $manager->getDevices();
        return response()->json([
            'status'  => 'success',
            'code'    => 0,
            'devices' => $devices
        ]);
    }

    /**
     * @param DeviceRequest $request
     * @param UserDeviceManager $manager
     * @return \Illuminate\Http\JsonResponse
     */
    public function addDevice(DeviceRequest $request, UserDeviceManager $manager) {
        $data            = $request->validated();
        $data['user_id'] = $request->user();
        $manager->createDevice($data);
        return response()->json([
            'status'  => 'success',
            'code'    => 0,
            'devices' => $manager->getDevices()
        ]);
    }

    /**
     * @param $id
     * @param DeviceRequest $request
     * @param UserDeviceManager $manager
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateDevice($id, DeviceRequest $request, UserDeviceManager $manager) {
        \Validator::validate(['id' => $id], [
            'id' => 'exists:users__devices,id'
        ]);
        $manager->updateDevice($id, $request->validated());
        return response()->json([
            'status'  => 'success',
            'code'    => 0,
            'devices' => $manager->getDevices()
        ]);
    }

    /**
     * @param $id
     * @param UserDeviceManager $manager
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function deleteDevice($id, UserDeviceManager $manager) {
        \Validator::validate(['id' => $id], [
            'id' => 'integer|exists:users__devices,id'
        ]);
        $manager->deleteDevices([$id]);
        return response()->json([
            'status'  => 'success',
            'code'    => 0,
            'devices' => $manager->getDevices()
        ]);
    }
}