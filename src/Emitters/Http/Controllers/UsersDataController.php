<?php

namespace Emitters\Http\Controllers;

use App\LibrariesAir\Balance\BalanceManager;
use App\LibrariesAir\UsersData\UsersDataManager;
use App\LibrariesAir\PaymentSystems\PaymentSystemsManager;
use Emitters\Affiliate\AffiliateStructures;
use Emitters\UsersData\Requests\ValidateChangePasswordByAdmin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
class UsersDataController extends Controller
{
    /**
     * @param Request $request
     * @param UsersDataManager $manager
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, UsersDataManager $manager){
        $new_users_count = $manager->countNewUsers();
        $total_users = $manager->countTotalUsers();
        $users = $manager->getUsers($request->all());
        return response()->json(compact('users', 'new_users_count', 'total_users'));
	}

    /**
     * @param                       $id
     * @param UsersDataManager      $manager
     * @param PaymentSystemsManager $paymentSystemsManager
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id, UsersDataManager $manager, PaymentSystemsManager $paymentSystemsManager){
        $info = $manager->getUserInfo($id);
        $wallets = $paymentSystemsManager->getUserWalletWithPaymentSystem($info);
        $activity = $manager->getUserActivity($info);
        $last_activity = null;
        if($activity->count() > 0){
            $last_activity = $activity[0];
            $last_activity->diff_for_humans = Carbon::parse($last_activity->created_at)->diffForHumans();
        }

        return response()->json(compact('info', 'wallets', 'activity', 'last_activity'));
	}

    /**
     * @param                  $id
     * @param Request          $request
     * @param UsersDataManager $manager
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function blocked($id, Request $request, UsersDataManager $manager) {
	    $manager->updateBlocked($id, $request->input('block_message'), $request->input('blocked_up'));
        return response()->json([
            'status' => 'success',
            'code'   => 0
        ]);
	}

    /**
     * @param                               $id
     * @param ValidateChangePasswordByAdmin $request
     * @param UsersDataManager              $manager
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function change_password($id, ValidateChangePasswordByAdmin $request, UsersDataManager $manager){
        $manager->setNewPassword($id, $request->input('password'));
        return response()->json([
            'status' => 'success',
            'code'   => 0
        ]);
	}

    /**
     * @param                  $id
     * @param Request          $request
     * @param UsersDataManager $manager
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request, UsersDataManager $manager){
	    $manager->updateInfo($id, $request->all());
        return response()->json([
            'status' => 'success',
            'code'   => 0
        ]);
	}

    /**
     * @param                  $id
     * @param Request          $request
     * @param UsersDataManager $manager
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function save_wallets($id, Request $request, UsersDataManager $manager) {
        $manager->saveWallets($id, $request->all());
        return response()->json([
            'status' => 'success',
            'code'   => 0
        ]);
	}

    /**
     * @param $id
     * @param PaymentSystemsManager $systemsManager
     * @param UsersDataManager $usersDataManager
     * @param AffiliateStructures $affiliateStructures
     * @return \Illuminate\Http\JsonResponse
     */
    public function balance_info($id, PaymentSystemsManager $systemsManager, UsersDataManager $usersDataManager, AffiliateStructures $affiliateStructures) {
        $user = $usersDataManager->getUserInfo($id);
        $balances = $systemsManager->getAll($user);
        $ref_statistics = $affiliateStructures->getStatisticsByUser($user);
        return response()->json(compact('balances', 'ref_statistics'));
	}
}