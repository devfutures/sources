<?php

namespace Emitters\LibrariesAir\Commands;

use Illuminate\Console\Command;
use Artisan;
use Illuminate\Support\Facades\Storage;
class CreateReadme extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'libair:createreadme';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create one file readme';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $string = '';
        $dir = __DIR__.'/../../';
        $libs = [
            'LibrariesAir',
            'PaymentSystems',
            'Balance',
            'DepositPlans',
            'DepositUsers'
        ];
        foreach($libs as $row){
            $tmp = file_get_contents($dir.$row.'/README.md');
            $string .= $tmp;
            $string .= "\n\n\n";
        }
        file_put_contents($dir.'../../'.'README.md', $string);
    }
}
