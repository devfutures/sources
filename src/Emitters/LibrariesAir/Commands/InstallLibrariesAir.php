<?php

namespace Emitters\LibrariesAir\Commands;

use Illuminate\Console\Command;
use Artisan;
class InstallLibrariesAir extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'libair:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install lib air';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $providers = resolve('config')['app']['providers'];
        $dirs = scandir(__DIR__.'/../LibToCopy');
        if(in_array('.DS_Store', $dirs)){
            $find = array_search('.DS_Store', $dirs);
            unset($dirs[$find]);
        }
        if(in_array('.', $dirs)){
            $find = array_search('.', $dirs);
            unset($dirs[$find]);
        }
        if(in_array('..', $dirs)){
            $find = array_search('..', $dirs);
            unset($dirs[$find]);
        }
        $write_service_provider = [];
        foreach($dirs as $row){
            $tmp = "App\LibrariesAir\\".$row."\\".$row."ServiceProvider";
            if(!in_array($tmp, $providers)){
                $write_service_provider[] = $tmp;
            }
        }

        Artisan::call('vendor:publish', ['--tag' => 'LibrariesAir']);
        $this->info('Libraries copied successfully, see in App\LibrariesAir\*');
        if(count($write_service_provider) > 0){
            $this->info('Once it\'s installed, you can register the service provider in config/app.php in the providers array:');
            $this->info('\'providers\' = [');
            $this->info('// ...');
            foreach($write_service_provider as $row){
                $this->info($row.'::class,');
            }

            $this->info('// ...');
            $this->info("\r]");
        }
        $this->info('Run composer dump-autoload');
    }
}
