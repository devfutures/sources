<?php

use Carbon\Carbon;

if(!function_exists('df_div')) {
    function df_div($left, $right, $scale = null) {
        return isset($scale) ? bcdiv($left, $right, $scale) : bcdiv($left, $right, config('emitters.bc_scale'));
    }
}

if(!function_exists('df_mul')) {
    function df_mul($left, $right, $scale = null) {
        return isset($scale) ? bcmul($left, $right, $scale) : bcmul($left, $right, config('emitters.bc_scale'));
    }
}

if(!function_exists('df_add')) {
    function df_add($left, $right, $scale = null) {
        return isset($scale) ? bcadd($left, $right, $scale) : bcadd($left, $right, config('emitters.bc_scale'));
    }
}

if(!function_exists('df_sub')) {
    function df_sub($left, $right, $scale = null) {
        return isset($scale) ? bcsub($left, $right, $scale) : bcsub($left, $right, config('emitters.bc_scale'));
    }
}

if(!function_exists('number')) {
    function number($amount, $points = 2, $afters = '') {
        return number_format($amount, $points, '.', $afters);
    }
}

if(!function_exists('is_json')) {
    function is_json($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}

if(!function_exists('real_ip')) {
    function real_ip() {
        $ip = (\Request::header('x-real-ip') == null) ? \Request::ip() : \Request::header('x-real-ip');
        return $ip;
    }
}

if(!function_exists('create_calendar_zeros_chart')) {
    function create_calendar_zeros_chart($from, $to) {
        $returned = [];
        $from     = Carbon::parse($from);
        $to       = Carbon::parse($to);
        $tmp      = $from;
        while(true) {
            $returned[$tmp->format('Y-m-d')] = [
                'purchase' => 0,
                'withdraw' => 0,
            ];
            if($tmp->format('Y-m-d') == $to->format('Y-m-d')) break;
            $tmp = $tmp->addDay();
        }
        return $returned;
    }
}
if(!function_exists('respond_with_token')) {
    function respond_with_token($token) {
        return [
            'access_token' => $token,
            'token_type'   => 'bearer',
            'expires_in'   => auth()->factory()->getTTL() * 60
        ];
    }
}

if(!function_exists('calc_of_amount_percent')) {
    function calc_of_amount_percent($amount, $percent) {
        return df_div(df_mul($amount, $percent), 100);
    }
}

if(!function_exists('convert_from_bigInteger')) {
    function convert_from_bigInteger($value, $scale = null) {
        return df_div($value, config('emitters.bc_big_integer_multiplier'), $scale);
    }
}

if(!function_exists('convert_to_bigInteger')) {
    function convert_to_bigInteger($value, $scale = null) {
        return df_mul($value, config('emitters.bc_big_integer_multiplier'), $scale);
    }
}

if(!function_exists('usersdata')) {
    /**
     * @return Emitters\UsersData\UsersDataManager
     */
    function usersdata() {
        return resolve('libair.usersdata');
    }
}

if(!function_exists('paymentsystems')) {
    /**
     * @return App\LibrariesAir\PaymentSystems\PaymentSystemsManager
     */
    function paymentsystems() {
        return resolve('libair.paymentsystems');
    }
}

if(!function_exists('users_operations')) {
    /**
     * @return Emitters\UsersOperations\UsersOperations
     */
    function users_operations() {
        return resolve('users_operations');
    }
}

if(!function_exists('converting_currency_rate')) {
    /**
     * @return App\LibrariesAir\ConvertingCurrencyRate\ConvertingCurrencyRateManager
     */
    function converting_currency_rate() {
        return resolve('libair.converting_currency_rate');
    }
}


if(!function_exists('balance')) {
    /**
     * @return App\LibrariesAir\Balance\BalanceManager
     */
    function balance() {
        return resolve('librariesair.balance');
    }
}

if(!function_exists('partnership_accrual')) {
    /**
     * @return Emitters\Affiliate\PartnershipAccrual
     */
    function partnership_accrual() {
        return resolve('partnership.accrual');
    }
}

if(!function_exists('deposit_accruals')) {
    /**
     * @return Emitters\DepositAccruals\DepositAccrualsManager
     */
    function deposit_accruals() {
        return resolve('deposit.accruals');
    }
}


if (!function_exists('getErrorResponseData')) {
    function getErrorResponseData($error, $errorMessage, $errorCode = 1) {
        $data = [
            'code'    => $errorCode,
            'message' => 'The given data was invalid.',
            'errors'  => [
                $error => [$errorMessage]
            ]
        ];
        return $data;
    }
}

if (!function_exists('mergeDataInfo')) {
    function mergeDataInfo($data_info_now, $add_to) {
        if($data_info_now && count($data_info_now) > 0) {
            $data_info_now = array_merge($data_info_now, $add_to);
        } else {
            $data_info_now = $add_to;
        }

        return $data_info_now;
    }
}