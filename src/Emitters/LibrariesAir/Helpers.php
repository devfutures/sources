<?php
use Carbon\Carbon;
if(!function_exists('number')){
	function number($amount, $points = 2, $afters = ''){
		return number_format($amount, $points, '.', $afters);
	}
}

if(!function_exists('is_json')){
	function is_json($string) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}
}

if(!function_exists('real_ip')){
	function real_ip() {
		$ip = (\Request::header('x-real-ip') == null)?\Request::ip():\Request::header('x-real-ip');
		return $ip;
	}
}

if(!function_exists('create_calendar_zeros_chart')){
	function create_calendar_zeros_chart($from, $to){
		$returned = [];
		$from = Carbon::parse($from);
		$to = Carbon::parse($to);
		$tmp = $from;
		while(true){
			$returned[$tmp->format('Y-m-d')] = [
				'purchase' => 0,
				'withdraw' => 0,
			];
			if($tmp->format('Y-m-d') == $to->format('Y-m-d')) break;
			$tmp = $tmp->addDay();
		}
		return $returned;
	}
}