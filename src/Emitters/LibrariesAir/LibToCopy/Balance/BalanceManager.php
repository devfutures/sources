<?php

namespace App\LibrariesAir\Balance;

use Emitters\Balance\BalanceManager as BalanceManagerOriginal;

class BalanceManager extends BalanceManagerOriginal {

    public function __construct($model, $calculateBalance) {
        $this->calculateBalance = $calculateBalance;
        $this->model            = $model;
    }
}