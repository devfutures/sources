<?php

namespace App\LibrariesAir\Balance;
use Illuminate\Support\ServiceProvider;
use Emitters\Balance\Models\Users_Balance; 
use Emitters\Balance\CalculateBalance;

class BalanceServiceProvider extends ServiceProvider
{
    public function boot()
    {

    }
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(BalanceManager::class, function ($app) {
            
            return new BalanceManager(new Users_Balance(), new CalculateBalance(new Users_Balance(), $app['config']['emitters']['balance_driver']));
        });

        $this->app->alias(BalanceManager::class, 'librariesair.balance');
    }
}
