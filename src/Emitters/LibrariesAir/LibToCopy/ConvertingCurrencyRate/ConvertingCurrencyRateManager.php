<?php

namespace App\LibrariesAir\ConvertingCurrencyRate;

use Emitters\Contracts\ConvertingCurrencyRate\DriverContract;
use Emitters\ConvertingCurrencyRate\ConvertingCurrencyRateManager as ConvertingCurrencyRateManagerOriginal;

class ConvertingCurrencyRateManager extends ConvertingCurrencyRateManagerOriginal
{
    /**
     * ConvertingCoursesManager constructor.
     * @param DriverInterface $coin_driver
     */
    public function __construct(DriverContract $coin_driver, $model, $payment_systems)
    {
        $this->parser          = $coin_driver;
        $this->model           = $model;
        $this->payment_systems = $payment_systems;
    }
}