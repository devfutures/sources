<?php

namespace App\LibrariesAir\ConvertingCurrencyRate;

use Emitters\Contracts\ConvertingCurrencyRate\DriverContract;
use Emitters\ConvertingCurrencyRate\ConvertingCurrencyRateManager as ConvertingCurrencyRateManagerOriginal;

class ConvertingCurrencyRateManager extends ConvertingCurrencyRateManagerOriginal {
    /**
     * ConvertingCoursesManager constructor.
     * @param DriverContract $coin_driver
     * @param DriverContract $fiat_driver
     * @param $model
     * @param $payment_systems
     */
    public function __construct(DriverContract $coin_driver, DriverContract $fiat_driver, $model, $payment_systems) {
        $this->parser          = $coin_driver;
        $this->fiatParser      = $fiat_driver;
        $this->model           = $model;
        $this->payment_systems = $payment_systems;
    }
}