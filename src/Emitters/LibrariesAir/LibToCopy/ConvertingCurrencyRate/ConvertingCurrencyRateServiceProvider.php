<?php

namespace App\LibrariesAir\ConvertingCurrencyRate;

use Emitters\ConvertingCurrencyRate\Drivers\ApiExchangerRateDriver;
use Emitters\ConvertingCurrencyRate\Drivers\CoinMarketCap;
use Illuminate\Support\ServiceProvider;
use Emitters\ConvertingCurrencyRate\Models\Currency_Rate;

class ConvertingCurrencyRateServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ConvertingCurrencyRateManager::class, function () {
            return new ConvertingCurrencyRateManager(new CoinMarketCap(), new ApiExchangerRateDriver(), new Currency_Rate(), paymentsystems());
        });

        $this->app->alias(ConvertingCurrencyRateManager::class, 'libair.converting_currency_rate');
    }
}