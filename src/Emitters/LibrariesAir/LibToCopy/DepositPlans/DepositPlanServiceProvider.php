<?php

namespace App\LibrariesAir\DepositPlans;

use Illuminate\Support\ServiceProvider;
use Emitters\DepositPlans\Models\Deposit_Plans;
class DepositPlansServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(DepositPlansManager::class, function () {
            return new DepositPlansManager(new Deposit_Plans(), resolve('libair.paymentsystems'), resolve('libair.converting_currency_rate'));
        });
        $this->app->alias(DepositPlansManager::class, 'libair.depositplans');
    }
}
