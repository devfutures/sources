<?php

namespace App\LibrariesAir\DepositPlans;

use Emitters\DepositPlans\DepositPlansManager as DepositPlansManagerOriginal;

class DepositPlansManager extends DepositPlansManagerOriginal {
    public function __construct($model, $paymentSystems, $convertingCurrencyRate, $userDeposits) {
        $this->model                  = $model;
        $this->paymentSystems         = $paymentSystems;
        $this->convertingCurrencyRate = $convertingCurrencyRate;
        $this->depositUsers           = $userDeposits;
    }
}