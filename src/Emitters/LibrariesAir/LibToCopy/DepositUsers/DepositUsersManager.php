<?php

namespace App\LibrariesAir\DepositUsers;

use Emitters\DepositUsers\DepositUsersManager as DepositUsersManagerOriginal;

class DepositUsersManager extends DepositUsersManagerOriginal {
    protected $model;
    protected $depositPlans;
    protected $paginateNum = null;
    protected $operationsManager = null;
    protected $balanceManger = null;

    public function __construct($model,$depositPlans,$operationsManager,$balanceManger){
        $this->model = $model;
        $this->depositPlans = $depositPlans;
        $this->paginateNum = config('emitters.paginate_num');
        $this->operationsManager = $operationsManager;
        $this->balanceManger = $balanceManger;
    }
}