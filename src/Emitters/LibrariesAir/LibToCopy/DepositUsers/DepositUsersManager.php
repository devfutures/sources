<?php

namespace App\LibrariesAir\DepositUsers;

use Emitters\DepositUsers\DepositUsersManager as DepositUsersManagerOriginal;

class DepositUsersManager extends DepositUsersManagerOriginal {
    protected $model;
    protected $depositPlans;
	protected $paginateNum = null;
	public function __construct($model, $depositPlans){
        $this->model = $model;
        $this->depositPlans = $depositPlans;
        $this->paginateNum = config('emmiters.paginate_num');
	}
}