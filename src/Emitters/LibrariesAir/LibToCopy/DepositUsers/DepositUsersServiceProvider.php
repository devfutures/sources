<?php

namespace App\LibrariesAir\DepositUsers;

use Emitters\DepositUsers\Models\Deposit_Users;
use Illuminate\Support\ServiceProvider;
class DepositUsersServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            DepositUsersManager::class, function () {
            return new DepositUsersManager(Deposit_Users::class, resolve('libair.depositplans'));
        });

        $this->app->alias(DepositUsersManager::class, 'libair.depositusers');
    }
}
