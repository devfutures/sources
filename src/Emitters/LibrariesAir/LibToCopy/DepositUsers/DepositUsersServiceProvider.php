<?php

namespace App\LibrariesAir\DepositUsers;

use Emitters\DepositUsers\Models\Deposit_Users;
use Illuminate\Support\ServiceProvider;
class DepositUsersServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            DepositUsersManager::class, function () {
            return new DepositUsersManager(Deposit_Users::class, resolve('libair.depositplans'), users_operations(), resolve('librariesair.balance'));
        });

        $this->app->alias(DepositUsersManager::class, 'libair.depositusers');

        $this->app->bind('libair.depositusers.wo.plan', function () {
            return new DepositUsersManager(Deposit_Users::class, null, users_operations(), resolve('librariesair.balance'));
        });
    }
}