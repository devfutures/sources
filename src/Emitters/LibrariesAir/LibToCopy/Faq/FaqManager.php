<?php


namespace App\LibrariesAir\Faq;


use Emitters\Faq\Models\Faqs;
use Emitters\Faq\Models\Faqs_Category;
use Emitters\Faq\Models\Faqs_Content;
use Emitters\Faq\FaqManager as FaqManagerOriginal;

class FaqManager extends FaqManagerOriginal {

    /**
     * FaqManager constructor.
     *
     * @param Faqs $faq
     * @param Faqs_Category $faqsCategory
     * @param Faqs_Content $faqContent
     */
    public function __construct(Faqs $faq, Faqs_Category $faqsCategory, Faqs_Content $faqContent) {
        $this->faqModel         = $faq;
        $this->faqCategoryModel = $faqsCategory;
        $this->faqContentModel  = $faqContent;
    }
}