<?php


namespace App\LibrariesAir\Faq;


use Emitters\Faq\Models\Faqs;
use Emitters\Faq\Models\Faqs_Category;
use Emitters\Faq\Models\Faqs_Content;
use Illuminate\Support\ServiceProvider;

class FaqServiceProvider extends ServiceProvider {
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->app->bind(FaqManager::class, function ($app) {
            return new FaqManager(new Faqs(), new Faqs_Category(), new Faqs_Content());
        });

        $this->app->alias(FaqManager::class, 'librair.faq');
    }
}