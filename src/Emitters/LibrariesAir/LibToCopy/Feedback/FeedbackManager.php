<?php


namespace App\LibrariesAir\Feedback;


use Emitters\Feedback\Models\Feedback;
use Emitters\Feedback\FeedbackManager as FeedbackManagerOriginal;

class FeedbackManager extends FeedbackManagerOriginal {

    /**
     * FeedbackManager constructor.
     * @param Feedback $feedback
     */
    public function __construct(Feedback $feedback) {
        $this->model = $feedback;
    }
}