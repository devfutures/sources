<?php


namespace App\LibrariesAir\Feedback;


use Emitters\Feedback\Models\Feedback;
use Illuminate\Support\ServiceProvider;

class FeedbackServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->app->bind(FeedbackManager::class, function ($app) {
            return new FeedbackManager(new Feedback());
        });

        $this->app->alias(FeedbackManager::class, 'librair.feedback');
    }
}