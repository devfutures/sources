<?php


namespace App\LibrariesAir\Google2FA;


use Emitters\Google2FA\Google2FAManager as Google2FAManagerOriginal;
use App\LibrariesAir\UsersData\UsersDataManager;
use PragmaRX\Google2FA\Google2FA;

class Google2FAManager extends Google2FAManagerOriginal {
    /**
     * GoogleTwoFactoryAuthManager constructor.
     * @param Google2FA $google2FA
     * @param UsersDataManager $manager
     */
    public function __construct(Google2FA $google2FA, UsersDataManager $manager) {
        $this->google2FA       = $google2FA;
        $this->userDataManager = $manager;
    }
}