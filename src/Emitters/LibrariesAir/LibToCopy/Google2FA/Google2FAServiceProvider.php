<?php


namespace App\LibrariesAir\Google2FA;


use Illuminate\Support\ServiceProvider;

class Google2FAServiceProvider extends ServiceProvider {
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->app->bind(Google2FAManager::class, function ($app) {
            return new Google2FAManager(resolve('pragmarx.google2fa'), resolve('libair.usersdata'));
        });

        $this->app->alias(Google2FAManager::class, 'librair.google2fa');
    }
}