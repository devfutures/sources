<?php


namespace App\LibrariesAir\News;

use Emitters\News\Models\News;
use Emitters\News\Models\News_Contents;
use Emitters\News\NewsManager as NewsManagerOriginal;

class NewsManager extends NewsManagerOriginal {

    public function __construct(News $newsModel, News_Contents $newsTranslatesModel) {
        $this->newsModel     = $newsModel;
        $this->contentsModel = $newsTranslatesModel;
    }
}