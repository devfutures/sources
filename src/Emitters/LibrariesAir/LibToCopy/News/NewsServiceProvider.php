<?php


namespace App\LibrariesAir\News;

use Emitters\News\Models\News;
use Emitters\News\Models\News_Contents;
use Illuminate\Support\ServiceProvider;


class NewsServiceProvider extends ServiceProvider {
    public function register() {
        $this->app->bind(NewsManager::class, function () {
            return new NewsManager(new News(), new News_Contents());
        });

        $this->app->alias(NewsManager::class, 'libair.news');
    }
}