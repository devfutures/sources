<?php
namespace App\LibrariesAir\PaymentSystems;
use Emitters\PaymentSystems\PaymentSystemsManager as PaymentSystemsManagerOriginal;
class PaymentSystemsManager extends PaymentSystemsManagerOriginal{
	
	public function __construct($model, $usersdata, $balance){
        $this->psModel   = $model;
        $this->usersdata = $usersdata;
        $this->balance   = $balance;
	}
}