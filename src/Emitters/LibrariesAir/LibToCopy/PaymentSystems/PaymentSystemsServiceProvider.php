<?php
namespace App\LibrariesAir\PaymentSystems;

use Illuminate\Support\ServiceProvider;
use Emitters\PaymentSystems\Models\Payment_System;

class PaymentSystemsServiceProvider extends ServiceProvider
{
    public function boot()
    {

    }
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PaymentSystemsManager::class, function () {
            return new PaymentSystemsManager(new Payment_System(), usersdata(), resolve('librariesair.balance'));
        });

        $this->app->alias(PaymentSystemsManager::class, 'libair.paymentsystems');
    }
}
