<?php

namespace App\LibrariesAir\Reviews;

use Emitters\Reviews\Models\Reviews;
use Emitters\Reviews\ReviewsManager as ReviewsManagerOriginal;

class ReviewsManager extends ReviewsManagerOriginal {

    /**
     * ReviewsManager constructor.
     * @param Reviews $reviews
     */
    public function __construct(Reviews $reviews) {
        $this->reviewModel = $reviews;
    }
}