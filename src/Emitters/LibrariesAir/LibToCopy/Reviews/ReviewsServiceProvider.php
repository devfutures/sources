<?php


namespace App\LibrariesAir\Reviews;

use Emitters\Reviews\Models\Reviews;
use Illuminate\Support\ServiceProvider;

class ReviewsServiceProvider extends ServiceProvider {
    public function boot() {
        //
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->app->bind(ReviewsManager::class, function () {
            return new ReviewsManager(new Reviews());
        });

        $this->app->alias(ReviewsManager::class, 'libair.reviews');
    }
}