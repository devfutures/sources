<?php


namespace App\LibrariesAir\Statistics;


use Emitters\ConvertingCurrencyRate\ConvertingCurrencyRateManager;
use Emitters\PaymentSystems\PaymentSystemsManager;
use Emitters\Statistics\Models\Accounting;
use Emitters\Statistics\StatisticsManager as StatisticsManagerOriginal;
use Emitters\UsersOperations\UsersOperations;

class StatisticsManager extends StatisticsManagerOriginal {

    /**
     * StatisticsManager constructor.
     * @param Accounting                    $accounting
     * @param UsersOperations               $usersOperations
     * @param ConvertingCurrencyRateManager $convertingCurrencyRate
     * @param PaymentSystemsManager         $paymentSystemsManager
     */
    public function __construct(Accounting $accounting, UsersOperations $usersOperations, ConvertingCurrencyRateManager $convertingCurrencyRate, PaymentSystemsManager $paymentSystemsManager) {
        $this->accountingModel        = $accounting;
        $this->userOperations         = $usersOperations;
        $this->convertingCurrencyRate = $convertingCurrencyRate;
        $this->paymentSystemsManager  = $paymentSystemsManager;
    }
}