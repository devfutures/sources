<?php


namespace App\LibrariesAir\Statistics;


use Emitters\Statistics\Models\Accounting;
use Illuminate\Support\ServiceProvider;

class StatisticsServiceProvider extends ServiceProvider {
    public function boot() {
        //
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->app->bind(StatisticsManager::class, function () {
            return new StatisticsManager(new Accounting(), resolve('users_operations'), resolve('libair.converting_currency_rate'), resolve('libair.paymentsystems'));
        });

        $this->app->alias(StatisticsManager::class, 'libair.statistics');
    }
}