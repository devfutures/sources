<?php
namespace App\LibrariesAir\UsersData;

use Emitters\UsersData\UsersDataManager as UsersDataManagerOriginal;

class UsersDataManager extends UsersDataManagerOriginal{
    public function __construct ($walletsModel , $userModel , $libBalance , $userActivity , $userContacts) {
        $this->walletsModel = $walletsModel;
        $this->userModel = $userModel;
        $this->libBalance = $libBalance;
        $this->userActivity = $userActivity;
        $this->userContacts = $userContacts;
    }
}