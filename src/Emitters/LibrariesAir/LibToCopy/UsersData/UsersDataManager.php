<?php
namespace App\LibrariesAir\UsersData;

use Emitters\UsersData\UsersDataManager as UsersDataManagerOriginal;

class UsersDataManager extends UsersDataManagerOriginal{
	public function __construct($walletsModel, $userModel){
		$this->walletsModel = $walletsModel;
		$this->userModel    = $userModel;
	}
}