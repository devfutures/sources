<?php

namespace App\LibrariesAir\UsersData;

use Emitters\UsersData\Models\Users_Activity;
use Emitters\UsersData\Models\Users_Contacts;
use Illuminate\Support\ServiceProvider;
use Emitters\UsersData\Models\Users_Wallet;
use App\User;
class UsersDataServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UsersDataManager::class, function () {
            return new UsersDataManager(
                new Users_Wallet(),
                new User(),
                resolve('librariesair.balance'),
                new Users_Activity(),
                new Users_Contacts()
            );
        });
        $this->app->alias(UsersDataManager::class, 'libair.usersdata');
    }
}
