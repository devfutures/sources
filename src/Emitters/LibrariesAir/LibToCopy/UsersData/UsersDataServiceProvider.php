<?php

namespace App\LibrariesAir\UsersData;

use Illuminate\Support\ServiceProvider;
use Emitters\UsersData\Models\Users_Wallet;
use App\User;
class UsersDataServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UsersDataManager::class, function () {
            return new UsersDataManager(
                new Users_Wallet(), 
                new User()
            );
        });
        $this->app->alias(UsersDataManager::class, 'libair.usersdata');
    }
}
