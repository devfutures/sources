<?php

namespace Emitters\LibrariesAir;

use Illuminate\Support\ServiceProvider;
use Emitters\LibrariesAir\Commands\InstallLibrariesAir;
use Emitters\LibrariesAir\Commands\CreateReadme;
class LibrariesAirServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/LibToCopy/'        => app_path('LibrariesAir'),
            __DIR__.'/config/emitters.php' => config_path('emitters.php'),
        ], 'LibrariesAir');

        require_once __DIR__.'/Helpers.php';
    }

    public function register(){
        $this->commands([
            InstallLibrariesAir::class,
            CreateReadme::class,
        ]);
    }
}
