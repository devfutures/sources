<?php

namespace Emitters\LibrariesAir;

use App\LibrariesAir\PaymentSystems\PaymentSystemsManager;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Emitters\LibrariesAir\Commands\InstallLibrariesAir;
use Emitters\LibrariesAir\Commands\CreateReadme;

class LibrariesAirServiceProvider extends ServiceProvider {
    protected $adminControllerNamespace = 'Emitters\Http\Controllers';
    protected $authControllerNamespace = 'Emitters\Http\Controllers\Auth';
    protected $testControllerNamespace = 'Emitters\Http\Controllers\ForTests';
    protected $clientControllerNamespace = 'Emitters\Http\Controllers\Clients';

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function boot() {
        if(env('REDIRECT_HTTPS')){
            \URL::forceScheme('https');
            $this->app['request']->server->set('HTTPS','on');
        }

        $this->publishes([
            __DIR__ . '/LibToCopy/'          => app_path('LibrariesAir'),
            __DIR__ . '/config/emitters.php' => config_path('emitters.php'),
        ], 'LibrariesAir');

        require_once __DIR__ . '/Helpers.php';

        Route::prefix(config('emitters.admin_panel_prefix'))
            ->middleware(config('emitters.admin_middleware'))
            ->namespace($this->adminControllerNamespace)
            ->group(__DIR__ . '/routes/admin.php');

        Route::prefix(config('emitters.route_prefix').'/auth')
            ->middleware('web')
            ->namespace($this->authControllerNamespace)
            ->group(__DIR__ . '/routes/auth.php');

        Route::middleware(config('emitters.route_prefix').'/web')
            ->namespace($this->clientControllerNamespace)
            ->group(__DIR__ . '/routes/clients.php');

        if(env('APP_ENV') == 'local' OR env('APP_ENV') == 'testing') {
            Route::prefix(config('emitters.route_prefix').'/for_tests')
                ->middleware('web')
                ->namespace($this->testControllerNamespace)
                ->group(__DIR__ . '/routes/for_tests.php');
        }
        Collection::macro('toSmallAmount', function () {
            return $this->map(function ($row) {
                if(array_key_exists('amount', $row->toArray()))
                    $row->amount = df_div((string)$row->amount, (string)config('emitters.bc_big_integer_multiplier'));

                if(array_key_exists('from_amount', $row->toArray()))
                    $row->from_amount = df_div((string)$row->from_amount, (string)config('emitters.bc_big_integer_multiplier'));

                if(array_key_exists('commission', $row->toArray()))
                    $row->commission = df_div((string)$row->commission, (string)config('emitters.bc_big_integer_multiplier'));

                if(array_key_exists('default_amount', $row->toArray()))
                    $row->default_amount = df_div((string)$row->default_amount, (string)config('emitters.bc_big_integer_multiplier'));

                if(array_key_exists('total_amount_accruals', $row->toArray()))
                    $row->total_amount_accruals = df_div((string)$row->total_amount_accruals, (string)config('emitters.bc_big_integer_multiplier'));

                return $row;
            });
        });

        Collection::macro('bindPaymentSystems', function ($payment_systems = null) {
            if($payment_systems == null) $payment_systems = paymentsystems()->get([], null);
            return $this->map(function ($row) use ($payment_systems) {
                if(array_key_exists('payment_system_id', $row->toArray()))
                    $row->payment_system = $payment_systems->where('id', $row->payment_system_id)->first();

                if(array_key_exists('from_payment_system_id', $row->toArray()))
                    $row->from_payment_system = $payment_systems->where('id', $row->from_payment_system_id)->first();
                return $row;
            });
        });
    }

    public function register() {
        $this->mergeConfigFrom(
            __DIR__ . '/config/emitters.php', 'emitters'
        );

        $this->commands([
            InstallLibrariesAir::class,
            CreateReadme::class,
        ]);
    }
}
