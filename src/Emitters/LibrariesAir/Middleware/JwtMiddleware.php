<?php

namespace Emitters\LibrariesAir\Middleware;

use Closure;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;

class JwtMiddleware {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        try {
            $user = JWTAuth::parseToken()->authenticate();
        } catch (Exception | JWTException $e) {
            if ($e instanceof TokenInvalidException) {
                return response()->json(['status' => 'Token is Invalid'], 401);
            } else if ($e instanceof TokenExpiredException) {
                try {
                    $newToken = auth()->refresh(true, true);
                    $user     = \Auth::user();
                    $response = response()->json(['status' => 'Token is Expired', 'token' => respond_with_token($newToken)], 401);
                     if ($user && $user->role_id != 0) {
                         $cookie = \Cookie::make('token', $newToken);
                         $response->cookie($cookie);
                     }
                    return $response;
                } catch (TokenBlacklistedException | TokenExpiredException $exception) {
                    return response()->json(['status' => 'Token blacklisted'], 401);
                }
            } else {
                return response()->json(['status' => 'Authorization Token not found'], 401);
            }
        }
        if($request->header('Content-Language')){
            app()->setLocale($request->header('Content-Language'));
        }
        return $next($request);
    }
}
