# DevFutures

### How to install
```php
composer require devfutures/sources
```
After install execute commands
```php
php artisan libair:install
```

* `config/emitters.php` - copy file
* `App/LibAir/` - copy all libs to directory
* `config/app.php` - change privoder for load libair

```php
php artisan migrate
```
---
### Develope packeges
Install laravel last version
```php
composer create-project --prefer-dist laravel/laravel dev_futures
```
Add docker files to main dir from `devfutures/sources/docker.zip` - unzip

Run docker
```php
docker-compose up -d
docker-compose exec app bash
```
* `http://localhost:3099` - site
* `http://localhost:8989` - phpmyadmin
* `DB name` - `emitters`
* `DB user` - `root`
* `DB password` - `root`

Create dir `packages/` and git clone `devfutures/sources`

Add to file `composer.json`
```json
"autoload": {
	"psr-4": {
		"Emitters\\": "packages/DevFutures/Sources/src/Emitters/"
    }
}
```

Add providers `config/app.php` `providers`
```php
Emitters\PaymentSystems\PaymentSystemsServiceProvider::class,
Emitters\UsersData\UsersDataServiceProvider::class,
Emitters\Balance\BalanceServiceProvider::class,
Emitters\ConvertingCurrencyRate\ConvertingCurrencyRateServiceProvider::class,
Emitters\LibrariesAir\LibrariesAirServiceProvider::class,
Emitters\DepositPlans\DepositPlansServiceProvider::class,
Emitters\DepositUsers\DepositUsersServiceProvider::class,
```
