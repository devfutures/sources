# DevFutures

### How to install
```php
composer require devfutures/sources
```
After install execute commands
```php
php artisan libair:install
```

* `config/emitters.php` - copy file
* `App/LibAir/` - copy all libs to directory
* `config/app.php` - change privoder for load libair

```php
php artisan migrate
```

Add trait to `App\User`

```php
use Tymon\JWTAuth\Contracts\JWTSubject;
use Emitters\UsersData\Traits\AddUpline;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, AddUpline;
```

Open file `App\Http\Kernel.php`
```php
protected $middleware = [
    // ...
    \Barryvdh\Cors\HandleCors::class,
]

protected $routeMiddleware = [
    // ...
    'jwt.verify' => \Emitters\LibrariesAir\Middleware\JwtMiddleware::class,
]
```

---
### Develope packeges
Install laravel last version
```php
composer create-project --prefer-dist laravel/laravel dev_futures
```
Add docker files to main dir from `devfutures/sources/docker.zip` - unzip

Run docker
```php
docker-compose up -d
docker-compose exec app bash
```
* `http://localhost:3099` - site
* `http://localhost:8989` - phpmyadmin
* `DB name` - `emitters`
* `DB user` - `root`
* `DB password` - `root`
* `REDIRECT_HTTPS` - `false`

Create dir `packages/` and git clone `devfutures/sources`

Add to file `composer.json`
```json
"autoload": {
	"psr-4": {
		"Emitters\\": "packages/DevFutures/Sources/src/Emitters/"
    }
}
```

Add providers `config/app.php` `providers`
```php
Emitters\PaymentSystems\PaymentSystemsServiceProvider::class,
Emitters\UsersData\UsersDataServiceProvider::class,
Emitters\Balance\BalanceServiceProvider::class,
Emitters\ConvertingCurrencyRate\ConvertingCurrencyRateServiceProvider::class,
Emitters\LibrariesAir\LibrariesAirServiceProvider::class,
Emitters\DepositPlans\DepositPlansServiceProvider::class,
Emitters\DepositUsers\DepositUsersServiceProvider::class,
Emitters\AdminControl\AdminControlServiceProvider::class,
Emitters\News\NewsManagerServiceProvider::class,
Emitters\Faq\FaqServiceProvider::class,
Emitters\Reviews\ReviewsServiceProvider::class,
Emitters\UsersOperations\UsersOperationsServiceProvider::class,
Emitters\DepositAccruals\DepositAccrualsServiceProvider::class,
Emitters\Reviews\ReviewsServiceProvider::class,
Emitters\Statistics\StatisticsServiceProvider::class,
```

### Database seed's

```php
php artisan db:seed --class='Emitters\PaymentSystems\seed\PaymentSystemSeeder'
php artisan db:seed --class='Emitters\UsersData\seed\CreateAdminUserSeeder'
php artisan db:seed --class='Emitters\Reviews\seed\ReviewsSeeder'
```
