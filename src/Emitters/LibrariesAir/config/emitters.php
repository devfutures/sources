<?php

return [
    'route_prefix' => env('ROUTE_PREFIX', 'api'),
    /**
     * ConvertingCurrencyRate
     */
    'auto_update'               => env('AUTO_UPDATE', true),
    'minutes_update'            => env('MINUTES_UPDATE', 30),
    'default_currency_systems'  => env('DEFAULT_CURRENCY_SYSTEMS', 'USD'),
    'default_payment_system_id' => env('DEFAULT_PAYMENT_SYSTEM_ID', 0),

    /**
     * Balance driver
     * getWithUnion - Get results with union all FAST (recommended)
     * getWithMathRealTime - Result math (buy-sell) in real time MIDDLE FAST
     * getWithGroup - Get group results, SLOW work
     */
    'balance_driver'            => env('BALANCE_DRIVER', 'getWithUnion'),

    /**
     * Paginate num
     */
    'paginate_num'              => env('PAGINATE_NUM', 20),

    /**
     * Admin Panel url prefix
     */
    'admin_panel_prefix'        => env('ADMIN_PANEL_PREFIX', 'admin'),

    /**
     * Admin page middleware
     * available web|jwt.verify
     */
    'admin_middleware'          => [
        'web', 'jwt.verify', 'role.access'
    ],

    /**
     * Personal area after auth
     * available web|auth|jwt.verify
     */
    'office_middleware'         => [
        'jwt.verify'
    ],

    /**
     * BCMath scale value
     */
    'bc_scale'                  => env('BC_SCALE', 8),
    'bc_big_integer_multiplier' => env('BC_BIG_INTEGER_MULTIPLIER', 100000000),

    /**
     * NEWS
     * -------------------------------------------------------------------------------------------------
     * news_pagination - amount of items per page (uses in getNewsForPublish)
     * news_description_length - amount of characters in description of news (uses in getNewsForPublish)
     * news_image_storage_path - path to image folder for news ('public/news')
     * -------------------------------------------------------------------------------------------------
     */
    'news_pagination'           => env('NEWS_PAGINATION', 6),
    'news_description_length'   => env('NEWS_DESCRIPTION_LENGTH', 200),
    'news_image_storage_path'   => './public/news',

    /**
     * PAYMENT_SYSTEM
     */
    'payment_system'            => [
        /**
         * Modules for payment system
         */
        'available_modules' => [
            'bitcoin'      => [
                'Selfreliance\CoinPayments\CoinPayments',
                'Selfreliance\PulseBitcoin\PulseBitcoin',
                'Selfreliance\BitGo\BitGo',
            ],
            'litecoin'     => [
                'Selfreliance\CoinPayments\CoinPayments',
                'Selfreliance\BitGo\BitGo',
            ],
            'ethereum'     => [
                'Selfreliance\CoinPayments\CoinPayments',
                'Selfreliance\BitGo\BitGo',
            ],
            'tether'       => [
                'Selfreliance\CoinPayments\CoinPayments',
            ],
            'perfectmoney' => [
                'Selfreliance\PerfectMoney\PerfectMoney'
            ],
            'payeer'       => [
                'Selfreliance\Payeer\Payeer'
            ],
        ],
        'icon_storage'      => './public/payment_system'
    ],

    /**
     * Action after payment completed
     * ADD_FUNDS_TO_BALANCE - add funds for balance
     * CREATE_DEPOSIT - create deposit for this amount and plan in data_info
     * EXCHANGE - exchange this amount for system payment system
     */
    'after_add_funds'           => env('AFTER_ADD_FUNDS', 'ADD_FUNDS_TO_BALANCE'),

    /**
     * Withdrawal commission in percents, min 0 max 100
     */
    'withdrawal_commission'     => env('WITHDRAWAL_COMMISSION', '0'),

    /**
     * Google re-captcha
     */
    'captcha_status'            => env('CAPTCHA_STATUS', false),
    'captcha_secret'            => env('CAPTCHA_SECRET', 'captcha-secret'),
    'captcha_site_key'          => env('CAPTCHA_SITE_KEY', 'captcha-site-key'),

    /**
     * Google 2fa.
     */
    'google2fa_status'          => env('GOOGLE2FA_STATUS', false),
    'google2fa_features'        => ['login'],
    'google2fa_method'          => 'verifyKeyNever',

    /**
     * Date format for Model Operations
     */
    'operation_date_format' => 'd/m/Y H:i:s',

    'site_settings' => [
        'APP_NAME',
        'AUTO_UPDATE',
        'MINUTES_UPDATE',
        'DEFAULT_CURRENCY_SYSTEMS',
        'DEFAULT_PAYMENT_SYSTEM_ID',
        'BALANCE_DRIVER',
        'PAGINATE_NUM',
        'BC_SCALE',
        'BC_BIG_INTEGER_MULTIPLIER',
        'NEWS_PAGINATION',
        'NEWS_DESCRIPTION_LENGTH',
        'AFTER_ADD_FUNDS',
        'WITHDRAWAL_COMMISSION',
        'CAPTCHA_STATUS',
        'CAPTCHA_SECRET',
        'CAPTCHA_SITE_KEY',
        'GOOGLE2FA_STATUS'
    ]
];