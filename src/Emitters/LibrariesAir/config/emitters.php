<?php

return [
	/**
	 * ConvertingCurrencyRate
	 */
	'auto_update'    => true,
	'minutes_update' => 30,


	/**
	 * Balance driver
	 * getWithUnion - Get results with union all FAST (recomend)
	 * getWithMathRealTime - Result math (buy-sell) in real time MIDDLE FAST
	 * getWithGroup - Get group results, SLOW work
	 */
	'balance_driver' => 'getWithUnion',

	/**
	 * Paginate num
	 */
	'paginate_num' => 20,
];