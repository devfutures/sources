<?php

use Illuminate\Support\Facades\Route;

Route::prefix('admin_control')->group(function () {
    Route::get('index', 'AdminControlController@getRoles')->name('admin_control.index');
    Route::post('create_role', 'AdminControlController@addRole')->name('admin_control.addRole');
    Route::post('set_user_role', 'AdminControlController@setRoleToUser')->name('admin_control.setRoleToUser');
    Route::post('detach_user_role', 'AdminControlController@detachUserRole')->name('admin_control.detachUserRole');
    Route::patch('role/{id}', 'AdminControlController@updateRole')->name('admin_control.updateRole');
    Route::delete('role/{id}', 'AdminControlController@deleteRole')->name('admin_control.deleteRole');
    Route::post('login_with_user', 'LoginWithUserController@login')->name('admin_control.loginWithUser');
    Route::get('settings', 'SettingsController@getEnvironment')->name('admin_control.getEnv');
    Route::post('settings', 'SettingsController@setEnvironmentVariable')->name('admin_control.setEnv');
});

Route::prefix('news')->group(function () {
    Route::post('index', 'NewsController@getAll')->name('news.all');
    Route::post('new', 'NewsController@addNews')->name('news.new');
    Route::post('update', 'NewsController@updateNews')->name('news.update');
    Route::post('delete', 'NewsController@deleteNews')->name('news.delete');
});

Route::prefix('currency_rate')->group(function () {
    Route::get('index', 'CurrencyRateController@index')->name('currencyrate.index');
    Route::post('new', 'CurrencyRateController@store')->name('currencyrate.new');
    Route::patch('edit', 'CurrencyRateController@update')->name('currencyrate.update');
});

Route::prefix('deposit_plans')->group(function () {
    Route::post('index', 'DepositPlansController@index')->name('depositplans.index');
    Route::get('create', 'DepositPlansController@getDataForCreateDepositPlan')->name('depositplans.data');
    Route::post('create', 'DepositPlansController@create')->name('depositplans.create');
    Route::patch('update/{id}', 'DepositPlansController@update')->name('depositplans.update');
    Route::delete('destroy/{id}', 'DepositPlansController@delete')->name('depositplans.delete');
});

Route::prefix('deposit_users')->group(function () {
    Route::post('index', 'DepositUsersController@index')->name('depositsusers.index');
    Route::get('view/{id}', 'DepositUsersController@operations')->name('depositsusers.view');
    Route::patch('update/{id}', 'DepositUsersController@update')->name('depositsusers.update');
    Route::delete('destroy/{id}', 'DepositUsersController@delete')->name('depositsusers.delete');
//    Route::get('create', 'HomeController@index');
//    Route::post('create', 'HomeController@index');
//    Route::get('view/{id}', 'HomeController@index');
//    Route::get('edit/{id}', 'HomeController@index');
//    Route::post('edit/{id}', 'HomeController@index');
//    Route::delete('destroy/{id}', 'HomeController@index');
//

});


Route::prefix('payment_system')->group(function () {
    Route::post('index', 'PaymentSystemController@index')->name('payment_system.index');
    Route::post('sort', 'PaymentSystemController@updateSort')->name('payment_system.sort');
    Route::get('create', 'PaymentSystemController@getModulesList')->name('payment_system.modules');
    Route::post('create', 'PaymentSystemController@create')->name('payment_system.create');
    Route::patch('update/{id}', 'PaymentSystemController@update')->name('payment_system.update');
    Route::delete('delete/{id}', 'PaymentSystemController@delete')->name('payment_system.delete');
});

Route::prefix('faq')->group(function () {
    Route::post('create_category', 'FaqController@createFaqCategory')->name('faq.create.category');
    Route::post('create_faqwcontent', 'FaqController@createFaqWithContent')->name('faq.create.faqwcontent');
    Route::post('index', 'FaqController@getAllFaq')->name('faq.get.all');
    Route::get('categories', 'FaqController@getAllFaqCategory')->name('faq.get.categories');
    Route::post('delete_category', 'FaqController@deleteCategory')->name('faq.delete.category');
    Route::post('delete_faq', 'FaqController@deleteFaq')->name('faq.delete.faq');
    Route::post('updateFaq', 'FaqController@updateFaq')->name('faq.update.faq');
    Route::post('toDraft', 'FaqController@toDraft')->name('faq.update.toDraft');
    Route::post('publish', 'FaqController@publishDrafts')->name('faq.update.publish');
});

Route::prefix('reviews')->group(function () {
    Route::post('index', 'ReviewsController@getReviews')->name('reviews.getAll');
    Route::post('create', 'ReviewsController@addReview')->name('reviews.create');
    Route::patch('update', 'ReviewsController@updateReviews')->name('reviews.update');
    Route::delete('delete', 'ReviewsController@deleteReviews')->name('reviews.delete');
});

Route::prefix('users')->group(function () {
    Route::get('index', 'UsersDataController@index')->name('users.index');
    Route::get('edit/{id}', 'UsersDataController@edit')->name('users.edit');
    Route::post('edit/{id}', 'UsersDataController@update')->name('user.update');
    Route::post('blocked/{id}', 'UsersDataController@blocked')->name('user.blocked');
    Route::post('change_password/{id}', 'UsersDataController@change_password')->name('user.change_password');
    Route::post('save_wallets/{id}', 'UsersDataController@save_wallets')->name('user.save_wallets');
    Route::get('search', 'UsersDataController@search')->name('users.search');
    Route::get('balance_info/{id}', 'UsersDataController@balance_info')->name('users.balance_info');
});

Route::prefix('affiliate')->group(function () {
    Route::get('index', 'AffiliateController@index')->name('affiliate.index');
    Route::post('index', 'AffiliateController@save')->name('affiliate.save');
});

Route::prefix('statistics')->group(function () {
    Route::get('index', 'StatisticsController@index')->name('statistics.index');
    Route::get('graph', 'StatisticsController@graphData')->name('statistics.graph_data');
    Route::post('add', 'StatisticsController@create')->name('statistics.add');
    Route::patch('update', 'StatisticsController@updateAccounting')->name('statistics.update');
    Route::delete('delete/{id}', 'StatisticsController@deleteAccounting')->name('statistics.delete');
    Route::get('balance', 'StatisticsController@getBalance')->name('statistics.balance');
});

Route::prefix('users_devices')->group(function () {
    Route::get('index', 'UserDevicesController@getDevices')->name('users_devices.index');
    Route::post('create', 'UserDevicesController@addDevice')->name('users_devices.create');
    Route::patch('update/{id}', 'UserDevicesController@updateDevice')->name('users_devices.update');
    Route::delete('delete/{id}', 'UserDevicesController@deleteDevice')->name('users_devices.delete');
});

Route::prefix('push_notifications')->group(function () {
    Route::post('send', 'SendNotificationController@send')->name('push_notifications.send');
});

Route::prefix('operations')->group(function () {
    Route::get('index', 'OperationController@index')->name('operations.index');
    Route::put('action', 'OperationController@action')->name('operations.action');
    Route::post('create', 'OperationController@create')->name('operations.create');
    Route::post('status', 'OperationController@status')->name('operations.status');
    Route::patch('edit/{id}', 'OperationController@edit')->name('operations.edit');
    Route::delete('delete/{id}', 'OperationController@destroy')->name('operations.destroy');
    Route::put('massive', 'OperationController@massive')->name('operations.massive');
});

Route::prefix('feedback')->group(function () {
    Route::get('index', 'FeedbackController@index')->name('feedback.index');
    Route::delete('delete', 'FeedbackController@delete')->name('feedback.delete');
    Route::patch('send/{id}', 'FeedbackController@response_answer')->name('feedback.response_answer');
    Route::patch('change_status', 'FeedbackController@change_status')->name('feedback.change_status');
});

Route::prefix('team')->group(function () {
    Route::get('index', 'TeamController@index')->name('team.index');
    Route::get('index/{id}/{level?}', 'TeamController@inspectUserTeam')->name('team.inspect');
    Route::get('byLevel/{id}', 'TeamController@getUserTeamInfoByLevel')->name('team.user.base');
});
