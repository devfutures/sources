<?php

use Illuminate\Support\Facades\Route;

Route::post('/login', 'LoginController@login')->name('login.post');
Route::post('/register', 'RegisterController@register')->name('register.post');
Route::post('/logout', 'LoginController@logout')->name('logout');


Route::post('/password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::post('/password/reset', 'ResetPasswordController@reset')->name('password.reset_post');
Route::get('/password/reset', 'ResetPasswordController@showResetForm')->name('password.reset');

Route::get('/user_info', 'UsersDataController@getUserInfo')->name('user_info')->middleware('jwt.verify');

Route::post('/verification/verify', 'VerificationController@verify')->name('verification.verify');
Route::post('/verification/resend', 'VerificationController@resend')->name('verification.resend');

Route::get('/isAdmin', 'UsersDataController@isAdmin')->name('is_admin');

Route::get('/test_middleware', function () {
    return response('ok');
})->middleware('jwt.verify')->name('test.middleware');
