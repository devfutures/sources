<?php

use Illuminate\Support\Facades\Route;

Route::prefix('bitgo')->group(function () {
    Route::any('block', 'BitGoController@new_block')->name('bitgo.block');
});