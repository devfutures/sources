<?php

use Illuminate\Support\Facades\Route;


Route::post('/create_deposit', 'CreateDepositClientController@create')->name('client.create_deposit');
Route::post('/create_review', 'CreateReviewClientController@create')->name('client.create_review');