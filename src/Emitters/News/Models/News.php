<?php

namespace Emitters\News\Models;

use App\User;
use Emitters\Filters\Traits\AddFilters;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model {
    use SoftDeletes, AddFilters;
    protected $fillable = [
        'image',
        'publish_date',
        'user_id',
        'start_show_date',
        'end_show_date'
    ];

    public function getImageAttribute($value) {
        return $this->attributes['image'] = asset('storage/'.$value);
    }

    public function content() {
        return $this->hasMany(News_Contents::class, 'news_id');
    }

    public function author() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
