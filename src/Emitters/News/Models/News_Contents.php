<?php

namespace Emitters\News\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News_Contents extends Model {
    use SoftDeletes;
    protected $fillable = [
        'news_id',
        'language',
        'title',
        'body',
    ];
}
