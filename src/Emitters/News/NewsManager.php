<?php


namespace Emitters\News;


use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use LaravelGettext;
use Emitters\Filters\Filters;
use Emitters\News\Models\News;
use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image;
use Emitters\Abstracts\News\NewsAbstract;
use Emitters\News\Requests\StoreNewsRequest;
use Illuminate\Pagination\LengthAwarePaginator;

class NewsManager extends NewsAbstract {

    /**
     * @param array $data
     * @param UploadedFile|null $file
     * @return mixed
     */
    public function createNews(array $data, UploadedFile $file = null) {
        $news = $this->storeNews($data['news_data']);
        $this->storeContent($news->id, $data['content_data']);
        if ($file) $this->storeNewsImage($news, $file);
        return $news->load(['content', 'author']);
    }

    public function update(array $newsData, UploadedFile $file = null) {
        $news = $this->updateNews($newsData['news']['id'], $newsData['news']);
        if (array_key_exists('content', $newsData)) {
            $this->contentsModel::query()->where('news_id', $newsData['news']['id'])->delete();
            $this->storeContent($newsData['news']['id'], $newsData['content']);
        }

        if (isset($file)) {
            $this->storeNewsImage($news, $file);
        }

        return $news->load(['content', 'author']);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function storeNews(array $data = null) {
        return (isset($data)) ? $this->newsModel::query()->create($data) : null;
    }

    /**
     * @param int $newsID
     * @param array $contentData
     * @return null
     */
    public function storeContent(int $newsID, array $contentData = null) {
        if (!isset($contentData)) return null;
        /** @var News $news */
        $news = $this->newsModel::query()->find($newsID);
        foreach ($contentData as $contentDatum) {
            $news->content()->updateOrCreate(['language' => $contentDatum['language']], $contentDatum);
        }
        return $news->load('content');
    }

    /**
     * @param $newsID
     * @param array|null $newsData
     * @return News|News[]|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function updateNews($newsID, array $newsData = null) {
        $news = $this->newsModel::with('content')->find($newsID);
        if (isset($newsData)){
            if (array_key_exists('publish_date', $newsData)) {
                $newsData['publish_date'] = Carbon::createFromFormat('d.m.Y', $newsData['publish_date']);
            }
            $news->update($newsData);
        }
        return $news;
    }

    /**
     * @param $contentID
     * @param array|null $contentData
     * @return bool
     */
    public function updateContent($contentID, array $contentData = null) {
        if (isset($contentData)) {
            $this->contentsModel::query()->where('id', $contentID)->update($contentData);
            return true;
        }
        return false;
    }

    /**
     * @param $newsID
     * @param bool $withContent
     */
    public function deleteNews($newsID, $withContent = false) {
        $this->newsModel::destroy($newsID);
        if ($withContent == true) $this->contentsModel::query()->where('news_id', $newsID)->delete();
    }

    /**
     * @param $contentID
     */
    public function deleteContent($contentID) {
        $this->contentsModel::destroy($contentID);
    }

    /**
     * @param array $filters
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getAll(array $filters = []) {
        if(!isset($filters['orderBy'])) $filters['orderBy'] = [['by' => 'created_at', 'direction' => 'desc']];
        $news                = $this->newsModel::filters(new Filters($filters, $this->availableFiltered))->with(['content', 'author'])->paginate(config('emitters.news_pagination'));
        $completeNews        = $this->addMissedContentToNews($news);
        $newsWithDescription = $this->addDescriptionToNews($completeNews);
        return $newsWithDescription->appends($filters);
    }

    /**
     * @param $id
     * @param string $lang
     * @param bool $isAdmin
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function getByID($id, $lang = 'en', $isAdmin = false) {
        $news = ($isAdmin == false) ? $this->newsModel::query()->find($id)->with(['content' => function ($query) use ($lang) {
            $query->where('language', $lang);
        }]) : $this->newsModel::query()->find($id)->with('content');

        return $news;
    }

    /**
     * @param string $lang
     * @return mixed
     */
    public function getNewsForPublish($lang = 'en') {
        $news = $this->newsModel::query()
            ->where([['start_show_date', '<=', Carbon::now()], ['end_show_date', '>=', Carbon::now()]])
            ->orWhere(function ($query) {
                $query->whereNull('start_show_date')->whereNull('end_show_date');
            })
            ->orWhere(function ($query) {
                $query->whereNull('start_show_date')->where('end_show_date', '>=', Carbon::now());
            })
            ->orWhere(function ($query) {
                $query->where('start_show_date', '<=', Carbon::now())->whereNull('end_show_date');
            })
            ->with(['content' => function ($query) use ($lang) {
                $query->where('language', $lang);
            }])->paginate(config('emitters.news_pagination'));

        $completeNews        = $this->addMissedContentToNews($news);
        $newsWithDescription = $this->addDescriptionToNews($completeNews);

        return $newsWithDescription;
    }

    /**
     * @param LengthAwarePaginator $news
     * @return LengthAwarePaginator
     */
    protected function addMissedContentToNews(LengthAwarePaginator $news) {
        $news->getCollection()->transform(function ($value) {
            if ($value->content->first() == null) {
                $value->load(['content' => function ($query) {
                    $query->where('language', LaravelGettext::getLocaleLanguage());
                }]);
            }
            return $value;
        });
        return $news;
    }

    /**
     * @param  $news
     * @return LengthAwarePaginator
     */
    protected function addDescriptionToNews($news) {
        $news->getCollection()->transform(function ($value) {
            if($value->content->first()){
                $value->content->first()->setAttribute('description', $this->cutBody($value->content->first()->body));
            }
            return $value;
        });

        return $news;
    }

    /**
     * @param string $body
     * @return string
     */
    protected function cutBody(string $body) {
        $body = strip_tags($body);

        $limitedStr        = substr($body, 0, config('emitters.news_description_length'));
        $wordsInLimitedStr = str_word_count($limitedStr, 2);

        $words_positions    = array_keys($wordsInLimitedStr);
        $last_word_position = end($words_positions);

        $description = substr($limitedStr, 0, $last_word_position) . '...';
        return $description;
    }

    /**
     * @param News $news
     * @param UploadedFile $newsImage
     * @return null
     */
    public function storeNewsImage(News $news, UploadedFile $newsImage) {
        if ($news == null) return null;
//        $imageName = time() . "." . $newsImage->getClientOriginalExtension();
//        $newsImage->store(config('emitters.news_image_storage_path') . "/" . $imageName,'public');
//        $news->update(['image' => $imageName]);
//        $image = Storage::disk('public')->put('news/1', $newsImage);
//        $image = Image::make($newsImage)->save(config('emitters.news_image_storage_path') . "/" . $imageName);

        $image = Storage::disk('public')->put(config('emitters.news_image_storage_path'), $newsImage);
        $news->update(['image' => $image]);

        return $image;
    }

    /**
     * @param User $user
     * @param StoreNewsRequest $request
     * @return array
     */
    public function extractStoreNewsData(User $user, StoreNewsRequest $request) {
        $startShowDate = ($request->has('start_show_date')) ? Carbon::createFromFormat('d.m.Y', $request->input('start_show_date')) : null;
        $endShowDate   = ($request->has('end_show_date')) ? Carbon::createFromFormat('d.m.Y', $request->input('end_show_date')) : null;
        $publishDate   = Carbon::createFromFormat('!d.m.Y', $request->input('publish_date'));

        $newsData = [
            'user_id'         => $user->id,
            'publish_date'    => $publishDate,
            'start_show_date' => $startShowDate,
            'end_show_date'   => $endShowDate
        ];

        $newsContentData = $request->input('content');

        return [
            'news_data'    => $newsData,
            'content_data' => $newsContentData
        ];
    }
}