---
### News
#### Извлечение News
##### Получение через IoC

```php
$convertingCurrencyRateManager = resolve('libair.news');
```

##### Использование в контроллере с помощью type-hinting

```php
use App\LibrariesAir\News\NewsManager;
...
public function index(NewsManager $newsManager) {
    ...
}
```

#### Методы NewsManager

##### Создание новости

```php
$newsManager->createNews(StoreNewsRequest $request);
```
Правила валидации для StoreNewsRequest
```php
[
    'image'           => 'image|mimes:jpeg,png|max:2000',
    'publish_date'    => 'required|date_format: "d.m.Y"',
    'start_show_date' => 'required|date_format: "d.m.Y"',
    'end_show_date'   => 'required|date_format: "d.m.Y"',
    
    'content'            => 'required|array',
    'content.*.title'    => 'required|string',
    'content.*.language' => 'required|string|in:' . implode(',', config('laravel-gettext.supported-locales')),
    'content.*.body'     => 'required|string'
];
```

* return - object News

##### Изменение новости

```php
$newsManager->updateNews($newsID, $newsData);
```

* return - object News

##### Изменение контента
```php
$contentID = 1;
$contentData = [
    'language' => 'ru',
    'title'    => 'some title',
    'body'     => 'text', 
];
$newsManager->updateContent($contentID, $contentData);
```
* return - boolean

##### Уданение новости
```php
$newsManager->deleteNews($newsID, true);
```
* true - удалить новость со всем контентом
* false - удалить только новость

##### Получение всех новостей с контентом на всех языках
```php
$newsManager->getAll($filters);
```
* return - LengthAwarePagination

##### Получение новости по ID
```php
$newsManager->getByID($id, $lang, $isAdmin);
```
* $lang - default 'en'
* $isAdmin - default false
* Третьим аргументом передается булевое значение, если true то новости вернутся с контентом на всех языках.

##### Получение новостей для публикации с пагинацией и описанием
```php
$newsManager->getNewsForPublish($lang);
```

##### Конфигурация новостей
* Значение пагинации, размера описания и storage path для картинок новостей заданы в конфиге emitters.php.
```php
'news_pagination'         => 10,
'news_description_length' => 200,
'news_image_storage_path' => './public/news'
```

