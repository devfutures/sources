<?php

namespace Emitters\News\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreNewsRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'image'           => 'image|mimes:jpeg,png|max:2000',
            'publish_date'    => 'required|date_format: "d.m.Y"',
            'start_show_date' => 'date_format: "d.m.Y"',
            'end_show_date'   => 'date_format: "d.m.Y"',

            'content'            => 'required|array',
            'content.*.title'    => 'required|string',
            'content.*.language' => 'required|string|in:' . implode(',', config('laravel-gettext.supported-locales')),
            'content.*.body'     => 'required|string'
        ];
    }
}
