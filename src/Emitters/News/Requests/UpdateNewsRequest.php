<?php


namespace Emitters\News\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateNewsRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'news'                 => 'required|array',
            'news.id'              => 'required|integer',
            'news.image'           => 'image|mimes:jpeg,png|max:2000',
            'news.publish_date'    => 'date_format: "d.m.Y"',
            'news.start_show_date' => 'date_format: "d.m.Y"',
            'news.end_show_date'   => 'date_format: "d.m.Y"',

            'content'            => 'array',
            'content.*.id'       => 'sometimes|integer',
            'content.*.title'    => 'sometimes|string',
            'content.*.language' => 'sometimes|string|in:' . implode(',', config('laravel-gettext.supported-locales')),
            'content.*.body'     => 'sometimes|string'
        ];
    }
}