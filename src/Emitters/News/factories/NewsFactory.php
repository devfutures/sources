<?php

use Faker\Generator as Faker;


$factory->define(\Emitters\News\Models\News_Contents::class, function (Faker $faker) {
    return [
        'language' => 'en',
        'title'    => $faker->title,
        'body'     => $faker->realText(300),
        'news_id'  => function () {
            return factory(\Emitters\News\Models\News::class)->create()->id;
        }
    ];
});

$factory->define(\Emitters\News\Models\News::class, function (Faker $faker) {
    return [
        'publish_date' => \Carbon\Carbon::now(),
        'user_id'      => function () {
            return factory(\App\User::class)->create()->id;
        }
    ];
});