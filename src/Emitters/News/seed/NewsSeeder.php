<?php

namespace Emitters\News\seed;

use Emitters\News\Models\News_Contents;
use Illuminate\Database\Seeder;

class NewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(News_Contents::class, 30)->create();
    }
}
