<?php

namespace Emitters\PaymentSystems\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment_System extends Model {
    use SoftDeletes;
    protected $appends = ['public_title', 'name_input'];
    protected $fillable = ['title', 'currency'];

    public function getPublicTitleAttribute() {
        if ($this->is_fiat) {
            return $this->title . ' ' . _i("wallet");
        }
        return $this->title . ' ' . _i("address");
    }

    public function getNameInputAttribute() {
        return mb_strtolower($this->title);
    }
}
