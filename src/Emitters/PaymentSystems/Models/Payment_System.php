<?php

namespace Emitters\PaymentSystems\Models;

use Emitters\Filters\Traits\AddFilters;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment_System extends Model {
    use SoftDeletes, AddFilters;
    protected $appends = ['public_title', 'name_input'];
    protected $fillable = [
        'title',
        'currency',
        'scale',
        'class_name',
        'status',
        'is_fiat',
        'redirect_another_site',
        'sort',
        'purchase',
        'min_sum_purchase',
        'max_sum_purchase',
        'withdraw',
        'min_sum_withdraw',
        'max_sum_withdraw',
        'example_wallet',
        'real_payment_system',
        'supported_massive_payment'
    ];

    public function getIconAttribute($value) {
        return $this->attributes['icon'] = asset('storage/payment_system/' . $value);
    }

    public function getPublicTitleAttribute() {
        if ($this->is_fiat) {
            return $this->title . ' ' . _i("wallet");
        }
        return $this->title . ' ' . _i("address");
    }

    public function getNameInputAttribute() {
        return mb_strtolower($this->title);
    }
}
