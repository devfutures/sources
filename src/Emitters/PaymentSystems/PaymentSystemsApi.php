<?php
namespace Emitters\PaymentSystems;

use Cache;
use Emitters\Contracts\PaymentSystems\PaymentSystemsContract;
use Emitters\Abstracts\PaymentSystems\PaymentSystemsAbstract;

use Emitters\Filters\Filters;
use Illuminate\Http\UploadedFile;

class PaymentSystemsApi {

    /**
     * @param $class_name
     * @param $currency
     * @param $supported_massive_payment
     * @param $operation_id
     * @param $amount
     * @param $wallet
     * @return mixed
     * @throws \Exception
     */
    public function doSendMoney($class_name, $currency, $operation_id, $amount, $wallet) {
        if(!class_exists($class_name)) {
            throw new \Exception('PAYMENT_SYSTEM_CLASS_NAME_ERROR');
        }

        $abstract_payment_class = $class_name;
        $ex = app()->make($abstract_payment_class);

        if(!method_exists($ex, 'send_money')) {
            throw new \Exception('PAYMENT_SYSTEM_CLASS_METHOD_ERROR');
        }
        return $ex->send_money($operation_id, $amount, $wallet, $currency);
    }

    /**
     * @param $class_name
     * @param $currency
     * @return mixed
     * @throws \Exception
     */
    public function getBalance($class_name, $currency) {
        $abstract_payment_class = $class_name;
        $ex = app()->make($abstract_payment_class);

        if(!method_exists($ex, 'balance')) {
            throw new \Exception('PAYMENT_SYSTEM_CLASS_METHOD_ERROR');
        }
        return $ex->balance($currency);
    }
}