<?php
namespace Emitters\PaymentSystems;

use Cache;
use Emitters\Contracts\PaymentSystems\PaymentSystemsContract;
use Emitters\Abstracts\PaymentSystems\PaymentSystemsAbstract;

use Emitters\Filters\Filters;
use Illuminate\Http\UploadedFile;

class PaymentSystemsManager extends PaymentSystemsAbstract implements PaymentSystemsContract{
    /**
     * @param array $filters
     * @param int $status
     * @return model results
     */
    public function get(array $filters = [], $status = 1){
        if(!array_key_exists('orderBy', $filters)) $filters['orderBy'] = [['by' => 'sort', 'direction' => 'asc']];
		$ps = $this->psModel::filters(new Filters($filters, $this->availableFilters))
            ->where(function($query) use ($status){
                if($status){
                    $query->where('status', $status);
                }
            })
            ->paginate(config('emitters.paginate_num'))->appends($filters);
        return $ps;
	}

    /**
     * @return mixed
     */
    public function grabAll() {
        return Cache::remember($this->cache_name, 10, function () {
            return $this->get();
        });
    }

    /**
     * @return array
     */
    public function getCounts() {
        $total     = $this->psModel::query()->count();
        $active    = $this->psModel::query()->where('status', 1)->count();
        return [
            'total'     => $total,
            'active'    => $active,
        ];
    }

    /**
     * @param array $data
     */
    public function updateSort(array $data) {
        if(!empty($data)) {
            foreach ($data as $sortData) {
                $this->psModel::where('id', $sortData['id'])->update(['sort' => $sortData['sort']]);
            }
        }
    }

    /**
     * @param        $id
     * @param array $data
     * @param string $field
     *
     * @return true
     */
    public function change($id, array $data, $field = 'id'){
        $ps = $this->psModel::where('id', $id)->update($data);
        return $ps;
	}

    /**
     * @param array $data
     *
     * @return true
     */
    public function addPaymentSystem(array $data){
        $ps = $this->psModel::create($data);
		return $ps;
	}

    /**
     * @param        $id
     * @param string $field
     *
     * @return true
     */
    public function destroy($id, $field = 'id'){
		return $this->psModel::where('id', $id)->delete();
	}

    /**
     * @param        $id
     * @param string $field
     *
     * @return true
     */
    public function restore($id, $field = 'id'){
		return $this->psModel::withTrashed()->where($field, $id)->restore();
	}

    /**
     * @param        $id
     * @param string $field
     *
     * @return one result get by fields
     */
    public function getOne($id, $field = 'id'){
		return $this->psModel::where($field, $id)->first();
	}

    /**
     * @param       $user
     * @param array $sort
     *
     * @return many results
     */
    public function getAll($user, array $sort = ['sort', 'asc']){
		return $this->getWithWalletBalance($user, $sort);
	}

    /**
     * @param $currency
     * @param $transaction
     *
     * @return string
     */
    public function addBlockchainLink($currency, $transaction){
		$currency = mb_strtolower($currency);
		if(!property_exists($this, $currency)) return $currency;
		
		return sprintf($this->{$currency}, $transaction);
	}

    /**
     * @param $currency
     * @param $value
     * @param $amount
     *
     * @return string
     */
    public function addBlockchainPayLink($currency, $value, $amount){
		$currency = mb_strtolower($currency);
		if(!array_key_exists($currency, $this->crypto_payment_link)) return '';

		return sprintf($this->crypto_payment_link[$currency], $value, $amount);
	}

    /**
     * @param $user
     * @param $payment_system
     *
     * @return mixed
     */
    protected function addWallets($user, $payment_system){
		$wallets = $this->usersdata->getWallets($user);
        $payment_system = $this->merge($payment_system, $wallets, 'wallet', 'payment_system_id', 'id', 'wallet');
		return $payment_system;
	}

    /**
     * @param       $user
     * @param array $sort
     *
     * @return model|mixed
     */
    public function getUserWalletWithPaymentSystem($user, array $sort = ['sort', 'asc']) {
        $payment_systems = $this->get($sort);
        $payment_systems = $this->addWallets($user , $payment_systems);
        return $payment_systems;
    }

    /**
     * @param $user
     * @param $payment_system
     *
     * @return mixed
     */
    protected function addBalance($user, $payment_system){
		$balance = $this->balance->getAll($user, $payment_system->pluck('id')->toArray());
        $payment_system = $this->merge($payment_system, $balance, 'balance', 'payment_system_id', 'id', 'balance_now', 0);
        $payment_system->each(function($row){
//            if($row->is_fiat){
                $row->balance = df_div($row->balance, "1", $row->scale);
                $row->default_balance = converting_currency_rate()->convertAmount($row->balance, $row->currency, config('emitters.default_currency_systems'), $row->scale);
//            }
        });
        return $payment_system;
	}

    /**
     * merge addons filed to original object
     * @param  [type] $original      [description]
     * @param  [type] $to_merge      [description]
     * @param  [type] $add_field     [description]
     * @param  [type] $field_to_find [description]
     * @param  [type] $foreign_key   [description]
     * @param  [type] $local_key     [description]
     * @param  [type] $default_value [description]
     * @return [type]                [description]
     */
    public function merge($original, $to_merge, $add_field, $field_to_find, $foreign_key, $local_key, $default_value = null){
        $original->each(function($row) use ($to_merge, $add_field, $field_to_find, $foreign_key, $local_key, $default_value){
            $row->{$add_field} = $default_value;
            if(is_array($to_merge)){
                $find_of = collect($to_merge)->where($field_to_find, $row->{$foreign_key})->first();
                if($find_of)
                    $find_of = $find_of[$local_key];
            }else{
                $find_of = $to_merge->where($field_to_find, $row->{$foreign_key})->first();
                if($find_of) 
                    $find_of = $find_of->{$local_key};
            }
            if($find_of)
                $row->{$add_field} = $find_of;
        });
        return $original;
    }

    /**
     * @param       $user
     * @param array $sort
     *
     * @return model|mixed
     */
    protected function getWithWalletBalance($user, array $sort = ['sort', 'asc']){
		$payment_systems = $this->get($sort);
	    $payment_systems = $this->addWallets($user, $payment_systems);
	    $payment_systems = $this->addBalance($user, $payment_systems);
        $payment_systems = $this->addByOperations('buy', $user, ['ADD_FUNDS'], $payment_systems);
        $payment_systems = $this->addByOperations('sell', $user, ['WITHDRAW'], $payment_systems);
	    return $payment_systems;
	}

    /**
     * @param $payment_systems
     * @param $all_add_funds_user
     *
     * @return mixed
     */
    protected function addByOperations ($type, $user, array $operations, $payment_systems) {
        $all_add_funds_user = $this->balance->allOperationsGroupBalance($type, $user->id, $operations, $payment_systems);
        $payment_systems->each(function ($row) use ($all_add_funds_user, $type, $operations) {
            $varible_name = 'total_'.strtolower(implode('_',$operations));
            $row->{$varible_name} = df_div(0 , "1" , $row->scale);
            if ($find = $all_add_funds_user->where('payment_system_id' , $row->id)->first()) {
                $row->{$varible_name} = df_div($find->aggregate , "1" , $row->scale);
            }
        });
        return $payment_systems;
    }

    /**
     * @param  string $search_key pluck one key
     * @param  array $ignore for filter ignore
     * @param int $fiat
     * @return array             values
     */
    public function getPluckUniqValues($search_key = 'currency', $ignore = ['usd'], $fiat = 0){
        $currencys = $this->get()->where('is_fiat', $fiat)->pluck($search_key)->unique()->map(function ($name) {
            return strtolower($name);
        });

        if(count($ignore)){
            $currencys = $currencys->filter(function($value) use ($ignore){
                if(in_array($value, $ignore)) return false;
                return true;
            });
        }

        return $currencys->values()->all();
    }

    /**
     * Get's available modules for payment_systems from config.
     *
     * @return array
     */
    public function getAvailableModules() {
        return config('emitters.payment_system.available_modules');
    }

    /**
     * @return mixed
     */
    public function getAllSystems() {
        return Cache::remember('systems-without-pagination', 10, function () {
            return $this->psModel::query()->get();
        });
    }
}