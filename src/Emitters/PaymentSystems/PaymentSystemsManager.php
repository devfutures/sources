<?php
namespace Emitters\PaymentSystems;

use Emitters\Contracts\PaymentSystems\PaymentSystemsContract;
use Emitters\Abstracts\PaymentSystems\PaymentSystemsAbstract;
class PaymentSystemsManager extends PaymentSystemsAbstract implements PaymentSystemsContract{
    /**
     * @param array $sort
     *
     * @return model results
     */
    public function get(array $sort = ['sort', 'asc']){
		return $this->psModel::orderBy($sort[0], $sort[1])->get();
	}

    /**
     * @param        $id
     * @param array  $data
     * @param string $field
     *
     * @return true
     */
    public function change($id, array $data, $field = 'id'){
		return $this->psModel::where('id', $id)->update($data);
	}

    /**
     * @param array $data
     *
     * @return true
     */
    public function addPaymentSystem(array $data){
		return $this->psModel::create($data);
	}

    /**
     * @param        $id
     * @param string $field
     *
     * @return true
     */
    public function destroy($id, $field = 'id'){
		return $this->psModel::where($field, $id)->delete();
	}

    /**
     * @param        $id
     * @param string $field
     *
     * @return true
     */
    public function restore($id, $field = 'id'){
		return $this->psModel::withTrashed()->where($field, $id)->restore();
	}

    /**
     * @param        $id
     * @param string $field
     *
     * @return one result get by fields
     */
    public function getOne($id, $field = 'id'){
		return $this->psModel::where($field, $id)->first();
	}

    /**
     * @param       $user
     * @param array $sort
     *
     * @return many results
     */
    public function getAll($user, array $sort = ['sort', 'asc']){
		return $this->getWithWalletBalance($user, $sort);
	}

    /**
     * @param $currency
     * @param $transaction
     *
     * @return string
     */
    public function addBlockchainLink($currency, $transaction){
		$currency = mb_strtolower($currency);
		if(!property_exists($this, $currency)) return $value;
		
		return sprintf($this->{$currency}, $transaction);
	}

    /**
     * @param $currency
     * @param $value
     * @param $amount
     *
     * @return string
     */
    public function addBlockchainPayLink($currency, $value, $amount){
		$currency = mb_strtolower($currency);
		if(!array_key_exists($currency, $this->crypto_payment_link)) return '';

		return sprintf($this->crypto_payment_link[$currency], $value, $amount);
	}

    /**
     * @param $user
     * @param $payment_system
     *
     * @return mixed
     */
    protected function addWallets($user, $payment_system){
		$wallets = $this->usersdata->getWallets($user);
        $payment_system = $this->merge($payment_system, $wallets, 'wallet', 'payment_system_id', 'id', 'wallet');
		return $payment_system;
	}

    /**
     * @param $user
     * @param $payment_system
     *
     * @return mixed
     */
    protected function addBalance($user, $payment_system){
		$balance = $this->balance->getAll($user, $payment_system->pluck('id')->toArray());
        $payment_system = $this->merge($payment_system, $balance, 'balance', 'payment_system_id', 'id', 'balance_now', 0);
        $payment_system->each(function($row){
            if($row->is_fiat){
                $row->balance = bcdiv($row->balance, "1", "2");
            }
        });
        return $payment_system;
	}

    /**
     * merge addons filed to original object
     * @param  [type] $original      [description]
     * @param  [type] $to_merge      [description]
     * @param  [type] $add_field     [description]
     * @param  [type] $field_to_find [description]
     * @param  [type] $foreign_key   [description]
     * @param  [type] $local_key     [description]
     * @param  [type] $default_value [description]
     * @return [type]                [description]
     */
    public function merge($original, $to_merge, $add_field, $field_to_find, $foreign_key, $local_key, $default_value = null){
        $original->each(function($row) use ($to_merge, $add_field, $field_to_find, $foreign_key, $local_key, $default_value){
            $row->{$add_field} = $default_value;
            if(is_array($to_merge)){
                $find_of = collect($to_merge)->where($field_to_find, $row->{$foreign_key})->first();
                if($find_of)
                    $find_of = $find_of[$local_key];
            }else{
                $find_of = $to_merge->where($field_to_find, $row->{$foreign_key})->first();
                if($find_of) 
                    $find_of = $find_of->{$local_key};
            }
            if($find_of)
                $row->{$add_field} = $find_of;
        });
        return $original;
    }

    /**
     * @param       $user
     * @param array $sort
     *
     * @return model|mixed
     */
    protected function getWithWalletBalance($user, array $sort = ['sort', 'asc']){
		$payment_systems = $this->get($sort);
	    $payment_systems = $this->addWallets($user, $payment_systems);
	    $payment_systems = $this->addBalance($user, $payment_systems);
	    return $payment_systems;
	}

    /**
     * @param  string $search_key pluck one key
     * @param  array  $ignore     for filter ignore
     * @return array             values
     */
    public function getPluckUniqValues($search_key = 'currency', $ignore = ['usd']){
        $currencys = $this->get()->pluck($search_key)->unique()->map(function ($name) {
            return strtolower($name);
        });
        
        if(count($ignore)){
            $currencys = $currencys->filter(function($value) use ($ignore){
                if(in_array($value, $ignore)) return false;
                return true;
            });
        }

        return $currencys->values()->all();
    }
}