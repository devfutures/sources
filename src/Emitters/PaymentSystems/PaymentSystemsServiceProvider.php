<?php

namespace Emitters\PaymentSystems;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory as EloquentFactory;

class PaymentSystemsServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/migrations');

        $this->publishes([
            __DIR__.'/migrations/' => database_path('migrations')
        ], 'migrations');

        $this->app->make(EloquentFactory::class)->load(__DIR__ . '/factories/');
    }

    public function register()
    {
        $this->app->bind(PaymentSystemsApi::class, function () {
            return new PaymentSystemsApi();
        });

        $this->app->alias(PaymentSystemsApi::class, 'payment_systems.api');
    }
}
