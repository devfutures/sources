---
### Payment Systems
Использование в коде
```php
use App\LibrariesAir\PaymentSystems\PaymentSystemsManager;
function load(PaymentSystemsManager $payment_systems_manager){
	// Get list from db
    $payment_systems_manager->get();
}

$payment_systems_manager = app()->make('libair.paymentsystems');
// Get list from db
$payment_systems_manager->get();

// Get balance and users wallet
$payment_systems_manager->getAll(App\User::find(1));
```


Получение не модифицированого списка платежных систем
```php
public function get(array $sort = ['sort', 'asc']);
```
Изменить платежную систему по идентификатору
```php
public function change($id, array $data, $field = 'id');
```
Добавить платежную систему
```php
public function addPaymentSystem(array $data);
```
Удалить платежную систему по идентификатору
```php
public function destroy($id, $field = 'id');
```
Востановить удаленную платежную систему
```php
public function restore($id, $field = 'id');
```
Получить платежную систему по идентификатору 1 результат
```php
public function getOne($id, $field = 'id');
```
Получить платежные системы с кошельками и балансом
```php
public function getAll($user, array $sort = ['sort', 'asc']);
```
Модификатор добавления ссылки на blockchain
```php
public function addBlockchainLink($currency, $transaction);
```
Модификатор ссылки для платежа в блокчейне
```php
public function addBlockchainPayLink($currency, $value, $amount);
```
Платежные системы с кошельками пользователя
```php
abstract protected function addWallets($user, $payment_system);
```
Платежные системы с балансом
```php
abstract protected function addBalance($user, $payment_system);
```
Получить платежные системы с балансом и кошельками
```php
abstract protected function getWithWalletBalance($user, array $sort);
```
Получить одно уникальное значение из платежных систем, например только уникальные title или currency, вторым аргументом массив который нужно исключить из конечного результата
```php
public function getPluckUniqValues($search_key = 'currency', $ignore = ['usd']);
```