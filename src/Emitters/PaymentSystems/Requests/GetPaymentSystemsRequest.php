<?php


namespace Emitters\PaymentSystems\Requests;


use Illuminate\Foundation\Http\FormRequest;

class GetPaymentSystemsRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'where'         => 'array',
            'where.*.field' => 'string|in:status,purchase,withdraw',
            'where.*.value' => 'integer|in:1,0',

            'amount'              => 'array',
            'amount.*.fields'     => 'array',
            'amount.*.fields.min' => 'string|in:min_sum_purchase,min_sum_withdraw',
            'amount.*.fields.max' => 'string|in:max_sum_purchase,max_sum_withdraw',
            'amount.*.min'        => 'numeric',
            'amount.*.max'        => 'numeric',

            'orderBy'           => 'array',
            'orderBy.by'        => 'string|in:id,status,scale',
            'orderBy.direction' => 'string|in:asc,desc'
        ];
    }
}