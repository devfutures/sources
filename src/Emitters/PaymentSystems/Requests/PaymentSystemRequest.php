<?php


namespace Emitters\PaymentSystems\Requests;


use Illuminate\Foundation\Http\FormRequest;

class PaymentSystemRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'title'                     => 'required|string',
            'class_name'                => 'required|string',
            'currency'                  => 'required|string',
            'is_fiat'                   => 'required|in:1,0',
            'scale'                     => 'required|integer|max:8',
            'example_wallet'            => 'present',
            'status'                    => 'integer|in:1,0',
            'sort'                      => 'integer',
            'withdraw'                  => 'integer|in:1,0',
            'purchase'                  => 'integer|in:1,0',
            'redirect_another_site'     => 'integer|in:1,0',
            'min_sum_purchase'          => 'required|numeric',
            'max_sum_purchase'          => 'required|numeric',
            'min_sum_withdraw'          => 'required|numeric',
            'max_sum_withdraw'          => 'required|numeric',
            'supported_massive_payment' => 'integer|in:1,0',
            'real_payment_system'       => 'integer|in:1,0',
        ];
    }
}