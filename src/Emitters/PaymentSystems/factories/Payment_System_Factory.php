<?php

use App\User;
use Faker\Generator as Faker;

$factory->define(\Emitters\PaymentSystems\Models\Payment_System::class, function (Faker $faker) {
    $title  = $faker->randomElement(array_keys(config('emitters.payment_system.available_modules')));
    $module = $faker->randomElement(config('emitters.payment_system.available_modules')[$title]);

    return [
        'title'                     => $title,
        'class_name'                => $module,
        'currency'                  => 'usd',
        'status'                    => $faker->numberBetween(0, 1),
        'is_fiat'                   => $faker->numberBetween(0, 1),
        'supported_massive_payment' => 0,
        'redirect_another_site'     => $faker->numberBetween(0, 1),
        'sort'                      => 1,
        'purchase'                  => $faker->numberBetween(0, 1),
        'min_sum_purchase'          => $faker->numberBetween(10, 500),
        'max_sum_purchase'          => $faker->numberBetween(500, 1000),
        'withdraw'                  => $faker->numberBetween(0, 1),
        'min_sum_withdraw'          => $faker->numberBetween(5, 250),
        'max_sum_withdraw'          => $faker->numberBetween(250, 500),
        'scale'                     => 8,
        'example_wallet'            => \Illuminate\Support\Str::random(16),
        'real_payment_system'       => 1
    ];
});