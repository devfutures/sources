<?php

use App\User;
use Faker\Generator as Faker;

$factory->define(\Emitters\PaymentSystems\Models\Payment_System::class, function (Faker $faker) {

    return [
        'title'    => 'Bitcoin',
        'currency' => 'btc',
    ];
});