<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentSystemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment__systems', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');

            $table->string('class_name')->default("");
            
            $table->string('currency');
            $table->tinyInteger('status')->default(1);
            $table->tinyInteger('is_fiat')->default(0)->index();
            $table->tinyInteger('supported_massive_payment')->default(0)->index();

            $table->tinyInteger('redirect_another_site')->default(0);
            $table->integer('sort')->default(0)->index();

            $table->tinyInteger('purchase')->default(0)->index();
            $table->double('min_sum_purchase',14,8)->default(0);
            $table->double('max_sum_purchase',14,8)->default(0);

            $table->tinyInteger('withdraw')->default(0)->index();
            $table->double('min_sum_withdraw',14,8)->default(0);
            $table->double('max_sum_withdraw',14,8)->default(0);

            $table->tinyInteger('scale')->default(8);
            $table->boolean('real_payment_system')->default(0)->index();

            $table->string('example_wallet')->nullable();
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment__systems');
    }
}
