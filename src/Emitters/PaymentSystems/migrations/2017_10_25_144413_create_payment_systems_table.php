<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentSystemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment__systems', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('icon')->default("");

            $table->string('class_name')->default("");
            
            $table->string('currency');
            $table->tinyInteger('is_fiat')->default(0)->index();
            $table->tinyInteger('redirect_another_site')->default(0);
            $table->integer('sort')->default(0)->index();

            $table->tinyInteger('purchase')->default(0)->index();
            $table->double('min_sum_purchase',14,3)->default(0);
            $table->double('max_sum_purchase',14,3)->default(0);

            $table->tinyInteger('withdraw')->default(0)->index();
            $table->double('min_sum_withdraw',14,3)->default(0);
            $table->double('max_sum_withdraw',14,3)->default(0);

            $table->string('example_wallet')->default("");
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment__systems');
    }
}
