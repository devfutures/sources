<?php

namespace Emitters\PaymentSystems\seed;

use Illuminate\Database\Seeder;

class PaymentSystemSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('Emitters\PaymentSystems\Models\Payment_System')->create([
            'title' => 'Bitcoin', 'class_name' => 'Selfreliance\BitGo\BitGo', 'currency' => 'BTC', 'status' => 1, 'purchase' => 1, 'withdraw' => 1, 'is_fiat' => 0, 'min_sum_purchase' => 0.005, 'max_sum_purchase' => 0, 'supported_massive_payment' => 1
        ]);

        factory('Emitters\PaymentSystems\Models\Payment_System')->create([
            'title' => 'Ethereum', 'class_name' => 'Selfreliance\CoinPayments\CoinPayments', 'currency' => 'ETH', 'status' => 1, 'purchase' => 1, 'withdraw' => 1, 'is_fiat' => 0, 'min_sum_purchase' => 0.08, 'max_sum_purchase' => 700, 'supported_massive_payment' => 1
        ]);

        factory('Emitters\PaymentSystems\Models\Payment_System')->create([
            'title' => 'Ripple', 'class_name' => 'Selfreliance\CoinPayments\CoinPayments', 'currency' => 'XRP', 'status' => 1, 'purchase' => 1, 'withdraw' => 1, 'is_fiat' => 0, 'min_sum_purchase' => 100, 'max_sum_purchase' => 0, 'supported_massive_payment' => 1
        ]);

        factory('Emitters\PaymentSystems\Models\Payment_System')->create([
            'title' => 'Litecoin', 'class_name' => 'Selfreliance\BitGo\BitGo', 'currency' => 'LTC', 'status' => 1, 'purchase' => 1, 'withdraw' => 1, 'is_fiat' => 0, 'min_sum_purchase' => 0.22, 'max_sum_purchase' => 2000, 'supported_massive_payment' => 1
        ]);

        factory('Emitters\PaymentSystems\Models\Payment_System')->create([
            'title' => 'Dashcoin', 'class_name' => 'Selfreliance\BitGo\BitGo', 'currency' => 'DASH', 'status' => 1, 'purchase' => 1, 'withdraw' => 1, 'is_fiat' => 0, 'min_sum_purchase' => 0.22, 'max_sum_purchase' => 2000, 'supported_massive_payment' => 1
        ]);

        factory('Emitters\PaymentSystems\Models\Payment_System')->create([
            'title' => 'BitcoinCash', 'class_name' => 'Selfreliance\BitGo\BitGo', 'currency' => 'BCH', 'status' => 1, 'purchase' => 1, 'withdraw' => 1, 'is_fiat' => 0, 'min_sum_purchase' => 0.22, 'max_sum_purchase' => 2000, 'supported_massive_payment' => 1
        ]);

        factory('Emitters\PaymentSystems\Models\Payment_System')->create([
            'title' => 'Stellar', 'class_name' => 'Selfreliance\BitGo\BitGo', 'currency' => 'XLM', 'status' => 1, 'purchase' => 1, 'withdraw' => 1, 'is_fiat' => 0, 'min_sum_purchase' => 100, 'max_sum_purchase' => 0, 'supported_massive_payment' => 1
        ]);

        factory('Emitters\PaymentSystems\Models\Payment_System')->create([
            'title' => 'Zcash', 'class_name' => 'Selfreliance\BitGo\BitGo', 'currency' => 'ZEC', 'status' => 1, 'purchase' => 1, 'withdraw' => 1, 'is_fiat' => 0, 'min_sum_purchase' => 100, 'max_sum_purchase' => 0, 'supported_massive_payment' => 1
        ]);

        factory('Emitters\PaymentSystems\Models\Payment_System')->create([
            'title' => 'PerfectMoney', 'class_name' => 'Selfreliance\PerfectMoney\PerfectMoney', 'currency' => 'USD', 'status' => 1, 'purchase' => 1, 'withdraw' => 1, 'scale' => 2, 'is_fiat' => 1, 'min_sum_purchase' => 10, 'max_sum_purchase' => 100000, 'supported_massive_payment' => 0
        ]);

        factory('Emitters\PaymentSystems\Models\Payment_System')->create([
            'title' => 'Payeer', 'class_name' => '', 'currency' => 'USD', 'status' => 1, 'purchase' => 1, 'withdraw' => 1, 'scale' => 2, 'is_fiat' => 1, 'min_sum_purchase' => 10, 'max_sum_purchase' => 100000, 'supported_massive_payment' => 0
        ]);

        factory('Emitters\PaymentSystems\Models\Payment_System')->create([
            'title' => 'Tether', 'class_name' => 'Selfreliance\CoinPayments\CoinPayments', 'currency' => 'USDT', 'status' => 1, 'purchase' => 1, 'withdraw' => 1, 'is_fiat' => 0, 'min_sum_purchase' => 100, 'max_sum_purchase' => 0, 'supported_massive_payment' => 1
        ]);
    }
}
