<?php

namespace Emitters\Reviews\Models;

use App\User;
use Emitters\Filters\Traits\AddFilters;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reviews extends Model {
    use SoftDeletes, AddFilters;

    protected $fillable = [
        'user_id',
        'review_text',
        'youtube_link',
        'status'
    ];

    public function author() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
