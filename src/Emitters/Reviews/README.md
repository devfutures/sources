---
### Reviews
#### Извлечение ReviewsManager
##### Получение через IoC
```php
$reviewsManager = resolve('librair.reviews');
```
##### Использование в контроллере с помощью type-hinting
```php
use App\LibrariesAir\Reviews\ReviewsManager;
...
public function index(ReviewsManager $reviewsManager) {
    ...
}
```
#### Методы ReviewsManager

##### Создание отзыва 
```php
$review = [
    'review_text' => 'Abc',
    'youtube_link' => 'https://www.youtube.com/watch?v=2hbdaKpsfZH4',
    'status' => 'ON_CHECK' // доступно ON_CHECK,APPROVED,DECLINED,DELETED
];
$reviewManager->createReview(Auth::user(), $review);
```

##### Получение всех отзывов с заданием сортировки и фильтров
```php
$reviewManager->getReviews($filters);
```
* $filters - массив фильтров
* return type - Collection

##### Изменение отзыва
```php
$faqManager->updateReview($reviewID, $reviewData);
```
* return type - int

##### Удаление отзыва
```php
$faqManager->deleteReview($id);
```
* return type - int

