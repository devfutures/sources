<?php

namespace Emitters\Reviews\Requests;


use Illuminate\Foundation\Http\FormRequest;

class AddReviewsRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return (auth()->check()) ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'review_text'  => 'required_without:youtube_link|string|min:50',
            'youtube_link' => 'required_without:review_text|url',
        ];
    }
}