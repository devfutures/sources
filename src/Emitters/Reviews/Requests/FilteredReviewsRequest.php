<?php

namespace Emitters\Reviews\Requests;


use Illuminate\Foundation\Http\FormRequest;

class FilteredReviewsRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'whereIn'           => 'array',
            'whereIn.*.field'   => 'string|in:status',
            'whereIn.*.value'   => 'array',
            'whereIn.*.value.*' => 'string|in:ON_CHECK,DECLINED,APPROVED,DELETED',

            'dateBetween'                    => 'array',
            'dateBetween.*.from_date_column' => 'string|in:created_at',
            'dateBetween.*.to_date_column'   => 'string|in:created_at',
            'dateBetween.*.from'             => 'date_format: "d.m.Y"',
            'dateBetween.*.to'               => 'date_format: "d.m.Y"',

            'whereNotNull'         => 'array',
            'whereNotNull.*.field' => 'string|in:youtube_link',

            'orderBy'             => 'array',
            'orderBy.*.by'        => 'string|in:created_at',
            'orderBy.*.direction' => 'string|in:asc,desc'
        ];
    }
}