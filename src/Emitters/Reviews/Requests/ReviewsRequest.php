<?php

namespace Emitters\Reviews\Requests;


use Illuminate\Foundation\Http\FormRequest;

class ReviewsRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            '*.id'           => 'integer|exists:reviews,id',
            '*.review_text'  => 'string',
            '*.youtube_link' => 'url',
            '*.status'       => 'string|in:ON_CHECK,DECLINED,APPROVED,DELETED'
        ];
    }
}