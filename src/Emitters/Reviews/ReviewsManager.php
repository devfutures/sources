<?php

namespace Emitters\Reviews;


use App\User;
use Emitters\Abstracts\Reviews\ReviewsAbstract;
use Emitters\Filters\Filters;
use Illuminate\Support\Facades\DB;

class ReviewsManager extends ReviewsAbstract {

    /**
     * @param User $user
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function createReview(User $user, array $data) {
        $data['user_id'] = $user->id;
        return $this->reviewModel::query()->create($data);
    }

    /**
     * @param array $filters
     * @return mixed
     */
    public function getReviews($filters = []) {
        return $this->reviewModel::filters(new Filters($filters, $this->availableFilters))->with('author')->paginate(config('emitters.paginate_num'))->appends($filters);
    }

    /**
     * @return array
     */
    public function countByStatus() {
        $counts      = DB::table('reviews')
            ->selectRaw('status, count(*) as total')
            ->groupBy('status')
            ->get();
        $statusArray = [
            'on_check_count' => $counts->where('status', 'ON_CHECK')->first()->total ?? 0,
            'approved_count' => $counts->where('status', 'APPROVED')->first()->total ?? 0,
            'declined_count' => $counts->where('status', 'DECLINED')->first()->total ?? 0,
            'deleted_count'  => $counts->where('status', 'DELETED')->first()->total ?? 0,
        ];
        return $statusArray;
    }

    /**
     * @param int $reviewID
     * @param array $data
     * @return int
     */
    public function updateReview(int $reviewID, array $data) {
        return $this->reviewModel::query()->where('id', $reviewID)->update($data);
    }

    /**
     * @param int $id
     * @return int
     */
    public function deleteReview(int $id) {
        return $this->reviewModel::destroy($id);
    }
}