<?php

use Faker\Generator as Faker;

$factory->define(\Emitters\Reviews\Models\Reviews::class, function (Faker $faker) {
    $statusEnum = ['ON_CHECK', 'DELETED', 'DECLINED', 'APPROVED'];

    return [
        'user_id'      => function () {
            return factory(\App\User::class)->create()->id;
        },
        'review_text'  => $faker->realText(300),
        'youtube_link' => $faker->imageUrl(),
        'status'       => array_random($statusEnum, 1)[0]
    ];
});