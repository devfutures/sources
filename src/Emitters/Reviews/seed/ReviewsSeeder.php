<?php

namespace Emitters\Reviews\seed;

use Emitters\Reviews\Models\Reviews;
use Illuminate\Database\Seeder;

class ReviewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Reviews::class, 30)->create();
    }
}
