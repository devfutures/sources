<?php

namespace Emitters\Statistics\Models;

use Carbon\Carbon;
use Emitters\Filters\Traits\AddFilters;
use Illuminate\Database\Eloquent\Model;
use Emitters\PaymentSystems\Models\Payment_System;
use Illuminate\Database\Eloquent\SoftDeletes;

class Accounting extends Model {
    use SoftDeletes, AddFilters;

    protected $fillable = [
        'payment_system_id',
        'type',
        'source',
        'amount_usd',
        'amount_ps',
        'return_amount_usd',
        'period_from',
        'period_to'
    ];

    public function getPeriodFromAttribute($value) {
        return $value ? Carbon::createFromFormat('Y-m-d H:i:s', $value) : null;
    }

    public function getPeriodToAttribute($value) {
        return $value ? Carbon::createFromFormat('Y-m-d H:i:s', $value) : null;
    }

    public function setAmountUsdAttribute($value) {
        $this->attributes['amount_usd'] = df_mul($value, config('emitters.bc_big_integer_multiplier'));
    }

    public function getAmountUsdAttribute($value) {
        return df_div($value, config('emitters.bc_big_integer_multiplier'), 2);
    }

    public function setAmountPsAttribute($value) {
        $this->attributes['amount_ps'] = df_mul($value, config('emitters.bc_big_integer_multiplier'));
    }

    public function getAmountPsAttribute($value) {
        return df_div($value, config('emitters.bc_big_integer_multiplier'), $this->system()->first()->scale);
    }

    public function setReturnAmountUsdAttribute($value) {
        $this->attributes['return_amount_usd'] = df_mul($value, config('emitters.bc_big_integer_multiplier'));
    }

    public function getReturnAmountUsdAttribute($value) {
        return df_div($value, config('emitters.bc_big_integer_multiplier'), 2);
    }

    public function system() {
        return $this->hasOne(Payment_System::class, 'id', 'payment_system_id');
    }
}
