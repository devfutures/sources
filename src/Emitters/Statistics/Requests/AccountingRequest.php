<?php


namespace Emitters\Statistics\Requests;


use Illuminate\Foundation\Http\FormRequest;

class AccountingRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'id'                => 'sometimes|integer|exists:accountings,id',
            'payment_system_id' => 'required|integer|exists:payment__systems,id',
            'type'              => 'required|string|in:INCOME,EXPENSE',
            'source'            => 'required|string',
            'amount_usd'        => 'numeric',
            'amount_ps'         => 'numeric',
            'return_amount_usd' => 'numeric',
            'period_from'       => 'date_format: "d.m.Y"',
            'period_to'         => 'date_format: "d.m.Y"',
            'need_transfer'     => 'sometimes|required|in:0,1',
            'wallet_recipient'  => 'required_if:need_transfer,1',
        ];
    }
}