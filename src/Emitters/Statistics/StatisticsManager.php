<?php

namespace Emitters\Statistics;

use Cache;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Collection;
use Emitters\Abstracts\Statistics\StatisticsAbstract;

class StatisticsManager extends StatisticsAbstract {

    /**
     * @param $periodFrom
     * @param $periodTo
     * @param bool $with_hidden
     * @return Collection
     */
    public function getGraphData($periodFrom, $periodTo, $with_hidden = false) {
        $key       = $periodFrom . '-' . $periodTo;
        $graphData = Cache::remember($key, 15, function () use ($periodFrom, $periodTo, $with_hidden) {
            $operationsArray = ['WITHDRAW', 'ADD_FUNDS'];
            $operations      = $this->userOperations->getOperationsByPeriod($operationsArray, $periodFrom, $periodTo, $with_hidden);
            $operations->transform(function ($item) {
                $item->default_amount = convert_from_bigInteger($item->default_amount);
                return $item;
            });

            $operations = $operations->groupBy(function ($item) {
                return Carbon::createFromFormat(config('emitters.operation_date_format'), $item->created_at)->startOfDay()->timestamp;
            });

            $operations->transform(function ($item) {
                return $item->groupBy('operation')->transform(function ($item) {
                    return number($item->sum('default_amount'));
                });
            });

            $operations->transform(function ($item) use ($operationsArray) {
                foreach ($operationsArray as $operation) {
                    if (!$item->has($operation)) {
                        $item->put($operation, '0.00');
                    }
                }
                return $item;
            });

            $period          = CarbonPeriod::create($periodFrom, $periodTo);
            $datesCollection = collect();
            foreach ($period as $date) {
                $datesCollection->put($date->startOfDay()->timestamp, collect([
                    'ADD_FUNDS' => '0.00',
                    'WITHDRAW'  => '0.00'
                ]));
            }

            $datesCollection->each(function ($value, $date) use ($operations) {
                if (!$operations->has($date)) {
                    $operations->put($date, $value);
                }
            });

            $operations->sort();
            return $operations;
        });

        return $graphData;
    }

    /**
     * @param int $year
     * @return mixed
     */
    public function getAccounting(int $year = null) {
        $year           = $year ?? Carbon::now()->format('Y');
        $paymentSystems = $this->paymentSystemsManager->getAllSystems();

        /** @var Collection $accounting */
        $accounting = $this->accountingModel::whereYear('period_from', $year)->get();
        $accounting->transform(function ($item) use ($paymentSystems) {
            $item->payment_system = $paymentSystems->where('id', $item->payment_system_id)->first();
            return $item;
        });

        $data            = collect();
        $monthCollection = collect();
        foreach ($this->months as $key => $month) {
            $monthDate    = Carbon::createFromFormat('F', $month)->year($year);
            $monthIncomes = $accounting
                ->where('period_from', '>=', $monthDate->startOfMonth())
                ->where('period_from', '<=', $monthDate->endOfMonth())
                ->where('type', 'INCOME');

            $monthExpenses = $accounting
                ->where('period_from', '>=', $monthDate->startOfMonth())
                ->where('period_from', '<=', $monthDate->endOfMonth())
                ->where('type', 'EXPENSE');

            $monthCollection->put($month, [
                'INCOME'               => $monthIncomes->values()->all(),
                'EXPENSE'              => $monthExpenses->values()->all(),
                'total_month_incomes'  => $monthIncomes->sum('amount_usd'),
                'total_month_expenses' => $monthExpenses->sum('amount_usd')
            ]);
        }
        $data->put($year, [
            'months'           => $monthCollection,
            "total_incomes"    => number($monthCollection->sum('total_month_incomes'), 2),
            "average_incomes"  => number($monthCollection->avg('total_month_incomes'), 2),
            "total_expenses"   => number($monthCollection->sum('total_month_expenses'), 2),
            "average_expenses" => number($monthCollection->avg('total_month_expenses'), 2)
        ]);

        return $data;
    }

    /**
     * @return array
     */
    public function getStatistic() {
        $accounting = $this->getAccountingSums();
        return [
            'expenses'  => $accounting['expenses'],
            'incomes'   => $accounting['incomes'],
            'returns'   => $accounting['returns'],
            'add_funds' => $this->getSumsOfOperationByPeriod($this->userOperations->getByOperation('ADD_FUNDS')),
            'withdraw'  => $this->getSumsOfOperationByPeriod($this->userOperations->getByOperation('WITHDRAW'))
        ];
    }

    /**
     * @return array
     */
    public function getAccountingSums() {
        $today     = $this->accountingModel::query()->selectRaw("type, 'today' as period, sum(amount_usd) as amount_sum, sum(return_amount_usd) as return_sum")->whereBetween('period_from', [Carbon::now()->startOfDay(), Carbon::now()->endOfDay()])->groupBy('type');
        $yesterday = $this->accountingModel::query()->selectRaw("type, 'yesterday' as period, sum(amount_usd) as amount_sum, sum(return_amount_usd) as return_sum")->whereBetween('period_from', [Carbon::now()->subDay()->startOfDay(), Carbon::now()->subDay()->endOfDay()])->groupBy('type');
        $week      = $this->accountingModel::query()->selectRaw("type, 'week' as period, sum(amount_usd) as amount_sum, sum(return_amount_usd) as return_sum")->whereBetween('period_from', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->groupBy('type');
        $sums      = $this->accountingModel::query()->selectRaw("type, 'total' as period, sum(amount_usd) as amount_sum, sum(return_amount_usd) as return_sum")->groupBy('type')->union($today)->union($yesterday)->union($week)->get();

        $todayExpenses     = $sums->where('period', 'today')->where('type', 'EXPENSE')->first();
        $yesterdayExpenses = $sums->where('period', 'yesterday')->where('type', 'EXPENSE')->first();
        $weekExpenses      = $sums->where('period', 'week')->where('type', 'EXPENSE')->first();
        $totalExpenses     = $sums->where('period', 'total')->where('type', 'EXPENSE')->first();

        $todayIncomes     = $sums->where('period', 'today')->where('type', 'INCOME')->first();
        $yesterdayIncomes = $sums->where('period', 'yesterday')->where('type', 'INCOME')->first();
        $weekIncomes      = $sums->where('period', 'week')->where('type', 'INCOME')->first();
        $totalIncomes     = $sums->where('period', 'total')->where('type', 'INCOME')->first();

        return [
            'expenses' => [
                'today_sum'     => convert_from_bigInteger($todayExpenses->amount_sum ?? 0),
                'yesterday_sum' => convert_from_bigInteger($yesterdayExpenses->amount_sum ?? 0),
                'week_sum'      => convert_from_bigInteger($weekExpenses->amount_sum ?? 0),
                'total_sum'     => convert_from_bigInteger($totalExpenses->amount_sum ?? 0)
            ],
            'incomes'  => [
                'today_sum'     => convert_from_bigInteger($todayIncomes->amount_sum ?? 0),
                'yesterday_sum' => convert_from_bigInteger($yesterdayIncomes->amount_sum ?? 0),
                'week_sum'      => convert_from_bigInteger($weekIncomes->amount_sum ?? 0),
                'total_sum'     => convert_from_bigInteger($totalIncomes->amount_sum ?? 0)
            ],
            'returns'  => [
                'today_sum'     => convert_from_bigInteger(($todayIncomes->return_sum ?? 0) + ($todayExpenses->return_sum ?? 0)),
                'yesterday_sum' => convert_from_bigInteger(($yesterdayIncomes->return_sum ?? 0) + ($yesterdayExpenses->return_sum ?? 0)),
                'week_sum'      => convert_from_bigInteger(($weekIncomes->return_sum ?? 0) + ($weekExpenses->return_sum ?? 0)),
                'total_sum'     => convert_from_bigInteger(($totalIncomes->return_sum ?? 0) + ($totalExpenses->return_sum ?? 0))
            ]
        ];
    }

    /**
     * @param Collection $operations
     * @return mixed
     */
    public function getSumsOfOperationByPeriod($operations) {
        $groupByPeriod = $operations->groupBy(function ($item) {
            $created_at = Carbon::createFromFormat(config('emitters.operation_date_format'), $item->created_at);
            if ($created_at->isSameDay(Carbon::now())) {
                return 'today';
            } elseif ($created_at->isSameDay(Carbon::now()->subDay())) {
                return 'yesterday';
            } elseif ($created_at->between(Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek())) {
                return 'week';
            }
        });
        $groupByPeriod->put('total', $operations);
        $groupByPeriod->transform(function ($item) {
            return convert_from_bigInteger($item->sum('default_amount'));
        });
        return [
            'today_sum'     => $groupByPeriod['today'] ?? 0,
            'yesterday_sum' => $groupByPeriod['yesterday'] ?? 0,
            'week_sum'      => $groupByPeriod['week'] ?? 0,
            'total_sum'     => $groupByPeriod['total'] ?? 0,
        ];
    }

    /**
     * @param array $data
     * @return array
     */
    public function addRow(array $data) {
        if (array_key_exists('period_from', $data)) $data['period_from'] = Carbon::createFromFormat('d.m.Y', $data['period_from']);
        if (array_key_exists('period_to', $data)) $data['period_to'] = Carbon::createFromFormat('d.m.Y', $data['period_to']);
        $text = '';
        if($data['type'] == "EXPENSE" && array_key_exists('need_transfer', $data) && $data['need_transfer'] == 1){
            $payment_system = paymentsystems()->grabAll()->where('id', $data['payment_system_id'])->first();

            $result = resolve('payment_systems.api')->doSendMoney($payment_system->class_name, $payment_system->currency, 0, $data['amount_ps'], $data['wallet_recipient']);

            $text = trans('translate-statistics::statistics.success_transfer', [
                'amount' => $data['amount_ps'],
                'currency' => $payment_system->currency,
                'recipient' => $data['wallet_recipient'],
                'transaction' => $result->transaction,
                'time' => Carbon::now()->format('d.m.Y H:i:s'),
            ]);
        }
        return [$this->accountingModel::query()->create($data), $text];
    }

    /**
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function updateRow(array $data) {
        if (array_key_exists('period_from', $data)) $data['period_from'] = Carbon::createFromFormat('d.m.Y', $data['period_from']);
        if (array_key_exists('period_to', $data)) $data['period_to'] = Carbon::createFromFormat('d.m.Y', $data['period_to']);
        $row = $this->accountingModel::query()->find($data['id']);
        $row->update($data);
        return $row;
    }

    /**
     * @param $id
     * @return int
     */
    public function deleteRow($id) {
        return $this->accountingModel::destroy($id);
    }
}