<?php


namespace Emitters\Statistics;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory as EloquentFactory;

class StatisticsServiceProvider extends ServiceProvider{
    public function boot() {
        $this->loadMigrationsFrom(__DIR__ . '/migrations');

        $this->publishes([
            __DIR__ . '/migrations/' => database_path('migrations')
        ], 'migrations');

        $this->app->make(EloquentFactory::class)->load(__DIR__ . '/factories/');

        $this->loadTranslationsFrom(__DIR__.'/resources/lang', 'translate-statistics');
    }
}