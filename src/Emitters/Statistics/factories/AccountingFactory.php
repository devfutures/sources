<?php

use Faker\Generator as Faker;

$factory->define(\Emitters\Statistics\Models\Accounting::class, function (Faker $faker) {
    $types = ['INCOME', 'EXPENSE'];
    $ps             = \Emitters\PaymentSystems\Models\Payment_System::all();
    $paymentSystems = $ps->isNotEmpty() ? $ps : collect([factory(\Emitters\PaymentSystems\Models\Payment_System::class)->create()]);
    $system         = $paymentSystems->random();
    return [
        'payment_system_id' => $system->id,
        'type'              => array_random($types, 1)[0],
        'source'            => $faker->text(200),
        'amount_usd'        => $faker->numberBetween(1, 10000),
        'amount_ps'         => $faker->numberBetween(1, 2000),
        'return_amount_usd' => $faker->numberBetween(1, 10000),
        'period_from'       => \Carbon\Carbon::now()->subDay(rand(0,128)),
        'period_to'         => \Carbon\Carbon::now()->addWeek()
    ];
});