<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountingsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('accountings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('payment_system_id')->index()->nullable();
            $table->string('type')->nullable();
            $table->text('source')->nullable();
            $table->bigInteger('amount_usd')->nullable();
            $table->bigInteger('amount_ps')->nullable();
            $table->bigInteger('return_amount_usd')->nullable();
            $table->timestamp('period_from')->nullable();
            $table->timestamp('period_to')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('accountings');
    }
}
