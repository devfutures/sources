<?php

return [
    'success_transfer' => 'The transfer of :amount :currency to the wallet :recipient was successfully completed. <br/>Transaction: :transaction <br/>Time: :time'
];