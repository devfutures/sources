<?php

namespace Emitters\UsersData\Models;

use Illuminate\Database\Eloquent\Model;

class Users_Contacts extends Model
{
    protected $fillable = [
        'user_id', 'facebook'
    ];
}
