<?php

namespace Emitters\UsersData\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
class Users_Wallet extends Model
{
	use SoftDeletes;

	protected $fillable = [
        'user_id', 'payment_system_id', 'wallet'
    ];
}
