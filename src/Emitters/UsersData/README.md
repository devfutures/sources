---
### UsersData
#### Методы UsersDataManager

##### Получение кошельков пользователя
```php
$userDataManager->getWallets($user);
```
* $user - объект модели User
* return - коллекцию кошельков пользователя.

##### Создание конкретного кошелька по платежной системе и адресу.
```php
$userDataManager->addWalletUser($user, $payment_system_id, $wallet);
```
* $user - объект модели User
* $payment_system_id - id валюты в системе.
* $wallet - адрес кошелька.
* return - User_Wallet

##### Изменение кошелька
```php
$userDataManager->changeWalletUser($user, $payment_system_id, $wallet, $id);
```
* $user - объект модели User
* $payment_system_id - id валюты в системе.
* $wallet - адрес кошелька.
* $id - id записи в бд.
* return - 1 в случае успешного изменения.

##### Удаление кошелька пользователя
```php
$userDataManager->destroyWalletUser($user, $id);
```
"Soft deleting" кошелька.
* $user - объект модели User
* $id - id записи в бд.
* return - 1 в случае успешного удаления.

##### Восстановление удаленного кошелька
```php
$userDataManager->restoreWalletUser($id);
```
* $id - id записи в бд.
* return - true в случае успеха.