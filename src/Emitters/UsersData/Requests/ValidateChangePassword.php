<?php

namespace Emitters\UsersData\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Factory as ValidatonFactory;

class ValidateChangePassword extends FormRequest
{
    public function __construct(ValidatonFactory $factory)
    {
        $factory->extend(
            'oldpass',
            function ($attribute, $value, $parameters, $validator) {
                // value
                $current_password = Auth::user()->password;
                if(Hash::check($value, $current_password)){
                    return true;
                }
                return false;
            }
        );
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(Auth::user()->id){
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'oldpass'          => 'required|min:6|oldpass',
            'password'         => 'required|min:6',
            'password_confirm' => 'required|same:password'
        ];
    }

    public function messages(){
        return [
            'oldpass.oldpass' => _i('The current password was entered incorrectly')
        ];
    }
}
