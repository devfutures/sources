<?php
namespace Emitters\UsersData\Traits;
use Carbon\Carbon;
use Emitters\AdminControl\Models\AdminRole;
use Emitters\Affiliate\Models\Users_Affiliate_Structure;
use Emitters\Filters\Traits\AddFilters;

trait AddUpline{
    use AddFilters;

    public function __construct(array $attributes = []) {
        $fillable = ['upline_id', 'route_id', 'login', 'g2fa_status', 'g2fa_secret', 'g2fa_ts', 'g2fa_settings', 'aff_ref'];
        $this->fillable = array_merge($this->fillable, $fillable);
        $hidden = ['g2fa_secret'];
        $this->hidden = array_merge($this->hidden, $hidden);
        $this->casts = array_merge($this->casts, ['g2fa_settings' => 'array']);
        parent::__construct($attributes);
    }

    public function upline() {
        return $this->hasOne('App\User', 'id', 'upline_id');
    }

    public function referrals() {
        return $this->hasMany(Users_Affiliate_Structure::class, 'user_id', 'id');
    }

    public function activity() {
        return $this->belongsTo('Emitters\UsersData\Models\Users_Activity', 'user_id', 'id');
    }

    public function contacts() {
        return $this->hasOne('Emitters\UsersData\Models\Users_Contacts', 'user_id', 'id');
    }

    public function wallets() {
        return $this->hasMany('Emitters\UsersData\Models\Users_Wallet', 'user_id', 'id');
    }

    public function role() {
        return $this->belongsTo(AdminRole::class);
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d.m.Y H:i');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d.m.Y H:i');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
}
