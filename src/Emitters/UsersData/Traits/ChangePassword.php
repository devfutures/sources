<?php
namespace Emitters\UsersData\Traits;

use Illuminate\Support\Facades;
trait ChangePassword{
	public function setNewPassword($user, $newPassword){
		if(!$user) return false;
		$userGet = $this->userModel->where('id', $user->id)->first();
		
		if(!$userGet) return false;
		$userGet->password = $this->hashNewPassword($newPassword);
		return $userGet->save();
	}

	public function hashNewPassword($password){
		return Hash::make($password);
	}
}