<?php
namespace Emitters\UsersData\Traits;

use Hash;

trait ChangePassword{
	public function setNewPassword($user_id, $newPassword){
		if(!$user_id) return false;
		$userGet = $this->userModel->where('id', $user_id)->first();
		
		if(!$userGet) return false;
		$userGet->password = $this->hashNewPassword($newPassword);
		return $userGet->save();
	}

	public function hashNewPassword($password){
		return Hash::make($password);
	}
}