<?php
namespace Emitters\UsersData;
/**
 * Interface
 */
use Emitters\Contracts\UsersData\UsersDataContract;
use Emitters\Abstracts\UsersData\UsersDataAbstract;

/**
 * Traits
 */
use Emitters\UsersData\Traits\ChangePassword;

class UsersDataManager extends UsersDataAbstract implements UsersDataContract{
	use ChangePassword;

	public function getWallets($user){
		return $this->walletsModel::where('user_id', $user->id)->get();
	}

	public function addWalletUser($user, $payment_system_id, $wallet){
		return $this->walletsModel::create([
			'user_id'           => $user->id,
			'payment_system_id' => $payment_system_id,
			'wallet'            => $wallet
		]);
	}

	public function changeWalletUser($user, $payment_system_id, $wallet, $id, $field = 'id'){
		return $this->walletsModel::where($field, $id)->update([
			'user_id'           => $user->id,
			'payment_system_id' => $payment_system_id,
			'wallet'            => $wallet
		]);
	}

	public function destroyWalletUser($user, $id, $field = 'id'){
		return $this->walletsModel::where('user_id', $user->id)->where($field, $id)->delete();
	}

	public function restoreWalletUser($id, $field = 'id'){
		return $this->walletsModel::withTrashed()->where($field, $id)->restore();
	}
}