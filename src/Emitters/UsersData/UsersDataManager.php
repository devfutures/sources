<?php

namespace Emitters\UsersData;

/**
 * Interface
 */
use Carbon\Carbon;
use Emitters\Abstracts\UsersData\UsersDataAbstract;
use Emitters\Contracts\UsersData\UsersDataContract;

use Emitters\Filters\Filters;

/**
 * Traits
 */

use Emitters\UsersData\Traits\ChangePassword;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;


class UsersDataManager extends UsersDataAbstract implements UsersDataContract {
    use ChangePassword;

    /**
     * @param array $filters
     * @return mixed
     */
    public function getUsers(array $filters = []) {
        $users = $this->userModel::
            filters(new Filters($filters, $this->availableFiltered))->
            with('upline')->
            orderBy('id', 'desc')->
            paginate(config('emitters.paginate_num'))->
            appends($filters);
        $users->each(function ($item) {
            $now = Carbon::now()->subDay(2);
            $item->new_user = $now->lte(Carbon::createFromFormat('d.m.Y H:i', $item->created_at)) ? true : false;
        });
        $users = $this->addSpecifyAttributes($users);
        return $users;
    }

    /**
     * @param $id
     *
     * @param array $with
     * @return mixed
     */
    public function getUserInfo($id, array $with = ['upline', 'contacts']) {
        $user = $this->userModel::where('id', $id)->with($with)->first();
        $user = (isset($user->role_id) && count($with) > 0) ? $user->load('role') : $user;
        return $user;
    }

    /**
     * @param string $email
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function getUserByEmail(string $email) {
        $user = $this->userModel::where('email', $email)->firstOrFail();
        return $user;
    }

    /**
     * @param $user
     *
     * @return mixed
     */
    public function getUserActivity($user) {
        return $this->userActivity->where('user_id', $user->id)->paginate(config('emitters.paginate_num'));
    }

    /**
     * @param $users
     */
    public function addSpecifyAttributes ($users) {
        $payment_systems_list = paymentsystems()->get();
        $now = Carbon::now();
        $users->each(function ($row) use ($payment_systems_list, $now){
            $row->total_purchase = $this->libBalance->totalUserBalanceActions('buy', $row->id, ['ADD_FUNDS'], $payment_systems_list);
            $row->total_withdraw = $this->libBalance->totalUserBalanceActions('sell', $row->id, ['WITHDRAW'], $payment_systems_list);
        });
        return $users;
    }

    /**
     * @param int $days
     *
     * @return mixed
     */
    public function countNewUsers($days = 2) {
        $date = Carbon::now()->subDay($days);
        return $this->userModel::where('created_at', '>=', $date)->count();
    }

    /**
     * @return mixed
     */
    public function countTotalUsers() {
        return $this->userModel->count();
    }

    /**
     * @param $user
     *
     * @return \Emitters\Contracts\UsersData\return
     */
    public function getWallets($user) {
        return $this->walletsModel::where('user_id', $user->id)->get();
    }

    /**
     * @param $user
     * @param $payment_system_id
     * @return array
     */
    public function getWallet($user, $payment_system_id) {
        $wallet = $this->walletsModel::where('user_id', $user->id)->where('payment_system_id', $payment_system_id)->first();
        return $wallet;
    }

    /**
     * @param $user
     * @param $payment_system_id
     * @param $wallet
     *
     * @return mixed
     */
    public function addWalletUser($user, $payment_system_id, $wallet) {
        return $this->walletsModel::create([
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system_id,
            'wallet'            => $wallet
        ]);
    }

    /**
     * @param        $user
     * @param        $payment_system_id
     * @param        $wallet
     * @param        $id
     * @param string $field
     *
     * @return mixed
     */
    public function changeWalletUser($user, $payment_system_id, $wallet, $id = null, $field = 'id') {
        if ($id) {
            return $this->walletsModel::query()
                ->where($field, $id)
                ->where('user_id', $user->id)
                ->where('payment_system_id', $payment_system_id)
                ->update([
                    'wallet' => $wallet
                ]);
        } else {
            return $this->walletsModel::updateOrCreate([
                'user_id' => $user->id,
                'payment_system_id' => $payment_system_id,
            ], [
                'wallet' => $wallet
            ]);
        }
    }

    /**
     * @param        $user
     * @param        $id
     * @param string $field
     *
     * @return mixed
     */
    public function destroyWalletUser($user, $id, $field = 'id') {
        return $this->walletsModel::where('user_id', $user->id)->where($field, $id)->delete();
    }

    /**
     * @param        $id
     * @param string $field
     *
     * @return mixed
     */
    public function restoreWalletUser($id, $field = 'id') {
        return $this->walletsModel::withTrashed()->where($field, $id)->restore();
    }

    /**
     * @param $user_id
     * @param $message
     * @param $blocked_up
     */
    public function updateBlocked($user_id, $message, $blocked_up) {
        return $this->userModel::where('id', $user_id)->update([
           'block_message' => $message,
           'blocked_up' => $blocked_up,
        ]);
    }

    /**
     * @param $user_id
     * @param $data
     *
     * @return mixed
     */
    public function updateInfo($user_id, $data) {
        if(array_key_exists('contacts', $data)){
            $contacts = $data['contacts'];
            unset($data['contacts']);
        }
        $user = $this->userModel::where('id', $user_id);
        $user->update($data);
        $contacts['user_id'] = $user_id;
        $find = $this->userContacts->where('user_id', $user_id)->first();
        if($find){
            foreach($contacts as $key => $value){
                $find->{$key} = $value;
            }
            $find->save();
        }else{
            $this->userContacts->create($contacts);
        }
        return $user;
    }

    /**
     * @param $user_id
     * @param $wallets
     *
     * @return bool
     */
    public function saveWallets($user_id, $wallets) {
        $user = $this->getUserInfo($user_id);
        $wallets_have_wallets = $this->getWallets($user);

        if($wallets){
            foreach($wallets as $row){
                if($find = $wallets_have_wallets->where('payment_system_id', $row['id'])->first()){
                    $this->destroyWalletUser($user, $find->id);
                    if($row['address'] != null){
                        $this->addWalletUser($user, $row['id'], $row['address']);
                    }
                }else{
                    if($row['address'] != null)
                        $this->addWalletUser($user, $row['id'], $row['address']);
                }
            }
        }

        return true;
    }

    public function user_registered(array $request, $user) {

    }

    /**
     * @param Request $request
     * @param       $user
     */
    public function logActivity(Request $request, $user) {
        $data = [
            'user_id' => $user->id,
            'browser' => $request->userAgent(),
            'ip'      => $request->ip()
        ];
        $this->userActivity::query()->create($data);
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getActivityByUser($id) {
        return $this->userActivity::query()->where('user_id', $id)->get();
    }

    /**
     * @param $userID
     * @param $roleID
     */
    public function setRoleToUser($userID, $roleID) {
        $this->userModel::where('id', $userID)->update(['role_id' => $roleID]);
    }

    /**
     * @param $userID
     */
    public function detachUserRole($userID) {
        $this->userModel::where('id', $userID)->update(['role_id' => null]);
    }

    /**
     * @param $userID
     * @param array $data
     */
    public function addGoogle2Fa($userID, array $data) {
        $this->userModel::where('id', $userID)->update([
            'g2fa_secret'   => $data['g2fa_secret'],
            'g2fa_status'   => $data['g2fa_status'],
            'g2fa_settings' => json_encode($data['g2fa_settings'])
        ]);
    }

    /**
     * @param $userID
     * @param array $settings
     */
    public function updateGoogle2FaSettings($userID, array $settings) {
        $this->userModel::where('id', $userID)->update([
            'g2fa_settings' => json_encode($settings)
        ]);
    }

    /**
     * @param $userID
     */
    public function disableGoogle2Fa($userID) {
        $this->userModel::where('id', $userID)->update(['g2fa_status' => 0]);
    }

    /**
     * @return bool|string
     */
    public function createUserAffRef(){
        $value = '';
        while(true){
            $random_link = str_random(8);
            $uniq = $this->getUserByAffRef($random_link);
            if(!$uniq){
                $value = $random_link;
                break;
            }
        }
        return ($value) ? $value : false;
    }

    /**
     * @param $aff_ref
     *
     * @return mixed
     */
    public function getUserByAffRef($aff_ref){
        return $this->userModel::where('aff_ref', $aff_ref)->first();
    }

    /**
     * @return mixed
     */
    public function getRandomUpline(){
        return $this->userModel::inRandomOrder()->first();
    }

    /**
     * @param $search
     *
     * @return null
     */
    public function searchUserBy($search){
        return (!$search) ? null : $this->userModel::where(function ($query) use ($search){
            $query->orWhere('name', 'LIKE', "%{$search}%");
            $query->orWhere('login', 'LIKE', "%{$search}%");
            $query->orWhere('email', 'LIKE', "%{$search}%");
            $query->orWhere('aff_ref', 'LIKE', "%{$search}%");
        })->orderBy('id', 'asc')->first();
    }


    /**
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function storeActivityLog(array $data) {
        return $this->userActivity::query()->create($data);
    }
}