<?php

namespace Emitters\UsersData;

use Illuminate\Support\ServiceProvider;
use Emitters\UsersData\Models\Users_Wallet;
use App\User;
class UsersDataServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/migrations');

        $this->publishes([
            __DIR__.'/database/migrations/' => database_path('migrations')
        ], 'migrations');
    }
}
