<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(\Emitters\UsersData\Models\Users_Activity::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory('App\User')->create()->id;
        },
        'browser' => $faker->userAgent,
        'ip' => $faker->ipv4,
        'country' => $faker->country
    ];
});