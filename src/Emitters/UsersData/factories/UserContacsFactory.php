<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(\Emitters\UsersData\Models\Users_Contacts::class, function (Faker $faker) {
    return [
        'user_id'      => function () {
            return factory('App\User')->create()->id;
        },
        'phone_number' => $faker->phoneNumber,
        'facebook'     => $faker->url,
        'linkedin'     => $faker->url,
        'twitter'      => $faker->userName,
        'telegram'     => $faker->userName,
        'website'      => $faker->url,
    ];
});