<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class,function (Faker $faker){
    return [
        'name'              => $faker->name,
        'login'             => $faker->name,
        'email'             => $faker->unique()->safeEmail,
        'upline_id'         => 0,
        'aff_ref'           => function (){
            return usersdata()->createUserAffRef();
        },
        'user_note'         => $faker->realText($faker->numberBetween(10,100)),
        'email_verified_at' => now(),
        'password'          => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token'    => str_random(10),
        'role_id'           => function (){
            return factory(\Emitters\AdminControl\Models\AdminRole::class)->create()->id;
        }
    ];
});
