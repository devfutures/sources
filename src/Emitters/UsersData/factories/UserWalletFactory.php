<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(\Emitters\UsersData\Models\Users_Wallet::class, function (Faker $faker) {
    return [
        'user_id'           => function () {
            return factory('App\User')->create()->id;
        },
        'payment_system_id' => function () {
            return factory('Emitters\PaymentSystems\Models\Payment_System')->create()->id;
        },
        'wallet'            => Str::random(16),
    ];
});