<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUplineCountry2faUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('login')->nullable()->after('name');
            $table->unsignedInteger('upline_id')->default(0)->after('id')->index();
            $table->unsignedInteger('role_id')->after('upline_id')->nullable()->index();
            $table->string('aff_ref')->after('role_id')->nullable();
            $table->text('user_note')->after('email')->nullable();
            $table->string('city')->after('email')->nullable();
            $table->string('country')->after('email')->nullable();
            $table->timestamp('blocked_up', 0)->after('remember_token')->nullable();
            $table->text('block_message')->after('remember_token')->nullable();
            $table->tinyInteger('g2fa_status')->after('block_message')->default(0);
            $table->string('g2fa_secret')->after('g2fa_status')->nullable();
            $table->integer('g2fa_ts')->after('g2fa_secret')->nullable();
            $table->json('g2fa_settings')->after('g2fa_secret')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['upline_id', 'role_id', '2fa_status', '2fa_settings', 'city', 'country', 'user_note', 'blocked_up', 'block_message']);
        });
    }
}
