<?php
namespace Emitters\UsersData\seed;

use Illuminate\Database\Seeder;

class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\User')->create([
            'email'     => 'admin@localhost.com',
            'password'  => bcrypt('123456')
        ]);
    }
}
