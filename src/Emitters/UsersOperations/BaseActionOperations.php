<?php


namespace Emitters\UsersOperations;


use Emitters\Contracts\Balance\BalanceManagerInterface;
use Emitters\Contracts\PaymentSystems\PaymentSystemsContract;
use Emitters\Contracts\UserDeposits\UserDepositsContract;
use Emitters\Contracts\UsersOperations\OperationRepositoryContract;
use Illuminate\Foundation\Auth\User;

class BaseActionOperations {
    /**
     * @var OperationRepositoryContract
     */
    protected $repository;

    /**
     * @var PaymentSystemsContract
     */
    protected $payment_systems;

    /**
     * @var BalanceManagerInterface
     */
    protected $balanceManager;

    /**
     * @var UserDepositsContract
     */
    protected $depositUsersManager;

    /**
     * @var UsersOperations
     */
    protected $usersOperationsManager;

    /**
     * BaseActionOperations constructor.
     */
    public function __construct() {
        $this->repository             = resolve('operations_repo');
        $this->payment_systems        = paymentsystems();
        $this->balanceManager         = resolve('librariesair.balance');
        $this->depositUsersManager    = resolve('libair.depositusers');
        $this->usersOperationsManager = users_operations();
    }

    /**
     * @param $email
     * @return mixed
     */
    protected function getUserByEmail($email) {
        return User::query()->where('email', $email)->first();
    }
}