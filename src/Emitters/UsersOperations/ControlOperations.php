<?php


namespace Emitters\UsersOperations;

use Emitters\Abstracts\UsersOperations\ControlOperationsAbstract;
use Emitters\Contracts\UsersOperations\ControlOperationsContract;

class ControlOperations extends ControlOperationsAbstract implements ControlOperationsContract {
    /**
     * @param array $request
     * @param $option
     * @return array
     * @throws \Exception
     */
    public function handlerActionOperations(array $request, $option) {
        $operations  = $this->repository->getOperationsByIds($request['id']);

        $update_info = [];

        if(!$operations) {
            throw new \Exception('Error get operations by ids');
        }

        foreach($operations as $row) {
            $update_info[] = $this->executionMethods($option, $row->id, $row->type, $row->operation, $row->status, $row, $request);
        }

        return $update_info;
    }

    /**
     * @param array $request
     * @return array|mixed
     */
    public function createOperation(array $request) {
        return $this->executionMethods('create', 0, $request['type'], $request['operation'], 'pending', $request, $request);
    }

    /**
     * @param array $request
     * @param string $status
     * @return
     * @throws \Exception
     */
    public function setStatusOperations(array $request, $status = 'completed') {
        $operations = $this->repository->getOperationsByIds($request['id']);

        if(!$operations) {
            throw new \Exception('Error get operations by ids');
        }
        $update_info = [];
        $operations->each(function ($row) use (&$update_info, $status) {
            $row->status = $status;
            $update_info[] = ['status' => $row->save(), 'id' => $row->id];
        });

        return $update_info;
    }

    /**
     * @param array $request
     * @throws \Exception
     */
    public function massiveOperation(array $request) {
        $operations = $this->repository->getOperationsByIds($request['id']);
        $update_info = [];
        if(!$operations) {
            throw new \Exception('Error get operations by ids');
        }

        $operations = $operations->filter(function($row, $key){
            return ($row->operation == 'WITHDRAW')? true: false;
        });

        $operations = $operations->mapToGroups(function ($item, $key) {
            return [$item['payment_system_id'] => $item];
        });
        $ps = paymentsystems()->grabAll()->where('supported_massive_payment', 1)->pluck('id')->toArray();

        $operations = $operations->filter(function($row, $key) use ($ps){
            return (in_array($key, $ps)) ? true : false;
        });
        foreach($operations as $row) {
            $update_info[] = $this->executionMethods('massive', $row->pluck('id')->toArray(), 'sell', 'WITHDRAW', 'pending', $row, $request);
        }

        return $update_info;
    }
}