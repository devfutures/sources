<?php

namespace Emitters\UsersOperations\Models;

use Carbon\Carbon;
use Emitters\Filters\Traits\AddFilters;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Users_Operations extends Model {
    use SoftDeletes, AddFilters;

    const CREATE_DEPOSIT = "Create deposit";
    const ACCRUALS = "Accrual of profit on deposit";
    const REFFERAL = "Income affiliate";
    const WITHDRAW = "Withdraw founds";
    const REFUND_DEPOSIT = "Refund of a deposit";
    const BUY_COIN = "Coins buy->sell";
    const SELL_COIN = "Coins sell->buy";
    const SELL_FUNDS = "Sell funds";
    const TRANSFER_BALANCE_BUY = "Transfer balance buy";
    const TRANSFER_BALANCE = "Transfer balance";
    const ADD_FUNDS = "Add funds";

    protected $fillable = [
        'type',
        'operation',
        'parent_id',
        'deposit_id',
        'user_id',
        'plan_id',
        'from_user_id',
        'payment_system_id',
        'from_payment_system_id',
        'amount',
        'from_amount',
        'default_amount',
        'commission',
        'status',
        'transaction',
        'data_info',
        'level',
        'is_hold',
        'hidden',
        'operation_note',
        'user_created'
    ];

    protected $casts = [
        'data_info' => 'array'
    ];

    /**
     * @param $date
     *
     * @return mixed
     */
    public function getCreatedAtAttribute($date) {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(config('emitters.operation_date_format'));
    }

    /**
     * @param $date
     *
     * @return mixed
     */
    public function getUpdatedAtAttribute($date) {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(config('emitters.operation_date_format'));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function from_user() {
        return $this->hasOne('App\User', 'id', 'from_user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user_balances() {
        return $this->hasOne('Emitters\Balance\Models\Users_Balance', 'id', 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function payment_system() {
        return $this->hasOne('Emitters\PaymentSystems\Models\Payment_System', 'id', 'payment_system_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function deposit_plan() {
        return $this->hasOne('Emitters\DepositPlans\Models\Deposit_Plans', 'id', 'plan_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function admin_created() {
        return $this->hasOne('App\User', 'id', 'user_created');
    }
}
