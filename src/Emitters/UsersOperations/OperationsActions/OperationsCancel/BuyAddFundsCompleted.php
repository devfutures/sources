<?php


namespace Emitters\UsersOperations\OperationsActions\OperationsCancel;

use Emitters\Contracts\UsersOperations\BaseActionOperationsContract;
use Emitters\UsersOperations\BaseActionOperations;

class BuyAddFundsCompleted extends BaseActionOperations implements BaseActionOperationsContract {
    /**
     * @param $operation
     * @param null $request
     * @return mixed
     * @throws \Exception
     */
    public function perform($operation, $request = null) {
        if(config('emitters.after_add_funds') != 'ADD_FUNDS_TO_BALANCE'){
            throw new \Exception('The operation cannot be canceled, a deposit has already been created');
        }
        $this->balanceManager->changeBalance([
            'user_id'           => $operation->user_id,
            'type'              => 'sell',
            'operation'         => 'CANCEL_ADD_FUNDS',
            'amount'            => $operation->amount,
            'payment_system_id' => $operation->payment_system_id,
            'parent_id'         => $operation->id,
        ]);
        return $this->repository->update($operation->id, ['status' => 'cancel']);
    }
}