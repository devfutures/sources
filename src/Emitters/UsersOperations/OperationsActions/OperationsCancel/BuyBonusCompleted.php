<?php


namespace Emitters\UsersOperations\OperationsActions\OperationsCancel;

use Emitters\Contracts\UsersOperations\BaseActionOperationsContract;
use Emitters\UsersOperations\BaseActionOperations;

class BuyBonusCompleted extends BaseActionOperations implements BaseActionOperationsContract{
    /**
     * @param $operation
     * @param null $request
     * @return mixed
     */
    public function perform($operation, $request = null) {
        $this->balanceManager->changeBalance([
            'user_id'           => $operation->user_id,
            'type'              => 'sell',
            'operation'         => 'CANCEL_BONUS',
            'amount'            => $operation->amount,
            'payment_system_id' => $operation->payment_system_id,
            'parent_id'         => $operation->id,
        ]);
        return $this->repository->update($operation->id, ['status' => 'cancel']);
    }
}