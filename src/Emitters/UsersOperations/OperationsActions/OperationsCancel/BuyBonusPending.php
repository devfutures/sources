<?php


namespace Emitters\UsersOperations\OperationsActions\OperationsCancel;

use Emitters\Contracts\UsersOperations\BaseActionOperationsContract;
use Emitters\UsersOperations\BaseActionOperations;

class BuyBonusPending extends BaseActionOperations implements BaseActionOperationsContract{
    /**
     * @param $operation
     * @param null $request
     * @return mixed
     */
    public function perform($operation, $request = null) {
        return $this->repository->update($operation->id, ['status' => 'cancel']);
    }
}