<?php


namespace Emitters\UsersOperations\OperationsActions\OperationsCancel;

use Emitters\Contracts\UsersOperations\BaseActionOperationsContract;
use Emitters\UsersOperations\BaseActionOperations;

class BuyRefferalCompleted extends BaseActionOperations implements BaseActionOperationsContract {
    /**
     * @param $operation
     * @param null $request
     * @return mixed
     */
    public function perform($operation, $request = null) {
        $this->balanceManager->changeBalance([
            'user_id'           => $operation->user_id,
            'type'              => 'sell',
            'operation'         => 'CANCEL_REFFERAL',
            'amount'            => $operation->amount,
            'payment_system_id' => $operation->payment_system_id,
            'parent_id'         => $operation->id,
            'level'             => $operation->level,
            'from_user_id'      => $operation->from_user_id,
        ]);
        return $this->repository->update($operation->id, ['status' => 'cancel']);
    }
}