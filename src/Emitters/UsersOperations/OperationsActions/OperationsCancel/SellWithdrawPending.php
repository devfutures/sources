<?php


namespace Emitters\UsersOperations\OperationsActions\OperationsCancel;

use Emitters\Contracts\UsersOperations\BaseActionOperationsContract;
use Emitters\UsersOperations\BaseActionOperations;

class SellWithdrawPending extends BaseActionOperations implements BaseActionOperationsContract {
    /**
     * @param $operation
     * @param null $request
     * @return mixed
     */
    public function perform($operation, $request = null) {
        $this->balanceManager->delete_operation_withdraw($operation->parent_id, $operation->user_id);
        return $this->repository->update($operation->id, ['status' => 'cancel']);
    }
}