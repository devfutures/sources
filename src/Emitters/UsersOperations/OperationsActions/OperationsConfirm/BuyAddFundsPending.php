<?php


namespace Emitters\UsersOperations\OperationsActions\OperationsConfirm;


use Emitters\Contracts\UsersOperations\BaseActionOperationsContract;
use Emitters\UsersOperations\BaseActionOperations;
use Illuminate\Foundation\Auth\User;

class BuyAddFundsPending extends BaseActionOperations implements BaseActionOperationsContract {
    public function perform($operation, $request = null) {
        if($operation->status == 'cancel'){
            $operation->status = 'pending';
            $operation->transaction = null;
            $operation->save();
        }

        if($operation->status == 'error'){
            $operation->status = 'pending';
            $operation->save();
        }
        $event              = app()->make('stdClass');
        $event->transaction = ($operation->transaction == null) ? str_random(25) : $operation->transaction;
        $event->payment_id  = $operation->id;
        $event->amount      = convert_from_bigInteger($operation->amount);
        $event->add_info    = [];
        $ps = $this->payment_systems->grabAll()->where('id', $operation->payment_system_id)->first();

        $this->depositUsersManager->eventAddFunds($event, true, $ps->class_name);
        return true;
    }
}