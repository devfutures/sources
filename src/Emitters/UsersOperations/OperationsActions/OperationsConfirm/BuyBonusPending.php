<?php


namespace Emitters\UsersOperations\OperationsActions\OperationsConfirm;


use Emitters\Contracts\UsersOperations\BaseActionOperationsContract;
use Emitters\UsersOperations\BaseActionOperations;
use Illuminate\Foundation\Auth\User;

class BuyBonusPending extends BaseActionOperations implements BaseActionOperationsContract {
    public function perform($operation, $request = null) {
        $balance = $this->balanceManager->addBonusToBalance($operation->user, $operation->amount, $operation->payment_system_id, 1, true);
        return $this->repository->update($operation->id, [
            'status'    => 'completed',
            'parent_id' => $balance->id
        ]);
    }
}