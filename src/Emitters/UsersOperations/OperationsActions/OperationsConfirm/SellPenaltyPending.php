<?php


namespace Emitters\UsersOperations\OperationsActions\OperationsConfirm;


use Emitters\Contracts\UsersOperations\BaseActionOperationsContract;
use Emitters\UsersOperations\BaseActionOperations;
use Illuminate\Foundation\Auth\User;

class SellPenaltyPending extends BaseActionOperations implements BaseActionOperationsContract {
    public function perform($operation, $request = null) {
        // TODO: добавить проверку баланс

        if($operation->status == 'cancel'){
            $operation->status = 'pending';
            $operation->transaction = null;
            $operation->save();
        }

        if($operation->status == 'error'){
            $operation->status = 'pending';
            $operation->save();
        }

        $this->balanceManager->changeBalance([
            'user_id'           => $operation->user_id,
            'type'              => 'sell',
            'operation'         => 'PENALTY',
            'amount'            => $operation->amount,
            'payment_system_id' => $operation->payment_system_id,
            'parent_id'         => $operation->id,
        ]);
        return $this->repository->update($operation->id, ['status' => 'completed']);
    }
}