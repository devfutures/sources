<?php


namespace Emitters\UsersOperations\OperationsActions\OperationsConfirm;


use Emitters\Contracts\UsersOperations\BaseActionOperationsContract;
use Emitters\UsersOperations\BaseActionOperations;
use Illuminate\Foundation\Auth\User;

class SellWithdrawPending extends BaseActionOperations implements BaseActionOperationsContract {
    public function perform($operation, $request = null) {
        $ps = $this->payment_systems->grabAll()->where('id', $operation->payment_system_id)->first();
        if($ps){
            try {
                $wallet = usersdata()->getWallet($operation->user, $ps->id);
                if(!$wallet) {
                    throw new \Exception('FOR_WITHDRAW_NEED_FILL_WALLET_ACTION');
                }
                $result = $this->sendMoney($ps, $operation->id, convert_from_bigInteger($operation->amount), $wallet->wallet);
                $operation->status = 'completed';
                $operation->transaction = $result->transaction;
                $operation->data_info = mergeDataInfo($operation->data_info, $result->add_info);
                $operation->save();
                return true;
            } catch (\Exception $e) {
                $error = [
                    "error_message" => $e->getMessage()
                ];
                $operation->status = 'error';
                $operation->data_info = mergeDataInfo($operation->data_info, $error);
                $operation->save();
            }
        }
        return false;
    }

    /**
     * @param $payment_system
     * @param $operation_id
     * @param $amount
     * @param $wallet
     * @return mixed
     * @throws \Exception
     */
    public function sendMoney($payment_system, $operation_id, $amount, $wallet) {
        if(!class_exists($payment_system->class_name)) {
            throw new \Exception('PAYMENT_SYSTEM_CLASS_NAME_ERROR');
        }

        $abstract_payment_class = $payment_system->class_name;
        $ex = app()->make($abstract_payment_class);

        if(!method_exists($ex, 'send_money')) {
            throw new \Exception('PAYMENT_SYSTEM_CLASS_METHOD_ERROR2');
        }
        return $ex->send_money($operation_id, $amount, $wallet, $payment_system->currency);
    }
}