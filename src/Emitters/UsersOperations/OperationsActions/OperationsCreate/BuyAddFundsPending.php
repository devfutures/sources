<?php


namespace Emitters\UsersOperations\OperationsActions\OperationsCreate;


use Emitters\Contracts\UsersOperations\BaseActionOperationsContract;
use Emitters\UsersOperations\BaseActionOperations;
use Illuminate\Foundation\Auth\User;

class BuyAddFundsPending extends BaseActionOperations implements BaseActionOperationsContract {
    public function perform($operation, $request = null) {
        $user = $this->getUserByEmail($request['user']);
//        $result = $this->repository->create([
//            'type'              => 'buy',
//            'operation'         => 'ADD_FUNDS',
//            'user_id'           => $user->id,
//            'plan_id'           => $request['plan_id'],
//            'amount'            => convert_to_bigInteger($request['amount']),
//            'payment_system_id' => $request['payment_system_id'],
//            'status'            => 'pending',
//            'transaction'       => $request['transaction'],
//        ]);

        $result                 = $this->depositUsersManager->addFundsToDeposit($user, [
            'payment_system' => $request['payment_system_id'],
            'amount'         => $request['amount'],
            'plan_id'        => (array_key_exists('plan_id', $request)) ? $request['plan_id'] : 0,
            'from_balance'   => 0,
            'transaction'    => (array_key_exists('transaction', $request) && $request['transaction'] != '') ? $request['transaction'] : str_random(10),
            'hidden'         => (array_key_exists('hidden', $request)) ? $request['hidden'] : 0
        ], true);
        $result->operation_note = (array_key_exists('operation_note', $request)) ? $request['operation_note'] : null;
        $result->user_created   = auth()->id();
        $result->save();
        $result->load(['user', 'deposit_plan']);
        $result = collect([$result])->toSmallAmount()->bindPaymentSystems();
        return $result;
    }
}