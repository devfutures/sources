<?php


namespace Emitters\UsersOperations\OperationsActions\OperationsCreate;


use Emitters\Contracts\UsersOperations\BaseActionOperationsContract;
use Emitters\UsersOperations\BaseActionOperations;
use Illuminate\Foundation\Auth\User;

class BuyBonusPending extends BaseActionOperations implements BaseActionOperationsContract {
    public function perform($operation, $request = null) {
        $user = $this->getUserByEmail($operation['user']);
        $operation = $this->usersOperationsManager->createOperationBonus(
            $user,
            0,
            convert_to_bigInteger($request['amount']),
            $request['payment_system_id'],
            'pending',
            (array_key_exists('is_hold', $request)) ? $request['is_hold']: 1
        );
        $operation->operation_note = (array_key_exists('operation_note', $request)) ? $request['operation_note'] : null;
        $operation->user_created   = auth()->id();
        $operation->save();
        $operation->load('user');
        $operation = collect([$operation])->toSmallAmount()->bindPaymentSystems();
        return $operation;
    }
}