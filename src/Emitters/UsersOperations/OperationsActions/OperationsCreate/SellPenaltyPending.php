<?php


namespace Emitters\UsersOperations\OperationsActions\OperationsCreate;


use Emitters\Contracts\UsersOperations\BaseActionOperationsContract;
use Emitters\UsersOperations\BaseActionOperations;
use Illuminate\Foundation\Auth\User;

class SellPenaltyPending extends BaseActionOperations implements BaseActionOperationsContract {
    public function perform($operation, $request = null) {
        $user = $this->getUserByEmail($operation['user']);
        $operation = $this->usersOperationsManager->createOperationPenalty($user, convert_to_bigInteger($request['amount']), $request['payment_system_id']);
        $operation->operation_note = (array_key_exists('operation_note', $request)) ? $request['operation_note'] : null;
        $operation->user_created   = auth()->id();
        $operation->save();

        $operation->user = $user;
        $operation = collect([$operation])->toSmallAmount()->bindPaymentSystems();
        return $operation;
    }
}