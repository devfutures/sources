<?php


namespace Emitters\UsersOperations\OperationsActions\OperationsMassive;


use Emitters\Contracts\UsersOperations\BaseActionOperationsContract;
use Emitters\UsersOperations\BaseActionOperations;

class SellWithdrawPending extends BaseActionOperations implements BaseActionOperationsContract{

    /**
     * @param $operation
     * @param null $request
     * @return mixed
     * @throws \Exception
     */
    public function perform($operation, $request = null) {
        $data = [];
        $operation->load(['user.wallets']);
        foreach($operation as $row){
            $wallet = $row->user->wallets->where('payment_system_id', $row->payment_system_id)->first();
            if(!$wallet){
                throw new \Exception('FOR_WITHDRAW_NEED_FILL_WALLET_ACTION_MASSIVE');
            }
            /**
             * TODO:поставить проверку если выводится XLM то знаков 7, а не 8
             */
            $data[] = [
                $wallet->wallet,
                $row->amount,
            ];
        }
        $ps = $this->payment_systems->grabAll()->where('id', $operation->first()->payment_system_id)->first();
        try {
            $result = $this->sendMoneyMulti($ps, $data);
            foreach($operation as $row){
                $row->status = 'completed';
                $row->transaction = $result->transaction;
                $row->data_info = mergeDataInfo($row->data_info, $result->add_info);
                $row->save();
            }
            return true;
        } catch (\Exception $e) {
            foreach($operation as $row){
                $error = [
                    "error_message" => $e->getMessage()
                ];
                $row->data_info = mergeDataInfo($row->data_info, $error);
                $row->status = 'error';
                $row->save();
            }
        }

        return false;
    }

    /**
     * @param $payment_system
     * @param $operation_id
     * @param $amount
     * @param $wallet
     * @return mixed
     * @throws \Exception
     */
    public function sendMoneyMulti($payment_system, $address_and_amount) {
        if(!class_exists($payment_system->class_name)) {
            throw new \Exception('PAYMENT_SYSTEM_CLASS_NAME_ERROR');
        }

        $abstract_payment_class = $payment_system->class_name;
        $ex = app()->make($abstract_payment_class);

        if(!method_exists($ex, 'send_multi')) {
            throw new \Exception('PAYMENT_SYSTEM_CLASS_METHOD_ERROR');
        }

        return $ex->send_multi($address_and_amount, $payment_system->currency);
    }
}