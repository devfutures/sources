<?php

namespace Emitters\UsersOperations\Repositories;

use Emitters\Contracts\UsersOperations\OperationRepositoryContract;
use Emitters\Filters\Filters;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Emitters\UsersOperations\Models\Users_Operations;

class OperationsRepository implements OperationRepositoryContract {
    /**
     * @var \Emitters\UsersOperations\Models\Users_Operations
     */
    protected $model;
    /**
     * @var array
     */
    protected $availableOperationsFilters = [
        'whereIn', 'where', 'user', 'dateBetween', 'orderBy', 'searchTransactions', 'transaction'
    ];

    /**
     * OperationsRepository constructor.
     *
     * @param $model
     */
    public function __construct($model) {
        $this->model = $model;
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function create($data) {
        return $this->model::query()->create($data);
    }


    public function delete($id, $field = 'id') {
        return $this->model->where($field, $id)->delete();
    }

    /**
     * @param $val
     * @param string $colom
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|mixed|null|object
     */
    public function first($val, $colom = 'id') {
        return $this->model::query()->where($colom, $val)->first();
    }

    /**
     * @param array $filters
     *
     * @param bool  $no_paginate
     * @return mixed
     */
    public function get(array $filters = [], $no_paginate = false) {
        $query = $this->model::query()->filters(new Filters($filters, $this->availableOperationsFilters))->with(['user', 'from_user'])->orderBy('id', 'desc');
        if ($no_paginate) {
            $operations = $query->get();
        } else {
            $operations = $query->paginate(config('emitters.paginate_num'), ['*'], 'operation_page')->appends($filters);
        }
        return $operations;
    }

    /**
     * @param string $operation
     *
     * @return Collection
     */
    public function getByOperation(string $operation) {
        $operations = $this->model::query()->where('operation', $operation)->where('hidden', 0)->where('status', 'completed')->get();
        return $operations;
    }

    /**
     * @param       $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data) {
        return $this->model::query()->where('id', $id)->update($data);
    }

    /**
     * @param       $user
     * @param array $operation
     * @param array $where
     * @param array $status
     *
     * @return
     */
    public function getCount($user, array $operation = [], array $where = [], array $status = []) {
        return $this->model::query()->
        where('user_id', $user->id)->
        whereIn('operation', $operation)->
        where($where[0], $where[1])->
        whereIn('status', $status)->
        count();
    }

    /**
     * @param $arg1
     * @param $val1
     * @param $arg2
     * @param $val2
     *
     * @return mixed
     */
    public function getByTwo($arg1, $val1, $arg2, $val2) {
        return $this->model::query()->
        where($arg1, $val1)->
        where($arg2, $val2)->
        first();
    }

    /**
     * @param        $arg1
     * @param        $val1
     * @param        $arg2
     * @param        $val2
     * @param        $arg3
     * @param        $val3
     * @param string $order
     * @param string $order_type
     *
     * @return mixed
     */
    public function getByThird($arg1, $val1, $arg2, $val2, $arg3, $val3, $order = 'id', $order_type = 'asc') {
        return $this->model::query()->
        where($arg1, $val1)->
        where($arg2, $val2)->
        where($arg3, $val3)->
        orderBy($order, $order_type)->
        first();
    }

    /**
     * @param $arg1
     * @param $val1
     * @param $arg2
     * @param $val2
     * @param $arg3
     * @param $val3
     * @param $arg4
     * @param $val4
     * @param string $order
     * @param string $order_type
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null|object
     */
    public function getByFour($arg1, $val1, $arg2, $val2, $arg3, $val3, $arg4, $val4, $order = 'id', $order_type = 'asc') {
        return $this->model::query()->
        where($arg1, $val1)->
        where($arg2, $val2)->
        where($arg3, $val3)->
        where($arg4, $val4)->
        orderBy($order, $order_type)->
        first();
    }

    /**
     * @param        $user_id
     * @param array  $from_user_ids
     * @param array  $operation
     * @param string $type
     *
     * @return mixed
     */
    public function getSumOperations($user_id, array $from_user_ids = [], array $operation = [], string $type = 'buy') {
        return $this->model::query()->selectRaw('sum(amount) as aggregate, from_user_id, payment_system_id, level')
            ->where(function ($query) use ($type) {
                ($type) ? $query->where('type', $type) : null;
            })
            ->where(function ($query) use ($operation) {
                if (count($operation) > 0) {
                    $query->whereIn('operation', $operation);
                }
            })
            ->where('status', 'completed')
            ->where('user_id', $user_id)
            ->where(function ($query) use ($from_user_ids) {
                if (count($from_user_ids) > 0) {
                    $query->whereIn('from_user_id', $from_user_ids);
                }
            })
            ->groupBy(['from_user_id', 'payment_system_id', 'level'])
            ->get();
    }


    /**
     * @param $ids
     * @return mixed
     */
    public function getOperationsByIds($ids) {
        return $this->get([
            'whereIn' => [
                ['field' => 'id', 'value' => $ids]
            ]
        ], true);
    }

    /**
     * @param $depositID
     * @return mixed
     */
    public function getSumOfAccrualsByDeposit($depositID) {
        return $this->model::query()
            ->selectRaw('sum(amount) as aggregate')
            ->where('deposit_id', $depositID)
            ->where('operation', 'ACCRUALS')
            ->where('status', 'completed')
            ->get();
    }

    /**
     * @param array $depositIDs
     * @return mixed
     */
    public function getTotalSumOfDeposits(array $depositIDs) {
        return $this->model::query()
            ->selectRaw('sum(amount) as aggregate, payment_system_id, deposit_id')
            ->whereIn('deposit_id', $depositIDs)
            ->where('operation', 'CREATE_DEPOSIT')
            ->where('status', 'completed')
            ->groupBy(['payment_system_id', 'deposit_id'])
            ->get();
    }

    /**
     * @param array $user_ids
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Query\Builder[]|Collection
     */
    public function getTotalSumDepositsByUser(array $user_ids) {
        return $this->model::query()
            ->selectRaw('sum(default_amount) as aggregate, user_id')
            ->whereIn('user_id', $user_ids)
            ->where('operation', 'CREATE_DEPOSIT')
            ->where('status', 'completed')
            ->groupBy(['user_id'])
            ->get();
    }

    public function getCommissionByReferrals($user_id, $max_level) {
        return $this->model::query()
            ->selectRaw('sum(default_amount) as aggregate, level')
            ->where('user_id', $user_id)
            ->where('operation', 'REFFERAL')
            ->where('level', '<=', $max_level)
            ->where('status', 'completed')
            ->groupBy('level')
            ->get();
    }

    public function getActiveUsersInStructure($user_id, $max_level) {
        return $this->model::query()
            ->selectRaw('level, count(DISTINCT from_user_id) as active_in_level')
            ->where('user_id', $user_id)
            ->where('operation', 'REFFERAL')
            ->where('level', '<=', $max_level)
            ->where('status', 'completed')
            ->groupBy(['level'])
            ->get();
    }

    /**
     * @param $user
     * @return mixed
     */
    public function getTotalSumOfWithdraws($user) {
        return $this->model::query()
            ->selectRaw('sum(amount) as aggregate, payment_system_id, status, created_at')
            ->where('user_id', $user->id)
            ->where('operation', 'WITHDRAW')
            ->whereIn('status', ['completed', 'pending'])
            ->groupBy(['payment_system_id', 'status', 'created_at'])
            ->get();
    }

    /**
     * @param array  $operationNames
     * @param Carbon $periodFrom
     * @param Carbon $periodTo
     * @param bool   $withHidden
     * @return Collection
     */
    public function getOperationsBetweenPeriod(array $operationNames, Carbon $periodFrom, Carbon $periodTo, $withHidden = false): Collection {

        return $this->model::query()
            ->where(function ($query) use($withHidden){
                if($withHidden == false){
                    $query->where('hidden', $withHidden);
                }
            })
            ->whereIn('operation', $operationNames)
            ->where('status', 'completed')
            ->whereBetween('created_at', [$periodFrom->startOfDay(), $periodTo->endOfDay()])
            ->get();
    }

    /**
     * @param string $operation
     * @param null $user_id
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Query\Builder[]|Collection
     */
    public function getSumOfOperation(string $operation, $user_id = null) {
        return $this->model::query()
            ->selectRaw('sum(default_amount) as total')
            ->where('operation', $operation)
            ->where('status', 'completed')
            ->where(function($query) use ($user_id){
                if($user_id){
                    $query->where('user_id', $user_id);
                }
            })
            ->get();
    }

    /**
     * @param array $users_id
     * @param $operation
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|object
     */
    public function getSumByUsersAndOperations(array $users_id, $operation) {
        return $this->model::query()
            ->selectRaw('sum(default_amount) as total')
            ->where('type', 'sell')
            ->where('operation', $operation)
            ->where('status', 'completed')
            ->whereIn('user_id', $users_id)
            ->first();
    }

    /**
     * @param array $deposit_ids
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|mixed|null|object
     */
    public function getSumByDeposits(array $deposit_ids) {
        return $this->model::query()
            ->selectRaw('sum(default_amount) as total')
            ->where('type', 'sell')
            ->where('operation', 'CREATE_DEPOSIT')
            ->where('status', 'completed')
            ->whereIn('deposit_id', $deposit_ids)
            ->first();
    }

    /**
     * @param $deposits_ids
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Query\Builder[]|Collection
     */
    public function getTotalAccrualsByDeposits($deposits_ids) {
        return $this->model::query()
            ->selectRaw('deposit_id, sum(amount) as amount_accruals, count(id) as count_accruals')
            ->whereIn('deposit_id', $deposits_ids)
            ->where('operation', 'ACCRUALS')
            ->where('status', 'completed')
            ->groupBy('deposit_id')
            ->get();
    }

    /**
     * @param $address
     * @param null $destination_tag
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null|object
     */
    public function getOperationByPaymentAddress($address, $destination_tag = null) {
        return $this->model::query()
            ->where('data_info->address', $address)
            ->where(function($query) use ($destination_tag){
                if($destination_tag){
                    $query->where('data_info->destination_tag', $destination_tag);
                }
            })
            ->first();
    }

    /**
     * @param $user_id
     * @param $users_ids
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getCommissionByUsersId($user_id, $users_ids) {
        return $this->model::query()
            ->where('user_id', $user_id)
            ->whereIn('from_user_id', $users_ids)
            ->where('status', 'completed')
            ->get();
    }

    /**
     * @param $user_id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null|object
     */
    public function getOperationWithdrawPending($user_id) {
        /**
         * TODO: fix 27 hours to 24 hours
         * sync php & mysql
         */
        return $this->model::query()
            ->selectRaw('payment_system_id, created_at, amount, default_amount, DATE_ADD(created_at, INTERVAL 27 HOUR) as approximately_accrual_time')
            ->where('user_id', $user_id)
            ->where('operation', 'WITHDRAW')
            ->where('status', 'pending')
            ->orderBy('created_at', 'asc')
            ->first();
    }

    /**
     * @param array $operations
     * @param int $items
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Query\Builder[]|Collection
     */
    public function getOperationLast(array $operations, $items = 10) {
        return $this->model::query()
            ->select(['id', 'operation', 'amount', 'default_amount', 'created_at', 'user_id', 'payment_system_id'])
            ->whereIn('operation', $operations)
            ->where('status', 'completed')
            ->orderBy('id', 'desc')
            ->with(['user'])
            ->take($items)
            ->get();
    }

//    /**
//     * @param $users_id
//     * @param int $status
//     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Query\Builder[]|Collection|mixed
//     */
//    public function getDepositsWithStatus($users_id, $status = 0) {
//        $res = $this->model::query()
//            ->select(['users__operations.deposit_id', 'users__operations.operation', 'users__operations.status', 'users__operations.user_id', 'deposit__users.status as deposit_status'])
//            ->where('users__operations.operation', 'CREATE_DEPOSIT')
//            ->where('users__operations.status', 'completed')
//            ->whereIn('users__operations.user_id', $users_id)
//            ->leftJoin('deposit__users', 'users__operations.deposit_id', '=', 'deposit__users.id')
//            ->groupBy('users__operations.deposit_id')
//            ->having('deposit_status', $status)
//            ->get();
//        return $res;
//    }
}