<?php

namespace Emitters\UsersOperations;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Collection;
use Emitters\Contracts\PaymentSystems\PaymentSystemsContract;
use Emitters\Contracts\UsersOperations\OperationRepositoryContract;
use Emitters\Contracts\UsersOperations\UsersOperationsContract;
use Emitters\Contracts\ConvertingCurrencyRate\ConvertingCurrencyRateManagerContract;

class UsersOperations implements UsersOperationsContract {
    /**
     * @var OperationRepositoryContract
     */
    protected $repository;

    /**
     * @var PaymentSystemsContract
     */
    protected $payment_systems;
    /**
     * @var ConvertingCurrencyRateManagerContract
     */
    protected $converting_currency_rate;

    /**
     * UsersOperations constructor.
     *
     * @param                                       $repository
     * @param PaymentSystemsContract $payment_systems
     * @param ConvertingCurrencyRateManagerContract $converting_currency_rate
     */
    public function __construct(OperationRepositoryContract $repository, PaymentSystemsContract $payment_systems, ConvertingCurrencyRateManagerContract $converting_currency_rate) {
        $this->repository               = $repository;
        $this->payment_systems          = $payment_systems;
        $this->converting_currency_rate = $converting_currency_rate;
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function updateOperation($id, array $data) {
        if(array_key_exists('user', $data)) {
            $data['user_id'] = User::query()->where('email', $data['user'])->first()->id;
            unset($data['user']);
        }
        $data['created_at'] = Carbon::createFromFormat(config('emitters.operation_date_format'), $data['created_at']);
        $data['updated_at'] = Carbon::createFromFormat(config('emitters.operation_date_format'), $data['updated_at']);

        $data['amount']         = convert_to_bigInteger($data['amount']);
        $data['amount']         = convert_to_bigInteger($data['amount']);
        $data['default_amount'] = convert_to_bigInteger($data['default_amount']);
        if(array_key_exists('from_amount', $data)){
            $data['from_amount'] = convert_to_bigInteger($data['from_amount']);
        }
        return $this->repository->update($id, $data);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteOperation($id) {
        return $this->repository->delete($id);
    }

    /**
     * @param $user
     * @param $payment_system_id
     * @param $amount
     *
     * @param int $plan_id
     * @param null $transaction
     * @param int $hidden
     * @return mixed
     */
    public function createOperationAddFunds($user, $payment_system_id, $amount, $plan_id = 0, $transaction = null, $hidden = 0) {
        $paymentSystem = $this->payment_systems->getOne($payment_system_id);
        $data          = [
            'type'              => 'buy',
            'operation'         => 'ADD_FUNDS',
            'user_id'           => $user->id,
            'plan_id'           => $plan_id,
            'payment_system_id' => $payment_system_id,
            'status'            => 'pending',
            'amount'            => $amount,
            'transaction'       => $transaction,
            'default_amount'    => $this->converting_currency_rate->convertAmount($amount, $paymentSystem->currency, config('emitters.default_currency_systems')),
            'hidden'            => $hidden
        ];
        return $this->repository->create($data);
    }

    /**
     * @param $user
     * @param $payment_system_id
     * @param $from_payment_system_id
     * @param $amount
     * @param $from_amount
     *
     * @return mixed
     */
    public function createOperationExchange($user, $payment_system_id, $from_payment_system_id, $amount, $from_amount) {
        $paymentSystem = $this->payment_systems->getOne($payment_system_id);

        $data = [
            'type'                   => 'buy',
            'operation'              => 'EXCHANGE',
            'user_id'                => $user->id,
            'payment_system_id'      => $payment_system_id,
            'from_payment_system_id' => $from_payment_system_id,
            'status'                 => 'completed',
            'amount'                 => $amount,
            'from_amount'            => $from_amount,
            'default_amount'         => $this->converting_currency_rate->convertAmount($amount, $paymentSystem->currency, config('emitters.default_currency_systems'))
        ];
        return $this->repository->create($data);
    }

    /**
     * @param $user
     * @param $sell_id
     * @param $payment_system_id
     * @param $amount
     * @param $commission
     *
     * @return mixed
     */
    public function createOperationWithdraw($user, $sell_id, $payment_system_id, $amount, $commission) {
        $paymentSystem = $this->payment_systems->getOne($payment_system_id);
        $data          = [
            'type'              => 'sell',
            'operation'         => 'WITHDRAW',
            'parent_id'         => $sell_id,
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system_id,
            'status'            => 'pending',
            'amount'            => $amount,
            'default_amount'    => $this->converting_currency_rate->convertAmount($amount, $paymentSystem->currency, config('emitters.default_currency_systems')),
            'commission'        => $commission
        ];
        return $this->repository->create($data);
    }

    /**
     * @param $user
     * @param int $parent_id
     * @param int $amount
     * @param null $payment_system_id
     * @param string $status
     * @param int $is_hold
     * @return mixed
     */
    public function createOperationBonus($user, $parent_id = 0, $amount = 0, $payment_system_id = null, $status = 'completed', $is_hold = 1) {
        if(!$payment_system_id) {
            $payment_system_id = config('emitters.default_payment_system_id');
        }
        if($amount == 0) {
            $amount = df_mul(1, (string)100000000, 0);
        }
        $paymentSystem = $this->payment_systems->getOne($payment_system_id);
        $data          = [
            'type'              => 'buy',
            'operation'         => 'BONUS',
            'parent_id'         => $parent_id,
            'user_id'           => $user->id,
            'amount'            => $amount,
            'status'            => $status,
            'payment_system_id' => $payment_system_id,
            'is_hold'           => $is_hold,
            'default_amount'    => ($paymentSystem) ? $this->converting_currency_rate->convertAmount($amount, $paymentSystem->currency, config('emitters.default_currency_systems')) : 0,
        ];
        return $this->repository->create($data);
    }

    /**
     * @param $user
     * @param int $parent_id
     * @param int $amount
     * @param null $payment_system_id
     * @param int $level
     * @param $from_user_id
     * @param int $deposit_id
     * @param int $from_amount
     * @return mixed
     */
    public function createOperationRefferal($user, $parent_id = 0, $amount = 0, $payment_system_id = null, $level = 0, $from_user_id, $deposit_id = 0, $from_amount = 0) {
        $paymentSystem = $this->payment_systems->getOne($payment_system_id);
        $data          = [
            'type'              => 'buy',
            'operation'         => 'REFFERAL',
            'parent_id'         => $parent_id,
            'user_id'           => $user->id,
            'amount'            => $amount,
            'default_amount'    => ($paymentSystem) ? $this->converting_currency_rate->convertAmount($amount, $paymentSystem->currency, config('emitters.default_currency_systems')) : 0,
            'status'            => 'completed',
            'payment_system_id' => $payment_system_id,
            'level'             => $level,
            'from_user_id'      => $from_user_id,
            'from_amount'       => $from_amount,
            'deposit_id'        => $deposit_id,
        ];
        return $this->repository->create($data);
    }

    /**
     * @param $user
     * @param $buy_id
     * @param $deposit_id
     * @param $payment_system_id
     * @param $amount
     *
     * @param $from_amount
     * @param $data_info
     *
     * @return mixed
     */
    public function createOperationAccruals($user, $buy_id, $deposit_id, $payment_system_id, $amount, $from_amount, $data_info) {
        $paymentSystem = $this->payment_systems->getOne($payment_system_id);
        $data          = [
            'type'              => 'buy',
            'operation'         => 'ACCRUALS',
            'parent_id'         => $buy_id,
            'deposit_id'        => $deposit_id,
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system_id,
            'status'            => 'completed',
            'amount'            => $amount,
            'from_amount'       => $from_amount,
            'default_amount'    => $this->converting_currency_rate->convertAmount($amount, $paymentSystem->currency, config('emitters.default_currency_systems')),
            'data_info'         => $data_info,
        ];
        return $this->repository->create($data);
    }

    /**
     * @param $user
     * @param $buy_id
     * @param $deposit_id
     * @param $payment_system_id
     * @param $amount
     * @param $from_amount
     * @param $data_info
     *
     * @return mixed
     */
    public function createOperationDepositClose($user, $buy_id, $deposit_id, $payment_system_id, $amount, $from_amount, $data_info) {
        $paymentSystem = $this->payment_systems->getOne($payment_system_id);
        $data          = [
            'type'              => 'buy',
            'operation'         => 'DEPOSIT_CLOSE',
            'parent_id'         => $buy_id,
            'deposit_id'        => $deposit_id,
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system_id,
            'status'            => 'completed',
            'amount'            => $amount,
            'from_amount'       => $from_amount,
            'default_amount'    => $this->converting_currency_rate->convertAmount($amount, $paymentSystem->currency, config('emitters.default_currency_systems')),
            'data_info'         => $data_info,
        ];
        return $this->repository->create($data);
    }

    /**
     * @param $user
     * @param $sell_id
     * @param $plan_id
     * @param $deposit_id
     * @param $payment_system_id
     * @param $amount
     * @return mixed
     */
    public function createOperationCreateDeposit($user, $sell_id, $plan_id, $deposit_id, $payment_system_id, $amount) {
        $paymentSystem = $this->payment_systems->getOne($payment_system_id);
        $data          = [
            'type'              => 'sell',
            'plan_id'           => $plan_id,
            'operation'         => 'CREATE_DEPOSIT',
            'parent_id'         => $sell_id,
            'deposit_id'        => $deposit_id,
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system_id,
            'default_amount'    => $this->converting_currency_rate->convertAmount($amount, $paymentSystem->currency, config('emitters.default_currency_systems')),
            'status'            => 'completed',
            'amount'            => $amount
        ];
        return $this->repository->create($data);
    }

    /**
     * @param $user
     * @param $amount
     * @param $payment_system_id
     * @param string $status
     * @return mixed
     */
    public function createOperationPenalty($user, $amount, $payment_system_id, $status = 'pending') {
        $paymentSystem = $this->payment_systems->getOne($payment_system_id);
        $data          = [
            'type'              => 'sell',
            'operation'         => 'PENALTY',
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system_id,
            'status'            => $status,
            'amount'            => $amount,
            'default_amount'    => $this->converting_currency_rate->convertAmount($amount, $paymentSystem->currency, config('emitters.default_currency_systems')),
        ];
        return $this->repository->create($data);
    }


    /**
     * @param $id
     *
     * @return mixed
     */
    public function getById($id) {
        return $this->repository->first($id);
    }

    /**
     * @param $transaction
     *
     * @return mixed
     */
    public function getByTransaction($transaction) {
        return $this->repository->getByTwo('transaction', $transaction, 'status', 'completed');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getOperationToAddFunds($id) {
        return $this->repository->getByThird('id', $id, 'operation', 'ADD_FUNDS', 'status', 'pending');
    }

    /**
     * @param       $user
     * @param array $operations
     * @param array $status
     *
     * @return mixed
     */
    public function getUserOperations($user, array $operations = [], array $status = []) {
        $filter            = [];
        $f_with_user       = ['field' => 'user_id', 'value' => $user->id];
        $filter['where'][] = $f_with_user;
        if($operations) {
            $filter['whereIn'][] = ['field' => 'operation', 'value' => $operations];
        }
        if($status) {
            $filter['whereIn'][] = ['field' => 'status', 'value' => $status];
        }
        return $this->getOperationsWithFilter($filter);
    }

    /**
     * @param array $filter
     *
     * @param bool $no_paginate
     * @return mixed
     */
    public function getOperationsWithFilter(array $filter = [], $no_paginate = false) {
        $operations = $this->repository->get($filter, $no_paginate);
        $operations->toSmallAmount()->bindPaymentSystems();
        return $operations;
    }

    /**
     * @param $operation
     *
     * @return mixed
     */
    public function getByOperation($operation) {
        return $this->repository->getByOperation($operation);
    }

    /**
     * @param       $user
     * @param       $deposit_id
     *
     * @return mixed
     */
    public function getCountAccruals($user, $deposit_id) {
        return $this->repository->getCount($user, ['ACCRUALS'], ['deposit_id', $deposit_id], ['completed']);
    }

    /**
     * @param $deposit_id
     *
     * @return
     */
    public function getOperationDepositClose($deposit_id) {
        return $this->repository->getByTwo('operation', 'DEPOSIT_CLOSE', 'deposit_id', $deposit_id);
    }

    /**
     * @param $user
     * @param $deposit_id
     *
     * @return mixed
     */
    public function getLastAccruals($user, $deposit_id) {
        return $this->repository->getByThird('operation', 'ACCRUALS', 'user_id', $user->id, 'deposit_id', $deposit_id, 'created_at', 'desc');
    }

    /**
     * @param       $user
     * @param array $user_ids
     *
     * @return mixed
     */
    public function getAffiliateBonusesFromUsers($user, array $user_ids = []) {
        //TODO: fix here for test
//        $sums            = $this->repository->getSumOperations($user->id, $user_ids, ['REFFERAL']);
//        $payment_systems = $this->payment_systems->get();
//        $groupByLevel    = $sums->transform(function ($item) use ($payment_systems) {
//            $amount_default = 0;
//            $payment_system = null;
//            $find_ps        = $payment_systems->where('id', $item->payment_system_id)->first();
//            if($find_ps) {
//                $payment_system = $find_ps;
//                $amount_default = $this->converting_currency_rate->convertAmount($item->aggregate, $find_ps->currency, config('emitters.default_currency_systems'));
//            }
//            $item->payment_system = $payment_system;
//            $item->amount_default = $amount_default;
//            unset($item->payment_system_id);
//            return $item;
//        })->groupBy('level');
//        return $groupByLevel;
    }

    /**
     * @param $user
     * @param $max_level
     * @return array
     */
    public function getCommissionAndActiveUsers($user, $max_level) {
        $refs    = $this->repository->getCommissionByReferrals($user->id, $max_level);
        $actives = $this->repository->getActiveUsersInStructure($user->id, $max_level);
        return [$refs, $actives];
    }

    /**
     * @param array $investors
     * @return array
     */
    public function sumInvestsByRefs(array $investors) {
        $total = $this->repository->getTotalSumDepositsByUser($investors);
        return $total;

//        $paymentSystems = $this->payment_systems->get();
//        $response       = [];
//        foreach($investors as $investor) {
//            $total          = $this->repository->getSumOperations($investor, [], ['CREATE_DEPOSIT'], 'sell');
//            $groupedByLevel = $total->transform(function ($item) use ($paymentSystems) {
//                $amount_default = $item->aggregate;
//                $payment_system = null;
//                $find_ps        = $paymentSystems->where('id', $item->payment_system_id)->first();
//                if($find_ps) {
//                    $payment_system = $find_ps;
//                    $sum            = convert_from_bigInteger($item->aggregate, $find_ps->scale);
//                    $amount_default = $this->converting_currency_rate->convertAmount($sum, $find_ps->currency, config('emitters.default_currency_systems'));
//                }
//                $item->amount_default = $amount_default;
//                $item->payment_system = $payment_system;
//                unset($item->payment_system_id);
//                return $item;
//            })->groupBy('level');
//            $groupedByLevel->each(function ($item, $lvl) use (&$response) {
//                $lvl            = 'lvl_' . $lvl;
//                $response[$lvl] = $item->sum('amount_default');
//            });
//        }
//        dd($response);
//        return $response;
    }

    /**
     * @param $depositID
     * @return mixed
     */
    public function getSumOfAccrualsByUserDeposit($depositID) {
        return $this->repository->getSumOfAccrualsByDeposit($depositID);
    }

    public function getSumOfDeposits($deposits) {
        $ids            = $deposits->pluck('id')->toArray();
        $sums           = $this->repository->getTotalSumOfDeposits($ids);
        $paymentSystems = $this->payment_systems->get();

        $activeSum = 0;
        $totalSum  = 0;

        $sums->each(function ($item) use ($paymentSystems, $deposits, &$activeSum, &$totalSum) {
            $ps      = $paymentSystems->where('id', $item->payment_system_id)->first();
            $deposit = $deposits->where('id', $item->deposit_id)->first();
            $sum     = $this->converting_currency_rate->convertAmount($item->aggregate, $ps->currency, config('emitters.default_currency_systems'));
            $sum     = convert_from_bigInteger($sum, 2);

            $totalSum += $sum;

            if($deposit->status == 0) {
                $activeSum += $sum;
            }
        });
        return [
            'total'  => number($totalSum),
            'active' => number($activeSum)
        ];
    }

    public function getSumOfWithdraws($user) {
        $lastWithDraw = null;
        $completedSum = $this->repository->getSumOfOperation('WITHDRAW', $user->id);
        if(count($completedSum) > 0) {
            $completedSum = convert_from_bigInteger($completedSum->first()->total);
        } else {
            $completedSum = 0;
        }

        $pendingSum = $this->repository->getOperationWithdrawPending($user->id);
        if($pendingSum) {
            $pendingSum = collect([$pendingSum]);
            $pendingSum->toSmallAmount()->bindPaymentSystems();

            $lastWithDraw = collect();
            $lastWithDraw->put('approximately_accrual_time', Carbon::parse($pendingSum[0]['approximately_accrual_time'])->timestamp);
            $ps         = $pendingSum[0]['payment_system'];
            $pendingSum = [
                'amount'   => number($pendingSum[0]['amount'], $ps['scale']),
                'currency' => $ps['currency']
            ];

//            $pendingSum->approximately_accrual_time = Carbon::createFromFormat('d/m/Y H:i:s', $pendingSum->created_at)->addHours(24)->timestamp;
        } else {
            $pendingSum = [
                'amount'   => number(0),
                'currency' => 'USD'
            ];
        }
        return [
            'completed'     => number($completedSum),
            'pending'       => $pendingSum,
            'last_withdraw' => $lastWithDraw
        ];
    }

    /**
     * @param array $operations
     * @param $periodFrom
     * @param $periodTo
     * @param bool $withHidden
     * @return Collection
     */
    public function getOperationsByPeriod(array $operations, $periodFrom, $periodTo, $withHidden = false): Collection {
        $periodFrom = Carbon::parse($periodFrom);
        $periodTo   = Carbon::parse($periodTo);
        return $this->repository->getOperationsBetweenPeriod($operations, $periodFrom, $periodTo, $withHidden);
    }

    /**
     * @param string $operation
     * @param null $user_id
     * @return mixed
     */
    public function getSumOfOperation(string $operation, $user_id = null) {
        return $this->repository->getSumOfOperation($operation, $user_id);
    }

    /**
     * @param array $users_id
     * @return mixed
     */
    public function getSumUsersDeposits(array $users_id) {
        return $this->repository->getSumByUsersAndOperations($users_id, 'CREATE_DEPOSIT');
    }

    /**
     * @param array $users_id
     * @return int|mixed
     */
    public function getSumUsersActiveDeposits(array $users_id) {
        $deposits = resolve('libair.depositusers')->getDepositUsersByStatus($users_id);
        if(count($deposits) == 0) {
            return 0;
        }
        $res = $this->repository->getSumByDeposits($deposits->pluck('id')->toArray());
        return $res->total;
    }

    /**
     * @param $deposits_ids
     * @return mixed
     */
    public function getTotalAccrualsByDeposits($deposits_ids) {
        return $this->repository->getTotalAccrualsByDeposits($deposits_ids);
    }

    /**
     * @param $deposit_id
     * @return mixed
     */
    public function getOperationsDeposit($deposit_id) {
        $filter            = [];
        $f_with_user       = ['field' => 'deposit_id', 'value' => $deposit_id];
        $filter['where'][] = $f_with_user;
//        $filter['whereIn'][] = ['field' => 'status', 'value' => ['completed']];
        return $this->getOperationsWithFilter($filter);
    }

    /**
     * @param $address
     * @param null $destination_tag
     * @return mixed
     */
    public function getOperationByPaymentAddress($address, $destination_tag = null) {
        return $this->repository->getOperationByPaymentAddress($address, $destination_tag);
    }

    /**
     * @param $user
     * @param $users_ids
     * @return mixed
     */
    public function getCommissionByUsersId($user, $users_ids) {
        return $this->repository->getCommissionByUsersId($user->id, $users_ids);
    }

    /**
     * @param array $operations
     * @param int $max_item
     * @return mixed
     */
    public function getOperationLast(array $operations, $max_item = 10) {
        return $this->repository->getOperationLast($operations, $max_item);
    }

    /**
     * @param $operation_id
     * @param $user_id
     * @param array $operations
     * @return mixed
     */
    public function getOperationByUserAndId($operation_id, $user_id, array $operations = []) {
        $filter            = [];
        $f_with_user       = ['field' => 'user_id', 'value' => $user_id];
        $f_with_id         = ['field' => 'id', 'value' => $operation_id];
        $filter['where'][] = $f_with_user;
        $filter['where'][] = $f_with_id;
        if($operations) {
            $filter['whereIn'][] = ['field' => 'operation', 'value' => $operations];
        }
        return $this->getOperationsWithFilter($filter);
    }
}
