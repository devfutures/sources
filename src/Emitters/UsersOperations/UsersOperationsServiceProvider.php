<?php

namespace Emitters\UsersOperations;

use Emitters\UsersOperations\Models\Users_Operations;
use Emitters\UsersOperations\Repositories\OperationsRepository;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory as EloquentFactory;
class UsersOperationsServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/migrations');

        $this->publishes([
            __DIR__.'/migrations/' => database_path('migrations')
        ], 'migrations');

        $this->app->make(EloquentFactory::class)->load(__DIR__ . '/factories/');
    }

    public function register()
    {
        $this->app->bind(OperationsRepository::class, function ($app) {
            return new OperationsRepository(new Users_Operations());
        });
        $this->app->alias(OperationsRepository::class, 'operations_repo');

        $this->app->bind(UsersOperations::class, function ($app) {
            return new UsersOperations(resolve('operations_repo'), paymentsystems(), converting_currency_rate());
        });
        $this->app->alias(UsersOperations::class, 'users_operations');

        $this->app->bind(ControlOperations::class, function ($app) {
            return new ControlOperations(resolve('operations_repo'));
        });
    }
}
