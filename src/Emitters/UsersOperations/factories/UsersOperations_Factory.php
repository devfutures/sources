<?php

use Faker\Generator as Faker;

$factory->define(\Emitters\UsersOperations\Models\Users_Operations::class, function (Faker $faker) {
    $statuses = $faker->randomElement(['pending', 'completed', 'cancel', 'error']);

    $operations = [
        'buy'  => [
            'ADD_FUNDS',
            'CREATE_DEPOSIT',
            'BONUS',
            'ACCRUALS',
            'REFFERAL'
        ],
        'sell' => [
            'WITHDRAW',
            'PENALTY'
        ]
    ];

    $ps             = \Emitters\PaymentSystems\Models\Payment_System::all();
    $paymentSystems = $ps->isNotEmpty() ? $ps : collect([factory(\Emitters\PaymentSystems\Models\Payment_System::class)->create()]);
    $system         = $paymentSystems->random();
    $users          = \App\User::all();
    $user_id        = $users->random(1)->first();
    $from_user_id   = $users->random(1)->first();
    $type           = $faker->randomKey($operations);
    $operation      = $faker->randomElement($operations[$type]);
    $amount         = $faker->numberBetween();
    $defaultAmount  = converting_currency_rate()->convertAmount($amount, $system->currency, config('emitters.default_currency_systems'));

    return [
        'type'              => $type,
        'operation'         => $operation,
        'parent_id'         => 0,
        'deposit_id'        => 0,
        'user_id'           => function () use ($user_id) {
            return $user_id->id ?? factory('App\User')->create()->id;
        },
        'from_user_id'      => ($operation == 'REFFERAL') ? function () use ($from_user_id) {
            return $from_user_id->id ?? factory('App\User')->create()->id;
        } : 0,
        'payment_system_id' => $system->id,
        'amount'            => $amount,
        'default_amount'    => $defaultAmount,
        'status'            => $statuses,
        'transaction'       => (in_array($operation, ['ADD_FUNDS', 'WITHDRAW'])) ? \Illuminate\Support\Str::random(32) : null,
        'data_info'         => (in_array($operation, ['ADD_FUNDS', 'WITHDRAW'])) ? $faker->creditCardDetails() : null,
        'level'             => ($operation == 'REFFERAL') ? 1 : 0,
        'hidden'            => $faker->numberBetween(0, 1),
        'created_at'        => \Carbon\Carbon::now()->subDays(rand(0, \Carbon\Carbon::now()->format('d')))
    ];
});