<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users__operations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type')->nullable()->index();
            $table->string('operation')->nullable()->index();
            $table->integer('parent_id')->default(0)->index();
            $table->integer('deposit_id')->default(0)->index();
            $table->integer('plan_id')->default(0)->index();
            $table->integer('user_id')->index();
            $table->integer('from_user_id')->default(0)->index();
            $table->integer('payment_system_id')->default(0)->index();
            $table->integer('from_payment_system_id')->default(0)->index();
            $table->biginteger('amount')->default(0);
            $table->biginteger('from_amount')->default(0);
            $table->biginteger('default_amount')->default(0);
            $table->bigInteger('commission')->default(0);
            $table->string('status')->default("pending");
            $table->string('transaction')->nullable();
            $table->json('data_info')->nullable();
            $table->integer('level')->default(0)->index();
            $table->integer('is_hold')->default(0)->index();
            $table->tinyInteger('hidden')->default(0)->index();
            $table->text('operation_note')->nullable();
            $table->integer('user_created')->default(0)->index();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users__operations');
    }
}
