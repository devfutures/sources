<?php


use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminControlControllerTest extends \Tests\TestCase {
    use RefreshDatabase;

    /** @var \Emitters\AdminControl\AdminRoles */
    protected $rolesManager;

    protected function setUp(): void {
        parent::setUp();
        $this->rolesManager = resolve('admin_role');
    }

    public function testControllerCreateRole() {
        $this->withJWT();
        $menuAccess    = new \Emitters\AdminControl\MenuAccess();
        $falseSections = $menuAccess->getSections();
        $data          = [
            'name'     => 'test_role',
            'status'   => 1,
            'sections' => $falseSections
        ];
        $response      = $this->json('POST', route('admin_control.addRole'), $data);
        $response->assertStatus(200);
    }

    public function testControllerGetAllRoles() {
        $this->withJWT();
        factory(\Emitters\AdminControl\Models\AdminRole::class, 5)->create();
        $response = $this->json('GET', route('admin_control.index'));
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'code',
            'roles',
            'sections'
        ]);
    }

    public function testControllerSetRoleToUser() {
        $this->withJWT();
        $role = factory(\Emitters\AdminControl\Models\AdminRole::class)->create();
        $user = factory(\App\User::class)->create();
        $this->json('POST', route('admin_control.setRoleToUser'), [
            'role_id' => $role->id,
            'user_id' => $user->id
        ]);
        $user = $user->fresh();
        $this->assertEquals($user->role_id, $role->id);
    }

    public function testControllerUpdateRole() {
        $this->withJWT();
        /** @var \Emitters\AdminControl\Models\AdminRole $role */
        $role     = factory(\Emitters\AdminControl\Models\AdminRole::class)->create();
        $roleData = factory(\Emitters\AdminControl\Models\AdminRole::class)->make();
        $request  = $this->json('PATCH', route('admin_control.updateRole', ['id' => $role->id]), $roleData->toArray());
        $request->assertStatus(200);
        $role = $role->fresh();
        $attr = $role->getFillable();
        foreach ($attr as $att) {
            $this->assertEquals($role->$att, $roleData->$att);
        }
    }

    public function testControllerDeleteRole() {
        $this->withJWT();
        $role    = factory(\Emitters\AdminControl\Models\AdminRole::class)->create();
        $request = $this->json('DELETE', route('admin_control.deleteRole', ['id' => $role->id]));
        $request->assertStatus(200);
        $role = $role->fresh();
        $this->assertTrue($role->trashed());
    }

    public function testControllerDeleteRoleThatAttachedToUser() {
        $this->withJWT();
        $role    = factory(\Emitters\AdminControl\Models\AdminRole::class)->create();
        $user = factory(\App\User::class)->create(['role_id' => $role->id]);
        $response = $this->json('DELETE', route('admin_control.deleteRole', ['id' => $role->id]));
        $response->assertStatus(422);
    }

    public function testControllerDetachRoleFromUser() {
        $this->withJWT();
        $role    = factory(\Emitters\AdminControl\Models\AdminRole::class)->create();
        $user = factory(\App\User::class)->create(['role_id' => $role->id]);
        $response = $this->json('POST', route('admin_control.detachUserRole'), ['user_id' => $user->id]);
        $response->assertStatus(200);
        $user = $user->fresh();
        $this->assertTrue($user->role_id == null);
    }

    public function testLoginWithUser() {
        $role = factory(\Emitters\AdminControl\Models\AdminRole::class)->create();
        $user = factory(\App\User::class)->create(['role_id' => $role]);
        $userForAuth = factory(\App\User::class)->create();
        $this->withJWT($user);
        $response = $this->json('POST', route('admin_control.loginWithUser'), [
            'user_id' => $userForAuth->id
        ]);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'token_for_access_under_user',
            'admin_token'
        ]);
    }
}