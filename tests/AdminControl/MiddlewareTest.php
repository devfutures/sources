<?php

use Illuminate\Foundation\Testing\RefreshDatabase;

class MiddlewareTest extends \Tests\TestCase {
    use RefreshDatabase;

    public function testAccessViaRoleMiddleware() {
         $this->withJWT();
         $role = factory(\Emitters\AdminControl\Models\AdminRole::class)->create();
         $user = factory(\App\User::class)->create(['role_id' => $role]);
         $response = $this->actingAs($user)->json('POST', route('depositplans.index'));
         $response->assertStatus(200);
    }

    public function testAccessViaRoleMiddlewareWithURIException() {
        $role = factory(\Emitters\AdminControl\Models\AdminRole::class)->create(['sections' => []]);
        $user = factory(\App\User::class)->create(['role_id' => $role]);
        $this->withJWT($user);
        $response = $this->json('POST', route('news.all'));
        $response->assertStatus(403);
    }
}