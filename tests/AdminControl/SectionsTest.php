<?php


use Illuminate\Foundation\Testing\RefreshDatabase;

class SectionsTest extends \Tests\TestCase {
    use RefreshDatabase;
    protected $lib;

    /**
     * BalanceManagerTest constructor.
     */
    public function setUp(): void {
        parent::setUp();
        $this->lib = resolve('admin_role');
    }

    public function test_get_empty_roles() {
        $roles = $this->lib->getRoles();
        $this->assertEmpty($roles);
    }

    public function test_create_role() {
        $role = factory('Emitters\AdminControl\Models\AdminRole')->make();
        $this->lib->createRole($role->toArray());
        $roles = $this->lib->getRoles();
        $this->assertSame($role->name, $roles[0]->name);
    }

    public function test_update_role(){
        $role_db = factory('Emitters\AdminControl\Models\AdminRole')->create();

        $this->lib->save_role($role_db->id, [
            'name' => 'Moderator', 'sections' => []
        ]);
        $this->assertDatabaseHas($role_db->getTable(), [
            'id' => $role_db->id,
            'name' => 'Moderator'
        ]);
    }
}