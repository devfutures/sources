<?php


use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SettingsTest extends TestCase {
    use RefreshDatabase;

    /** @var \Emitters\AdminControl\SiteSettings */
    protected $siteSettings;
    /** @var \Emitters\AdminControl\EnvManager */
    protected $envManager;

    public function setUp(): void {
        parent::setUp();
        $this->envManager   = resolve('env.manager');
        $this->siteSettings = resolve('site.settings');
    }

    public function testGetEnvVariables() {
        $return        = $this->envManager->getAvailableEnvironmentVariables('emitters.site_settings');
        $envKeyArr     = array_flip($return);
        $availableVars = config('emitters.site_settings');
        foreach ($availableVars as $var) {
            $this->assertArrayHasKey($var, $envKeyArr);
        }
    }

    public function testSetEnv() {
        config()->set('emitters.site_settings', ['FOR_TEST']);
        $envVar   = 'FOR_TEST';
        $getEnvironment = $this->siteSettings->getEnvironment();
        $oldValue = (!array_key_exists($envVar, $getEnvironment)) ? null : $getEnvironment[$envVar];
        $newValue = ($oldValue === false || $oldValue == 'false') ? "true" : "false";
        $envToSet = [
            [
                'env_var'     => $envVar,
                'env_var_val' => $newValue
            ]
        ];
        $result   = $this->siteSettings->setEnvVariable($envToSet);
        $this->assertTrue($result);
    }

    public function testSetEnvThatNotExistsInEnv() {
        config()->set('emitters.site_settings', ['FOR_TEST']);
        $envVar   = 'FOR_TEST';
        $envToSet = [
            [
                'env_var'     => $envVar,
                'env_var_val' => 'privet'
            ]
        ];
        $result   = $this->siteSettings->setEnvVariable($envToSet);
        $this->assertTrue($result);
    }

    public function testSetEnvThatNotInWhiteListAndGetFalse() {
        $envToSet = [
            [
                'env_var'     => 'WRONG_ENV',
                'env_var_val' => 'fail'
            ]
        ];
        $result   = $this->siteSettings->setEnvVariable($envToSet);
        $this->assertFalse($result);
    }

    public function testControllerSetEnvVariable() {
        config()->set('emitters.site_settings', ['FOR_TEST']);
        $this->withJWT();
        $response = $this->json('POST', route('admin_control.setEnv'), [
            [
                'env_var'     => 'FOR_TEST',
                'env_var_val' => 'true'
            ]
        ]);
        $response->assertStatus(200);
    }

    public function testControllerSetEnvWithWrongEnvVariable() {
        $this->withJWT();
        $response = $this->json('POST', route('admin_control.setEnv'), [
            [
                'env_var'     => 'WRONG_VAR',
                'env_var_val' => 'false'
            ]
        ]);
        $response->assertStatus(422);
    }

    public function testControllerGetEnv() {
        $this->withJWT();
        $response = $this->json('GET', route('admin_control.getEnv'));
        $response->assertStatus(200);
    }
}