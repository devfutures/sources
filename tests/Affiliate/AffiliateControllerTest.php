<?php


use Emitters\Affiliate\AffiliateManager;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Emitters\Affiliate\Models\Affiliate;

class AffiliateControllerTest extends \Tests\TestCase {
    use RefreshDatabase;
    /**
     * @var AffiliateManager
     */
    protected $lib;

    /**
     * BalanceManagerTest constructor.
     */
    public function setUp(): void{
        parent::setUp();
        $this->lib = resolve('affiliate');
    }

    public function testOpenPageWithDefaultData(){
        $this->withJWT();
        $response = $this->json('GET',route('affiliate.index'));
        $response->assertJsonStructure([
            'settings' => array_keys($this->lib->getAffiliateSettings()),
            'affiliate' => []
        ]);
        $response->assertStatus(200);
    }

    public function testOpenPageWithHaveData(){
        $this->withJWT();
        factory(Affiliate::class)->create();
        $response = $this->json('GET',route('affiliate.index'));
        $response->assertJsonStructure([
            'settings',
            'affiliate' => [
                ['name', 'condition', 'percents']
            ]
        ]);
        $response->assertStatus(200);
    }

    public function testAffiliateTotalSaveHasError(){
        $this->withJWT();
        $affiliate = factory(Affiliate::class)->make();
        $settings = $this->lib->getAffiliateSettings();
        $response = $this->json('POST',route('affiliate.save'),[
            'affiliates' => $affiliate,
            'settings'   => $settings
        ]);
        $response->assertStatus(422);
    }

    public function testSaveDataNeedAffiliateAndSettings(){
        $this->withJWT();
        $response = $this->json('POST',route('affiliate.save'),[

        ]);
        $response->assertStatus(422);
        $response->assertJsonCount(2,'errors');
    }

    public function testSaveDataNeedArrayToSave(){
        $this->withJWT();
        $response = $this->json('POST',route('affiliate.save'),[
            'affiliates' => 'string',
            'settings'   => 'string'
        ]);
        $response->assertStatus(422);
        $response->assertJsonCount(2,'errors');
    }

    public function testAffiliateTotalSave(){
        $this->withJWT();
        $affiliate = factory(Affiliate::class,1)->make();
        $settings = $this->lib->getAffiliateSettings();
        $response = $this->json('POST',route('affiliate.save'),[
            'affiliates' => $affiliate,
            'settings'   => $settings
        ]);
        $response->assertStatus(200);
    }
}