<?php


use App\User;
use Emitters\Affiliate\AffiliateStructures;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;

class AffiliateStructuresTest extends \Tests\TestCase {
    use RefreshDatabase;
    /**
     * @var AffiliateStructures
     */
    protected $lib;

    public function setUp(): void{
        parent::setUp();
        $this->lib = resolve('affiliate.structures');
    }

    public function testCreateAffiliateStructure() {
        $this->generateStructure();
        $this->assertSame(39, DB::table('users__affiliate__structures')->count());
    }

    public function generateStructure($user_id = 0) {
        $upline_id      = $user_id;
        $last_insert_id = 0;
        $i              = 0;
        while ($i <= 15) {
            if ($i == 1) $upline_id = $last_insert_id->id;
            if ($i == 3) $upline_id = $last_insert_id->id;
            if ($i == 9) $upline_id = $last_insert_id->id;
            if ($i == 12) $upline_id = $last_insert_id->id;

            $last_insert_id = factory(User::class)->create([
                'upline_id' => $upline_id,
                'role_id'   => 0
            ]);
            $this->lib->createUserStructures($last_insert_id);

            $i++;
        }
    }

    public function testGetStructureByUser() {
        $user = factory(User::class)->create();
        $this->generateStructure($user->id);
        $res = $this->lib->getUserAffiliateStructure($user, 1);
        $this->assertCount(1, $res);

        $res = $this->lib->getUserAffiliateStructure($user, 2);
        $this->assertCount(2, $res);

        $res = $this->lib->getUserAffiliateStructure($user, 3);
        $this->assertCount(6, $res);

        $res = $this->lib->getUserAffiliateStructure($user, 4);
        $this->assertCount(3, $res);

        $res = $this->lib->getUserAffiliateStructure($user, 5);
        $this->assertCount(4, $res);

        $res = $this->lib->getUserAffiliateStructure($user, 6);
        $this->assertCount(0, $res);
    }

    public function testMaxLevelIs() {
        $u    = factory(User::class)->create();
        $prev = $u->id;
        $i    = 0;
        while (true) {
            $user = factory(User::class)->create([
                'upline_id' => $prev,
                'role_id'   => 0
            ]);
            $prev = $user->id;
            if ($i > 5) break;
            $i++;
        }
        $path = [];
        $this->lib->getUserParentLevelPath($prev, $path);
        $this->assertCount(7, $path);

        $path                 = [];
        $this->lib->max_level = 4;
        $this->lib->getUserParentLevelPath($prev, $path);
        $this->assertCount(4, $path);
    }

    public function testSearchInStructure() {
        $user = factory(User::class)->create();
        $this->generateStructure($user->id);
        $partner = User::where('upline_id', $user->id)->orderBy('id', 'asc')->first();
        $res     = $this->lib->getUserAffiliateStructure($user, 1, ['search_partner' => $partner->login]);
        $this->assertCount(1, $res);
        $res = $this->lib->getUserAffiliateStructure($user, 1, ['search_partner' => str_random(4)]);
        $this->assertCount(0, $res);
    }

    public function testCommissionAndAddFundsStructures() {
        $user = factory(User::class)->create();
        $this->generateStructure($user->id);
        $partner = User::where('upline_id', $user->id)->orderBy('id', 'asc')->first();
        factory(Emitters\UsersOperations\Models\Users_Operations::class)->create([
            'type'         => 'buy',
            'user_id'      => $user->id,
            'operation'    => 'REFFERAL',
            'from_user_id' => $partner->id,
        ]);
        factory(Emitters\UsersOperations\Models\Users_Operations::class)->create([
            'type'              => 'buy',
            'user_id'           => $user->id,
            'operation'         => 'REFFERAL',
            'from_user_id'      => $partner->id,
            'payment_system_id' => 2
        ]);
        $res = $this->lib->getUserAffiliateStructure($user, 1)->toArray();
        $this->assertArrayHasKey('commission', $res['data'][0]);
        $this->assertArrayHasKey('commission_by_group', $res['data'][0]);
    }

    public function testGetAffiliateStatisticByUser() {
        $user      = factory(User::class)->create();
        $affiliate = factory(Emitters\Affiliate\Models\Affiliate::class)->create([
            'name'      => 'BY_CREATE_DEPOSIT',
            'condition' => 'NORMAL',
            'percents'  => [
                ['level' => 1, 'percentage' => [
                    ['from' => null, 'profit' => 10],
                ]],
                ['level' => 2, 'percentage' => [
                    ['from' => null, 'profit' => 2],
                ]],
                ['level' => 3, 'percentage' => [
                    ['from' => null, 'profit' => 2],
                ]]
            ]
        ]);
        $userRefs  = factory(User::class, 5)->create([
            'upline_id' => $user->id
        ]);
        $lvlKeys   = [];
        foreach ($affiliate->percents as $level) {
            array_push($lvlKeys, 'lvl_' . $level['level']);
        }
        $userRefs->each(function ($ref) use ($user) {
            $level = rand(1, 3);
            factory(\Emitters\Affiliate\Models\Users_Affiliate_Structure::class, 3)->create([
                'user_id'  => $user->id,
                'child_id' => $ref->id,
                'level'    => $level
            ]);
            factory(\Emitters\UsersOperations\Models\Users_Operations::class)->create([
                'user_id'      => $user->id,
                'from_user_id' => $ref->id,
                'operation'    => 'REFFERAL',
                'type'         => 'buy',
                'deposit_id'   => 0,
                'status'       => 'completed',
                'hidden'       => 0,
                'level'        => $level
            ]);

            factory(\Emitters\UsersOperations\Models\Users_Operations::class)->create([
                'user_id'   => $ref->id,
                'operation' => 'CREATE_DEPOSIT',
                'status'    => 'completed',
                'hidden'    => 0,
                'level'     => $level,
                'type'      => 'buy'
            ]);
        });
        //create default invest

        $result = $this->lib->getStatisticsByUser($user);
        foreach ($lvlKeys as $key) {
            $this->assertArrayHasKey($key, $result);
            $this->assertArrayHasKey('commission', $result[$key]);
            $this->assertArrayHasKey('active_refs', $result[$key]['commission']);
            $this->assertArrayHasKey('total_refs', $result[$key]['commission']);
            $this->assertArrayHasKey('commission_sum', $result[$key]['commission']);
            $this->assertArrayHasKey('invested', $result[$key]);
            $this->assertArrayHasKey('sum', $result[$key]['invested']);
        }
    }

//    public function testGetUsersTeamInfo() {
//        $user     = factory(User::class)->create([
//            'role_id' => null
//        ]);
//        $referral = factory(User::class)->create([
//            'upline_id' => $user->id,
//            'role_id'   => null
//        ]);
//        factory(\Emitters\Affiliate\Models\Users_Affiliate_Structure::class)->create([
//            'user_id'  => $user->id,
//            'child_id' => $referral->id,
//            'level'    => 1
//        ]);
//
//        $scndReferral = factory(User::class)->create([
//            'upline_id' => $referral->id,
//            'role_id'   => null
//        ]);
//
//        factory(\Emitters\Affiliate\Models\Users_Affiliate_Structure::class)->create([
//            'user_id'  => $user->id,
//            'child_id' => $scndReferral->id,
//            'level'    => 2
//        ]);
//
//        factory(\Emitters\Affiliate\Models\Users_Affiliate_Structure::class)->create([
//            'user_id'  => $referral->id,
//            'child_id' => $scndReferral->id,
//            'level'    => 1
//        ]);
//
//        $refSum      = 100000000;
//        $depositSum  = 200000000;
//        $withdrawSum = 150000000;
//
//        $refBonusOp = factory(\Emitters\UsersOperations\Models\Users_Operations::class)->create([
//            'type'           => 'buy',
//            'operation'      => 'REFFERAL',
//            'user_id'        => $user->id,
//            'from_user_id'   => $referral->id,
//            'amount'         => $refSum,
//            'default_amount' => $refSum,
//            'status'         => 'completed',
//            'level'          => 1,
//            'hidden'         => 0
//        ]);
//
//        $refDeposit = factory(\Emitters\UsersOperations\Models\Users_Operations::class)->create([
//            'type'           => 'buy',
//            'operation'      => 'CREATE_DEPOSIT',
//            'user_id'        => $referral->id,
//            'status'         => 'completed',
//            'hidden'         => 0,
//            'amount'         => $depositSum,
//            'default_amount' => $depositSum
//        ]);
//
//        $refWithdraw = factory(\Emitters\UsersOperations\Models\Users_Operations::class)->create([
//            'type'           => 'sell',
//            'operation'      => 'WITHDRAW',
//            'user_id'        => $referral->id,
//            'status'         => 'completed',
//            'hidden'         => 0,
//            'amount'         => $withdrawSum,
//            'default_amount' => $withdrawSum
//        ]);
//
//        $res = $this->lib->getUsersTeamInfo();
//        $this->assertEquals($res->first()->team_ref_bonus_sum, convert_from_bigInteger($refSum, 2));
//        $this->assertEquals($res->first()->team_withdraw_sum, convert_from_bigInteger($withdrawSum, 2));
//        $this->assertEquals($res->first()->team_deposit_sum, convert_from_bigInteger($depositSum, 2));
//        $this->assertEquals($res->first()->team_size, 2);
//    }
}