<?php


use Emitters\Affiliate\AffiliateManager;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AffiliateTest extends \Tests\TestCase {
    use RefreshDatabase;
    /**
     * @var AffiliateManager
     */
    protected $lib;

    /**
     * BalanceManagerTest constructor.
     */
    public function setUp(): void {
        parent::setUp();
        $this->lib = resolve('affiliate');
    }

    public function testGetAffiliateSettings(){
        $settings = $this->lib->getAffiliateSettings();
        $this->assertArrayHasKey('AFF_NEED_UPLINE', $settings);
        $this->assertArrayHasKey('AFF_RANDOM_UPLINE_SIGN_UP', $settings);
        $this->assertArrayHasKey('AFF_PAY_ACTIVE_REFERRAL', $settings);
        $this->assertArrayHasKey('AFF_NO_COMMISSION_BALANCE', $settings);
        $this->assertArrayHasKey('AFF_SET_PERSONAL_REFERRAL_BACK', $settings);
        $this->assertCount(5, $settings);
    }

    public function testGetEmptyAffiliateLevel(){
        $aff_level = $this->lib->getAffiliateLevels();
        $this->assertCount(0, $aff_level);
    }

    public function testGetNotEmptyAffiliateLevel(){
        $levels = factory(Emitters\Affiliate\Models\Affiliate::class)->create();
        $aff_level = $this->lib->getAffiliateLevels();
        $this->assertCount(1, $aff_level);
        $this->assertSame($levels->name, $aff_level[0]->name);
        $this->assertSame($levels->condition, $aff_level[0]->condition);
        $this->assertIsArray($aff_level[0]->percents);
    }

    public function testSaveAffiliateLevelsEmptyData(){
        $this->assertFalse($this->lib->saveAffiliateLevels([]));
    }

    /**
     * @expectedException \Emitters\Affiliate\Exceptions\AffiliateLevelsException
     * @expectedExceptionMessage ERROR_CREATE_LEVELS
     */
    public function testSaveAffiliateLevelsErrorData(){
        $this->assertFalse($this->lib->saveAffiliateLevels([
            'testdata' => false
        ]));
    }

    public function testSaveAffiliateLevels(){
        $data = factory(Emitters\Affiliate\Models\Affiliate::class, 1)->make();
        $this->lib->saveAffiliateLevels($data->toArray());
        $levels = $this->lib->getAffiliateLevels();
        $this->assertEquals($data[0]->name, $levels[0]->name);
        $this->assertEquals($data[0]->condition, $levels[0]->condition);
        $this->assertEquals($data[0]->percents, $levels[0]->percents);
    }

    public function testSaveAffiliateSettings(){
        $settings = $this->lib->getAffiliateSettings();
        $settings_no_original = [];
        foreach($settings as $key => $value){
            $settings_no_original[$key] = !$value;
        }
        $this->assertTrue($this->lib->saveSettings($settings_no_original));
    }
}