<?php


use App\User;
use Emitters\Affiliate\AffiliateStructures;
use Emitters\Contracts\Affiliate\PartnershipAccrualContract;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;

class PartnershipAccrualTest extends \Tests\TestCase {
    use RefreshDatabase;
    /**
     * @var PartnershipAccrualContract
     */
    protected $lib;

    public function setUp(): void {
        parent::setUp();
        $this->lib = partnership_accrual();
    }

    public function generateStructure() {
        $user    = factory(App\User::class)->create([
            'role_id' => null
        ]);
        $user2   = factory(App\User::class)->create([
            'upline_id' => $user->id
        ]);
        $user3   = factory(App\User::class)->create([
            'upline_id' => $user2->id
        ]);
        $user4   = factory(App\User::class)->create([
            'upline_id' => $user3->id
        ]);
        $user5   = factory(App\User::class)->create([
            'upline_id' => $user3->id
        ]);
        $aff_str = resolve('affiliate.structures');
        $aff_str->createUserStructures($user);
        $aff_str->createUserStructures($user2);
        $aff_str->createUserStructures($user3);
        $aff_str->createUserStructures($user4);
        $aff_str->createUserStructures($user5);
        return [$user, $user2, $user3, $user4, $user5];
    }

    public function testAffiliateNoNotHaveSettings() {
        $this->assertNotTrue($this->lib->affiliateBonus('BY_DEPOSIT', null, null, null, null));
        $this->assertNotTrue($this->lib->affiliateBonus('ADD_FUNDS', null, null, null, null));
        $this->assertNotTrue($this->lib->affiliateBonus('BY_PROFIT', null, null, null, null));
    }

    public function testIfOneProfitValueZero() {
        $test = factory(Emitters\Affiliate\Models\Affiliate::class)->create([
            'name'      => 'BY_PROFIT',
            'condition' => 'NORMAL',
            'percents'  => [
                ['level' => 1, 'percentage' => [
                    ['from' => null, 'profit' => null],
                ]]
            ]
        ]);
        $this->assertNotTrue($this->lib->affiliateBonus($test->name, null, null, null, null));
    }

    public function testNormal() {
        list($user, $user2, $user3, $user4) = $this->generateStructure();
        $test              = factory(Emitters\Affiliate\Models\Affiliate::class)->create([
            'name'      => 'BY_PROFIT',
            'condition' => 'NORMAL',
            'percents'  => [
                ['level' => 1, 'percentage' => [
                    ['from' => null, 'profit' => 10],
                ]],
                ['level' => 2, 'percentage' => [
                    ['from' => null, 'profit' => 2],
                ]]
            ]
        ]);
        $payment_system_id = paymentsystems()->grabAll()->first()->id;
        $deposit_id        = 2;
        config(['emitters_affiliate.aff_pay_active_referral' => false]);
        $this->lib->affiliateBonus($test->name, $user4, convert_to_bigInteger(10, 0), $deposit_id, $payment_system_id);
        $this->assertDatabaseHas('users__balances', [
            'type'              => 'buy',
            'operation'         => 'REFFERAL',
            'user_id'           => $user3->id,
            'payment_system_id' => $payment_system_id,
            'amount'            => convert_to_bigInteger(1),
            'level'             => 1,
        ]);
        $this->assertDatabaseHas('users__operations', [
            'type'              => 'buy',
            'operation'         => 'REFFERAL',
            'user_id'           => $user3->id,
            'payment_system_id' => $payment_system_id,
            'amount'            => convert_to_bigInteger(1),
            'level'             => 1,
            'from_amount'       => convert_to_bigInteger(10, 0),
            'deposit_id'        => $deposit_id,
        ]);

        $this->assertDatabaseHas('users__balances', [
            'type'      => 'buy',
            'operation' => 'REFFERAL',
            'user_id'   => $user2->id,

            'payment_system_id' => $payment_system_id,
            'amount'            => convert_to_bigInteger(0.2),
            'level'             => 2,
        ]);
        $this->assertDatabaseHas('users__operations', [
            'type'              => 'buy',
            'operation'         => 'REFFERAL',
            'user_id'           => $user2->id,
            'payment_system_id' => $payment_system_id,
            'amount'            => convert_to_bigInteger(0.2),
            'level'             => 2,
            'from_amount'       => convert_to_bigInteger(10, 0),
            'deposit_id'        => $deposit_id,
        ]);

        $this->assertDatabaseMissing('users__balances', [
            'type'      => 'buy',
            'operation' => 'REFFERAL',
            'user_id'   => $user->id,

            'payment_system_id' => $payment_system_id,
            'level'             => 3,
        ]);
        $this->assertDatabaseMissing('users__operations', [
            'type'              => 'buy',
            'operation'         => 'REFFERAL',
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system_id,
            'level'             => 3,
            'from_amount'       => convert_to_bigInteger(10, 0),
            'deposit_id'        => $deposit_id,
        ]);
    }

    public function testOnlyPayCommissionActiveReferrals() {
        list($user, $user2, $user3, $user4) = $this->generateStructure();
        $test              = factory(Emitters\Affiliate\Models\Affiliate::class)->create([
            'name'      => 'BY_PROFIT',
            'condition' => 'NORMAL',
            'percents'  => [
                ['level' => 1, 'percentage' => [
                    ['from' => null, 'profit' => 10],
                ]],
                ['level' => 2, 'percentage' => [
                    ['from' => null, 'profit' => 2],
                ]]
            ]
        ]);
        $payment_system_id = paymentsystems()->grabAll()->first()->id;
        $deposit_id        = 2;
        factory(Emitters\UsersOperations\Models\Users_Operations::class)->create([
            'operation' => 'CREATE_DEPOSIT',
            'status'    => 'completed',
            'user_id'   => $user2->id,
        ]);
        config(['emitters_affiliate.aff_pay_active_referral' => true]);
        $this->lib->affiliateBonus($test->name, $user4, convert_to_bigInteger(10, 0), $deposit_id, $payment_system_id);
        $this->assertDatabaseMissing('users__balances', [
            'type'              => 'buy',
            'operation'         => 'REFFERAL',
            'user_id'           => $user3->id,
            'payment_system_id' => $payment_system_id,
            'amount'            => convert_to_bigInteger(1),
            'level'             => 1,
        ]);
        $this->assertDatabaseMissing('users__operations', [
            'type'              => 'buy',
            'operation'         => 'REFFERAL',
            'user_id'           => $user3->id,
            'payment_system_id' => $payment_system_id,
            'amount'            => convert_to_bigInteger(1),
            'level'             => 1,
            'from_amount'       => convert_to_bigInteger(10, 0),
            'deposit_id'        => $deposit_id,
        ]);

        $this->assertDatabaseHas('users__balances', [
            'type'              => 'buy',
            'operation'         => 'REFFERAL',
            'user_id'           => $user2->id,
            'payment_system_id' => $payment_system_id,
            'amount'            => convert_to_bigInteger(0.2),
            'level'             => 2,
        ]);
        $this->assertDatabaseHas('users__operations', [
            'type'              => 'buy',
            'operation'         => 'REFFERAL',
            'user_id'           => $user2->id,
            'payment_system_id' => $payment_system_id,
            'amount'            => convert_to_bigInteger(0.2),
            'level'             => 2,
            'from_amount'       => convert_to_bigInteger(10, 0),
            'deposit_id'        => $deposit_id,
        ]);
    }

    public function testCountActiveRefLine() {
        list($user, $user2, $user3, $user4) = $this->generateStructure();
        $test = $this->generateAffiliateData('ADD_FUNDS', 'COUNT_ACTIVE_REF_LINE');

        $payment_system_id = paymentsystems()->grabAll()->first()->id;
        $deposit_id        = 2;
        config(['emitters_affiliate.aff_pay_active_referral' => false]);
        factory(Emitters\UsersOperations\Models\Users_Operations::class)->create([
            'type'              => 'buy',
            'operation'         => 'REFFERAL',
            'payment_system_id' => $payment_system_id,
            'status'            => 'completed',
            'user_id'           => $user3->id,
            'from_user_id'      => $user4->id,
            'amount'            => 1,
            'level'             => 1,
        ]);
        partnership_accrual()->affiliateBonus($test->name, $user4, convert_to_bigInteger(10, 0), $deposit_id, $payment_system_id);
        $this->assertDatabaseHas('users__balances', [
            'type'              => 'buy',
            'operation'         => 'REFFERAL',
            'user_id'           => $user3->id,
            'payment_system_id' => $payment_system_id,
            'amount'            => convert_to_bigInteger(1.3, 0),
            'level'             => 1,
        ]);
    }

    public function testCountActiveRefAllLine() {
        list($user, $user2, $user3, $user4, $user5) = $this->generateStructure();
        $test = $this->generateAffiliateData('ADD_FUNDS', 'COUNT_ACTIVE_REF_ALL_LINE');

        $payment_system_id = paymentsystems()->grabAll()->first()->id;
        factory(Emitters\UsersOperations\Models\Users_Operations::class)->create([
            'type'              => 'buy',
            'operation'         => 'REFFERAL',
            'payment_system_id' => $payment_system_id,
            'status'            => 'completed',
            'user_id'           => $user2->id,
            'from_user_id'      => $user3->id,
            'amount'            => 1,
            'level'             => 2,
        ]);
        factory(Emitters\UsersOperations\Models\Users_Operations::class)->create([
            'type'              => 'buy',
            'operation'         => 'REFFERAL',
            'payment_system_id' => $payment_system_id,
            'status'            => 'completed',
            'user_id'           => $user2->id,
            'from_user_id'      => $user5->id,
            'amount'            => 1,
            'level'             => 2,
        ]);

        $deposit_id = 2;
        config(['emitters_affiliate.aff_pay_active_referral' => false]);

        partnership_accrual()->affiliateBonus($test->name, $user3, convert_to_bigInteger(10, 0), $deposit_id, $payment_system_id);
        $this->assertDatabaseHas('users__balances', [
            'type'              => 'buy',
            'operation'         => 'REFFERAL',
            'user_id'           => $user2->id,
            'payment_system_id' => $payment_system_id,
            'amount'            => convert_to_bigInteger(1.2, 0),
            'level'             => 1,
        ]);
        $this->assertDatabaseHas('users__balances', [
            'type'              => 'buy',
            'operation'         => 'REFFERAL',
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system_id,
            'amount'            => convert_to_bigInteger(0.2, 0),
            'level'             => 2,
        ]);
    }

    public function testAmountDepositRef() {
        list($user, $user2, $user3, $user4, $user5) = $this->generateStructure();
        $test = $this->generateAffiliateData('ADD_FUNDS', 'AMOUNT_DEPOSIT_REF');

        $payment_system_id = paymentsystems()->grabAll()->first()->id;

        $deposit_id = 2;
        config(['emitters_affiliate.aff_pay_active_referral' => false]);

        factory(Emitters\UsersOperations\Models\Users_Operations::class)->create([
            'type'              => 'sell',
            'operation'         => 'CREATE_DEPOSIT',
            'payment_system_id' => $payment_system_id,
            'status'            => 'completed',
            'user_id'           => $user3->id,
            'amount'            => convert_to_bigInteger(200, 0),
            'default_amount'    => convert_to_bigInteger(200, 0),
        ]);

        partnership_accrual()->affiliateBonus($test->name, $user3, convert_to_bigInteger(10, 0), $deposit_id, $payment_system_id);
        $this->assertDatabaseHas('users__balances', [
            'type'              => 'buy',
            'operation'         => 'REFFERAL',
            'user_id'           => $user2->id,
            'payment_system_id' => $payment_system_id,
            'amount'            => convert_to_bigInteger(1.2, 0),
            'level'             => 1,
        ]);
        $this->assertDatabaseHas('users__balances', [
            'type'              => 'buy',
            'operation'         => 'REFFERAL',
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system_id,
            'amount'            => convert_to_bigInteger(0.5, 0),
            'level'             => 2,
        ]);
    }

    public function testAmountActiveDeposits() {
        list($user, $user2, $user3, $user4, $user5) = $this->generateStructure();
        $test = $this->generateAffiliateData('ADD_FUNDS', 'AMOUNT_ACTIVE_DEPOSITS');

        $payment_system_id = paymentsystems()->grabAll()->first()->id;

        $deposit_id = 2;
        config(['emitters_affiliate.aff_pay_active_referral' => false]);
        $deposit_user = factory(Emitters\DepositUsers\Models\Deposit_Users::class)->create([
            'user_id'           => $user2->id,
            'payment_system_id' => $payment_system_id,
            'status'            => 0,
        ]);
        factory(Emitters\UsersOperations\Models\Users_Operations::class)->create([
            'type'              => 'sell',
            'operation'         => 'CREATE_DEPOSIT',
            'deposit_id'        => $deposit_user->id,
            'payment_system_id' => $deposit_user->payment_system_id,
            'status'            => 'completed',
            'user_id'           => $deposit_user->user_id,
            'default_amount'    => convert_to_bigInteger(200, 0),
        ]);


        $deposit_user = factory(Emitters\DepositUsers\Models\Deposit_Users::class)->create([
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system_id,
            'status'            => 1,
        ]);
        factory(Emitters\UsersOperations\Models\Users_Operations::class)->create([
            'type'              => 'sell',
            'operation'         => 'CREATE_DEPOSIT',
            'deposit_id'        => $deposit_user->id,
            'payment_system_id' => $deposit_user->payment_system_id,
            'status'            => 'completed',
            'user_id'           => $deposit_user->user_id,
            'default_amount'    => convert_to_bigInteger(20, 0),
        ]);

        partnership_accrual()->affiliateBonus($test->name, $user3, convert_to_bigInteger(10, 0), $deposit_id, $payment_system_id);
        $this->assertDatabaseHas('users__balances', [
            'type'              => 'buy',
            'operation'         => 'REFFERAL',
            'user_id'           => $user2->id,
            'payment_system_id' => $payment_system_id,
            'amount'            => convert_to_bigInteger(1.2, 0),
            'level'             => 1,
        ]);
        $this->assertDatabaseHas('users__balances', [
            'type'              => 'buy',
            'operation'         => 'REFFERAL',
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system_id,
            'amount'            => convert_to_bigInteger(0.2, 0),
            'level'             => 2,
        ]);
    }

    public function testAmountTotalDeposits() {
        list($user, $user2, $user3, $user4, $user5) = $this->generateStructure();
        $test = $this->generateAffiliateData('ADD_FUNDS', 'AMOUNT_TOTAL_DEPOSITS');

        $payment_system_id = paymentsystems()->grabAll()->first()->id;

        $deposit_id = 2;
        config(['emitters_affiliate.aff_pay_active_referral' => false]);

        factory(Emitters\UsersOperations\Models\Users_Operations::class)->create([
            'type'              => 'sell',
            'operation'         => 'CREATE_DEPOSIT',
            'payment_system_id' => $payment_system_id,
            'status'            => 'completed',
            'user_id'           => $user2->id,
            'default_amount'    => convert_to_bigInteger(200, 0),
        ]);
        factory(Emitters\UsersOperations\Models\Users_Operations::class)->create([
            'type'              => 'sell',
            'operation'         => 'CREATE_DEPOSIT',
            'payment_system_id' => $payment_system_id,
            'status'            => 'completed',
            'user_id'           => $user->id,
            'default_amount'    => convert_to_bigInteger(20, 0),
        ]);

        partnership_accrual()->affiliateBonus($test->name, $user3, convert_to_bigInteger(10, 0), $deposit_id, $payment_system_id);
        $this->assertDatabaseHas('users__balances', [
            'type'              => 'buy',
            'operation'         => 'REFFERAL',
            'user_id'           => $user2->id,
            'payment_system_id' => $payment_system_id,
            'amount'            => convert_to_bigInteger(1.2, 0),
            'level'             => 1,
        ]);
        $this->assertDatabaseHas('users__balances', [
            'type'              => 'buy',
            'operation'         => 'REFFERAL',
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system_id,
            'amount'            => convert_to_bigInteger(0.5, 0),
            'level'             => 2,
        ]);
    }

    private function generateAffiliateData($name = 'ADD_FUNDS', $condition) {
        return factory(Emitters\Affiliate\Models\Affiliate::class)->create([
            'name'      => $name,
            'condition' => $condition,
            'percents'  => [
                ['level' => 1, 'percentage' => [
                    ['from' => null, 'profit' => 10],
                    ['from' => 1, 'profit' => 13],
                    ['from' => 2, 'profit' => 12],
                ]],
                ['level' => 2, 'percentage' => [
                    ['from' => null, 'profit' => 2],
                    ['from' => 1, 'profit' => 3],
                    ['from' => 2, 'profit' => 5],
                ]]
            ]
        ]);
    }
}