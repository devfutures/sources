<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthControllerTest extends TestCase {
    use RefreshDatabase;
    const USER_ORIGINAL_PASSWORD = 'secret';

    public function testLogin(){
        $user = factory('App\User')->create([
            'password' => bcrypt(self::USER_ORIGINAL_PASSWORD)
        ]);
        config()->set('emitters.captcha_status',false);
        config()->set('emitters.google2fa_status',false);
        $response = $this->post(route('login.post'),[
            'email'    => $user->email,
            'password' => self::USER_ORIGINAL_PASSWORD,
        ]);
        $response->assertStatus(200);

        $response->assertJsonStructure([
            'user',
            'token'
        ]);
    }

    public function testFaildLogin(){
        $user = factory('App\User')->create([
            'password' => bcrypt(self::USER_ORIGINAL_PASSWORD)
        ]);
        config()->set('emitters.captcha_status',false);
        config()->set('emitters.google2fa_status',false);
        $this->json('POST',route('login.post'),[
            'email'    => $user->email,
            'password' => str_random()
        ])->assertStatus(422);
    }

    public function testRegisterSuccess(){
        $user = factory('App\User')->make();
        config()->set('emitters_affiliate.aff_need_upline', false);
        $response = $this->post(route('register.post'),[
            'name'                  => $user->name,
            'email'                 => $user->email,
            'login'                 => $user->login,
            'password'              => self::USER_ORIGINAL_PASSWORD,
            'password_confirmation' => self::USER_ORIGINAL_PASSWORD,
        ]);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'user' => [
                'id',
                'aff_ref',
                'login'
            ],
            'token'
        ]);
    }

    public function testRegisterWithUplineSuccess(){
        $user_upline = factory('App\User')->create();
        $user = factory('App\User')->make();
        $response = $this->json('POST',route('register.post'),[
            'name'                  => $user->name,
            'email'                 => $user->email,
            'login'                 => $user->login,
            'inviter_aff_ref'       => $user_upline->aff_ref,
            'password'              => self::USER_ORIGINAL_PASSWORD,
            'password_confirmation' => self::USER_ORIGINAL_PASSWORD,
        ]);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'user' => [
                'id',
                'aff_ref',
                'login'
            ],
            'token'
        ]);
    }

    public function testRegisterNotFoundUpline(){
        $user = factory('App\User')->make();
        $response = $this->json('POST',route('register.post'),[
            'name'                  => $user->name,
            'email'                 => $user->email,
            'login'                 => $user->login,
            'inviter_aff_ref'       => str_random(8),
            'password'              => self::USER_ORIGINAL_PASSWORD,
            'password_confirmation' => self::USER_ORIGINAL_PASSWORD,
        ]);
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'errors' => [
                'inviter_aff_ref',
            ]
        ]);
    }

    public function testRegisterIfNeedUpline(){
        $user = factory('App\User')->make();
        config()->set('emitters_affiliate.aff_need_upline', true);
        $response = $this->json('POST',route('register.post'),[
            'name'                  => $user->name,
            'email'                 => $user->email,
            'login'                 => $user->login,
            'password'              => self::USER_ORIGINAL_PASSWORD,
            'password_confirmation' => self::USER_ORIGINAL_PASSWORD,
        ]);
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'errors' => [
                'inviter_aff_ref',
            ]
        ]);
    }

    public function testRegisterRandomUpline(){
        $user = factory('App\User')->make();
        config()->set('emitters_affiliate.aff_need_upline', false);
        config()->set('emitters_affiliate.aff_random_upline_sign_up', true);
        $response = $this->json('POST',route('register.post'),[
            'name'                  => $user->name,
            'email'                 => $user->email,
            'login'                 => $user->login,
            'password'              => self::USER_ORIGINAL_PASSWORD,
            'password_confirmation' => self::USER_ORIGINAL_PASSWORD,
        ]);
        $response->assertStatus(200);
    }

    public function testFaildRegister(){
        $user = factory('App\User')->make();
        $this->json('POST',route('register.post'),[
            'name'                  => $user->name,
            'email'                 => $user->email,
            'password'              => self::USER_ORIGINAL_PASSWORD,
            'password_confirmation' => str_random()
        ])->assertStatus(422);
    }

    public function testOpenPageWithJwtAccess(){
        list($user,$token) = $this->withJWT();
        $response = $this->get(route('test.middleware'));
        $response->assertStatus(200);
    }

    public function testOpenPageWithJwtAccessFail(){
        list($user,$token) = $this->withJWT();
        $this->get(route('test.middleware'))->assertStatus(200);
//        sleep(61);
//        $this->get(route('test.middleware'))->assertStatus(422)->assertJsonStructure([
//            'status',
//            'token'
//        ]);
//        $this->get(route('test.middleware'))->assertStatus(422);
    }

    public function testLoginFailWithCaptchaEnabled(){
        $user = factory('App\User')->create([
            'password' => bcrypt(self::USER_ORIGINAL_PASSWORD)
        ]);
        config()->set('emitters.captcha_status',true);
        $response = $this->json('POST',route('login.post'),[
            'email'    => $user->email,
            'password' => self::USER_ORIGINAL_PASSWORD,
        ]);
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'errors' => [
                'g-recaptcha-response'
            ],
        ]);
        $this->assertEquals(json_decode($response->getContent())->errors->{'g-recaptcha-response'}[0],'The g-recaptcha-response field is required.');
    }

    public function testLoginFailWithInvalidCaptchaResponse(){
        $user = factory('App\User')->create([
            'password' => bcrypt(self::USER_ORIGINAL_PASSWORD)
        ]);
        config()->set('emitters.captcha_status',true);
        config()->set('emitters.google2fa_status',false);

        $this->instance('captcha',Mockery::mock(\Buzz\LaravelGoogleCaptcha\Captcha::class,function ($mock){
            $mock->shouldReceive('verify')->andReturn(false);
        }));

        $response = $this->json('POST',route('login.post'),[
            'email'                => $user->email,
            'password'             => self::USER_ORIGINAL_PASSWORD,
            'g-recaptcha-response' => 'response'
        ]);
        $response->assertStatus(422);
        $this->assertEquals(json_decode($response->getContent())->errors->{'g-recaptcha-response'}[0],'validation.captcha');
    }

    public function testLoginSuccessWithCaptchaEnabled(){
        $user = factory('App\User')->create([
            'password' => bcrypt(self::USER_ORIGINAL_PASSWORD)
        ]);
        config()->set('emitters.captcha_status',true);
        config()->set('emitters.google2fa_status',false);

        $this->instance('captcha',Mockery::mock(\Buzz\LaravelGoogleCaptcha\Captcha::class,function ($mock){
            $mock->shouldReceive('verify')->andReturn(true);
        }));

        $response = $this->json('POST',route('login.post'),[
            'email'                => $user->email,
            'password'             => self::USER_ORIGINAL_PASSWORD,
            'g-recaptcha-response' => 'response'
        ]);

        $response->assertStatus(200);
    }

    public function testRegisterFailWithCaptchaEnabled(){
        $user = factory('App\User')->make();
        config()->set('emitters.captcha_status',true);
        config()->set('emitters.google2fa_status',false);
        $response = $this->json('POST',route('register.post'),[
            'name'                  => $user->name,
            'email'                 => $user->email,
            'password'              => self::USER_ORIGINAL_PASSWORD,
            'password_confirmation' => self::USER_ORIGINAL_PASSWORD
        ]);
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'errors' => [
                'g-recaptcha-response'
            ],
        ]);
        $this->assertEquals(json_decode($response->getContent())->errors->{'g-recaptcha-response'}[0],'The g-recaptcha-response field is required.');
    }

    public function testRegisterSuccessWithCaptchaEnabled(){
        $user = factory('App\User')->make();
        config()->set('emitters.captcha_status',true);
        config()->set('emitters.google2fa_status',false);
        config()->set('emitters_affiliate.aff_need_upline', false);
        
        $this->instance('captcha',Mockery::mock(\Buzz\LaravelGoogleCaptcha\Captcha::class,function ($mock){
            $mock->shouldReceive('verify')->andReturn(true);
        }));

        $response = $this->json('POST',route('register.post'),[
            'name'                  => $user->name,
            'email'                 => $user->email,
            'password'              => self::USER_ORIGINAL_PASSWORD,
            'password_confirmation' => self::USER_ORIGINAL_PASSWORD,
            'g-recaptcha-response'  => 'qwe'
        ]);
        $response->assertStatus(200);
    }

    public function testLoginFailWithoutGoogleSecret(){
        $user = factory('App\User')->create([
            'password'      => bcrypt(self::USER_ORIGINAL_PASSWORD),
            'g2fa_status'   => true,
            'g2fa_secret'   => 'qe',
            'g2fa_settings' => ['login' => true]
        ]);
        config()->set('emitters.captcha_status',false);
        config()->set('emitters.google2fa_status',true);
        $response = $this->json('POST',route('login.post'),[
            'email'    => $user->email,
            'password' => self::USER_ORIGINAL_PASSWORD,
        ]);

        $response->assertStatus(422);
        $response->assertJsonStructure([
            'errors' => [
                'g-2fa-secret'
            ],
        ]);
        $this->assertEquals(json_decode($response->getContent())->errors->{'g-2fa-secret'}[0],'The g-2fa-secret field is required.');
    }

    public function testLoginSuccessWithGoogle2FaEnabled(){
        $user = factory('App\User')->create([
            'password'      => bcrypt(self::USER_ORIGINAL_PASSWORD),
            'g2fa_status'   => true,
            'g2fa_secret'   => 'qe',
            'g2fa_settings' => ['login' => true]
        ]);
        config()->set('emitters.captcha_status',false);

        $this->instance('pragmarx.google2fa',Mockery::mock(PragmaRX\Google2FA\Google2FA::class,function ($mock){
            if(config('emitters.google2fa_method') == 'verifyKeyNever') {
                $mock->shouldReceive('verifyKeyNewer')->andReturn(true);
            } else {
                $mock->shouldReceive('verifyKey')->andReturn(true);
            }
        }));

        $response = $this->json('POST',route('login.post'),[
            'email'        => $user->email,
            'password'     => self::USER_ORIGINAL_PASSWORD,
            'g-2fa-secret' => 'test'
        ]);
        $response->assertStatus(200);
    }

    public function testLoginFailWithInvalidGoogleSecret(){
        $user = factory('App\User')->create([
            'password'      => bcrypt(self::USER_ORIGINAL_PASSWORD),
            'g2fa_status'   => true,
            'g2fa_secret'   => 'qe',
            'g2fa_settings' => ['login' => true]
        ]);
        config()->set('emitters.captcha_status',false);
        config()->set('emitters.google2fa_status',true);
        $this->instance('pragmarx.google2fa',Mockery::mock(PragmaRX\Google2FA\Google2FA::class,function ($mock){
            if(config('emitters.google2fa_method') === 'verifyKeyNever') {
                $mock->shouldReceive('verifyKeyNewer')->andReturn(false);
            } else {
                $mock->shouldReceive('verifyKey')->andReturn(false);
            }
        }));

        $response = $this->json('POST',route('login.post'),[
            'email'        => $user->email,
            'password'     => self::USER_ORIGINAL_PASSWORD,
            'g-2fa-secret' => 'invalid one time password'
        ]);
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'errors' => [
                'g-2fa-secret'
            ]
        ]);
        $this->assertEquals(json_decode($response->getContent())->errors->{'g-2fa-secret'}[0],'Invalid secret.');
    }

    public function testLogActivityOnLogin() {
        $user = factory('App\User')->create([
            'password' => bcrypt(self::USER_ORIGINAL_PASSWORD)
        ]);
        config()->set('emitters.captcha_status', false);
        config()->set('emitters.google2fa_status', false);
        $response = $this->post(route('login.post'), [
            'email'    => $user->email,
            'password' => self::USER_ORIGINAL_PASSWORD,
        ]);
        $activity = usersdata()->getActivityByUser($user->id);
        $this->assertNotNull($activity);
        $this->assertInstanceOf(\Emitters\UsersData\Models\Users_Activity::class, $activity->first());
    }

    public function testLogActivityOnRegister() {
        $user     = factory('App\User')->make();
        config()->set('emitters_affiliate.aff_need_upline', false);
        $response = $this->json('POST', route('register.post'), [
            'name'                  => $user->name,
            'email'                 => $user->email,
            'login'                 => $user->login,
            'password'              => self::USER_ORIGINAL_PASSWORD,
            'password_confirmation' => self::USER_ORIGINAL_PASSWORD,
        ]);
        $response->assertStatus(200);
        $user_id = json_decode($response->getContent())->user->id;
        $activity = usersdata()->getActivityByUser($user_id);
        $this->assertNotNull($activity);
        $this->assertInstanceOf(\Emitters\UsersData\Models\Users_Activity::class, $activity->first());
    }
}