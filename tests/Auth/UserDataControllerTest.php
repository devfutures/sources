<?php


use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserDataControllerTest extends TestCase {
    use RefreshDatabase;

    public function testIsAdmin() {
        $admin      = factory(\App\User::class)->create();
        $adminToken = auth()->tokenById($admin->id);

        $user      = factory(\App\User::class)->create(['role_id' => 0]);
        $userToken = auth()->tokenById($user->id);

        $adminResponse = $this->call('GET', route('is_admin'), [], ['token' => $adminToken]);
        $adminResponse->assertStatus(200);

        $userResponse = $this->call('GET', route('is_admin'), [], ['token' => $userToken]);
        $userResponse->assertStatus(403);
    }
}