<?php


use App\User;
use Emitters\Balance\Models\Users_Balance;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Config;

class BalanceManagerTest extends \Tests\TestCase {
    use RefreshDatabase;

    /** @var \Emitters\Balance\BalanceManager */
    protected $balanceManager;

    /**
     * BalanceManagerTest constructor.
     */
    public function setUp(): void {
        parent::setUp();
        // TODO: Bonus work only if balance driver the "getWithMathRealTime"
        // tmp set driver
        Config::set('emitters.balance_driver', 'getWithMathRealTime');
        $this->balanceManager = resolve('librariesair.balance');
    }

    /**
     * @group balance-get-all
     */
    public function testGetAll() {
        $balance = factory(Users_Balance::class)->create();
        $user = User::find($balance->user_id);
        $paymentSystemId = $balance->payment_system_id;

        $balances = $this->balanceManager->getAll($user, [$paymentSystemId]);
        foreach ($balances as $balance) {
            $this->assertArrayHasKey('balance_now', $balance);
            $this->assertIsString($balance['balance_now']);

            $this->assertArrayHasKey('payment_system_id', $balance);
        }

        $balancesOriginals = $this->balanceManager->getAll($user, [$paymentSystemId], true);
        foreach ($balancesOriginals as $balance) {
            $this->assertArrayHasKey('balance_now', $balance);
            $this->assertIsString($balance['balance_now']);

            $this->assertArrayHasKey('payment_system_id', $balance);
        }

        $callWithEmptyIds = $this->balanceManager->getAll($user, []);
        $this->assertFalse($callWithEmptyIds);
    }

    /**
     * @group balance-decimal
     */
    public function testDecimalBalance() {
        $balance = factory(Users_Balance::class)->create();
        $user = User::find($balance->user_id);
        $paymentSystemId = $balance->payment_system_id;

        $balancesOriginals = $this->balanceManager->getAll($user, [$paymentSystemId], true);
        $decimedBalances   = $this->balanceManager->decimalBalance($balancesOriginals);

        foreach ($decimedBalances as $balance) {
            $this->assertIsString($balance['balance_now']);
            $this->assertStringContainsString('.', $balance['balance_now']);
        }
    }

    /**
     * @group balance-change
     */
    public function testChangeBalance() {
        $balance = factory(Users_Balance::class)->create();
        $user = User::find($balance->user_id);
        $paymentSystemId = $balance->payment_system_id;
        $data = [
            'user_id'           => $user->id,
            'payment_system_id' => $paymentSystemId,
            'type'              => 'buy',
            'amount'            => 12
        ];

        /** @var Users_Balance $inserted */
        $inserted = $this->balanceManager->changeBalance($data);
        $find     = Users_Balance::query()->find($inserted->id);

        $this->assertTrue($inserted->is($find));
        $this->assertEquals($inserted->user_id, $find->user_id);
        $this->assertEquals($inserted->payment_system_id, $find->payment_system_id);
        $this->assertEquals($inserted->type, $find->type);
        $this->assertEquals($inserted->amount, $find->amount);
    }

    public function testBalanceWithBonus() {
        $user   = factory('App\User')->create();
        $paymentSystemId = 1;
        $balance = factory(Users_Balance::class)->create([
            'user_id' => $user->id,
            'type' => 'buy',
            'amount' => 1,
            'balance_now' => 1,
            'is_hold' => 1,
            'payment_system_id' => $paymentSystemId
        ]);

        $balances1 = $this->balanceManager->getAll($user, [$paymentSystemId], false, true);
        $this->assertEquals($balances1[0]['balance_now'], '0.00000000');

        $balances2 = $this->balanceManager->getAll($user, [$paymentSystemId], false, false);
        $this->assertEquals($balances2[0]['balance_now'], '0.00000001');

        factory(Users_Balance::class)->create([
            'user_id' => $user->id,
            'type' => 'buy',
            'amount' => 5,
            'balance_now' => 5,
            'is_hold' => 0,
            'payment_system_id' => $paymentSystemId
        ]);
        $balances3 = $this->balanceManager->getAll($user, [$paymentSystemId], false, true);
        $this->assertEquals($balances3[0]['balance_now'], '0.00000005');
        $balances4 = $this->balanceManager->getAll($user, [$paymentSystemId], false, false);
        $this->assertEquals($balances4[0]['balance_now'], '0.00000006');

    }
}