<?php


use Emitters\Balance\Models\Users_Balance;

class BalanceManagerTest extends \Tests\TestCase {
    use \Illuminate\Foundation\Testing\RefreshDatabase;

    /** @var \Emitters\Balance\BalanceManager */
    protected $balanceManager;
    protected $user;
    protected $balances;

    /**
     * BalanceManagerTest constructor.
     */
    public function setUp() {
        parent::setUp();
        $this->balanceManager = resolve('librariesair.balance');
        $this->user           = factory(App\User::class)->create();
        $this->balances       = factory(Users_Balance::class, 5)->create();
    }

    /**
     * @group balance-get-all
     */
    public function testGetAll() {
        $balances = $this->balanceManager->getAll($this->user, [1]);
        foreach ($balances as $balance) {
            $this->assertArrayHasKey('balance_now', $balance);
            $this->assertIsString($balance['balance_now']);

            $this->assertArrayHasKey('payment_system_id', $balance);
            $this->assertIsInt($balance['payment_system_id']);
        }

        $balancesOriginals = $this->balanceManager->getAll($this->user, [1], true);
        foreach ($balancesOriginals as $balance) {
            $this->assertArrayHasKey('balance_now', $balance);
            $this->assertIsInt($balance['balance_now']);

            $this->assertArrayHasKey('payment_system_id', $balance);
            $this->assertIsInt($balance['payment_system_id']);
        }

        $callWithEmptyIds = $this->balanceManager->getAll($this->user->first(), []);
        $this->assertFalse($callWithEmptyIds);
    }

    /**
     * @group balance-decimal
     */
    public function testDecimalBalance() {
        $balancesOriginals = $this->balanceManager->getAll($this->user, [1], true);
        $decimedBalances   = $this->balanceManager->decimalBalance($balancesOriginals);
        foreach ($decimedBalances as $balance) {
            $this->assertIsString($balance['balance_now']);
            $this->assertStringContainsString('.', $balance['balance_now']);
        }
    }

    /**
     * @group balance-change
     */
    public function testChangeBalance() {
        $data = [
            'user_id'           => $this->user->id,
            'payment_system_id' => 1,
            'type'              => 'buy',
            'amount'            => 12
        ];

        /** @var Users_Balance $inserted */
        $inserted = $this->balanceManager->changeBalance($data);
        $find = Users_Balance::query()->find($inserted->id);

        $this->assertTrue($inserted->is($find));
        $this->assertEquals($inserted->user_id, $find->user_id);
        $this->assertEquals($inserted->payment_system_id, $find->payment_system_id);
        $this->assertEquals($inserted->type, $find->type);
        $this->assertEquals($inserted->amount, $find->amount);
    }
}