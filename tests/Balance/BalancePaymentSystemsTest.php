<?php


use App\User;
use Emitters\Balance\Models\Users_Balance;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Config;

class BalancePaymentSystemsTest extends \Tests\TestCase {
    use RefreshDatabase;

    /** @var \Emitters\Balance\BalancePaymentSystems */
    protected $balanceManager;

    /**
     * BalanceManagerTest constructor.
     */
    public function setUp(): void {
        parent::setUp();
        $this->balanceManager = resolve('balance_payment_system');
    }

    public function testGetBalancePaymentSystemsByArray() {
        $ps = paymentsystems()->grabAll()->where('title', 'Bitcoin')->first();
        $this->mock($ps->class_name, function ($mock) {
            $mock->shouldReceive('balance')->andReturn(0);
        });
        $result = $this->balanceManager->showBalance([$ps->id])[0];
        $this->assertTrue($result['status']);
        $this->assertSame(0, $result['balance']);
        $this->assertSame($ps->id, $result['id']);
    }

    public function testGetBalancePaymentSystemsByUrl() {
        $this->withJWT();
        $ps = paymentsystems()->grabAll()->where('title', 'Bitcoin')->first();
        $response = $this->json('GET', route('statistics.balance'), [
            'id' => [$ps->id]
        ]);
        $response->assertStatus(200);
    }
}