<?php


use App\User;
use Emitters\Balance\Models\Users_Balance;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Config;

class WithdrawBalanceManagerTest extends \Tests\TestCase {
    use RefreshDatabase;

    /** @var \Emitters\Balance\BalanceManager */
    protected $balanceManager;
    protected $operationsManger;

    /**
     * BalanceManagerTest constructor.
     */
    public function setUp(): void{
        parent::setUp();
        $this->balanceManager = resolve('librariesair.balance');
        $this->operationsManger = users_operations();
    }

    public function testWithdrawNoUser(){
        $this->expectException('Exception');
        $this->expectExceptionMessage('USER_NOT_CORRECTLY');

        $this->balanceManager->withdrawBalance(null,null,null);
    }

    public function testWithdrawAmountZero(){
        $this->expectException('Exception');
        $this->expectExceptionMessage('AMOUNT_DOES_NOT_FIT');

        $balance = factory(Users_Balance::class)->create([
            'type'      => 'buy',
            'operation' => 'ADD_FUNDS'
        ]);

        $user = User::find($balance->user_id);
        $paymentSystemId = $balance->payment_system_id;

        $this->balanceManager->withdrawBalance($user,0,$paymentSystemId);
    }

    public function testWithdrawNoWallet(){
        $this->expectException('Exception');
        $this->expectExceptionMessage('FOR_WITHDRAW_NEED_FILL_WALLET');

        $balance = factory(Users_Balance::class)->create([
            'type'      => 'buy',
            'operation' => 'ADD_FUNDS'
        ]);

        $user = User::find($balance->user_id);
        $paymentSystemId = $balance->payment_system_id;

        $this->balanceManager->withdrawBalance($user,1,$paymentSystemId);
    }

    public function testWithdrawSomeBalanceSmall(){
        $this->expectException('Exception');
        $this->expectExceptionMessage('NOT_ENOUGH_FUNDS_ON_BALANCE');

        $balance = factory(Users_Balance::class)->create([
            'type'      => 'buy',
            'operation' => 'ADD_FUNDS'
        ]);

        $user = User::find($balance->user_id);
        $paymentSystemId = $balance->payment_system_id;
        factory('Emitters\UsersData\Models\Users_Wallet')->create([
            'user_id'           => $user->id,
            'payment_system_id' => $paymentSystemId,
        ]);

        $balance = $this->balanceManager->getAll($user,[$paymentSystemId]);
        $amount_withdraw = $balance[0]['balance_now'] + 50;

        $this->balanceManager->withdrawBalance($user,$amount_withdraw,$paymentSystemId);
        $balance = $this->balanceManager->getAll($user,[$paymentSystemId]);
        $this->assertSame($balance[0]['balance_now'],'0.00000000');
    }

    public function testWithdraw(){
        $balance = factory(Users_Balance::class)->create([
            'type'      => 'buy',
            'operation' => 'ADD_FUNDS'
        ]);

        $user = User::find($balance->user_id);
        $paymentSystemId = $balance->payment_system_id;
        factory('Emitters\UsersData\Models\Users_Wallet')->create([
            'user_id'           => $user->id,
            'payment_system_id' => $paymentSystemId,
        ]);

        $balance1 = $this->balanceManager->getAll($user,[$paymentSystemId]);
        $amount_withdraw = $balance1[0]['balance_now'];
        config(['emitters.after_add_funds' => 'ADD_FUNDS_TO_BALANCE']);
        $this->balanceManager->withdrawBalance($user,$amount_withdraw,$paymentSystemId);
        $balance2 = $this->balanceManager->getAll($user,[$paymentSystemId]);
        $this->assertSame($balance2[0]['balance_now'],'0.00000000');

        $operations = $this->operationsManger->getUserOperations($user,['WITHDRAW'],['pending'])->toArray();
        $this->assertSame((string)$paymentSystemId,$operations['data'][0]['payment_system_id']);
        $this->assertSame($amount_withdraw,df_add($operations['data'][0]['amount'],$operations['data'][0]['commission']));
    }

    public function testWithdrawWithDepositTypeExchange(){
        factory('Emitters\ConvertingCurrencyRate\Models\Currency_Rate')->create();

        $ps_normal = factory('Emitters\PaymentSystems\Models\Payment_System')->create([
            'real_payment_system' => 1,
            'currency'            => 'BTC'
        ]);

        $system_ps = factory('Emitters\PaymentSystems\Models\Payment_System')->create([
            'real_payment_system' => 0,
            'currency'            => 'USD'
        ]);

        $balance = factory(Users_Balance::class)->create([
            'type'              => 'buy',
            'operation'         => 'ADD_FUNDS',
            'payment_system_id' => $system_ps->id
        ]);
        Config::set('emitters.default_payment_system_id',$system_ps->id);
        Config::set('emitters.after_add_funds','EXCHANGE');

        $user = User::find($balance->user_id);

        $paymentSystemId = $ps_normal->id;
        factory('Emitters\UsersData\Models\Users_Wallet')->create([
            'user_id'           => $user->id,
            'payment_system_id' => $paymentSystemId,
        ]);
        $balance1 = $this->balanceManager->getAll($user,[$system_ps->id]);
        $amount_withdraw = $balance1[0]['balance_now'];
        $this->balanceManager->withdrawBalance($user,$amount_withdraw,$ps_normal->id);
        $operations = $this->operationsManger->getUserOperations($user)->toArray();
        $this->assertCount(2, $operations['data']);
    }
}