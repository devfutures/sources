<?php

namespace Emitters\Tests\ConvertingCurrencyRate;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AdminControllerTest extends TestCase
{
    use RefreshDatabase;
    protected $lib;
    public function setUp(): void{
        parent::setUp();
        $this->lib = resolve('libair.converting_currency_rate');
    }

    function test_get_currency_rate(){
        $this->withJWT();
        $currency = factory('Emitters\ConvertingCurrencyRate\Models\Currency_Rate')->create();
        $this->get(route('currencyrate.index'))->assertJson([
            'id' => $currency->id,
        ]);
    }

    function test_insert_currency_rate(){
        $this->withJWT();
        $currency = factory('Emitters\ConvertingCurrencyRate\Models\Currency_Rate')->make();
        $this->post(route('currencyrate.new'), $currency->toArray())->status(200);
        $this->assertDatabaseHas($currency->getTable(), $currency->toArray());
    }

//    function test_update_currency_rate(){
//        $currency = factory('Emitters\ConvertingCurrencyRate\Models\Currency_Rate')->create();
//        $this->post(route('currencyrate.update', ['id' => $currency->id]), [
//            'btc_usd' => 200
//        ])->status(200);
//        $this->assertDatabaseHas($currency->getTable(), [
//            'id' => $currency->id,
//            'btc_usd' => 200
//        ]);
//    }
}
