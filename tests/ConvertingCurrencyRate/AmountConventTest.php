<?php

namespace Emitters\Tests\ConvertingCurrencyRate;

use Emitters\PaymentSystems\Models\Payment_System;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AmountConventTest extends TestCase {
    use RefreshDatabase;
    /**
     * @var /App\LibrariesAir\ConvertingCurrencyRate\ConvertingCurrencyRateManager
     */
    protected $lib;

    public function setUp(): void {
        parent::setUp();
        $this->lib = converting_currency_rate();
    }

    public function testParseCurrencyRate() {
        factory(Payment_System::class)->create([
            'title'    => 'Euro',
            'currency' => 'eur',
            'is_fiat'  => 1
        ]);
        $qwe = $this->lib->parseCurrencyRate();
        $this->assertNotNull($qwe);
    }

    public function testInsertCurrencyRate() {
        $currency = factory('Emitters\ConvertingCurrencyRate\Models\Currency_Rate')->make();
        $this->lib->insertNewCurrencyRate($currency->toArray());
        $this->assertDatabaseHas($currency->getTable(), $currency->toArray());
    }

    public function testGetCurrencyRate() {
        $currency = factory('Emitters\ConvertingCurrencyRate\Models\Currency_Rate')->create();
        $rate     = $this->lib->getCurrencyRate();
        $this->assertEquals($currency->btc_usd, $rate->btc_usd);
    }

    public function testConvertAmountFromToAndReverse() {
        $currency = factory('Emitters\ConvertingCurrencyRate\Models\Currency_Rate')->create();
        $array1 = [];
        $array2 = [];
        foreach ($currency->getFillable() as $row) {
            list($from, $to) = explode('_', $row);
            $tmp = $this->lib->convertAmount(1, $from, $to, config('emitters.bc_scale'));
            $array1[] = $tmp;
            $array2[] = df_div($currency->{$row},1);

            $tmp = $this->lib->convertAmount($currency->{$row}, $to, $from,config('emitters.bc_scale'));
            $array1[] = $tmp;
            $array2[] = df_div(1,1,config('emitters.bc_scale'));
        }

        $this->assertSame($array1, $array2);
    }

    public function testDataForFill() {
        $preFill       = [
            ['symbol' => 'btc', 'price_usd' => 1]
        ];
        $preFillObject = json_decode(json_encode($preFill));
        $sameResult    = [];
        foreach ($preFill as $key => $val) {
            $sameResult[$val['symbol'] . '_usd'] = $val['price_usd'];
        }
        $return = $this->lib->getDataForFill($preFillObject, 'usd');
        $this->assertSame($sameResult, $return);
    }
}
