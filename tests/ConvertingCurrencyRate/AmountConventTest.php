<?php

namespace Emitters\Tests\ConvertingCurrencyRate;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

// use Emitters\ConvertingCurrencyRate\Drivers\CoinMarketCap;
// use Emitters\ConvertingCurrencyRate\Models\Currency_Rate;
// use Emitters\ConvertingCurrencyRate\ConvertingCurrencyRateManager;

class AmountConventTest extends TestCase
{
    use DatabaseTransactions;
    protected $lib;
    public function setUp(){
        parent::setUp();
        // $this->lib = new ConvertingCurrencyRateManager(new CoinMarketCap(), $currency_rate, resolve('paymentsystems'));
        $this->lib = resolve('libair.converting_currency_rate');
    }
    public function testInsertCurrencyRate()
    {
        $currencyrate_now = $this->lib->insertNewCurrencyRate();
        $this->assertTrue($currencyrate_now->id ? true : false);
    }

    public function testConvertBtcToUsd(){
        foreach($this->lib->model->getFillable() as $row){
            $expl = explode('_', $row);
            $key = $expl[0];
            $val = $expl[1];

            $tmp = $this->lib->convertAmount(1, $key, $val);
            $res_compare = (string)$this->lib->getCurrencyRate()->{$key."_".$val};
            
            $this->assertSame($tmp, $res_compare);

            $tmp = $this->lib->convertAmount($this->lib->getCurrencyRate()->{$key."_".$val}, $val, $key);
            $this->assertSame(number($tmp, 8), (string) $tmp);
        }
    }

    public function testDataForFill(){
        $preFill = [
            ['symbol' => 'btc', 'price_usd' => 1]
        ];
        $preFillObject = json_decode(json_encode($preFill));
        $sameResult = [];
        foreach($preFill as $key=> $val){
            $sameResult[$val['symbol'].'_usd'] = $val['price_usd'];
        }
        $return  = $this->lib->getDataForFill($preFillObject, 'usd');
        $this->assertSame($sameResult, $return);
    }
}
