<?php

namespace Emitters\Tests\ConvertingCurrencyRate;

use Carbon\Carbon;
use Emitters\Balance\Models\Users_Balance;
use Emitters\DepositPlans\Models\Deposit_Plans;
use Emitters\DepositUsers\Models\Deposit_Users;
use Emitters\UsersOperations\Models\Users_Operations;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery;
use Tests\TestCase;

class DepositAccrualsTest extends TestCase {
    use RefreshDatabase;
    protected $percent_profit = 2;

    public function setUp(): void{
        parent::setUp();
    }

    // одно начисление и закрытие депозита
    public function testPlanHasDurrationOneState(){
        list($user,$plan,$deposit) = $this->createDepositData(86400,1,100,100);

        $amount_profit = df_div($deposit->amount * $this->percent_profit / 100 + $deposit->amount,1);

        $this->instance('libair.depositplans',Mockery::mock(\App\LibrariesAir\DepositPlans\DepositPlansManager::class,function ($mock){
            $mock->shouldReceive('getPercentByDate')->andReturn($this->percent_profit);
            $mock->shouldReceive('getNextAccruals')->andReturn(Carbon::now());
            $mock->shouldReceive('getPlansWillBuyAllParticipants')->andReturn([]);
        }));
        $lib = resolve('deposit.accruals');
        Carbon::setTestNow($deposit->accrual_at);
        $lib->accrualsDataPreparation();

        $deposit = $deposit->fresh();
        $this->assertSame('1',$deposit->status);
        $this->assertNull($deposit->accrual_at);
        $operations = Users_Operations::where('deposit_id',$deposit->id)->where('user_id',$user->id)->get();
        $balance = Users_Balance::where('parent_id',$deposit->id)->where('user_id',$user->id)->get();
        $this->assertCount(1,$operations->where('operation','ACCRUALS')->all());
        $this->assertCount(1,$balance->where('operation','ACCRUALS')->all());

        $this->assertCount(1,$operations->where('operation','DEPOSIT_CLOSE')->all());
        $this->assertCount(1,$balance->where('operation','DEPOSIT_CLOSE')->all());
        $this->assertSame($amount_profit,df_div($balance->sum('amount'),1));
        $this->assertSame($amount_profit,df_div($operations->sum('amount'),1));
    }

    //Когда начисления быстрее чем нужно

    public function createDepositData($accrual_period = 86400,$duration_plan = 1,$principal_return_percent = 0,$amount = 100){
        $user = factory(User::class)->create();
        $plan = factory(Deposit_Plans::class)->create([
            'status'                   => 1,
            'accrual_period'           => $accrual_period,
            'duration_plan'            => $duration_plan,
            'principal_return_percent' => $principal_return_percent
        ]);
        $deposit = factory(Deposit_Users::class)->create([
            'plan_id'    => $plan->id,
            'user_id'    => $user->id,
            'accrual_at' => Carbon::now()->addSecond($accrual_period),
            'amount'     => $amount,
            'status'     => 0
        ]);

        return [$user,$plan,$deposit];
    }

    // Когда начисления произошло но accrual_at не обновилось

    public function testDepositAccrualAtNotCorrectly(){
        list($user,$plan,$deposit) = $this->createDepositData(86400,1,100,100);
        $deposit->accrual_at = Carbon::now()->subHour();
        $deposit->save();

        $this->instance('libair.depositplans',Mockery::mock(\App\LibrariesAir\DepositPlans\DepositPlansManager::class,function ($mock){
            $mock->shouldReceive('getPercentByDate')->andReturn($this->percent_profit);
            $mock->shouldReceive('getNextAccruals')->andReturn(Carbon::now());
            $mock->shouldReceive('getPlansWillBuyAllParticipants')->andReturn([]);
        }));
        $lib = resolve('deposit.accruals');

        $lib->accrualsDataPreparation();

        $deposit = $deposit->fresh();
        $this->assertSame($deposit->status,'2');
    }

    // закрытие депозита без возвтра

    public function testDepositAccrualAtDoNotUpdate(){
        list($user,$plan,$deposit) = $this->createDepositData(86400,50,100,100);
        $deposit->created_at = Carbon::now()->subDay();
        $lib = resolve('deposit.accruals');
        Carbon::setTestNow($deposit->accrual_at);

        $deposit_realy_run = $lib->accrualsDataPreparation();
        $deposit = $deposit->fresh();

        $deposit->accrual_at = Carbon::now();
        $deposit->save();

        $deposit_realy_run = $lib->accrualsDataPreparation();

        $operations = Users_Operations::get();
        $this->assertCount(1,$operations);
        $deposit = $deposit->fresh();
        $this->assertSame($deposit->status,'2');
    }

    // если процент начисления больше 100

    public function testPlanNoPrincipalReturn(){
        list($user,$plan,$deposit) = $this->createDepositData(86400,1,0,100);

        $amount_profit = df_div($deposit->amount * $this->percent_profit / 100,1);

        $this->instance('libair.depositplans',Mockery::mock(\App\LibrariesAir\DepositPlans\DepositPlansManager::class,function ($mock){
            $mock->shouldReceive('getPercentByDate')->andReturn($this->percent_profit);
            $mock->shouldReceive('getNextAccruals')->andReturn(Carbon::now());
            $mock->shouldReceive('getPlansWillBuyAllParticipants')->andReturn([]);
        }));
        $lib = resolve('deposit.accruals');
        Carbon::setTestNow($deposit->accrual_at);
        $lib->accrualsDataPreparation();

        $deposit = $deposit->fresh();
        $operations = Users_Operations::where('deposit_id',$deposit->id)->where('user_id',$user->id)->get();
        $balance = Users_Balance::where('parent_id',$deposit->id)->where('user_id',$user->id)->get();
        $this->assertSame($amount_profit,df_div($balance->sum('amount'),1));
        $this->assertSame($amount_profit,df_div($operations->sum('amount'),1));
    }

    // полный цикл завершения депозита

    public function testCalcOfAmountAccruals(){
        $res = calc_of_amount_percent('86','3.687');
        $this->assertSame($res,df_div('3.17082',1));
    }

    public function testIfDepositPercentProfitMustBeBigger() {
        list($user, $plan, $deposit) = $this->createDepositData(86400, 1, 0, 100);

        $this->percent_profit = 103;

        $amount_profit = df_div($deposit->amount * $this->percent_profit / 100, 1);

        //Use this in cases when you want to test original methods of DepositPlanManager
        //getPercentByDate only one method that will be mocked (you can add several by separating them with "," e.g [method1, method2])
        /*$mock = Mockery::mock('\App\LibrariesAir\DepositPlans\DepositPlansManager[getPercentByDate]',
            array(
                new Deposit_Plans(),
                resolve('libair.paymentsystems'),
                resolve('libair.converting_currency_rate'),
                resolve('libair.depositusers.wo.plan')
            ),
            function ($mock) {
                $mock->shouldReceive('getPercentByDate')->andReturn($this->percent_profit);
            }
        );*/

        $this->instance('libair.depositplans',Mockery::mock(\App\LibrariesAir\DepositPlans\DepositPlansManager::class,function ($mock){
            $mock->shouldReceive('getPercentByDate')->andReturn($this->percent_profit);
            $mock->shouldReceive('getNextAccruals')->andReturn(Carbon::now());
            $mock->shouldReceive('getPlansWillBuyAllParticipants')->andReturn([]);
        }));
        $lib = resolve('deposit.accruals');
        Carbon::setTestNow($deposit->accrual_at);
        $lib->accrualsDataPreparation();
        $deposit = $deposit->fresh();

        $operations = Users_Operations::where('deposit_id', $deposit->id)->where('user_id', $user->id)->get();
        $balance    = Users_Balance::where('parent_id', $deposit->id)->where('user_id', $user->id)->get();

        $this->assertSame('1', $deposit->status);
        $this->assertNull($deposit->accrual_at);

        $this->assertSame($amount_profit, df_div($balance->sum('amount'), 1));
        $this->assertSame($amount_profit, df_div($operations->sum('amount'), 1));
    }

    public function testDepositDone(){
        list($user,$plan,$deposit) = $this->createDepositData(86400,1,100,100);
        $lib = resolve('deposit.accruals');
        $i = 0;
        $sum_amount_profit = 0;
        Carbon::setTestNow($deposit->accrual_at);
        while($i < 20) {
            $deposit = $deposit->fresh();
            if($deposit->status == 0) {
                $sum_amount_profit += $deposit->amount * $plan->payments_days_percent[strtolower(Carbon::now()->format('l'))]['percent'] / 100;
            }
            $lib->accrualsDataPreparation();
            Carbon::setTestNow(Carbon::now()->addSecond($plan->accrual_period));
            $i++;
        }
        $this->assertSame('1',$deposit->status);
        $this->assertNull($deposit->accrual_at);
        $operations = Users_Operations::where('deposit_id',$deposit->id)->where('user_id',$user->id)->get();
        $this->assertSame($operations->sum('amount'),$sum_amount_profit + $deposit->amount);
    }

    public function testDepositDoneSmallPeriod(){
        list($user,$plan,$deposit) = $this->createDepositData(60,50,0,100);
        $lib = resolve('deposit.accruals');
        $i = 0;
        $sum_amount_profit = 0;
        Carbon::setTestNow($deposit->accrual_at);
        while($i < 60) {
            $deposit = $deposit->fresh();
            if($deposit->status == 0) {
                $sum_amount_profit += $deposit->amount * $plan->payments_days_percent[strtolower(Carbon::now()->format('l'))]['percent'] / 100;
            }
            $lib->accrualsDataPreparation();
            Carbon::setTestNow(Carbon::now()->addSecond($plan->accrual_period));
            $i++;
        }
        $this->assertSame('1',$deposit->status);
        $this->assertNull($deposit->accrual_at);
        $operations = Users_Operations::where('deposit_id',$deposit->id)->where('user_id',$user->id)->get();
        $this->assertSame($operations->sum('amount'),$sum_amount_profit);
    }

    public function testDepositDoneWithNoPrincipalReturn(){
        list($user,$plan,$deposit) = $this->createDepositData(86400,10,0,100);
        $lib = resolve('deposit.accruals');
        Carbon::setTestNow($deposit->accrual_at);
        $i = 0;
        $sum_amount_profit = 0;
        while($i < 20) {
            $deposit = $deposit->fresh();
            if($deposit->status == 0) {
                $sum_amount_profit += $deposit->amount * $plan->payments_days_percent[strtolower(Carbon::now()->format('l'))]['percent'] / 100;
            }
            $lib->accrualsDataPreparation();
            Carbon::setTestNow(Carbon::now()->addSecond($plan->accrual_period));
            $i++;
        }
        $this->assertSame('1',$deposit->status);
        $this->assertNull($deposit->accrual_at);
        $operations = Users_Operations::where('deposit_id',$deposit->id)->where('user_id',$user->id)->get();
        $this->assertSame($operations->sum('amount'),$sum_amount_profit);
    }

    public function testWillBuyAllParticipants(){
        $user = factory(User::class)->create();
        $plan = factory(Deposit_Plans::class)->create([
            'name'                      => 'blue',
            'description'               => 'this lot is blue',
            'status'                    => 1,
            'accrual_period'            => 43200,
            'duration_plan'             => 1,
            'principal_return_percent'  => 100,
            'will_buy_all_participants' => 1,
            'limit_deps_user'           => 1,
            'delay_accruals'            => 0,
            'available_count_deposits'  => 2
        ]);
        $deposit = factory(Deposit_Users::class,1)->create([
            'plan_id'    => $plan->id,
            'user_id'    => $user->id,
            'accrual_at' => Carbon::now()->addSecond($plan->accrual_period),
            'amount'     => $plan->minimum_amount,
            'status'     => 0
        ]);
        $lib = resolve('deposit.accruals');
        $lib->accrualsDataPreparation();

        $operations = Users_Operations::where('deposit_id',$deposit->first()->id)->where('user_id',$user->id)->get();

        $this->assertCount(0,$operations);

        $users = factory(User::class,1)->create();
        foreach($users as $user_item) {
            $deposit = factory(Deposit_Users::class)->create([
                'plan_id'    => $plan->id,
                'user_id'    => $user_item->id,
                'accrual_at' => Carbon::now()->addSecond($plan->accrual_period),
                'amount'     => $plan->minimum_amount,
                'status'     => 0
            ]);
        }

        $lib->accrualsDataPreparation();
        $operations = Users_Operations::where('deposit_id',$deposit->first()->id)->where('user_id',$user->id)->get();
        $this->assertCount(2,$operations);
    }


    public function testRunCommandForAccruals(){
        $this->artisan('deposit_accruals:run')
            ->expectsOutput('Done')
            ->assertExitCode(0);
    }

    // проверка если депозит уже ранее закрыт
    // проверка если в плане процент 0 или выключен день
}
