<?php

namespace Emitters\Tests\DepositPlans;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Carbon\Carbon;

class CreateDepositPlansTest extends TestCase {
    use RefreshDatabase;

    public function setUp(): void {
        parent::setUp();
        $this->lib = resolve('libair.depositplans');
    }

    public function testCreatePlans() {
        $plan = factory('Emitters\DepositPlans\Models\Deposit_Plans')->make([
            'able_to_deposit_from' => Carbon::now()->subDay(),
            'able_to_deposit_to'   => Carbon::now()->addDay(),
        ]);
        $this->lib->store($plan->toArray());

        $this->assertDatabaseHas($plan->getTable(), [
            'name' => $plan->name
        ]);
    }

    /**
     * disabled.
     */
    /*public function testCreateTwoSamePlans(){
        $plan = factory('Emitters\DepositPlans\Models\Deposit_Plans')->make();
        $this->lib->store($plan->toArray());
        $this->lib->store($plan->toArray());
    }*/

    public function testStructureDefaultValues(){
        $this->assertSame($this->lib->defaultPaymentDaysPercent(), [
            "monday"    => ['status' => false, 'percent' => 0],
            "tuesday"   => ['status' => false, 'percent' => 0],
            "wednesday" => ['status' => false, 'percent' => 0],
            "thursday"  => ['status' => false, 'percent' => 0],
            "friday"    => ['status' => false, 'percent' => 0],
            "saturday"  => ['status' => false, 'percent' => 0],
            "sunday"    => ['status' => false, 'percent' => 0]
        ]);
    }

    public function testAddDefaultValue(){
        $this->assertSame($this->lib->comparePaymentDaysPercent([
                'tuesday' => [
                    'status'  => true,
                    'percent' => 10
                ]
            ]), [
            "monday"    => ['status' => false, 'percent' => 0],
            "tuesday"   => ['status' => true, 'percent' => 10],
            "wednesday" => ['status' => false, 'percent' => 0],
            "thursday"  => ['status' => false, 'percent' => 0],
            "friday"    => ['status' => false, 'percent' => 0],
            "saturday"  => ['status' => false, 'percent' => 0],
            "sunday"    => ['status' => false, 'percent' => 0]
        ]);
    }

    public function testGetPlans(){
        $plan = factory('Emitters\DepositPlans\Models\Deposit_Plans', 10)->create();
        $count = $this->lib->getPlans([])->total();
        $this->assertCount($count, $plan);
    }

    public function testGetPlanByAmount() {
        $first = factory('Emitters\DepositPlans\Models\Deposit_Plans')->create([
            'minimum_amount' => 10,
            'maximum_amount' => 49,
        ]);
        $second = factory('Emitters\DepositPlans\Models\Deposit_Plans')->create([
            'minimum_amount'           => 50,
            'maximum_amount'           => 99,
        ]);
        $third = factory('Emitters\DepositPlans\Models\Deposit_Plans')->create([
            'minimum_amount'           => 100,
            'maximum_amount'           => 1000,
        ]);

        $res = $this->lib->getPlanByAmount(10);
        $this->assertEquals($first->id, $res->id);

        $res = $this->lib->getPlanByAmount(65);
        $this->assertEquals($second->id, $res->id);

        $res = $this->lib->getPlanByAmount(500);
        $this->assertEquals($third->id, $res->id);

        $res = $this->lib->getPlanByAmount(1000);
        $this->assertEquals($third->id, $res->id);
    }

    public function testCalculateProfitByPercent(){
        $amount = $this->lib->getAmountToProfit(100, 1);
        // 100*1/100 = 1
        $amount_test = df_div(1,1); // get string with 1.00000000
        $this->assertSame($amount, $amount_test);

        $amount = $this->lib->getAmountToProfit(0.002, 3.54);
        // 0.002*3.54/100 = 0.0000708
        $amount_test = df_div("0.0000708",1); // get string with 1.00000000
        $this->assertSame($amount, $amount_test);

        $amount = $this->lib->getAmountToProfit(95800000000, 3.54);
        $amount_test = df_div("3391320000",1); // get string with 1.00000000
        $this->assertSame($amount, $amount_test);
    }

    public function testReCalcMinMaxAmountPlan(){
        $plan = factory('Emitters\DepositPlans\Models\Deposit_Plans')->create([
            'minimum_amount' => 10,
            'maximum_amount' => 100,
        ]);
        $payment_system = factory('Emitters\PaymentSystems\Models\Payment_System')->create();
        $modifyPlan = $this->lib->getMinMaxAmount($plan);
        $amountConvert = $this->lib->convertingCurrencyRate->convertAmount(10, 'USD', $payment_system->currency);
        $name = 'minimum_amount_'.mb_strtolower($payment_system->currency);
        $this->assertSame($modifyPlan->{$name}, $amountConvert);
    }

    public function testBreakEventDate() {
        $date = Carbon::now();
        $now_week = strtolower($date->format('l'));
        $plan = factory('Emitters\DepositPlans\Models\Deposit_Plans')->make([
            'able_to_deposit_from' => Carbon::now()->subDay(),
            'able_to_deposit_to' => Carbon::now()->addDay(),
            'payments_days_percent' => [
                $now_week => [
                    'status'  => true,
                    'percent' => 100
                ]
            ]
        ]);
        $plan = $this->lib->store($plan->toArray());
        $plan = $this->lib->getBreakEvenDate($plan);
        $this->assertSame($plan->break_event->format('Y-m-d H:i'), $date->addDays(7)->format('Y-m-d H:i'));
    }

    public function testDateCloseDeposit() {
        $plan = factory('Emitters\DepositPlans\Models\Deposit_Plans')->create([
            'duration_plan' => 78
        ]);
        $date = Carbon::now();
        $plan = $this->lib->getDateCloseDeposit($plan);
        $this->assertSame($plan->deposit_close->format('Y-m-d H:i'), $date->addDays(78)->format('Y-m-d H:i'));
    }
}
