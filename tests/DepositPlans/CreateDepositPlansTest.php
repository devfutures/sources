<?php

namespace Emitters\Tests\DepositPlans;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Carbon\Carbon;
class CreateDepositPlansTest extends TestCase {
    use DatabaseTransactions;

    public function setUp () {
        parent::setUp();
        $this->lib = app()->make('libair.depositplans');
    }
    
    public function testCreatePlans(){
    	$date = Carbon::now();
        $now_week = strtolower($date->format('l'));


        $insertRes = $this->lib->store([
			'name'                     => '6 weeks',
			'duration_plan'            => '42',
			'accrual_period'           => '86400',
			'status'                   => 0,
			'minimum_amount'           => 1000000000,
			'maximum_amount'           => 10000000000000,
			'principal_return_percent' => 0,
            'payments_days_percent' => [
                $now_week => [
                    'status'  => true,
                    'percent' => 10
                ]
            ]
    	]);
        $this->assertTrue($insertRes->id ? true : false);

        $getRes = $this->lib->getPlanById($insertRes->id);
        $this->assertEquals($getRes->id, $insertRes->id);

        $res = $this->lib->getPercent($insertRes->id);
        $this->assertEquals($res, 10);
    }

    public function testStructureDefaultValues(){
        $this->assertSame($this->lib->defaultPaymentDaysPercent(), [
            "monday"    => ['status' => false, 'percent' => 0],
            "tuesday"   => ['status' => false, 'percent' => 0],
            "wednesday" => ['status' => false, 'percent' => 0],
            "thursday"  => ['status' => false, 'percent' => 0],
            "friday"    => ['status' => false, 'percent' => 0],
            "saturday"  => ['status' => false, 'percent' => 0],
            "sunday"    => ['status' => false, 'percent' => 0]
        ]);
    }

    public function testAddDefaultValue(){
        $this->assertSame($this->lib->comparePaymentDaysPercent([
                'tuesday' => [
                    'status'  => true,
                    'percent' => 10
                ]
            ]), [
            "monday"    => ['status' => false, 'percent' => 0],
            "tuesday"   => ['status' => true, 'percent' => 10],
            "wednesday" => ['status' => false, 'percent' => 0],
            "thursday"  => ['status' => false, 'percent' => 0],
            "friday"    => ['status' => false, 'percent' => 0],
            "saturday"  => ['status' => false, 'percent' => 0],
            "sunday"    => ['status' => false, 'percent' => 0]
        ]);
    }

    public function testGetPlans(){
        $count = $this->lib->getPlans()->count();
        $this->assertTrue($count >= 0 ? true : false);
    }

    public function testGetPlanByAmount(){
        $first = $this->lib->store([
            'minimum_amount'           => 10,
            'maximum_amount'           => 49,
        ]);
        $second = $this->lib->store([
            'minimum_amount'           => 50,
            'maximum_amount'           => 99,
        ]);
        $third = $this->lib->store([
            'minimum_amount'           => 100,
            'maximum_amount'           => 1000,
        ]);
        $fourth = $this->lib->store([
            'minimum_amount'           => 1001,
            'maximum_amount'           => 2000,
        ]);

        $res = $this->lib->getPlanByAmount(10);
        $this->assertEquals($first->id, $res->id);

        $res = $this->lib->getPlanByAmount(65);
        $this->assertEquals($second->id, $res->id);

        $res = $this->lib->getPlanByAmount(500);
        $this->assertEquals($third->id, $res->id);

        $res = $this->lib->getPlanByAmount(1000);
        $this->assertEquals($third->id, $res->id);
    }

    public function testCalculateProfitByPercent(){
        $amount = $this->lib->getAmountToProfit(100, 1);
        // 100*1/100 = 1
        $amount_test = bcdiv(1,1,8); // get string with 1.00000000
        $this->assertSame($amount, $amount_test);

        $amount = $this->lib->getAmountToProfit(0.002, 3.54);
        // 0.002*3.54/100 = 0.0000708
        $amount_test = bcdiv("0.0000708",1,8); // get string with 1.00000000
        $this->assertSame($amount, $amount_test);

        $amount = $this->lib->getAmountToProfit(95800000000, 3.54);
        $amount_test = bcdiv("3391320000",1,8); // get string with 1.00000000
        $this->assertSame($amount, $amount_test);
    }

    public function testReCalcMinMaxAmountPlan(){
        $plan = $this->lib->store([
            'minimum_amount'           => 10,
            'maximum_amount'           => 100,
        ]);
        $modifyPlan = $this->lib->getMinMaxAmount($plan);
        $currency = $this->lib->paymentSystems->get()[0]->currency;
        $amountConvert = $this->lib->convertingCurrencyRate->convertAmount(10, 'USD', $currency);
        $name = 'minimum_amount_'.mb_strtolower($currency);
        $this->assertSame($modifyPlan->{$name}, $amountConvert);
    }

    public function testBreakEventDate(){
        $date = Carbon::now();
        $now_week = strtolower($date->format('l'));


        $plan = $this->lib->store([
            'duration_plan'            => '100',
            'accrual_period'           => '86400',
            'minimum_amount'           => 10,
            'maximum_amount'           => 100,
            'payments_days_percent' => [
                $now_week => [
                    'status'  => true,
                    'percent' => 100
                ]
            ]
        ]);
        $plan = $this->lib->getBreakEvenDate($plan);
        $this->assertSame($plan->break_event->format('Y-m-d H:i'), $date->addDays(7)->format('Y-m-d H:i'));
        
    }

    public function testDateCloseDeposit(){
        $plan = $this->lib->store([
            'duration_plan'            => 78,
            'accrual_period'           => '86400',
            'minimum_amount'           => 10,
            'maximum_amount'           => 100,
            'payments_days_percent' => [
                "monday"    => ['status' => true, 'percent' => 2],
                "tuesday"   => ['status' => true, 'percent' => 2],
                "wednesday" => ['status' => true, 'percent' => 2],
                "thursday"  => ['status' => true, 'percent' => 2],
                "friday"    => ['status' => true, 'percent' => 2],
                "saturday"  => ['status' => true, 'percent' => 0.5],
                "sunday"    => ['status' => true, 'percent' => 0.5]
            ]
        ]);
        $date = Carbon::now();
        $plan = $this->lib->getDateCloseDeposit($plan);
        $this->assertSame($plan->deposit_close->format('Y-m-d H:i'), $date->addDays(78)->format('Y-m-d H:i'));
    }

    // public function testGetPlansView(){
    //     $plans = $this->lib->getPlans();
    //     dump($plans);
    // }
}
