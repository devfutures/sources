<?php


namespace Emitters\Tests\DepositPlans;

use App\User;
use Carbon\Carbon;
use Emitters\DepositPlans\Models\Deposit_Plans;
use Emitters\PaymentSystems\Models\Payment_System;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DepositPlansControllerTest extends TestCase {
    use RefreshDatabase;

    protected $lib;

    public function setUp(): void {
        parent::setUp();
        $this->lib = resolve('libair.depositplans');
    }

    public function testGetPlans() {
        $this->withJWT();
        factory(Payment_System::class)->create();
        \Artisan::call('convertingcurrency:parse');
        $plan     = factory(Deposit_Plans::class, 15)->create();
        $filters  = [
            'where'   => [
                [
                    'field' => 'status',
                    'value' => 1
                ]
            ],
            'orderBy' => [
                [
                    'by'        => 'principal_return_percent',
                    'direction' => 'desc'
                ]
            ],
            'amount'  => [
                [
                    'fields' => [
                        'min' => 'minimum_amount',
                        'max' => 'maximum_amount'
                    ],
                    'min'    => 1,
                    'max'    => 3000
                ]
            ]
        ];
        $response = $this->json('POST', route('depositplans.index'), $filters);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'code',
            'plans' => [
                'current_page',
                'data' => [
                    [
                        'id',
                        'name',
                        'description',
                        'able_to_deposit_from',
                        'able_to_deposit_to',
                        'accrual_period',
                        'delay_accruals',
                        'status',
                        'payments_days_percent' => [
                            'monday'    => [
                                'status',
                                'percent'
                            ],
                            'tuesday'   => [
                                'status',
                                'percent'
                            ],
                            'wednesday' => [
                                'status',
                                'percent'
                            ],
                            'thursday'  => [
                                'status',
                                'percent'
                            ],
                            'friday'    => [
                                'status',
                                'percent'
                            ],
                            'saturday'  => [
                                'status',
                                'percent'
                            ],
                            'sunday'    => [
                                'status',
                                'percent'
                            ],
                        ],
                        'principal_return_percent',
                        'allow_from_balance',
                        'allow_add_funds',
                        'limit_deps_user',
                        'break_event',
                        'deposit_close',
                        'minimum_amount_usd',
                        'maximum_amount_usd',
                    ]
                ]
            ]
        ]);
    }

    public function testGetDataForCreateDeposit() {
        $this->withJWT();
        factory(Payment_System::class)->create();
        $this->artisan('convertingcurrency:parse');
        $response = $this->json('GET', route('depositplans.data'));
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'currency_rate'        => [
                'id',
                'btc_usd'
            ],
            'payment_days_percent' => [
                'monday'    => [
                    'status',
                    'percent'
                ],
                'tuesday'   => [
                    'status',
                    'percent'
                ],
                'wednesday' => [
                    'status',
                    'percent'
                ],
                'thursday'  => [
                    'status',
                    'percent'
                ],
                'friday'    => [
                    'status',
                    'percent'
                ],
                'saturday'  => [
                    'status',
                    'percent'
                ],
                'sunday'    => [
                    'status',
                    'percent'
                ],
            ]
        ]);
    }

    public function testDepositPlanCreate() {
        $this->withJWT();
        $planData = factory(Deposit_Plans::class)->make();

        $data = $planData->toArray();
        $data['able_to_deposit_from'] = Carbon::now()->subDay()->format('d.m.Y');
        $data['able_to_deposit_to'] = Carbon::now()->subDay()->format('d.m.Y');
        $response = $this->json('POST', route('depositplans.create'), $data);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'code',
            'plan' => [
                'id',
                'name',
                'duration_plan',
                'able_to_deposit_from',
                'able_to_deposit_to',
                'accrual_period',
                'status',
                'payments_days_percent' => [
                    'monday'    => [
                        'status',
                        'percent'
                    ],
                    'tuesday'   => [
                        'status',
                        'percent'
                    ],
                    'wednesday' => [
                        'status',
                        'percent'
                    ],
                    'thursday'  => [
                        'status',
                        'percent'
                    ],
                    'friday'    => [
                        'status',
                        'percent'
                    ],
                    'saturday'  => [
                        'status',
                        'percent'
                    ],
                    'sunday'    => [
                        'status',
                        'percent'
                    ],
                ],
                'principal_return_percent',
                'allow_from_balance',
                'allow_add_funds',
            ]
        ]);
    }

    public function testUpdatePlan() {
        $plan     = factory(Deposit_Plans::class)->create();
        $planData = factory(Deposit_Plans::class)->make();

        $data = $planData->toArray();
        $data['able_to_deposit_from'] = Carbon::now()->subDay()->format('d.m.Y');
        $data['able_to_deposit_to'] = Carbon::now()->subDay()->format('d.m.Y');

        $user = factory(User::class)->create();
        $this->withJWT($user);
        $response = $this->json('PATCH', route('depositplans.update', ['id' => $plan->id]), $data);
        $response->assertStatus(200);
        $plan = $plan->fresh();
        $attr = $plan->getAttributes();
        foreach ($attr as $att) {
            $this->assertEquals($plan->$att, $planData->$att);
        }
    }

    public function testDeletePlan() {
        $this->withJWT();
        $plan     = factory(Deposit_Plans::class)->create();
        $response = $this->json('DELETE', route('depositplans.delete', ['id' => 1]));
        $plan     = $plan->fresh();
        $this->assertTrue($plan->trashed());
    }
}