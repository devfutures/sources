<?php


namespace Emitters\Tests\DepositPlans;


use Carbon\Carbon;
use Emitters\Balance\Models\Users_Balance;
use Emitters\DepositUsers\Models\Deposit_Users;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Config;
use Tests\TestCase;

class DepositControllerClientTest extends TestCase {
    use RefreshDatabase;

    public function testNoDataSend(){
        $this->withJWT();
        $response = $this->json('POST',route('client.create_deposit'));
        $response->assertStatus(422);
        $content = json_decode($response->getContent(),true);
        $this->assertCount(1,$content['errors']);
        $this->assertArrayHasKey('payment_system',$content['errors']);
    }

    public function testWithPaymentSystemString(){
        $this->withJWT();
        $response = $this->json('POST',route('client.create_deposit'),[
            'payment_system' => "string_value"
        ]);
        $response->assertStatus(422);
        $content = json_decode($response->getContent(),true);
        $this->assertCount(2,$content['errors']);
        $this->assertCount(2,$content['errors']['payment_system']);
        $this->assertArrayHasKey('payment_system',$content['errors']);
        $this->assertArrayHasKey('amount',$content['errors']);
    }

    public function testWithPaymentSystem(){
        $this->withJWT();
        $ps = factory('Emitters\PaymentSystems\Models\Payment_System')->create([
            'purchase'         => 0,
            'status'           => 0,
            'min_sum_purchase' => 0.0004,
            'max_sum_purchase' => 10
        ]);
        $response = $this->json('POST',route('client.create_deposit'),[
            'payment_system' => $ps->id
        ]);
        $response->assertStatus(422);
        $content = json_decode($response->getContent(),true);
        $this->assertCount(2,$content['errors']);
        $this->assertCount(1,$content['errors']['payment_system']);
        $this->assertArrayHasKey('amount',$content['errors']);
    }

    public function testWithPaymentSystemCorrectly(){
        $this->withJWT();
        $ps = factory('Emitters\PaymentSystems\Models\Payment_System')->create([
            'purchase'         => 1,
            'status'           => 1,
            'min_sum_purchase' => 0.0004,
            'max_sum_purchase' => 10
        ]);
        $response = $this->json('POST',route('client.create_deposit'),[
            'payment_system' => $ps->id
        ]);
        $response->assertStatus(422);
        $content = json_decode($response->getContent(),true);
        $this->assertCount(1,$content['errors']);
        $this->assertArrayHasKey('amount',$content['errors']);
        $this->assertArrayNotHasKey('payment_system',$content['errors']);
    }

    public function testWithAmount(){
        $this->withJWT();
        $ps = factory('Emitters\PaymentSystems\Models\Payment_System')->create([
            'purchase'         => 1,
            'status'           => 1,
            'min_sum_purchase' => 0.0004,
            'max_sum_purchase' => 10
        ]);
        $response = $this->json('POST',route('client.create_deposit'),[
            'payment_system' => $ps->id,
            'amount'         => 0
        ]);
        $response->assertStatus(422);
        $content = json_decode($response->getContent(),true);
        $this->assertCount(1,$content['errors']);
        $this->assertArrayHasKey('amount',$content['errors']);
    }

    public function testWithAmountCorrectly(){
        $this->withJWT();
        $ps = factory('Emitters\PaymentSystems\Models\Payment_System')->create([
            'purchase'         => 1,
            'status'           => 1,
            'min_sum_purchase' => 0.0004,
            'max_sum_purchase' => 10
        ]);
        $response = $this->json('POST',route('client.create_deposit'),[
            'payment_system' => $ps->id,
            'amount'         => 0.0004
        ]);
        $content = json_decode($response->getContent(),true);
        $response->assertStatus(200);
    }

    public function testWithPlan(){
        $this->withJWT();
        $ps = factory('Emitters\PaymentSystems\Models\Payment_System')->create([
            'purchase'         => 1,
            'status'           => 1,
            'min_sum_purchase' => 0.0004,
            'max_sum_purchase' => 10
        ]);
        $response = $this->json('POST',route('client.create_deposit'),[
            'payment_system' => $ps->id,
            'amount'         => 0.0004,
            'plan_id'        => 0
        ]);
        $response->assertStatus(422);
        $content = json_decode($response->getContent(),true);
        $this->assertCount(2,$content['errors']);
        $this->assertArrayHasKey('plan_id',$content['errors']);
        $this->assertArrayHasKey('from_balance',$content['errors']);
    }

    public function testWithPlanCorrecly(){
        $this->withJWT();
        $ps = factory('Emitters\PaymentSystems\Models\Payment_System')->create([
            'purchase'         => 1,
            'status'           => 1,
            'min_sum_purchase' => 0.0004,
            'max_sum_purchase' => 10
        ]);
        $plan = factory('Emitters\DepositPlans\Models\Deposit_Plans')->create([
            'status'             => 1,
            'allow_from_balance' => 0
        ]);
        $response = $this->json('POST',route('client.create_deposit'),[
            'payment_system' => $ps->id,
            'amount'         => 0.0004,
            'plan_id'        => $plan->id
        ]);
        $response->assertStatus(422);
        $content = json_decode($response->getContent(),true);
        $this->assertCount(1,$content['errors']);
        $this->assertArrayHasKey('from_balance',$content['errors']);
        $this->assertArrayNotHasKey('plan_id',$content['errors']);
    }

    public function testWithFromBalanceButPlanDoNotSupportCorrecly(){
        $this->withJWT();
        $ps = factory('Emitters\PaymentSystems\Models\Payment_System')->create([
            'purchase'         => 1,
            'status'           => 1,
            'min_sum_purchase' => 0.0004,
            'max_sum_purchase' => 10
        ]);
        $plan = factory('Emitters\DepositPlans\Models\Deposit_Plans')->create([
            'status'             => 1,
            'allow_from_balance' => 0
        ]);
        $response = $this->json('POST',route('client.create_deposit'),[
            'payment_system' => $ps->id,
            'amount'         => 0.0004,
            'plan_id'        => $plan->id,
            'from_balance'   => 1
        ]);
        $response->assertStatus(422);
        $content = json_decode($response->getContent(),true);
        $this->assertCount(1,$content['errors']);
        $this->assertArrayHasKey('from_balance',$content['errors']);
    }

    public function testWithFromBalanceAndPlanSupportedCorrectly() {
        $user = factory('App\User')->create();
        $this->withJWT($user);

        $ps = factory('Emitters\PaymentSystems\Models\Payment_System')->create([
            'purchase'         => 1,
            'status'           => 1,
            'min_sum_purchase' => 0.0004,
            'max_sum_purchase' => 10
        ]);

        factory(Users_Balance::class)->create([
            'user_id'           => $user->id,
            'payment_system_id' => $ps->id,
            'amount'            => 40000,
            'balance_now'       => 40000
        ]);

        $plan = factory('Emitters\DepositPlans\Models\Deposit_Plans')->create([
            'status'             => 1,
            'allow_from_balance' => 1
        ]);

        $response = $this->json('POST', route('client.create_deposit'), [
            'payment_system' => $ps->id,
            'amount'         => 0.0004,
            'plan_id'        => $plan->id,
            'from_balance'   => 1
        ]);

        $response->assertStatus(200);
    }

    public function testWithFromBalanceAndPlanThrowsExceptionWhenNotEnoughFunds() {
        $this->withJWT();
        $ps = factory('Emitters\PaymentSystems\Models\Payment_System')->create([
            'purchase'         => 1,
            'status'           => 1,
            'min_sum_purchase' => 0.0004,
            'max_sum_purchase' => 10
        ]);
        $plan = factory('Emitters\DepositPlans\Models\Deposit_Plans')->create([
            'status'             => 1,
            'allow_from_balance' => 1
        ]);
        $response = $this->json('POST',route('client.create_deposit'),[
            'payment_system' => $ps->id,
            'amount'         => 0.0004,
            'plan_id'        => $plan->id,
            'from_balance'   => 1
        ]);
        $response->assertStatus(422);
    }

    public function testWithFromBalanceCorrectly(){
        $this->withJWT();
        $ps = factory('Emitters\PaymentSystems\Models\Payment_System')->create([
            'purchase'         => 1,
            'status'           => 1,
            'min_sum_purchase' => 0.0004,
            'max_sum_purchase' => 10
        ]);
        $plan = factory('Emitters\DepositPlans\Models\Deposit_Plans')->create([
            'status'             => 1,
            'allow_from_balance' => 0
        ]);
        $response = $this->json('POST',route('client.create_deposit'),[
            'payment_system' => $ps->id,
            'amount'         => 0.0004,
            'plan_id'        => $plan->id,
            'from_balance'   => 0
        ]);
        $response->assertStatus(200);
    }

    public function testCreatePaymentGoMerchant(){
        list($user,$jwt) = $this->withJWT();
        $ps = factory('Emitters\PaymentSystems\Models\Payment_System')->create([
            'title'            => 'PerfectMoney',
            'class_name'       => 'Selfreliance\PerfectMoney\PerfectMoney',
            'purchase'         => 1,
            'status'           => 1,
            'min_sum_purchase' => 1,
            'max_sum_purchase' => 10
        ]);

        $response = $this->json('POST',route('client.create_deposit'),[
            'payment_system' => $ps->id,
            'amount'         => 2,
        ]);
        $response->assertJsonStructure([
            'status',
            'form'
        ]);
    }

    public function testCreateEventPerfectMoney(){
        list($user,$jwt) = $this->withJWT();
        $module = config('emitters.payment_system.available_modules')['perfectmoney'];
        factory('Emitters\ConvertingCurrencyRate\Models\Currency_Rate')->create();

        $ps = factory('Emitters\PaymentSystems\Models\Payment_System')->create([
            'title'            => 'PerfectMoney',
            'currency'         => 'USD',
            'class_name'       => array_random($module),
            'purchase'         => 1,
            'status'           => 1,
            'min_sum_purchase' => 1,
            'max_sum_purchase' => 10
        ]);

        // create system coin
        $system_coin = factory('Emitters\PaymentSystems\Models\Payment_System')->create([
            'title'            => 'MyCoin',
            'currency'         => config('emitters.default_currency_systems'),
            'class_name'       => '',
            'purchase'         => 1,
            'status'           => 1,
            'min_sum_purchase' => 1,
            'max_sum_purchase' => 10
        ]);
        Config::set('emitters.default_payment_system_id',$system_coin->id);
        Config::set('emitters.after_add_funds','ADD_FUNDS_TO_BALANCE');

        $response = $this->json('POST',route('client.create_deposit'),[
            'payment_system' => $ps->id,
            'amount'         => $ps->min_sum_purchase,
        ]);

        $data = [
            'PAYMENT_ID'        => 1,
            'PAYMENT_AMOUNT'    => $ps->min_sum_purchase,
            'PAYEE_ACCOUNT'     => config('perfectmoney.payee_account'),
            'PAYMENT_BATCH_NUM' => mt_rand(100000000,999999999),
            'PAYER_ACCOUNT'     => 'U'.mt_rand(1000000,9999999),
            'TIMESTAMPGMT'      => Carbon::now()->timestamp
        ];
        $sign = @$data['PAYMENT_ID'].":".config('perfectmoney.payee_account').":".@$data['PAYMENT_AMOUNT'].":USD:".@$data['PAYMENT_BATCH_NUM'].":".@$data['PAYER_ACCOUNT'].":".strtoupper(md5(config('perfectmoney.alt'))).":".@$data['TIMESTAMPGMT'];
        $sign = strtoupper(md5($sign));
        $data['V2_HASH'] = $sign;

        $response = $this->json('POST',route('perfectmoney.confirm'),$data);

        $operations = users_operations()->getUserOperations($user,['ADD_FUNDS'],['completed']);
        $find = $operations->where('payment_system_id',$ps->id)->where('transaction',$data['PAYMENT_BATCH_NUM'])->first();
        $this->assertNotNull($find);
    }

    public function testCreateEventPayeer(){
        list($user,$jwt) = $this->withJWT();
        $module = config('emitters.payment_system.available_modules')['payeer'];
        factory('Emitters\ConvertingCurrencyRate\Models\Currency_Rate')->create();

        $ps = factory('Emitters\PaymentSystems\Models\Payment_System')->create([
            'title'            => 'Payeer',
            'currency'         => 'USD',
            'class_name'       => array_random($module),
            'purchase'         => 1,
            'status'           => 1,
            'min_sum_purchase' => 1,
            'max_sum_purchase' => 10
        ]);

        // create system coin
        $system_coin = factory('Emitters\PaymentSystems\Models\Payment_System')->create([
            'title'            => 'MyCoin',
            'currency'         => config('emitters.default_currency_systems'),
            'class_name'       => '',
            'purchase'         => 1,
            'status'           => 1,
            'min_sum_purchase' => 1,
            'max_sum_purchase' => 10
        ]);
        Config::set('emitters.default_payment_system_id',$system_coin->id);
        Config::set('emitters.after_add_funds','ADD_FUNDS_TO_BALANCE');

        $response = $this->json('POST',route('client.create_deposit'),[
            'payment_system' => $ps->id,
            'amount'         => $ps->min_sum_purchase,
        ]);

        $data = [
            'm_amount'             => $ps->min_sum_purchase,
            'm_orderid'            => 1,
            'm_operation_id'       => mt_rand(100000000,999999999),
            'm_sign'               => null,
            'm_operation_ps'       => 1,
            'm_operation_date'     => 1,
            'm_operation_pay_date' => Carbon::now()->timestamp,
            'm_shop'               => Carbon::now()->timestamp,
            'm_curr'               => 1,
            'm_desc'               => 1,
            'm_status'             => 'success',
        ];
        $arHash = [$data['m_operation_id'],
                   $data['m_operation_ps'],
                   $data['m_operation_date'],
                   $data['m_operation_pay_date'],
                   $data['m_shop'],
                   $data['m_orderid'],
                   $data['m_amount'],
                   $data['m_curr'],
                   $data['m_desc'],
                   $data['m_status'],
                   config('payeer.shop_secret_key')
        ];
        $data['m_sign'] = strtoupper(hash('sha256',implode(":",$arHash)));

        $response = $this->json('POST',route('payeer.confirm'),$data);

        $operations = users_operations()->getUserOperations($user,['ADD_FUNDS'],['completed']);
        $find = $operations->where('payment_system_id',$ps->id)->where('transaction',$data['m_operation_id'])->first();
        $this->assertNotNull($find);
    }

    public function testCreateDepositFromBalance(){
        list($user,$jwt) = $this->withJWT();
        $ps = factory('Emitters\PaymentSystems\Models\Payment_System')->create([
            'purchase'         => 1,
            'status'           => 1,
            'min_sum_purchase' => 0.0004,
            'max_sum_purchase' => 10
        ]);
        $plan = factory('Emitters\DepositPlans\Models\Deposit_Plans')->create([
            'duration_plan'            => 1,
            'accrual_period'           => 43200,
            'delay_accruals'           => 0,
            'available_count_deposits' => 2,
            'status'                   => 1,
            'allow_from_balance'       => 1,
            'able_to_deposit_from'     => Carbon::now()->subDays(2),
            'able_to_deposit_to'       => Carbon::now()->addDay(),
        ]);
        factory('Emitters\Balance\Models\Users_Balance')->create([
            'type'              => 'BUY',
            'operation'         => 'ADD_FUNDS',
            'user_id'           => $user->id,
            'payment_system_id' => $ps->id,
            'amount'            => '40000',
            'balance_now'       => '40000'
        ]);
        factory('Emitters\DepositUsers\Models\Deposit_Users')->create([
            'user_id' => 0,
            'plan_id' => $plan->id
        ]);

        $response = $this->json('POST',route('client.create_deposit'),[
            'payment_system' => $ps->id,
            'amount'         => 0.0004,
            'plan_id'        => $plan->id,
            'from_balance'   => 1
        ]);
        $response->assertStatus(200);
    }
}