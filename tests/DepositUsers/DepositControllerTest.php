<?php


namespace Emitters\Tests\DepositPlans;


use App\User;
use Carbon\Carbon;
use Emitters\DepositUsers\Models\Deposit_Users;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DepositControllerTest extends TestCase {
    use RefreshDatabase;

    public function testIndex() {
        $deposits = factory(Deposit_Users::class, 10)->create();
        $user = factory(User::class)->create();
        $this->withJWT($user);
        $response = $this->json('POST', route('depositsusers.index'), []);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'code',
            'deposits' => [
                'current_page',
                'data' => [
                    [
                        'id',
                        'user_id',
                        'plan_id',
                        'payment_system_id',
                        'amount',
                        'status',
                        'accrual_at',
                        'deposit_end_at',
                    ]
                ]
            ]
        ]);
    }

    public function testGetDepositsWithFilterAmount() {
        $deposits     = factory(Deposit_Users::class, 20)->create();
        $fromValue    = 1000;
        $toValue      = 5000;
        $amountFilter = [
            'whereBetween' => [
                [
                    'field' => 'amount',
                    'from'  => $fromValue,
                    'to'    => $toValue
                ]
            ]
        ];
        $this->withJWT();
        $response     = $this->json('POST', route('depositsusers.index'), $amountFilter);
        $depositsArr  = json_decode($response->getContent())->deposits->data;
        foreach ($depositsArr as $item) {
            $this->assertTrue($item->amount > $fromValue);
            $this->assertTrue($item->amount < $toValue);
        }
    }

    public function testGetDepositsWithFilterOrderBy() {
        $deposits           = factory(Deposit_Users::class, 20)->create();
        $this->withJWT();
        $orderBy            = [
            'orderBy' => [
                [
                    'by'        => 'amount',
                    'direction' => 'desc'
                ]
            ],
        ];
        $ordered            = $this->json('POST', route('depositsusers.index'), $orderBy);
        $orderedDepositsArr = json_decode($ordered->getContent())->deposits->data;
        $tmpArr             = collect([$orderedDepositsArr[0]]);
        foreach ($orderedDepositsArr as $dep) {
            $this->assertTrue($dep->amount <= $tmpArr->last()->amount);
            $tmpArr->push($dep);
        }
    }

    public function testGetDepositsWithFilterWhere() {
        factory(Deposit_Users::class, 10)->create();
        factory(Deposit_Users::class, 10)->create(['status' => 1]);
        $this->withJWT();
        $where              = [
            'where' => [
                [
                    'field' => 'status',
                    'value' => 1
                ]
            ]
        ];
        $byWhere            = $this->json('POST', route('depositsusers.index'), $where);
        $byWhereDepositsArr = json_decode($byWhere->getContent())->deposits->data;
        foreach ($byWhereDepositsArr as $dep) {
            $this->assertTrue($dep->status == 1);
        }
    }

    public function testGetDepositsWithFilterDateBetween() {
        factory(Deposit_Users::class, 3)->create([
            'deposit_end_at' => Carbon::now()->addDay(3)
        ]);
        factory(Deposit_Users::class, 2)->create([
            'deposit_end_at' => Carbon::now()
        ]);
        $dateFrom = Carbon::now();
        $dateTo   = Carbon::now()->addDay(2);
        $this->withJWT();
        $where = [
            'dateBetween' => [
                [
                    'from_date_column' => 'deposit_end_at',
                    'to_date_column'   => 'deposit_end_at',
                    'from'             => $dateFrom->format('d.m.Y'),
                    'to'               => $dateTo->format('d.m.Y'),
                ]
            ]
        ];
        $byWhere                = $this->json('POST', route('depositsusers.index'), $where);
        $dateBetweenDepositsArr = json_decode($byWhere->getContent())->deposits->data;

        foreach ($dateBetweenDepositsArr as $dep) {
            $this->assertTrue($dateFrom->gte(Carbon::createFromFormat('Y-m-d H:i:s', $dep->deposit_end_at)));
            $this->assertTrue($dateTo->gte(Carbon::createFromFormat('Y-m-d H:i:s', $dep->deposit_end_at)));
        }
    }
}