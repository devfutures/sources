<?php

namespace Emitters\Tests\DepositPlans;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DepositUsersTest extends TestCase {
	use RefreshDatabase;
    protected $depositManager;
	public function setUp(){
		parent::setUp();
		$this->depositManager = resolve('libair.depositusers');
	}
	
	public function testCreateDepositForUserWithPlan(){
		$user = factory('App\User')->create();
		$plan = factory('Emitters\DepositPlans\Models\Deposit_Plans')->create();
	    $deposits = factory('Emitters\DepositUsers\Models\Deposit_Users')->make();
		$deposit_user = $this->depositManager->createDeposit($user, $plan, $deposits->amount, $deposits->payment_system_id);
		$this->assertInstanceOf('Emitters\DepositUsers\Models\Deposit_Users', $deposit_user);
	}

	public function testGetDepositsByUser(){
		$user = factory('App\User')->create();
		$deposits = factory('Emitters\DepositUsers\Models\Deposit_Users', 2)->create([
			'user_id' => $user->id,
		]);

		$deposits_users = $this->depositManager->getDepositUser($user);
		$this->assertCount(count($deposits), $deposits_users);
	}

	public function testGetDepositEnd(){
        $user = factory('App\User')->create();

	    $normal = factory('Emitters\DepositUsers\Models\Deposit_Users', 2)->create([
	        'user_id' => $user->id
        ]);

	    $ends = factory('Emitters\DepositUsers\Models\Deposit_Users', 2)->create([
	        'deposit_end' => 1,
            'user_id' => $user->id
        ]);

        $deposits_users = $this->depositManager->getDepositUserEnd($user);
        $this->assertCount(count($ends), $deposits_users);
    }

    public function testDepositHasPlan(){
    	$deposits = factory('Emitters\DepositUsers\Models\Deposit_Users')->create();
    	$this->assertInstanceOf('Emitters\DepositPlans\Models\Deposit_Plans', $deposits->plan);
    }

    public function testDepositHasUser(){
    	$deposits = factory('Emitters\DepositUsers\Models\Deposit_Users')->create();
    	$this->assertInstanceOf('App\User', $deposits->owner);
    }

    public function testGetAllDeposits(){
        $deposits = factory('Emitters\DepositUsers\Models\Deposit_Users', 10)->create();
	    $listDeposits = $this->depositManager->getDeposits();
        $this->assertCount(10, $listDeposits);
    }

    public function testGetDepositsWithFilter(){
    	$user = factory('App\User')->create();
        $plan = factory('Emitters\DepositPlans\Models\Deposit_Plans')->create();

    	$depositsByUser = factory('Emitters\DepositUsers\Models\Deposit_Users', 3)->create([
    		'user_id' => $user->id
    	]);
    	$depositsByPlan = factory('Emitters\DepositUsers\Models\Deposit_Users', 2)->create([
            'plan_id' => $plan->id
        ]);

    	$request = [
    	    'user' => $user->email
        ];
    	$list = $this->depositManager->getDeposits($request);
        $this->assertCount(count($depositsByUser), $list);

        $request = [
            'plan' => $plan->id
        ];
        $list = $this->depositManager->getDeposits($request);

        $this->assertCount(count($depositsByPlan), $list);

        $request = [
            'plan' => $plan->id,
            'user' => $user->email
        ];
        $list = $this->depositManager->getDeposits($request);
        $this->assertCount(0, $list);
    }
}