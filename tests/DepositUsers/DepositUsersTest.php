<?php

namespace Emitters\Tests\DepositPlans;

use Carbon\Carbon;
use Emitters\DepositUsers\Exceptions\DepositCreateException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DepositUsersTest extends TestCase {
	use RefreshDatabase;

    protected $depositManager;

	public function setUp(): void{
		parent::setUp();
		$this->depositManager = resolve('libair.depositusers');
	}
	
	public function testCreateDepositForUserWithPlan(){
		$user = factory('App\User')->create();
		$plan = factory('Emitters\DepositPlans\Models\Deposit_Plans')->create([
		    'able_to_deposit_from' => Carbon::now()->subDay(1),
		    'able_to_deposit_to' => Carbon::now()->addMonth(),
        ]);
	    $deposits = factory('Emitters\DepositUsers\Models\Deposit_Users')->make();

		$deposit_user = $this->depositManager->createDeposit($user, $plan, $deposits->amount, $deposits->payment_system_id);
		$this->assertInstanceOf('Emitters\DepositUsers\Models\Deposit_Users', $deposit_user);
	}

    /**
     * @expectedException Emitters\DepositUsers\Exceptions\DepositCreateException
     * @expectedExceptionMessage LIMIT_FOR_USER
     */
    public function testIfPlanAllowsToCreateNumberOfDeposits(){
        $user = factory('App\User')->create();
        $plan = factory('Emitters\DepositPlans\Models\Deposit_Plans')->create([
            'limit_deps_user' => 1
        ]);
        $testDeposit = factory('Emitters\DepositUsers\Models\Deposit_Users')->make([
            'plan_id' => $plan->id,
            'user_id' => $user->id
        ]);
        $this->depositManager->createDeposit($user, $plan, $testDeposit->amount, $testDeposit->payment_system_id);

        $with_another_plan = factory('Emitters\DepositPlans\Models\Deposit_Plans')->create([
            'limit_deps_user' => 2
        ]);
        $deposits = factory('Emitters\DepositUsers\Models\Deposit_Users')->make([
            'plan_id' => $plan->id,
            'user_id' => $user->id
        ]);
        $this->depositManager->createDeposit($user, $with_another_plan, $deposits->amount, $deposits->payment_system_id);
        $this->depositManager->createDeposit($user, $with_another_plan, $deposits->amount, $deposits->payment_system_id);
        $this->depositManager->createDeposit($user, $with_another_plan, $deposits->amount, $deposits->payment_system_id);
    }

    public function testUpdateDeposit(){
        $plan = factory('Emitters\DepositPlans\Models\Deposit_Plans')->create([
            'allow_add_funds' => 1,
            'allow_from_balance' => 1
        ]);
        $createdDeposit = factory('Emitters\DepositUsers\Models\Deposit_Users')->create([
            'plan_id' => $plan->id
        ]);

        $updatedDeposit = $this->depositManager->updateDepositAmount($createdDeposit->id, $add_funds = 50, true);
        $this->assertSame(
            df_add(
                $createdDeposit->amount, 50
            ),
            $updatedDeposit->amount
        );
    }
    /**
     * @expectedException Emitters\DepositUsers\Exceptions\DepositUpdateException
     * @expectedExceptionMessage NOT_ALLOW_ADD_FUNDS
     */
    public function testUpdateDepositWithExceptionNotAllowAddFunds(){
        $createdDeposit = factory('Emitters\DepositUsers\Models\Deposit_Users')->create();
        $this->depositManager->updateDepositAmount($createdDeposit->id, $add_funds = 50, false);
    }

    //@expectedException Emitters\DepositUsers\Exceptions\DepositCreateException
    /*public function testDepositCreatePlanDepositPeriodExpired(){
        $user = factory('App\User')->create();
        $plan = factory('Emitters\DepositPlans\Models\Deposit_Plans')->create([
            'able_to_deposit_from' => Carbon::now()->subMonth(2),
            'able_to_deposit_to' => Carbon::now()->subMonth(1)
        ]);
        $deposits = factory('Emitters\DepositUsers\Models\Deposit_Users')->make();
        $deposit_user = $this->depositManager->createDeposit($user, $plan, $deposits->amount, $deposits->payment_system_id);
    }*/

    /**
     * @expectedException Emitters\DepositUsers\Exceptions\DepositUpdateException
     * @expectedExceptionMessage NOT_UPDATE_DEPOSIT_FROM_BALANCE
     */
    public function testUpdateDepositWithExceptionNoBalance(){
        $plan = factory('Emitters\DepositPlans\Models\Deposit_Plans')->create([
            'allow_add_funds' => 1,
            'allow_from_balance' => 0
        ]);
        $createdDeposit = factory('Emitters\DepositUsers\Models\Deposit_Users')->create([
            'plan_id' => $plan->id
        ]);
        $this->depositManager->updateDepositAmount($createdDeposit->id, $add_funds = 50, true);
    }

	public function testGetDepositsByUser(){
		$user = factory('App\User')->create();
		$deposits = factory('Emitters\DepositUsers\Models\Deposit_Users', 2)->create([
			'user_id' => $user->id,
		]);

		$deposits_users = $this->depositManager->getDepositUser($user);
		$this->assertCount(count($deposits), $deposits_users);
	}

	public function testGetDepositEnd(){
        $user = factory('App\User')->create();

	    $normal = factory('Emitters\DepositUsers\Models\Deposit_Users', 2)->create([
	        'user_id' => $user->id,
            'status' => 0
        ]);

	    $ends = factory('Emitters\DepositUsers\Models\Deposit_Users', 2)->create([
	        'status' => 1,
            'user_id' => $user->id
        ]);

        $deposits_users = $this->depositManager->getDepositUserEnd($user);
        $this->assertCount(count($ends), $deposits_users);
    }

    public function testDepositHasPlan(){
    	$deposits = factory('Emitters\DepositUsers\Models\Deposit_Users')->create();
    	$this->assertInstanceOf('Emitters\DepositPlans\Models\Deposit_Plans', $deposits->plan);
    }

    public function testDepositHasUser(){
    	$deposits = factory('Emitters\DepositUsers\Models\Deposit_Users')->create();
    	$this->assertInstanceOf('App\User', $deposits->owner);
    }

    public function testGetAllDeposits(){
        $deposits = factory('Emitters\DepositUsers\Models\Deposit_Users', 10)->create();
	    $listDeposits = $this->depositManager->getDeposits();
        $this->assertCount(10, $listDeposits);
    }

    public function testGetDepositsWithFilter(){
    	$user = factory('App\User')->create();
        $plan = factory('Emitters\DepositPlans\Models\Deposit_Plans')->create();
        $payment_system = factory('Emitters\PaymentSystems\Models\Payment_System')->create();

    	$depositsByUser = factory('Emitters\DepositUsers\Models\Deposit_Users', 3)->create([
    		'user_id' => $user->id
    	]);
    	$depositsByPlan = factory('Emitters\DepositUsers\Models\Deposit_Users', 2)->create([
            'plan_id' => $plan->id
        ]);
    	$depositsByPaymentSystem = factory('Emitters\DepositUsers\Models\Deposit_Users', 4)->create([
            'payment_system_id' => $payment_system->id
        ]);
    	$depositsByStatus = factory('Emitters\DepositUsers\Models\Deposit_Users', 1)->create([
            'status' => 2
        ]);

        // Filter user
        $request = [
            'where' => [
                [
                    'field' => 'user_id',
                    'value' => $user->id
                ]
            ]
        ];
    	$list = $this->depositManager->getDeposits($request);
        $this->assertCount(count($depositsByUser), $list);
        // end

        // Filter plan
        $request = [
            'where' => [
                [
                    'field' => 'plan_id',
                    'value' => $plan->id
                ]
            ]
        ];
        $list = $this->depositManager->getDeposits($request);
        $this->assertCount(count($depositsByPlan), $list);
        // end

        // Filter Payment System
        $request = [
            'where' => [
                [
                    'field' => 'payment_system_id',
                    'value' => $payment_system->id
                ]
            ]
        ];
        $list = $this->depositManager->getDeposits($request);
        $this->assertCount(count($depositsByPaymentSystem), $list);
        // end

        // Filter status
        $request = [
            'where' => [
                [
                    'field' => 'status',
                    'value' => 2
                ]
            ]
        ];
        $list = $this->depositManager->getDeposits($request);
        $this->assertCount(count($depositsByStatus), $list);
        // end

        $depositsByPaymentSystem = factory('Emitters\DepositUsers\Models\Deposit_Users', 4)->create([
            'user_id' => $user->id,
            'plan_id' => $plan->id,
            'payment_system_id' => $payment_system->id,
            'status' => 2
        ]);

        // Empty result with all filters
        $request = [
            'where' => [
                [
                    'field' => 'plan_id',
                    'value' => $plan->id
                ],
                [
                    'field' => 'user_id',
                    'value' => $user->id
                ],
                [
                    'field' => 'payment_system_id',
                    'value' => $payment_system->id
                ],
                [
                    'field' => 'status',
                    'value' => 2
                ],
            ]
        ];
        $list = $this->depositManager->getDeposits($request);
        $this->assertCount(count($depositsByPaymentSystem), $list);
    }
}