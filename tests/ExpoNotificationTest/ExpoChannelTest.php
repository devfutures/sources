<?php


use Emitters\ExpoNotification\ExpoChannel;
use Emitters\ExpoNotification\NotifyMessage;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExpoChannelTest extends \Tests\TestCase {
    use RefreshDatabase;

    /** @var ExpoChannel */
    protected $expoChannel;

    public function setUp(): void {
        parent::setUp();
        $this->expoChannel = resolve('expo_notification_driver');
    }

    public function testSendNotify() {
        $message  = new NotifyMessage(
            [
                'ExponentPushToken[fdblpmMQ5h83QDWKMbdp3H]'
            ],
            'Title string',
            'Subtitle string',
            'Body of notification',
            [
                'Some notification data' => [
                    '' => [
                        //
                    ]
                ]
            ],
            'some-channel');
        $response = $this->expoChannel->send($message);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testSendNotificationController() {
        factory(\Emitters\ExpoNotification\Models\Users_Devices::class)->create(['token' => 'ExponentPushToken[fdblpmMQ5h83QDWKMbdp3H]']);
        $this->withJWT();
        $data     = [
            'title'  => 'Test title',
            'body'   => 'Test body',
            'tokens' => [
                'ExponentPushToken[fdblpmMQ5h83QDWKMbdp3H]'
            ]
        ];
        $response = $this->json('POST', route('push_notifications.send'), $data);
        $response->assertStatus(200);
    }

    public function testSendNotificationToAllController() {
        factory(\Emitters\ExpoNotification\Models\Users_Devices::class)->create(['token' => 'ExponentPushToken[fdblpmMQ5h83QDWKMbdp3H]']);
        $this->withJWT();
        $data     = [
            'title'  => 'Test title',
            'body'   => 'Test body',
        ];
        $response = $this->json('POST', route('push_notifications.send'), $data);
        $response->assertStatus(200);
    }
}