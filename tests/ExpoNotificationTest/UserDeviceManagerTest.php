<?php


use Emitters\ExpoNotification\Models\Users_Devices;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserDeviceManagerTest extends \Tests\TestCase {
    use RefreshDatabase;

    /**
     * @var \Emitters\ExpoNotification\UserDeviceManager
     */
    protected $deviceManager;

    public function setUp(): void {
        parent::setUp();
        $this->deviceManager = resolve('users_devices_manager');
    }

    public function testCreateDevice() {
        $deviceData = factory(Users_Devices::class)->make();
        $device = $this->deviceManager->createDevice($deviceData->toArray());
        $this->assertInstanceOf(Users_Devices::class, $device);
    }

    public function testGetDevices() {
        factory(Users_Devices::class, 3)->create();
        $devices = $this->deviceManager->getDevices();
        $this->assertInstanceOf(\Illuminate\Database\Eloquent\Collection::class, $devices);
        $this->assertInstanceOf(Users_Devices::class, $devices->first());
    }

    public function testGetUserDevices() {
        $user = factory(\App\User::class)->create();
        factory(Users_Devices::class, 2)->create([
            'user_id' => $user->id
        ]);
        $devices = $this->deviceManager->getUserDevices($user->id);
        $this->assertInstanceOf(\Illuminate\Database\Eloquent\Collection::class, $devices);
        $this->assertInstanceOf(Users_Devices::class, $devices->first());
        foreach ($devices as $device) {
            $this->assertTrue($device->user_id == $user->id);
        }
    }

    public function testGetDevicesBy() {
        factory(Users_Devices::class, 2)->create(['token' => 'privet']);
        $devices = $this->deviceManager->getDeviceBy('privet', 'token');
        $this->assertInstanceOf(\Illuminate\Database\Eloquent\Collection::class, $devices);
        $this->assertInstanceOf(Users_Devices::class, $devices->first());
        foreach ($devices as $device) {
            $this->assertTrue($device->token == 'privet');
        }
    }

    public function testUpdateDevice() {
        $device     = factory(Users_Devices::class)->create();
        $deviceData = factory(Users_Devices::class)->make();
        $this->deviceManager->updateDevice($device->id, $deviceData->toArray());
        $device = $device->fresh();
        $attrs = $device->getFillable();
        foreach ($attrs as $att) {
            $this->assertEquals($device->$att, $deviceData->$att);
        }
    }

    public function testDeleteDevices() {
        $device     = factory(Users_Devices::class)->create();
        $this->deviceManager->deleteDevices([$device->id]);
        $device = $device->fresh();
        $this->assertNotNull($device->deleted_at);
    }

    public function testGetAllTokens() {
        $device     = factory(Users_Devices::class)->create();
        $tokens = $this->deviceManager->getAllTokens();
        $this->assertNotEmpty($tokens);
    }
}