<?php


use Emitters\ExpoNotification\Models\Users_Devices;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserDevicesControllerTest extends \Tests\TestCase {
    use RefreshDatabase;


    public function testCreateUserDeviceController() {
        $user = factory(\App\User::class)->create();
        $this->withJWT($user);
        $deviceData = factory(Users_Devices::class)->make(['user_id' => $user->id]);
        $response   = $this->json('POST', route('users_devices.create'), $deviceData->toArray());
        $response->assertStatus(200);
    }

    public function testGetUserDevicesController() {
        $user = factory(\App\User::class)->create();
        $this->withJWT($user);
        factory(Users_Devices::class)->create();
        $response = $this->json('GET', route('users_devices.index'));
        $response->assertStatus(200);
    }

    public function testUpdateDeviceController() {
        $user = factory(\App\User::class)->create();
        $this->withJWT($user);
        $deviceToUpdate = factory(Users_Devices::class)->create();
        $updateData     = factory(Users_Devices::class)->make();
        $response = $this->json('PATCH', route('users_devices.update', ['id' => $deviceToUpdate->id]), $updateData->toArray());
        $response->assertStatus(200);
    }

    public function testDeleteDeviceController () {
        $user = factory(\App\User::class)->create();
        $this->withJWT($user);
        $deviceToDelete = factory(Users_Devices::class)->create();
        $response = $this->json('DELETE', route('users_devices.delete', ['id' => $deviceToDelete->id]));
        $response->assertStatus(200);
    }
}