<?php

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FaqControllerTest extends TestCase {
    use RefreshDatabase;

    /** @var \App\LibrariesAir\Faq\FaqManager */
    protected $faqManager;

    public function setUp(): void {
        parent::setUp();
        $this->faqManager = resolve('librair.faq');
    }

    public function testGetAllFaq() {
        $this->withJWT();
        $category = factory(\Emitters\Faq\Models\Faqs_Category::class)->create();
        $user = factory(\App\User::class)->create();
        $faq = factory(\Emitters\Faq\Models\Faqs::class)->create(['category_id' => $category->id, 'user_id' => $user->id]);
        factory(\Emitters\Faq\Models\Faqs_Content::class)->create(['faq_id' => $faq->id]);
        factory(\Emitters\Faq\Models\Faqs_Content::class)->create(['faq_id' => $faq->id, 'language' => 'ru']);

        $response = $this->json('POST', route('faq.get.all'), [
            'faq_filters'      => [
                'whereIn'   => [
                    [
                        'field' => 'is_draft',
                        'value' => [0],
                    ]
                ],
                'orderBy' => [
                    [
                        'by'        => 'id',
                        'direction' => 'desc'
                    ]
                ]
            ],
            'content_filters'  => [
                'whereIn' => [
                    [
                        'field' => 'language',
                        'value' => ['ru'],
                    ]
                ]
            ]
        ]);
        $response->assertStatus(200);
    }

    public function testGetAllCategories() {
        $category = factory(\Emitters\Faq\Models\Faqs_Category::class, 5)->create();
        $user = factory(\App\User::class)->create();
        $this->withJWT();
        $response = $this->json('GET', route('faq.get.categories'));
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'code',
            'categories' => [
                [
                    'id',
                    'category'
                ]
            ]
        ]);
    }

    public function testCreateFaqCategory() {
        $this->withJWT();
        $categories = factory(\Emitters\Faq\Models\Faqs_Category::class, 3)->make();
        $response = $this->json('POST', route('faq.create.category'), $categories->toArray());
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'code',
            'categories' => [
                [
                    'id',
                    'category' => [
                        [
                            'language',
                            'name'
                        ]
                    ]
                ]
            ]
        ]);
    }

    public function testCreateFaqWithContent() {
        $category = factory(\Emitters\Faq\Models\Faqs_Category::class)->create();
        $user = factory(\App\User::class)->create();
        $this->withJWT($user);
        $faq = factory(\Emitters\Faq\Models\Faqs::class)->make(['category_id' => $category->id]);
        $faqData = $faq->toArray();
        unset($faqData['user_id']);
        $faqContent = factory(\Emitters\Faq\Models\Faqs_Content::class)->make();

        $response = $this->json('POST', route('faq.create.faqwcontent'), ['faq' => $faqData, 'content' => [$faqContent->toArray()]]);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'code',
            'faq' => [
                'data' => [
                    '1' => ['id',
                        'category_id',
                        'sort',
                        'is_draft',
                        'contents' => [
                            [
                                'id',
                                'faq_id',
                                'language',
                                'question',
                                'answer'
                            ]
                        ],
                        'category' => [
                            'id',
                            'category'
                        ]]

                ]
            ]
        ]);
    }

    public function testUpdateFaqWContent() {
        $category = factory(\Emitters\Faq\Models\Faqs_Category::class)->create();
        $user = factory(\App\User::class)->create();
        $this->withJWT($user);
        $faq = factory(\Emitters\Faq\Models\Faqs::class)->create(['category_id' => $category->id, 'user_id' => $user->id]);
        $faqContent = factory(\Emitters\Faq\Models\Faqs_Content::class)->create(['faq_id' => $faq->id]);

        $faqData = factory(\Emitters\Faq\Models\Faqs::class)->make(['id' => $faq->id]);
        unset($faqData['user_id']);
        $faqContentData = factory(\Emitters\Faq\Models\Faqs_Content::class)->make(['id' => $faqContent->id, 'faq_id' => $faq->id]);
        $newFaqContentData = factory(\Emitters\Faq\Models\Faqs_Content::class)->make(['faq_id' => $faq->id, 'language' => 'ru']);
        $response = $this->json('POST', route('faq.update.faq'), [
            'faq'     => [
                $faqData->toArray()
            ],
            'content' => [
                $faqContentData->toArray(),
                $newFaqContentData->toArray()
            ]
        ]);
        $this->assertDatabaseHas($faq->getTable(), $faqData->toArray());
        $this->assertDatabaseHas($faqContent->getTable(), $faqContentData->toArray());
        $this->assertDatabaseHas($faqContent->getTable(), $newFaqContentData->toArray());
        $response->assertStatus(200);
    }

    public function testDeleteCategory() {
        $this->withJWT();
        $category = factory(\Emitters\Faq\Models\Faqs_Category::class)->create();
        $response = $this->json('POST', route('faq.delete.category'), ['category_id' => $category->id]);
        $response->assertStatus(200);
    }

    public function testDeleteFaq() {
        $this->withJWT();
        $faqs = factory(\Emitters\Faq\Models\Faqs::class)->create();
        $delIDs = [1,3,5];
        $response = $this->json('POST', route('faq.delete.faq'), $delIDs);
        $response->assertStatus(200);
        $faqs->whereIn('id', $delIDs)->each(function ($item, $key) use ($delIDs) {
            $item = $item->fresh();
            $this->assertNotNull($item->deleted_at);
        });
    }

    public function testToDraftController() {
        $this->withJWT();
        $faqs = factory(\Emitters\Faq\Models\Faqs::class)->create();
        $toDraftIDs = [1,3,5];
        $response = $this->json('POST', route('faq.update.toDraft'), $toDraftIDs);
        $response->assertStatus(200);
        $faqs->whereIn('id', $toDraftIDs)->each(function ($item, $key) use ($toDraftIDs) {
            $item = $item->fresh();
            $this->assertTrue($item->is_draft == 1);
        });
    }

    public function testPublishDraftsController() {
        $user = factory(\App\User::class)->create();
        $this->withJWT($user);
        $faqs = factory(\Emitters\Faq\Models\Faqs::class)->create();
        $toPublishDraftIds = [1,3,5];
        $response = $this->json('POST', route('faq.update.publish'), $toPublishDraftIds);
        $response->assertStatus(200);
        $faqs->whereIn('id', $toPublishDraftIds)->each(function ($item, $key) use ($toPublishDraftIds) {
            $item = $item->fresh();
            $this->assertTrue($item->is_draft == 0);
        });
    }
}