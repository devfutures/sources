<?php

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FaqManagerTest extends TestCase {
    use RefreshDatabase;

    /** @var \App\LibrariesAir\Faq\FaqManager */
    protected $faqManager;

    public function setUp(): void {
        parent::setUp();
        $this->faqManager = resolve('librair.faq');
    }

    public function testCreateFaqCategory() {
        $faqCategoryData = factory(\Emitters\Faq\Models\Faqs_Category::class, 3)->make();
        $categories      = $this->faqManager->createFaqCategory($faqCategoryData->toArray());
        $this->assertInstanceOf(\Emitters\Faq\Models\Faqs_Category::class, $categories->first());

        $faqCategory        = factory(\Emitters\Faq\Models\Faqs_Category::class)->create();
        $faqCategoryNewData = factory(\Emitters\Faq\Models\Faqs_Category::class)->make(['id' => $faqCategory->id]);
        $categories         = $this->faqManager->createFaqCategory([$faqCategoryNewData->toArray()]);
        $this->assertInstanceOf(\Emitters\Faq\Models\Faqs_Category::class, $categories->first());
    }

    public function testCreateFaqWithContents() {
        /** @var \Emitters\Faq\Models\Faqs $faqData */
        $faqCategory = factory(\Emitters\Faq\Models\Faqs_Category::class)->create();

        $faqData        = factory(\Emitters\Faq\Models\Faqs::class)->make([
            'category_id' => $faqCategory->id,
        ]);
        unset($faqData['user_id']);
        $faqContentData = factory(\Emitters\Faq\Models\Faqs_Content::class, 2)->make(['faq_id' => null]);
        $user = factory(\App\User::class)->create();

        $res = $this->faqManager->createFaqWithContent($user, $faqData->toArray(), $faqContentData->toArray());
        $this->assertDatabaseHas($res->getTable(), $faqData->toArray());
        $this->assertInstanceOf(\Emitters\Faq\Models\Faqs_Content::class, $res->contents->first());
    }

    public function testGetAllFaq() {
        $faqCategory = factory(\Emitters\Faq\Models\Faqs_Category::class)->create();
        $faq         = factory(\Emitters\Faq\Models\Faqs::class)->create([
            'category_id' => $faqCategory->id
        ]);
        $faqContent  = factory(\Emitters\Faq\Models\Faqs_Content::class)->create(['faq_id' => $faq->id]);

        $faq = $this->faqManager->getAllFaq(['faq_filters' => [], 'content_filters' => []]);
        $this->assertInstanceOf(\Illuminate\Pagination\LengthAwarePaginator::class, $faq);
    }

    public function testGetFaq() {
        $faqCategory = factory(\Emitters\Faq\Models\Faqs_Category::class)->create();
        $faq         = factory(\Emitters\Faq\Models\Faqs::class)->create([
            'category_id' => $faqCategory->id
        ]);
        $faqContent  = factory(\Emitters\Faq\Models\Faqs_Content::class)->create(['faq_id' => $faq->id]);

        $faqsWithContent = $this->faqManager->getFaq();
        $this->assertNotNull($faqsWithContent);
        $this->assertTrue($faq->is($faqsWithContent->first()));
        $this->assertTrue($faqContent->is($faqsWithContent->first()->contents()->first()));
    }

    public function testGetFaqByID() {
        $faq        = factory(\Emitters\Faq\Models\Faqs::class)->create();
        $faqContent = factory(\Emitters\Faq\Models\Faqs_Content::class)->create(['faq_id' => $faq->id]);

        $faqByID = $this->faqManager->getFaqByID($faq->id);
        $this->assertEquals($faq->id, $faqByID->first()->id);
    }

    public function testGetFaqByCategory() {
        $faqCategory = factory(\Emitters\Faq\Models\Faqs_Category::class)->create();
        $faq         = factory(\Emitters\Faq\Models\Faqs::class)->create([
            'category_id' => $faqCategory->id
        ]);
        $faqContent  = factory(\Emitters\Faq\Models\Faqs_Content::class)->create(['faq_id' => $faq->id]);

        $faqByCategory = $this->faqManager->getFaqByCategory($faqCategory->id, 'en');
        $this->assertEquals($faqCategory->id, $faqByCategory->first()->category_id);
    }

    public function testUpdateFaq() {
        $faq     = factory(Emitters\Faq\Models\Faqs::class)->create();
        $faqData = factory(\Emitters\Faq\Models\Faqs::class)->make(['id' => $faq->id]);

        $updatedFaq = $this->faqManager->updateFaq($faqData->toArray());

        $this->assertTrue($updatedFaq == 1);
        $this->assertDatabaseHas($faq->getTable(), $faqData->toArray());
    }

    public function testUpdateFaqCategory() {
        $faqCategory     = factory(\Emitters\Faq\Models\Faqs_Category::class)->create();
        $faqCategoryData = factory(\Emitters\Faq\Models\Faqs_Category::class)->make();

        $updatedCategoryName = $this->faqManager->updateCategory($faqCategory, $faqCategoryData->toArray());
        $faqCategory = $faqCategory->fresh();
        $attrs = $faqCategory->getFillable();
        foreach ($attrs as $att) {
            $this->assertEquals($faqCategoryData->$att, $faqCategory->$att);
        }
    }

    public function testUpdateFaqContent() {
        $faqContent     = factory(\Emitters\Faq\Models\Faqs_Content::class)->create();
        $faqContentData = factory(\Emitters\Faq\Models\Faqs_Content::class)->make(['id' => $faqContent->id]);

        $this->faqManager->updateContent($faqContentData->toArray());

        $this->assertDatabaseHas($faqContent->getTable(), $faqContentData->toArray());
    }

    public function testMassDelete() {
        $faq     = factory(Emitters\Faq\Models\Faqs::class, 5)->create();
        $delData = [1, 3, 5];
        $this->faqManager->deleteFaq($delData);
        $faq->whereIn('id', $delData)->each(function ($item, $key) use ($delData) {
            $item = $item->fresh();
            $this->assertNotNull($item->deleted_at);
        });
    }

    public function testMassDraftUpdate() {
        $faq      = factory(Emitters\Faq\Models\Faqs::class, 5)->create();
        $draftIDs = [1, 3, 5];
        $this->faqManager->toDraft($draftIDs);
        $faq->whereIn('id', $draftIDs)->each(function ($item, $key) use ($draftIDs) {
            $item = $item->fresh();
            $this->assertTrue($item->is_draft == 1);
        });
    }

    public function testMassDraftPublish() {
        $faq      = factory(Emitters\Faq\Models\Faqs::class, 5)->create();
        $draftIDs = [1, 3, 5];
        $this->faqManager->publishDrafts($draftIDs);
        $faq->whereIn('id', $draftIDs)->each(function ($item, $key) use ($draftIDs) {
            $item = $item->fresh();
            $this->assertTrue($item->is_draft == 0);
        });
    }

    public function testCountFaqs(){
        $faq      = factory(Emitters\Faq\Models\Faqs::class, 5)->create();
        $res = $this->faqManager->countFaq();
        $this->assertEquals($res['total'], $faq->count());
        $this->assertEquals($res['drafts'], $faq->where('is_draft', 1)->count());
        $this->assertEquals($res['published'], $faq->where('is_draft', 0)->count());
    }
}