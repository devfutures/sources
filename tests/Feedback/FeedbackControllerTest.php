<?php


use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FeedbackControllerTest extends TestCase {
    use RefreshDatabase;

    /** @var \Emitters\Feedback\FeedbackManager */
    protected $lib;

    public function setUp(): void {
        parent::setUp();
        $this->lib = resolve('librair.feedback');
    }

    public function testFeedbackControllerIndex() {
        $this->withJWT();
        factory(\Emitters\Feedback\Models\Feedback::class, 25)->create();
        $response = $this->json('POST', route('feedback.index'));
        $response->assertJsonStructure([
            'status',
            'code',
            'feedback' => [
                'current_page',
                'data' => [
                    [
                        'id',
                        'email',
                        'name',
                        'message'
                    ]
                ]
            ]
        ]);
    }

    public function testFeedbackControllerDelete() {
        $this->withJWT();
        $deletable = factory(\Emitters\Feedback\Models\Feedback::class)->create();
        $response  = $this->json('DELETE', route('feedback.delete'), ['ids' => [$deletable->id]]);
        $deletable = $deletable->fresh();
        $this->assertNotNull($deletable->deleted_at);
    }
}