<?php


use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FeedbackManagerTest extends TestCase {
    use RefreshDatabase;
    /** @var \Emitters\Feedback\FeedbackManager */
    protected $lib;

    public function setUp(): void {
        parent::setUp();
        $this->lib = resolve('librair.feedback');
    }

    public function testAddFeedback() {
        $feedbackData = factory(\Emitters\Feedback\Models\Feedback::class)->make();
        $result       = $this->lib->add($feedbackData->toArray());
        $this->assertInstanceOf(\Emitters\Feedback\Models\Feedback::class, $result);
        $this->assertDatabaseHas($result->getTable(), $feedbackData->toArray());
    }

    public function testFeedbackGetOneBy() {
        $feedbackData = factory(\Emitters\Feedback\Models\Feedback::class)->create();
        $result       = $this->lib->getOneBy($feedbackData->name, 'name');
        $this->assertSame($result->name, $feedbackData->name);
        $this->assertInstanceOf(\Emitters\Feedback\Models\Feedback::class, $result);
        $this->assertDatabaseHas($result->getTable(), $feedbackData->toArray());
    }

    public function testFeedbackGetAll() {
        factory(\Emitters\Feedback\Models\Feedback::class, 25)->create();
        $result = $this->lib->getAll([], false);
        $this->assertInstanceOf(\Illuminate\Support\Collection::class, $result);
    }

    public function testFeedbackGetAllPaginated() {
        factory(\Emitters\Feedback\Models\Feedback::class, 25)->create();
        $result = $this->lib->getAll();
        $this->assertInstanceOf(\Illuminate\Pagination\LengthAwarePaginator::class, $result);
    }

    public function testFeedbackGetAllWithFilters() {
        factory(\Emitters\Feedback\Models\Feedback::class, 25)->create();
        $findable = factory(\Emitters\Feedback\Models\Feedback::class)->create([
            'name' => 'test'
        ]);
        $result = $this->lib->getAll([
            'where' => [
                [
                    'field' => 'name',
                    'value' => 'test'
                ]
            ]
        ], false);
        $this->assertNotEmpty($result);
        $result->each(function($item) use ($findable) {
            $this->assertEquals($findable->name, $item->name);
        });
    }

    public function testFeedbackDelete() {
        $deletable = factory(\Emitters\Feedback\Models\Feedback::class)->create();
        $result = $this->lib->delete([$deletable->id]);
        $this->assertEquals(1, $result);
        $deletable = $deletable->fresh();
        $this->assertNotNull($deletable->deleted_at);
    }
}