<?php


use Emitters\News\Models\News;
use Emitters\News\Models\News_Contents;
use Illuminate\Support\Carbon;
use Tests\TestCase;

class NewsTest extends TestCase {
    use \Illuminate\Foundation\Testing\RefreshDatabase;

    /** @var \App\LibrariesAir\News\NewsManager */
    protected $newsManager;

    public function setUp(): void {
        parent::setUp();
        $this->newsManager = resolve('libair.news');
    }

    public function testStoreImage() {
        $news = factory(News::class)->create();
        $file = \Illuminate\Http\UploadedFile::fake()->image('test.jpg');
        $img = $this->newsManager->storeNewsImage($news, $file);
        Storage::assertExists("public/news/" . $img->basename);
    }

    public function testGetAllNews() {
        factory(News_Contents::class, 5)->create();

        $newsCollection = $this->newsManager->getAll();
        $this->assertNotEmpty($newsCollection);
    }

    public function testGetForPublish() {
        factory(News_Contents::class, 3)->create();

        $newsCollection = $this->newsManager->getNewsForPublish();
        $this->assertNotEmpty($newsCollection);
        $this->assertTrue(\Carbon\Carbon::now() > $newsCollection->first()->start_show_date);
    }

    public function testUpdateNews() {
        $news = factory(News::class)->create();

        $newsContent = factory(News_Contents::class)->create([
            'news_id' => $news->id
        ]);

        $newsData        = factory(News::class)->make([
            'publish_date' => Carbon::now()->addWeek()->format('d.m.Y')
        ]);
        $newsContentData = factory(News_Contents::class)->make();

        $this->newsManager->updateNews($news->id, $newsData->toArray());
        $this->newsManager->updateContent($newsContent->id, $newsContentData->toArray());
        $news = $news->fresh();
        $attr = $news->getFillable();

        foreach ($attr as $att) {
            if ($att == 'image' || $att == 'publish_date') continue;
            $this->assertEquals($newsData->$att, $news->$att);
        }
        $this->assertDatabaseHas($newsContent->getTable(), $newsContentData->toArray());
    }

    public function testControllerGetAll() {
        $user = factory(App\User::class)->create();
        $this->withJWT($user);
        $data = \Carbon\Carbon::now()->addDay(2);
        $news = factory(News::class, 11)->create([
            'publish_date' => $data
        ]);

        foreach ($news as $one_news) {
            factory(News_Contents::class)->create([
                'news_id' => $one_news->id
            ]);
        }

        $response = $this->json('POST', route('news.all', [
            'date_between' => [
                [
                    'from_date_column' => 'publish_date',
                    'to_date_column'   => 'publish_date',
                    'from'             => $data->format('d.m.Y'),
                    'to'               => $data->format('d.m.Y')
                ]
            ],
            'orderBy'      => [
                [
                    'by'        => 'publish_date',
                    'direction' => 'desc'
                ]
            ]
        ]));
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'current_page',
            'data' => [
                [
                    'id',
                    'image',
                    'publish_date',
                    'content' => [
                        [
                            'id',
                            'news_id',
                            'language',
                            'title',
                            'body',
                            'description'
                        ]
                    ]
                ]
            ],
        ]);
    }

    public function testControllerCreateNews() {
        $user = factory(\App\User::class)->create();
        $this->withJWT($user);
        $news = [
            'publish_date' => Carbon::now()->format('d.m.Y'),
            'content'      => [
                [
                    'language' => 'en',
                    'title'    => 'Some great title',
                    'body'     => 'Some great body'
                ]
            ]
        ];

        $response = $this->actingAs($user)->json('POST', route('news.new'), $news);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'code',
            'news' => [
                'id',
                'user_id',
                'publish_date',
                'start_show_date',
                'end_show_date',
                'content' => [
                    [
                        'id',
                        'news_id',
                        'language',
                        'title',
                        'body',
                    ]
                ],
                'author'  => [
                    'id',
                    'name',
                ]
            ],
        ]);
    }

    public function testControllerUpdate() {
        $user = factory(\App\User::class)->create();
        $this->withJWT($user);
        $news = factory(News::class)->create();

        $newsContent = factory(News_Contents::class)->create([
            'news_id' => $news->id
        ]);

        $newsData        = factory(News::class)->make([
            'id'           => $news->id,
            'publish_date' => Carbon::now()->format('d.m.Y')
        ]);
        $newsContentData = factory(News_Contents::class)->make(['id' => $newsContent->id]);

        $updateData = [
            'news'    => $newsData->toArray(),
            'content' => [$newsContentData->toArray()]
        ];

        $response = $this->actingAs($user)->json('POST', route('news.update'), $updateData);
        $response->assertStatus(200);
        $this->assertDatabaseHas($newsContent->getTable(), $newsContentData->toArray());
    }
}