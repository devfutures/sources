<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PaymentSystemControllerTest extends TestCase {
    use RefreshDatabase;

    public function testGetModules() {
        $this->withJWT();
        $response = $this->json('GET', route('payment_system.modules'));
        $response->assertStatus(200);
        $response->assertSee('modules');
    }

    public function testCreatePaymentSystem() {
        $this->withJWT();
        $psData   = factory(\Emitters\PaymentSystems\Models\Payment_System::class)->make();
        $data = $psData->toArray();

        $response = $this->postJson(route('payment_system.create'), $data);
        $response->assertJsonStructure([
            'status',
            'code',
            'system' => [
                'title',
                'currency',
                'scale',
                'class_name',
                'status',
                'is_fiat',
                'redirect_another_site',
                'sort',
                'purchase',
                'min_sum_purchase',
                'max_sum_purchase',
                'withdraw',
                'min_sum_withdraw',
                'max_sum_withdraw',
                'example_wallet'
            ]
        ]);
    }

    public function testUpdatePaymentSystem() {
        $this->withJWT();
        $ps       = factory(\Emitters\PaymentSystems\Models\Payment_System::class)->create();
        $psData   = factory(\Emitters\PaymentSystems\Models\Payment_System::class)->make();
        $response = $this->json('PATCH', route('payment_system.update', ['id' => $ps->id]), $psData->toArray());
        $response->assertStatus(200);

        $ps   = $ps->fresh();
        $attr = $ps->getAttributes();
        foreach ($attr as $att) {
            $this->assertEquals($ps->$att, $psData->$att);
        }
    }

    public function testDeletePaymentSystem() {
        $this->withJWT();
        $ps       = factory(\Emitters\PaymentSystems\Models\Payment_System::class)->create();
        $response = $this->json('DELETE', route('payment_system.delete', ['id' => $ps->id]));
        $response->assertStatus(200);
        $ps       = $ps->fresh();
        $this->assertTrue($ps->trashed());
    }

    public function testGetPaymentSystems() {
        $this->withJWT();
        $ps       = factory(\Emitters\PaymentSystems\Models\Payment_System::class, 20)->create();
        $filters = [
            'where' => [
                [
                    'field' => 'status',
                    'value' => 0
                ],
                [
                    'field' => 'withdraw',
                    'value' => 0
                ]
            ],
            'orderBy' => [
                [
                    'by' => 'id',
                    'direction' => 'asc'
                ]
            ]
        ];
        $response = $this->json('POST', route('payment_system.index'), $filters);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'code',
            'systems' => [
                'current_page',
                'data' => [
                    [
                        'id',
                        'title',
                        'class_name',
                        'currency',
                        'status',
                        'is_fiat',
                        'redirect_another_site',
                        'sort',
                        'purchase',
                        'min_sum_purchase',
                        'max_sum_purchase',
                        'withdraw',
                        'min_sum_withdraw',
                        'max_sum_withdraw',
                        'scale',
                        'example_wallet',
                        'public_title',
                        'name_input'
                    ]
                ]
            ]
        ]);
    }

    public function testUpdateSortController() {
        $this->withJWT();
        $ps       = factory(\Emitters\PaymentSystems\Models\Payment_System::class, 3)->create();
        $sortData = [
            [
                'id' => 1,
                'sort' => 2,
            ],
            [
                'id' => 2,
                'sort' => 3,
            ],
            [
                'id' => 3,
                'sort' => 1,
            ]
        ];
        $response = $this->json('POST', route('payment_system.sort'), $sortData);
        $response->assertStatus(200);
        $ps->each(function ($item, $value) use ($sortData) {
            $item = $item->fresh();
            foreach ($sortData as $sort) {
                if ($sort['id'] == $item->id) $this->assertTrue($item->sort == $sort['sort']);
            }
        });
    }
}