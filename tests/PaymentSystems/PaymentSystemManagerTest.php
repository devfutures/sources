<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PaymentSystemManagerTest extends TestCase {
    use RefreshDatabase;
    protected $paymentSystemManger;
    protected $balanceManager;

    public function setUp(): void {
        parent::setUp();
        $this->paymentSystemManger = paymentsystems();
        $this->balanceManager = resolve('librariesair.balance');
    }

    public function testGetPaymentSystems() {
        $ps = factory('Emitters\PaymentSystems\Models\Payment_System', 3)->create();

        $user = factory('App\User')->create();
        $data = [
            'operation'         => 'ADD_FUNDS',
            'user_id'           => $user->id,
            'payment_system_id' => $ps[0]->id,
            'type'              => 'buy',
            'amount'            => 20000
        ];

        /** @var Users_Balance $inserted */
        $inserted = $this->balanceManager->changeBalance($data);

        $res = $this->paymentSystemManger->getAll($user);

        $first = $res->where('id', $ps[0]->id)->first()->toArray();
        $this->assertArrayHasKey('total_add_funds', $first);
        $this->assertSame(df_div($data['amount'], 100000000), $first['total_add_funds']);
        $this->assertArrayHasKey('total_withdraw', $first);
        $this->assertArrayHasKey('balance', $first);
        $this->assertArrayHasKey('wallet', $first);
        $this->assertArrayHasKey('public_title', $first);
        $this->assertArrayHasKey('name_input', $first);
    }
}