<?php

use Illuminate\Foundation\Testing\RefreshDatabase;

class ReviewsControllerTest extends \Tests\TestCase {
    use RefreshDatabase;

    public function testCreateReview() {
        $user = factory(\App\User::class)->create();
        $review = factory(\Emitters\Reviews\Models\Reviews::class)->make(['user_id' => $user->id]);
        $this->withJWT($user);
        $response = $this->json('POST', route('reviews.create'), [$review->toArray()]);
        $response->assertStatus(200);
        $this->assertDatabaseHas($review->getTable(), $review->toArray());
    }

    public function testCreateReviewForClient(){
        list($user, $token) = $this->withJWT();
        $response = $this->json('POST', route('client.create_review'), [
            'review_text' => str_random(50)
        ]);
        $response->assertStatus(200);
    }

    public function testGetReviews() {
        $reviews = factory(\Emitters\Reviews\Models\Reviews::class, 20)->create();
        $this->withJWT();
        $statusList = ['ON_CHECK', 'APPROVED'];
        $filters = [
            'whereIn' => [
                [
                    'field' => 'status',
                    'value' => $statusList
                ]
            ]
        ];
        $response = $this->json('POST', route('reviews.getAll'), $filters);
        $response->assertStatus(200);
        foreach (json_decode($response->getContent())->reviews->data as $review) {
            $this->assertContains($review->status, $statusList);
        }
    }

    public function testUpdateReviews() {
        $user = factory(\App\User::class)->create();
        $reviews = factory(\Emitters\Reviews\Models\Reviews::class, 2)->create(['user_id' => $user->id]);
        $rev1data = factory(\Emitters\Reviews\Models\Reviews::class)->make(['id' => 1, 'user_id' => $user->id]);
        $rev2data = factory(\Emitters\Reviews\Models\Reviews::class)->make(['id' => 2, 'user_id' => $user->id]);
        $this->withJWT($user);
        $response = $this->json('PATCH', route('reviews.update'), [$rev1data->toArray(), $rev2data->toArray()]);
        $this->assertDatabaseHas($rev1data->getTable(), $rev2data->toArray());
        $this->assertDatabaseHas($rev1data->getTable(), $rev2data->toArray());
    }

    public function testDeleteReviews() {
        $user = factory(\App\User::class)->create();
        $reviews = factory(\Emitters\Reviews\Models\Reviews::class, 3)->create(['user_id' => $user->id]);
        $this->withJWT($user);
        $response = $this->json('DELETE', route('reviews.delete'), $reviews->toArray());
        $reviews->each(function($item, $key) {
            $item = $item->fresh();
            $this->assertTrue($item->trashed());
        });
    }
}