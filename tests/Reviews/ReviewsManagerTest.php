<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReviewsManagerTest extends TestCase {
    use RefreshDatabase;
    /** @var \Emitters\Reviews\ReviewsManager */
    protected $reviewsManager;

    public function setUp(): void {
        parent::setUp();
        $this->reviewsManager = resolve('libair.reviews');
    }

    public function testAddReview() {
        $user   = factory(\App\User::class)->create();
        $review = factory(\Emitters\Reviews\Models\Reviews::class)->make(['user_id' => $user->id]);
        $this->reviewsManager->createReview($user, $review->toArray());
        $this->assertDatabaseHas($review->getTable(), $review->toArray());
    }

    public function testGetReviews() {
        $reviews           = factory(\Emitters\Reviews\Models\Reviews::class, 5)->create();
        $reviewsCollection = $this->reviewsManager->getReviews();
        $this->assertInstanceOf(\Emitters\Reviews\Models\Reviews::class, $reviewsCollection->first());
        $reviews->each(function ($item, $key) use ($reviewsCollection) {
            $this->assertTrue($reviewsCollection->contains($item));
        });
    }

    public function testUpdateReview() {
        $review     = factory(\Emitters\Reviews\Models\Reviews::class)->create();
        $reviewData = factory(\Emitters\Reviews\Models\Reviews::class)->make();
        $this->reviewsManager->updateReview($review->id, $reviewData->toArray());
        $review = $review->fresh();
        $attrs = $review->getFillable();
        foreach ($attrs as $att) {
            $this->assertEquals($review->$att, $reviewData->$att);
        }
    }

    public function testDeleteReview() {
        $review     = factory(\Emitters\Reviews\Models\Reviews::class)->create();
        $this->reviewsManager->deleteReview($review->id);
        $review = $review->fresh();
        $this->assertTrue($review->trashed());
    }
}