<?php


use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StatisticsControllerTest extends TestCase {
    use RefreshDatabase;

    public function testStatisticsIndexController() {
        $this->withJWT();
        $response = $this->json('GET', route('statistics.index'));
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'statistic'  => [
                'expenses'  => [

                ],
                'add_funds' => [

                ],
                'withdraw'  => [

                ]
            ],
            'accounting' => [
                '*' => [
                    'months' => [
                        "January"   => [
                            "INCOME"  => [],
                            "EXPENSE" => [],
                            "total_month_incomes",
                            "total_month_expenses"
                        ],
                        "February"  => [
                            "INCOME"  => [],
                            "EXPENSE" => [],
                            "total_month_incomes",
                            "total_month_expenses"
                        ],
                        "March"     => [
                            "INCOME"  => [],
                            "EXPENSE" => [],
                            "total_month_incomes",
                            "total_month_expenses"
                        ],
                        "April"     => [
                            "INCOME"  => [],
                            "EXPENSE" => [],
                            "total_month_incomes",
                            "total_month_expenses"
                        ],
                        "May"       => [
                            "INCOME"  => [],
                            "EXPENSE" => [],
                            "total_month_incomes",
                            "total_month_expenses"
                        ],
                        "June"      => [
                            "INCOME"  => [],
                            "EXPENSE" => [],
                            "total_month_incomes",
                            "total_month_expenses"
                        ],
                        "July"      => [
                            "INCOME"  => [],
                            "EXPENSE" => [],
                            "total_month_incomes",
                            "total_month_expenses"
                        ],
                        "August"    => [
                            "INCOME"  => [],
                            "EXPENSE" => [],
                            "total_month_incomes",
                            "total_month_expenses"
                        ],
                        "September" => [
                            "INCOME"  => [],
                            "EXPENSE" => [],
                            "total_month_incomes",
                            "total_month_expenses"
                        ],
                        "October"   => [
                            "INCOME"  => [],
                            "EXPENSE" => [],
                            "total_month_incomes",
                            "total_month_expenses"
                        ],
                        "November"  => [
                            "INCOME"  => [],
                            "EXPENSE" => [],
                            "total_month_incomes",
                            "total_month_expenses"
                        ],
                        "December"  => [
                            "INCOME"  => [],
                            "EXPENSE" => [],
                            "total_month_incomes",
                            "total_month_expenses"
                        ],
                    ],
                    "total_incomes",
                    "average_incomes",
                    "total_expenses",
                    "average_expenses",
                ]
            ]
        ]);
    }

    public function testGetGraphData() {
        $user = factory(\App\User::class)->create();
        $this->withJWT($user);
        $this->seed(\Emitters\PaymentSystems\seed\PaymentSystemSeeder::class);
        $this->artisan('convertingcurrency:parse');
        $now = \Carbon\Carbon::now();
        $days = $now->format('d');
        $subAmount =  $days == 1 ? 0 : $days;
        factory(\Emitters\UsersOperations\Models\Users_Operations::class, 50)->create([
            'created_at' => $now->subDays(rand(0, $subAmount)),
            'hidden' => 0
        ]);
        $response = $this->json('GET', route('statistics.graph_data'));
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status',
            'code',
            'graph_data' => [
                '*' => [
                    'WITHDRAW',
                    'ADD_FUNDS'
                ]
            ]
        ]);
    }

    public function testStatisticsCreateController() {
        $accounting = factory(\Emitters\Statistics\Models\Accounting::class)->make();
        $this->withJWT();

        $data                = $accounting->toArray();
        $data['period_from'] = \Carbon\Carbon::now()->subDay(1)->format('d.m.Y');
        $data['period_to']   = \Carbon\Carbon::now()->format('d.m.Y');

        $response = $this->json('POST', route('statistics.add'), $data);
        $response->assertStatus(200);
    }

    public function testStatisticsUpdateController() {
        $accounting = factory(\Emitters\Statistics\Models\Accounting::class)->create();
        $data       = factory(\Emitters\Statistics\Models\Accounting::class)->make();
        $this->withJWT();

        $data                = $accounting->toArray();
        $data['id']          = $accounting->id;
        $data['period_from'] = \Carbon\Carbon::now()->subDay(1)->format('d.m.Y');
        $data['period_to']   = \Carbon\Carbon::now()->format('d.m.Y');

        $response = $this->json('PATCH', route('statistics.update'), $data);
        $response->assertStatus(200);
    }

    public function testStatisticsDeleteController() {
        $accounting = factory(\Emitters\Statistics\Models\Accounting::class)->create();
        $this->withJWT();
        $response = $this->json('DELETE', route('statistics.delete', ['id' => $accounting->id]));
        $response->assertStatus(200);
    }
}