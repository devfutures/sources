<?php


use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StatisticsManagerTest extends TestCase {
    use RefreshDatabase;
    /** @var \Emitters\Statistics\StatisticsManager */
    protected $statisticsManager;

    public function setUp(): void {
        parent::setUp();
        $this->statisticsManager = resolve('libair.statistics');
    }

    public function testGetGraphData() {
        factory(App\User::class)->create();
        $addFundsOperations = factory(\Emitters\UsersOperations\Models\Users_Operations::class)->create([
            'operation'  => 'ADD_FUNDS',
            'hidden'     => 0,
            'status'     => 'completed',
            'created_at' => \Carbon\Carbon::now()
        ]);
        /** @var \Illuminate\Support\Collection $graphData */
        $graphData = $this->statisticsManager->getGraphData(\Carbon\Carbon::now(), \Carbon\Carbon::now());
        $fromDb    = number(convert_from_bigInteger(DB::table('users__operations')->first()->default_amount), 2);
        $this->assertEquals($graphData->first()['ADD_FUNDS'], $fromDb);
    }

    public function testAddRow() {
        $data                = factory(\Emitters\Statistics\Models\Accounting::class)->make()->toArray();
        $data['period_from'] = \Carbon\Carbon::now()->format('d.m.Y');
        $data['period_to']   = \Carbon\Carbon::now()->format('d.m.Y');
        list($acc, $message) = $this->statisticsManager->addRow($data);
        $this->assertInstanceOf(\Emitters\Statistics\Models\Accounting::class, $acc);
    }

    public function testUpdateRow() {
        $row                 = factory(\Emitters\Statistics\Models\Accounting::class)->create();
        $data                = factory(\Emitters\Statistics\Models\Accounting::class)->make(['id' => $row->id])->toArray();
        $data['period_from'] = \Carbon\Carbon::now()->format('d.m.Y');
        $data['period_to']   = \Carbon\Carbon::now()->format('d.m.Y');
        $this->statisticsManager->updateRow($data);
        $row  = $row->fresh();
        $attr = $row->getFillable();
        foreach($attr as $att) {
            if($att == 'period_from' || $att == 'period_to') {
                $this->assertEquals($row->$att, \Carbon\Carbon::createFromFormat('d.m.Y', $data[$att]));
            } else {
                $this->assertEquals($data[$att], $row->$att);
            }
        }
    }

    public function testDeleteRow() {
        $row = factory(\Emitters\Statistics\Models\Accounting::class)->create();
        $this->statisticsManager->deleteRow($row->id);
        $row = $row->fresh();
        $this->assertNotNull($row->deleted_at);
    }
}