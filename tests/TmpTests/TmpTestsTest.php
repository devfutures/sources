<?php

use Emitters\UsersData\Models\Users_Wallet;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Tests\TestCase;

class TmpTestsTest extends TestCase {
    use RefreshDatabase;
    /** @var \App\LibrariesAir\UsersData\UsersDataManager */
    protected $usersDataManager;

    public function setUp(): void {
        parent::setUp();
        $this->usersDataManager = resolve('libair.usersdata');
    }

    public function testTransitionPaymentSystem() {
        $dep = resolve('libair.depositusers');
        $ps  = factory(Emitters\PaymentSystems\Models\Payment_System::class)->create([
            'class_name' => 'Selfreliance\BitGo\BitGo',
            'title'      => 'Dashcoin'
        ]);
        $res = $dep->transitionToPaymentSystem(0, $ps, 100);
        $this->assertSame('success', $res['status']);
    }
}