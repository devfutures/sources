<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminUsersDataControllerTest extends TestCase {
    use RefreshDatabase;
    const USER_ORIGINAL_PASSWORD = 'secret';

    /** @var \App\LibrariesAir\UsersData\UsersDataManager */
    protected $usersDataManager;

    public function setUp(): void {
        parent::setUp();
        $this->usersDataManager = resolve('libair.usersdata');
    }

    public function testGetListUsers() {
        $this->withJWT();
        $response = $this->get(route('users.index'));
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'users' =>[
                'current_page',
                'data' => [
                    [
                        'id',
                        'name',
                        'email',
                        'upline',
                        'country',
                        'city',
                        'user_note',
                        'total_purchase',
                        'total_withdraw',
                    ]
                ]
            ],
            'new_users_count'
        ]);
    }

    public function testViewProfileUser() {
        $user = factory('App\User')->create();
        $this->withJWT($user);
        $user_open_profile = factory('App\User')->create([
            'upline_id' => $user->id
        ]);
        factory('Emitters\UsersData\Models\Users_Contacts')->create([
            'user_id' => $user_open_profile->id
        ]);
        factory('Emitters\UsersData\Models\Users_Wallet')->create([
            'user_id' => $user_open_profile->id
        ]);
        $logs = factory('Emitters\UsersData\Models\Users_Activity')->create([
            'user_id' => $user_open_profile->id
        ]);
        $response = $this->get(route('users.edit', ['id' => $user_open_profile->id]));

        $response->assertJsonStructure([
            'info',
            'wallets',
            'activity',
            'last_activity'
        ]);

    }

    public function testBlockedUser() {
        $user = factory('App\User')->create();
        $this->withJWT($user);
        $data = [
            'block_message' => 'Suspicion of hacking',
            'blocked_up' => Carbon::now()->addDays(2)->toDateTimeString()
        ];
        $response = $this->post(route('user.blocked', ['id' => $user->id]), $data);
        $response->assertStatus(200);
        $user = $user->fresh();
        $this->assertSame($data, [
            'block_message' => $user->block_message,
            'blocked_up' => $user->blocked_up
        ]);
    }

    public function testChangePasswordUser() {
        $user = factory('App\User')->create([
            'password' => bcrypt(self::USER_ORIGINAL_PASSWORD)
        ]);
        $this->withJWT($user);
        $response = $this->post(route('user.change_password', ['id' => $user->id]), [
            'password' => '123456',
            'password_confirm' => '123456'
        ]);
        $response->assertStatus(200);

        $user->refresh();
        $this->assertFalse(Hash::check(self::USER_ORIGINAL_PASSWORD,
            $user->password));

        $this->assertTrue(Hash::check('123456',
            $user->password));
    }

    public function testChangeContacts() {
        $user = factory('App\User')->create();
        $this->withJWT($user);
        factory('Emitters\UsersData\Models\Users_Contacts')->create([
            'user_id' => $user->id
        ]);
        $user_upd = factory('App\User')->make();
        $data = [
            'name' => $user_upd->name,
            'email' => $user_upd->email,
            'contacts' => [
                'phone_number' => "",
                'facebook' => '@em',
                'linkedin' => null,
                'twitter' => null,
                'telegram' => null,
                'website' => null,
            ],
            'city' => 'Kiev',
            'user_note' => 'Bla Bla Bla'
        ];
        $response = $this->post(route('user.update', ['id' => $user->id]), $data);
        $response->assertStatus(200);

        $user->fresh()->load('contacts');
        $this->assertNotSame($data['email'], $user->email);
        $this->assertSame($data['contacts']['facebook'], $user->contacts->facebook);
    }

    public function testSaveWallets() {
        $user = factory('App\User')->create();
        $this->withJWT($user);
        factory('Emitters\UsersData\Models\Users_Wallet')->create([
            'user_id' => $user->id
        ]);
        $data = [
            ['id' => 1, 'address' => 'Bitcoin address'],
            ['id' => 2, 'address' => 'Ethereum address'],
        ];
        $response = $this->post(route('user.save_wallets', ['id' => $user->id]), $data);
        $response->assertStatus(200);
    }

    public function testSearchUsers() {
        $users = factory('App\User', 10)->create();
        $this->withJWT($users[0]);
        $response = $this->get(
            route('users.index',
            ['search_user_keyword' => $users[0]->email]
        ));
        $response->assertStatus(200);
        $data = json_decode($response->getContent());
        $this->assertCount(1, $data->users->data);
    }
}