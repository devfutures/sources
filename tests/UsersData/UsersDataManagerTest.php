<?php

use Emitters\UsersData\Models\Users_Wallet;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Tests\TestCase;

class UsersDataManagerTest extends TestCase {
    use RefreshDatabase;
    /** @var \App\LibrariesAir\UsersData\UsersDataManager */
    protected $usersDataManager;

    public function setUp(): void {
        parent::setUp();
        $this->usersDataManager = resolve('libair.usersdata');
    }

    public function testGetWallets() {
        $user    = factory(\App\User::class)->create();
        $wallets = factory(Users_Wallet::class)->create([
            'user_id' => $user->id
        ]);

        $funcRes = $this->usersDataManager->getWallets($user);
        $funcRes->each(function ($wallet, $key) {
            $this->assertInstanceOf(Users_Wallet::class, $wallet);
        });
    }

    public function testAddWalletUser() {
        $user   = factory(\App\User::class)->create();
        $wallet = $this->usersDataManager->addWalletUser($user, 1, Str::random(16));
        $this->assertDatabaseHas('users__wallets', $wallet->toArray());
    }

    public function testChangeUserWallet() {
        $user = factory(\App\User::class)->create();

        $wallet = factory(Users_Wallet::class)->create([
            'user_id' => $user->id
        ]);

        $changeData = factory(Users_Wallet::class)->make([
            'user_id'           => $user->id,
            'payment_system_id' => $wallet->payment_system_id
        ]);

        $this->usersDataManager->changeWalletUser($user, $wallet->payment_system_id, $changeData->wallet, $wallet->id);

        $this->assertDatabaseHas('users__wallets', $changeData->toArray());
    }

    public function testDestroyUserWallet() {
        $user   = factory(\App\User::class)->create();
        $wallet = factory(Users_Wallet::class)->create([
            'user_id' => $user->id
        ]);

        $this->usersDataManager->destroyWalletUser($user, $wallet->id);
        $this->assertSoftDeleted('users__wallets', $wallet->toArray());
    }

    public function testRestoreWalletUser() {
        $user   = factory(\App\User::class)->create();
        $wallet = factory(Users_Wallet::class)->create([
            'user_id' => $user->id
        ]);

        $this->usersDataManager->restoreWalletUser($wallet->id);
        $this->assertDatabaseHas('users__wallets', $wallet->toArray());
    }

    public function testGetUsers() {
        $newUser = factory(\App\User::class)->create();
        $oldUser = factory(\App\User::class)->create(['created_at' => Carbon\Carbon::now()->subDay(3)]);
        $users   = $this->usersDataManager->getUsers();
        $this->assertTrue($users->contains($newUser));
        $this->assertTrue($users->where('id', $newUser->id)->first()->new_user);
        $this->assertFalse($users->where('id', $oldUser->id)->first()->new_user);
    }

    public function testSearchAffRefOnRegister(){
        $newUser = factory(\App\User::class)->create();
        $this->assertNull($this->usersDataManager->searchUserBy(''));
        $this->assertNull($this->usersDataManager->searchUserBy(str_random(4)));
        $this->assertNotNull($this->usersDataManager->searchUserBy($newUser->login));
        $this->assertNotNull($this->usersDataManager->searchUserBy($newUser->aff_ref));
    }
}