<?php

use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ControlOperationsTest extends TestCase {
    use RefreshDatabase;

    public function setUp(): void {
        parent::setUp();
    }

    public function testOpenPageOperations() {
        $this->withJWT();
        factory(Emitters\UsersOperations\Models\Users_Operations::class, 10)->create();
        $response = $this->json('GET', route('operations.index'), [

        ]);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'operations'
        ]);
    }

    public function testWithFilterOpenPageOperations() {
        $this->withJWT();
        $operations = factory(Emitters\UsersOperations\Models\Users_Operations::class)->create();
        $operations->load('user');
        $filters  = [
            'where'         => [
                [
                    'field' => 'id',
                    'value' => $operations->id
                ],
                [
                    'field' => 'payment_system_id',
                    'value' => $operations->payment_system_id
                ],
                [
                    'field' => 'operation',
                    'value' => $operations->operation
                ],
                [
                    'field' => 'status',
                    'value' => $operations->status
                ]
            ],
            'user'          => $operations->user->email,
            'where_between' => [
                [
                    'field' => 'created_at',
                    'from'  => Carbon::now()->subDay()->format('Y-m-d'),
                    'to'    => Carbon::now()->addDay()->format('Y-m-d'),
                ]
            ],
            'orderBy'       => [
                [
                    'by'        => 'id',
                    'direction' => 'asc'
                ]
            ]
        ];
        $response = $this->json('GET', route('operations.index'),
            $filters
        );
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'operations'
        ]);
    }

    public function testCancelAddFundsPending() {
        $this->withJWT();
        $operation = factory(Emitters\UsersOperations\Models\Users_Operations::class)->create([
            'type'      => 'buy',
            'operation' => 'ADD_FUNDS',
            'status'    => 'pending'
        ]);
        $response  = $this->json('PUT', route('operations.action'), [
            'type' => 'cancel',
            'id'   => [$operation->id]
        ]);
        $response->assertStatus(200);
        $operation = $operation->fresh();
        $this->assertSame('cancel', $operation->status);
    }

    public function testCancelAddFundsCompleted() {
        list($user, $jwt) = $this->withJWT();
        $balance   = factory(Emitters\Balance\Models\Users_Balance::class)->create([
            'type'      => 'buy',
            'operation' => 'ADD_FUNDS',
            'user_id'   => $user->id
        ]);
        $operation = factory(Emitters\UsersOperations\Models\Users_Operations::class)->create([
            'user_id'           => $user->id,
            'parent_id'         => $balance->id,
            'payment_system_id' => $balance->payment_system_id,
            'amount'            => $balance->amount,
            'type'              => 'buy',
            'operation'         => 'ADD_FUNDS',
            'status'            => 'completed'
        ]);
        config(['emitters.after_add_funds' => 'ADD_FUNDS_TO_BALANCE']);
        $response = $this->json('PUT', route('operations.action'), [
            'type' => 'cancel',
            'id'   => [$operation->id]
        ]);

        $response->assertStatus(200);
        $operation = $operation->fresh();
        $this->assertDatabaseHas($balance->getTable(), [
            'parent_id' => $operation->id,
            'type'      => 'sell',
            'operation' => 'CANCEL_ADD_FUNDS'
        ]);
        $this->assertSame('cancel', $operation->status);
    }

    public function testCancelBonusPending() {
        list($user, $jwt) = $this->withJWT();
        $operation = factory(Emitters\UsersOperations\Models\Users_Operations::class)->create([
            'user_id'   => $user->id,
            'type'      => 'buy',
            'operation' => 'BONUS',
            'status'    => 'pending'
        ]);

        $response = $this->json('PUT', route('operations.action'), [
            'type' => 'cancel',
            'id'   => [$operation->id]
        ]);
        $response->assertStatus(200);
        $operation = $operation->fresh();
        $this->assertSame('cancel', $operation->status);
    }

    public function testCancelBonusCompleted() {
        list($user, $jwt) = $this->withJWT();
        list($balance, $operation) = resolve('librariesair.balance')->addBonusToBalance($user, convert_to_bigInteger(1), 1);

        $response = $this->json('PUT', route('operations.action'), [
            'type' => 'cancel',
            'id'   => [$operation->id]
        ]);

        $response->assertStatus(200);
        $operation = $operation->fresh();
        $this->assertDatabaseHas($balance->getTable(), [
            'parent_id' => $operation->id,
            'type'      => 'sell',
            'operation' => 'CANCEL_BONUS'
        ]);
        $this->assertSame('cancel', $operation->status);
    }

    public function testCancelRefferalCompleted() {
        list($user, $jwt) = $this->withJWT();

        list($balance, $operation) = balance()->addRefferalToBalance($user, convert_to_bigInteger(5), 1, 1, '2');

        $response = $this->json('PUT', route('operations.action'), [
            'type' => 'cancel',
            'id'   => [$operation->id]
        ]);
        $response->assertStatus(200);
        $operation = $operation->fresh();
        $this->assertDatabaseHas($balance->getTable(), [
            'parent_id' => $operation->id,
            'type'      => 'sell',
            'operation' => 'CANCEL_REFFERAL'
        ]);
        $this->assertSame('cancel', $operation->status);
    }

    public function testCancelRefferalPending() {
        list($user, $jwt) = $this->withJWT();

        $operation = factory(Emitters\UsersOperations\Models\Users_Operations::class)->create([
            'user_id'   => $user->id,
            'type'      => 'buy',
            'operation' => 'REFFERAL',
            'status'    => 'pending'
        ]);

        $response = $this->json('PUT', route('operations.action'), [
            'type' => 'cancel',
            'id'   => [$operation->id]
        ]);
        $response->assertStatus(200);
        $operation = $operation->fresh();
        $this->assertSame('cancel', $operation->status);
    }

    public function testCancelWithdrawPending() {
        list($user, $jwt) = $this->withJWT();
        $payment_system = factory(Emitters\PaymentSystems\Models\Payment_System::class)->create([
            'status'              => 1,
            'withdraw'            => 1,
            'min_sum_withdraw'    => 0.005,
            'max_sum_withdraw'    => 10,
            'real_payment_system' => 1
        ]);
        $user->wallets()->create([
            'payment_system_id' => $payment_system->id,
            'wallet'            => str_random(10)
        ]);
        balance()->addRefferalToBalance($user, convert_to_bigInteger(5), $payment_system->id, 1, '2');
        config(['emitters.after_add_funds' => 'ADD_FUNDS_TO_BALANCE']);
        $operation = balance()->withdrawBalance($user, 5, $payment_system->id);
        $this->assertDatabaseHas('users__balances', [
            'id'         => $operation->parent_id,
            'type'       => 'sell',
            'operation'  => 'WITHDRAW',
            'deleted_at' => null
        ]);
        $response = $this->json('PUT', route('operations.action'), [
            'type' => 'cancel',
            'id'   => [$operation->id]
        ]);

        $response->assertStatus(200);
        $operation = $operation->fresh();
        $this->assertDatabaseMissing('users__balances', [
            'id'         => $operation->parent_id,
            'type'       => 'sell',
            'operation'  => 'WITHDRAW',
            'deleted_at' => null
        ]);
        $this->assertSame('cancel', $operation->status);
    }

    public function testCreateOperationAddFundsPendingAndConfirm() {
        list($user, $jwt) = $this->withJWT();

        $payment_system = factory(Emitters\PaymentSystems\Models\Payment_System::class)->create();

        $response = $this->json('POST', route('operations.create'), [
            'type'              => 'buy',
            'operation'         => 'ADD_FUNDS',
            'user'              => $user->email,
            'payment_system_id' => $payment_system->id,
            'amount'            => $payment_system->min_sum_purchase
        ]);
        $response->assertStatus(200);

        $this->assertDatabaseHas('users__operations', [
            'type'              => 'buy',
            'operation'         => 'ADD_FUNDS',
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system->id,
            'amount'            => convert_to_bigInteger($payment_system->min_sum_purchase),
            'status'            => 'pending'
        ]);
        $response->assertJsonStructure([
            'status' => [
                ['id', 'payment_system', 'user']
            ]
        ]);

        $body = collect(json_decode($response->getContent())->status);
        $ids  = $body->pluck('id');

        config(['emitters.after_add_funds' => 'ADD_FUNDS_TO_BALANCE']);

        $response = $this->json('PUT', route('operations.action'), [
            'type' => 'confirm',
            'id'   => [$ids]
        ]);
        $response->assertStatus(200);
        $this->assertDatabaseHas('users__operations', [
            'type'              => 'buy',
            'operation'         => 'ADD_FUNDS',
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system->id,
            'amount'            => convert_to_bigInteger($payment_system->min_sum_purchase),
            'status'            => 'completed'
        ]);

        $this->assertDatabaseHas('users__balances', [
            'type'              => 'buy',
            'operation'         => 'ADD_FUNDS',
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system->id,
            'amount'            => convert_to_bigInteger($payment_system->min_sum_purchase),
            'balance_before'    => 0,
        ]);

    }


    public function testCreateOperationAddFundsPendingAndConfirmCreateDeposit() {
        list($user, $jwt) = $this->withJWT();

        $payment_system = factory(Emitters\PaymentSystems\Models\Payment_System::class)->create();
        $plan           = factory(Emitters\DepositPlans\Models\Deposit_Plans::class)->create([

        ]);
        $response       = $this->json('POST', route('operations.create'), [
            'type'              => 'buy',
            'operation'         => 'ADD_FUNDS',
            'user'              => $user->email,
            'payment_system_id' => $payment_system->id,
            'amount'            => $payment_system->min_sum_purchase,
            'plan_id'           => $plan->id
        ]);
        // dd($response->getContent());
        $response->assertStatus(200);

        $this->assertDatabaseHas('users__operations', [
            'type'              => 'buy',
            'operation'         => 'ADD_FUNDS',
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system->id,
            'amount'            => convert_to_bigInteger($payment_system->min_sum_purchase),
            'status'            => 'pending'
        ]);
        $response->assertJsonStructure([
            'status' => [
                ['id', 'payment_system', 'user']
            ]
        ]);

        $body = collect(json_decode($response->getContent())->status);
        $ids  = $body->pluck('id');

        config(['emitters.after_add_funds' => 'CREATE_DEPOSIT']);

        $response = $this->json('PUT', route('operations.action'), [
            'type' => 'confirm',
            'id'   => [$ids]
        ]);

        $response->assertStatus(200);
        $this->assertDatabaseHas('users__operations', [
            'type'              => 'buy',
            'operation'         => 'ADD_FUNDS',
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system->id,
            'amount'            => convert_to_bigInteger($payment_system->min_sum_purchase),
            'status'            => 'completed'
        ]);

        $this->assertDatabaseHas('users__operations', [
            'type'              => 'sell',
            'operation'         => 'CREATE_DEPOSIT',
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system->id,
            'amount'            => convert_to_bigInteger($payment_system->min_sum_purchase),
            'status'            => 'completed'
        ]);

        $this->assertDatabaseHas('users__balances', [
            'type'              => 'buy',
            'operation'         => 'ADD_FUNDS',
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system->id,
            'amount'            => convert_to_bigInteger($payment_system->min_sum_purchase),
            'balance_before'    => 0,
        ]);

        $this->assertDatabaseHas('users__balances', [
            'type'              => 'sell',
            'operation'         => 'CREATE_DEPOSIT',
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system->id,
            'amount'            => convert_to_bigInteger($payment_system->min_sum_purchase),
            'balance_before'    => convert_to_bigInteger($payment_system->min_sum_purchase),
        ]);
    }


    public function testCreateOperationAddFundsPendingAndConfirmExchange() {
        list($user, $jwt) = $this->withJWT();

        $payment_system = factory(Emitters\PaymentSystems\Models\Payment_System::class)->create();
        $system_payment = factory(Emitters\PaymentSystems\Models\Payment_System::class)->create([
            'real_payment_system' => 0
        ]);
        $response       = $this->json('POST', route('operations.create'), [
            'type'              => 'buy',
            'operation'         => 'ADD_FUNDS',
            'user'              => $user->email,
            'payment_system_id' => $payment_system->id,
            'amount'            => $payment_system->min_sum_purchase
        ]);
        $response->assertStatus(200);

        $this->assertDatabaseHas('users__operations', [
            'type'              => 'buy',
            'operation'         => 'ADD_FUNDS',
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system->id,
            'amount'            => convert_to_bigInteger($payment_system->min_sum_purchase),
            'status'            => 'pending'
        ]);
        $response->assertJsonStructure([
            'status' => [
                ['id', 'payment_system', 'user']
            ]
        ]);

        $body = collect(json_decode($response->getContent())->status);

        $ids = $body->pluck('id');

        config(['emitters.after_add_funds' => 'EXCHANGE']);
        config(['emitters.default_payment_system_id' => $system_payment->id]);

        $response = $this->json('PUT', route('operations.action'), [
            'type' => 'confirm',
            'id'   => [$ids]
        ]);


        $response->assertStatus(200);
        $this->assertDatabaseHas('users__operations', [
            'type'              => 'buy',
            'operation'         => 'ADD_FUNDS',
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system->id,
            'amount'            => convert_to_bigInteger($payment_system->min_sum_purchase),
            'status'            => 'completed'
        ]);

        $this->assertDatabaseHas('users__operations', [
            'type'              => 'buy',
            'operation'         => 'EXCHANGE',
            'user_id'           => $user->id,
            'payment_system_id' => $system_payment->id,
            'amount'            => convert_to_bigInteger($payment_system->min_sum_purchase),
            'status'            => 'completed'
        ]);

        $this->assertDatabaseHas('users__balances', [
            'type'              => 'buy',
            'operation'         => 'ADD_FUNDS',
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system->id,
            'amount'            => convert_to_bigInteger($payment_system->min_sum_purchase),
            'balance_before'    => 0,
            'balance_now'       => convert_to_bigInteger($payment_system->min_sum_purchase),
        ]);

        $this->assertDatabaseHas('users__balances', [
            'type'              => 'sell',
            'operation'         => 'EXCHANGE',
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system->id,
            'amount'            => convert_to_bigInteger($payment_system->min_sum_purchase),
            'balance_before'    => convert_to_bigInteger($payment_system->min_sum_purchase),
            'balance_now'       => 0,
        ]);

        $this->assertDatabaseHas('users__balances', [
            'type'              => 'buy',
            'operation'         => 'EXCHANGE',
            'user_id'           => $user->id,
            'payment_system_id' => $system_payment->id,
            'amount'            => convert_to_bigInteger($payment_system->min_sum_purchase),
            'balance_now'       => convert_to_bigInteger($payment_system->min_sum_purchase),
            'balance_before'    => 0,
        ]);
    }

    public function testCreateOperationBonusPendingAndConfirm() {
        list($user, $jwt) = $this->withJWT();

        $payment_system = factory(Emitters\PaymentSystems\Models\Payment_System::class)->create();

        $response = $this->json('POST', route('operations.create'), [
            'type'              => 'buy',
            'operation'         => 'BONUS',
            'user'              => $user->email,
            'payment_system_id' => $payment_system->id,
            'amount'            => $payment_system->min_sum_purchase
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status' => [
                ['id', 'payment_system', 'user']
            ]
        ]);

        $this->assertDatabaseHas('users__operations', [
            'type'              => 'buy',
            'operation'         => 'BONUS',
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system->id,
            'amount'            => convert_to_bigInteger($payment_system->min_sum_purchase),
            'status'            => 'pending'
        ]);

        $body = collect(json_decode($response->getContent())->status);

        $ids = $body->pluck('id');

        $response = $this->json('PUT', route('operations.action'), [
            'type' => 'confirm',
            'id'   => [$ids]
        ]);
        $response->assertStatus(200);

        $this->assertDatabaseHas('users__balances', [
            'type'              => 'buy',
            'operation'         => 'BONUS',
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system->id,
            'amount'            => convert_to_bigInteger($payment_system->min_sum_purchase),
            'balance_before'    => 0,
        ]);

        $this->assertDatabaseHas('users__operations', [
            'type'              => 'buy',
            'operation'         => 'BONUS',
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system->id,
            'amount'            => convert_to_bigInteger($payment_system->min_sum_purchase),
            'status'            => 'completed'
        ]);
    }


    public function testCreateOperationWithdrawPendingAndConfirm() {
        list($user, $jwt) = $this->withJWT();

        $payment_system = factory(Emitters\PaymentSystems\Models\Payment_System::class)->create();
        config()->set('emitters.withdrawal_commission', 0);
        $user->wallets()->create([
            'payment_system_id' => $payment_system->id,
            'wallet'            => str_random(10)
        ]);
        balance()->addRefferalToBalance($user, convert_to_bigInteger($payment_system->min_sum_purchase), $payment_system->id, 1, '2');
        config(['emitters.after_add_funds' => 'ADD_FUNDS_TO_BALANCE']);
        $response = $this->json('POST', route('operations.create'), [
            'type'              => 'sell',
            'operation'         => 'WITHDRAW',
            'user'              => $user->email,
            'payment_system_id' => $payment_system->id,
            'amount'            => $payment_system->min_sum_purchase
        ]);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status' => [
                ['id', 'payment_system', 'user']
            ]
        ]);

        $this->assertDatabaseHas('users__operations', [
            'type'              => 'sell',
            'operation'         => 'WITHDRAW',
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system->id,
            'amount'            => convert_to_bigInteger($payment_system->min_sum_purchase),
            'status'            => 'pending'
        ]);

        $this->assertDatabaseHas('users__balances', [
            'type'              => 'sell',
            'operation'         => 'WITHDRAW',
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system->id,
            'amount'            => convert_to_bigInteger($payment_system->min_sum_purchase)
        ]);
//
        $body = collect(json_decode($response->getContent())->status);

        $ids = $body->pluck('id');

        $response = $this->json('PUT', route('operations.action'), [
            'type' => 'confirm',
            'id'   => [$ids]
        ]);
        $response->assertStatus(200);
    }

    public function testCreatePenaltyPendingAndConfirm() {
        list($user, $jwt) = $this->withJWT();

        $payment_system = factory(Emitters\PaymentSystems\Models\Payment_System::class)->create();

        $response = $this->json('POST', route('operations.create'), [
            'type'              => 'sell',
            'operation'         => 'PENALTY',
            'user'              => $user->email,
            'payment_system_id' => $payment_system->id,
            'amount'            => $payment_system->min_sum_purchase
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'status' => [
                ['id', 'payment_system', 'user']
            ]
        ]);

        $this->assertDatabaseHas('users__operations', [
            'type'              => 'sell',
            'operation'         => 'PENALTY',
            'user_id'           => $user->id,
            'payment_system_id' => $payment_system->id,
            'amount'            => convert_to_bigInteger($payment_system->min_sum_purchase),
            'status'            => 'pending'
        ]);

        $body = collect(json_decode($response->getContent())->status);

        $ids = $body->pluck('id');

        $response = $this->json('PUT', route('operations.action'), [
            'type' => 'confirm',
            'id'   => [$ids]
        ]);
        $response->assertStatus(200);
    }

    public function testSendOperationsStatus() {
        list($user, $jwt) = $this->withJWT();

        $operations = factory(Emitters\UsersOperations\Models\Users_Operations::class, 10)->create([
            'status' => 'pending'
        ]);

        $response = $this->json('POST', route('operations.status'), [
            'status' => 'completed',
            'id'     => $operations->pluck('id')
        ]);

        $response->assertStatus(200);

        $this->assertDatabaseMissing('users__operations', [
            'status' => 'pending'
        ]);
    }

    public function testConfirmOperationWithdrawButUserNotHaveWallet() {
        list($user, $jwt) = $this->withJWT();
        $ps         = paymentsystems()->grabAll()->where('title', 'PerfectMoney')->first();
        $operations = factory(Emitters\UsersOperations\Models\Users_Operations::class)->create([
            'type'              => 'sell',
            'operation'         => 'WITHDRAW',
            'status'            => 'pending',
            'payment_system_id' => $ps->id
        ]);

        $response = $this->json('PUT', route('operations.action'), [
            'type' => 'confirm',
            'id'   => $operations->pluck('id')
        ]);

        $body = collect(json_decode($response->getContent()));
        $this->assertNotTrue($body->where('id', $operations->id)->first()->status);
    }

    public function testConfirmOperationWithdrawUserHaveWalletPerfectMoney() {
        list($user, $jwt) = $this->withJWT();
        $ps         = paymentsystems()->grabAll()->where('title', 'PerfectMoney')->first();
        $operations = factory(Emitters\UsersOperations\Models\Users_Operations::class)->create([
            'user_id'           => $user->id,
            'type'              => 'sell',
            'operation'         => 'WITHDRAW',
            'status'            => 'pending',
            'payment_system_id' => $ps->id
        ]);

        $user->wallets()->create([
            'payment_system_id' => $ps->id,
            'wallet'            => str_random(10)
        ]);
        $returned_data              = new stdClass();
        $returned_data->transaction = str_random(32);
        $returned_data->sending     = true;
        $returned_data->add_info    = [
            "fee"       => 1 * 0.5 / 100,
            "full_data" => []
        ];

        $this->mock($ps->class_name, function ($mock) use ($returned_data) {
            $mock->shouldReceive('send_money')->andReturn($returned_data);
        });

        $response = $this->json('PUT', route('operations.action'), [
            'type' => 'confirm',
            'id'   => $operations->pluck('id')
        ]);

        $response->assertStatus(200);
        $this->assertDatabaseHas('users__operations', [
            'status' => 'completed'
        ]);
    }

    public function testMassiveBitcoinPayment() {
        list($user, $jwt) = $this->withJWT();
        $ps                         = paymentsystems()->grabAll()->where('title', 'Bitcoin');
        $ids                        = [];
        $returned_data              = new stdClass();
        $returned_data->transaction = str_random(32);
        $returned_data->sending     = true;
        $returned_data->add_info    = [
            "fee"       => 1 * 0.5 / 100,
            "full_data" => []
        ];
        foreach($ps as $row) {
            $user->wallets()->create([
                'payment_system_id' => $row->id,
                'wallet'            => str_random(10)
            ]);
            $operations = factory(Emitters\UsersOperations\Models\Users_Operations::class, 3)->create([
                'user_id'           => $user->id,
                'type'              => 'sell',
                'operation'         => 'WITHDRAW',
                'status'            => 'pending',
                'payment_system_id' => $row->id
            ]);
            $ids        = array_merge($ids, $operations->pluck('id')->toArray());
            $this->mock($row->class_name, function ($mock) use ($returned_data) {
                $mock->shouldReceive('send_multi')->andReturn($returned_data);
            });
        }

        $response = $this->json('PUT', route('operations.massive'), [
            'id' => $ids
        ]);
        $body     = collect(json_decode($response->getContent()))->first();
        $this->assertTrue($body->status);
    }

    public function testConfirmAddFundsCreateDeposit() {
        list($user, $jwt) = $this->withJWT();
        $payment_system1 = factory(Emitters\PaymentSystems\Models\Payment_System::class)->create();

        $payment_system2 = factory(Emitters\PaymentSystems\Models\Payment_System::class)->create();
        $plan            = factory(Emitters\DepositPlans\Models\Deposit_Plans::class)->create();

        $operations1 = factory(Emitters\UsersOperations\Models\Users_Operations::class)->create([
            'type'              => 'buy',
            'parent_id'         => 1,
            'deposit_id'        => 0,
            'operation'         => 'ADD_FUNDS',
            'plan_id'           => $plan->id,
            'payment_system_id' => $payment_system1->id,
            'status'            => 'completed',
        ]);

        $operations2 = factory(Emitters\UsersOperations\Models\Users_Operations::class)->create([
            'type'              => 'sell',
            'parent_id'         => 2,
            'deposit_id'        => 1,
            'operation'         => 'CREATE_DEPOSIT',
            'plan_id'           => $plan->id,
            'payment_system_id' => $payment_system1->id,
            'status'            => 'completed',
        ]);

        $operations3 = factory(Emitters\UsersOperations\Models\Users_Operations::class)->create([
            'type'              => 'buy',
            'operation'         => 'ADD_FUNDS',
            'parent_id'         => 7,
            'deposit_id'        => 0,
            'plan_id'           => $plan->id,
            'payment_system_id' => $payment_system1->id,
            'status'            => 'pending',
        ]);

        $operations4 = factory(Emitters\UsersOperations\Models\Users_Operations::class)->create([
            'type'              => 'buy',
            'operation'         => 'ADD_FUNDS',
            'parent_id'         => 5,
            'deposit_id'        => 0,
            'plan_id'           => $plan->id,
            'payment_system_id' => $payment_system2->id,
            'status'            => 'pending',
        ]);

//        dd(\DB::table('users__operations')->get());
        config(['emitters.after_add_funds' => 'CREATE_DEPOSIT']);
        $response = $this->json('PUT', route('operations.action'), [
            'type' => 'confirm',
            'id'   => [$operations4->id]
        ]);

//        dd($response->getContent());
    }
}