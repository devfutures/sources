<?php

use Emitters\UsersData\Models\Users_Wallet;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Tests\TestCase;

class UsersOperationsTest extends TestCase {
    use RefreshDatabase;
    /** @var \App\LibrariesAir\UsersData\UsersDataManager */
    protected $manager;

    public function setUp(): void{
        parent::setUp();
        $this->manager = users_operations();
    }

    public function testGetOperationsWithFilter(){
        $user = factory('App\User')->create();
        $operations = factory('Emitters\UsersOperations\Models\Users_Operations',10)->create([
            'user_id' => $user->id
        ]);
        $res = $this->manager->getUserOperations($user,['ADD_FUNDS'],['pending'])->toArray();
//        dd($operations, $res);
        $this->assertCount(count($res['data']),$operations->where('operation','ADD_FUNDS')->where('status','pending')->all());
    }

    public function testCreateBonusOperation(){
        $user = factory('App\User')->create();
        $this->manager->createOperationBonus($user);
        $res = $this->manager->getUserOperations($user,['BONUS'],['completed'])->toArray();
        $this->assertCount(1,$res['data']);
    }
}